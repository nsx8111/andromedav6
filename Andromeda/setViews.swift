//
//  setViews.swift
//
//  Created by 蒼月喵 on 2020/5/4.
//  Copyright © 2020 YangYang. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

extension UIViewController{
    func setActView(actView:UIView){
        /** 環形進度圈，用來表示讀取資料中 */
        let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        actView.addSubview(activityIndicator)
        //轉圈圈 loading 圖示的設定
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
        activityIndicator.center = view.center
        activityIndicator.startAnimating()
        self.view.addSubview(actView)
        actView.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
        }
        actView.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.4)
    }
    
    func setActView(openOrClose: Bool){
        if openOrClose{
//            for view in self.view.subviews{
//                if view.tag == 500{
//                    view.removeFromSuperview()
//                }
//            }
            self.cleanSubViewsBy(tag: 500)
            
            let actView = UIView()
            // for remove，注意別的地方不可把 tag 設為 500，以免被誤刪
            actView.tag = 500
            /** 環形進度圈，用來表示讀取資料中 */
            let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
            actView.addSubview(activityIndicator)
            //轉圈圈 loading 圖示的設定
            activityIndicator.hidesWhenStopped = true
            activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
            activityIndicator.center = view.center
            activityIndicator.startAnimating()
            self.view.addSubview(actView)
            actView.snp.makeConstraints { (makes) in
                makes.edges.equalToSuperview()
            }
            actView.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.4)
        }else{
//            for view in self.view.subviews{
//                if view.tag == 500{
//                    view.removeFromSuperview()
//                }
//            }
            cleanSubViewsBy(tag: 500)
        }
        
    }
    
    func setPwdView(pwdView:UIView, pwdBtns: [UIButton] ,pwdTextFields:[UITextField]){
        
        self.view.addSubview(pwdView)
        pwdView.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
        }
        pwdView.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.8)
        
        
        for i in 0..<4 {
            pwdTextFields[i].text = ""
            pwdView.addSubview(pwdTextFields[i])
            pwdTextFields[i].snp.makeConstraints { (makes) in
                makes.centerY.equalToSuperview().offset(-100)
                makes.left.equalToSuperview().offset(fullScreenSize.width/5 * CGFloat(i+1) - 25)
                makes.width.height.equalTo(50)
            }
        }
        
        let pwdTitleLabel = UILabel()
        pwdTitleLabel.textColor = .white
        pwdTitleLabel.text = "Input the Password :"
        pwdView.addSubview(pwdTitleLabel)
        pwdTitleLabel.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.bottom.equalTo(pwdTextFields[0].snp.top).offset(-20)
        }
        
        
        pwdBtns[1].setTitle("OK", for: .normal)
        pwdBtns[1].backgroundColor = .white
        pwdBtns[1].titleLabel?.textColor = .black
        pwdBtns[1].setTitleColor(.black, for: .normal)
        pwdView.addSubview(pwdBtns[1])
        pwdBtns[1].snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(pwdTextFields[0].snp.bottom).offset(20)
            makes.width.equalTo(80)
            makes.height.equalTo(40)
        }

        pwdBtns[0].setTitle("Cancel", for: .normal)
        pwdBtns[0].backgroundColor = .white
        pwdBtns[0].titleLabel?.textColor = .black
        pwdBtns[0].setTitleColor(.black, for: .normal)
        pwdView.addSubview(pwdBtns[0])
        pwdBtns[0].snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(pwdBtns[1].snp.bottom).offset(20)
            makes.width.equalTo(80)
            makes.height.equalTo(40)
        }
    }
    /**
     返回 LoginVC 用
     - Parm msg : 錯誤提示訊息
     - Date: 11/10  Mike
     */
    func backToLoginVC(title:String,msg: String,okMsg:String){
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: okMsg,
            style: .default,
            handler: {
                (action: UIAlertAction!) -> Void in
                self.dismiss(animated: true, completion: nil)
            }
        )
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    /**
     clean subView by tag
     - Date: 02/02
     */
    func cleanSubViewsBy(tag: Int){
        for sv in self.view.subviews{
            if sv.tag == tag{
                sv.removeFromSuperview()
            }
        }
    }
}
