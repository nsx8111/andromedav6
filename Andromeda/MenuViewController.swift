//
//  MenuViewController.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2018/10/2.
//  Copyright © 2018年 蒼月喵. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire
import SwiftyJSON

class MenuViewController: UIViewController {
    
    @IBOutlet weak var LeftMenu: UIView!
    @IBOutlet weak var RightMenu: UIView!
    
    var LeftMenuViewController: LeftMenuViewController?
    var SearchViewController: SearchViewController?
    var PackDetailViewController: PackDetailViewController?
    var CatalogMenuViewController: CatalogMenuViewController?
    
    //    @IBOutlet var HomePageView: UIView!
    @IBOutlet var RecommendMovieView: UIView!
    
    @IBOutlet var DeviceView: UIView!
    //    @IBOutlet var MovieView: UIView!
    @IBOutlet var CatalogMenuView: UIView!
    //    @IBOutlet var AdultView: UIView!
    
    @IBOutlet var HistoryView: UIView!
    @IBOutlet var RentListView: UIView!
    @IBOutlet var UserDataView: UIView!
    
    @IBOutlet var DevicesManageView: UIView!
    @IBOutlet var ParentalControlView: UIView!
    @IBOutlet var ChangePasswordView: UIView!
    @IBOutlet var QRCodeLoginSTBView: UIView!
    @IBOutlet var FaceADView: UIView!
    @IBOutlet var FavoriteView: UIView!
    
    @IBOutlet var PackDetailView: UIView!
    
    var backView = UIView()
    //0319
    var tempView = UIView()
    //0324
//    var workingView = UIView()
    var tmpView: UIView!
    
    var portraitConstraints : [Constraint] = []
    var landscapeConstraints : [Constraint] = []
    var p1 : Constraint?
    var p2 : Constraint?
    var p3 : Constraint?
    var p4 : Constraint?
    var l1 : Constraint?
    var l2 : Constraint?
    var l3 : Constraint?
    var l4 : Constraint?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        
        let name = NSNotification.Name("SearchToDetail")
        NotificationCenter.default.addObserver(self, selector:#selector(searchToDetail(noti:)), name: name, object: nil)
        
        //        LeftMenu.frame = CGRect(x: 0, y: 0, width: fullScreenSize.width*0.8, height: fullScreenSize.height)
        LeftMenu.snp.makeConstraints { (makes) in
            makes.width.equalTo(shortSide*0.8)
            //            p1 = makes.width.equalTo(shortSide*0.8).constraint
            p1 = makes.height.equalTo(longSide).constraint
            //            p2 = makes.top.left.equalToSuperview().constraint
            makes.top.left.equalToSuperview()
            
            l1 = makes.height.equalTo(shortSide).constraint
        }
        portraitConstraints.append(p1!)
        landscapeConstraints.append(l1!)
        
        RightMenu.frame = CGRect(x: fullScreenSize.width*0.2, y: 0, width: fullScreenSize.width*0.8, height: fullScreenSize.height)
        RightMenu.snp.makeConstraints { (makes) in
            makes.width.equalTo(shortSide*0.8)
            makes.top.right.equalToSuperview()
            p1 = makes.height.equalTo(longSide).constraint
            l1 = makes.height.equalTo(shortSide).constraint
        }
        portraitConstraints.append(p1!)
        landscapeConstraints.append(l1!)
        
        //        let tempView = UIView()
        tempView.backgroundColor = .black
//        tempView.frame = view.frame
        
        view.addSubview(tempView)
        
        tempView.snp.makeConstraints { (makes) in
//            makes.edges.equalToSuperview()
            makes.top.left.equalToSuperview()
            
            p1 = makes.height.equalTo(longSide).constraint
            p2 = makes.width.equalTo(shortSide).constraint
            
            l1 = makes.height.equalTo(shortSide).constraint
            l2 = makes.width.equalTo(longSide).constraint
        }
        portraitConstraints.append(p1!)
        portraitConstraints.append(p2!)
        landscapeConstraints.append(l1!)
        landscapeConstraints.append(l2!)
        
        /**
         01/19 理論上在這個頁面不用考慮橫置/直立了
         先 mark 掉看看有沒有問題
         - TODO: 把 portraitConstraints, landscapeConstraints 設定都刪掉
         */
        setConstraint()
    }
    /**
     點擊SearchVC的Cell會回彈並隱藏SearchVC 進入DetailVC
     - Date: 11/06 Yang
    */
    @objc func searchToDetail(noti: Notification) {
        print("MenuOriginX \(navigationController?.view.superview?.frame.origin.x as Any)")
        if PackDetailViewController?.navigationController?.view.superview?.frame.origin.x == -navigationOriginX {
            PackDetailViewController?.navigationController?.view.menu2()
            PackDetailViewController?.removeSubview()
        }
    }
    /**
     從背景回到前景時呼叫的程式 呼叫SearchVC
     - Date: 11/06 Yang
     */
    @objc func applicationDidBecomeActive(notification: NSNotification) {
        // Application is back in the foreground
        SearchViewController?.textFieldResignFirstResponder()
    }
    /**
     因為 iPhone 的橫置已經被禁止
     所以 constraints 固定用直立的
     
     - Date: 08/10  Mike
     */
    func iPhoneConstraint(){
        for c in portraitConstraints{
            c.activate()
        }
        for c in landscapeConstraints{
            c.deactivate()
        }
    }
    
    func setConstraint(){
        for c in portraitConstraints{
            c.activate()
        }
        for c in landscapeConstraints{
            c.deactivate()
        }
        
        // 01/19 Mike
//        switch UIDevice.current.orientation {
//        case .landscapeLeft, .landscapeRight:
//            for c in portraitConstraints{
//                c.deactivate()
//            }
//            for c in landscapeConstraints{
//                c.activate()
//            }
//        case .portrait, .portraitUpsideDown:
//            for c in portraitConstraints{
//                c.activate()
//            }
//            for c in landscapeConstraints{
//                c.deactivate()
//            }
//        // 08/10  Mike
//        default:
//            print("@setConstraint UIDevice.current.orientation load error")
//            for c in portraitConstraints{
//                c.activate()
//            }
//            for c in landscapeConstraints{
//                c.deactivate()
//            }
//        }
    }
    
    /**
     這個應該不用了，先 mark 移除
     - NOTE: #上架 移除
     */
//    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
//        super.viewWillTransition(to: size, with: coordinator)
//
//        print("@MenuViewController tempView.frame:\(tempView.frame)")
//        setConstraint()
//        print("@MenuViewController tempView.frame:\(tempView.frame)")
//        print("view.subviews @viewWillTransition")
//        
//        switch UIDevice.current.orientation {
//        case .landscapeLeft, .landscapeRight:
//            print("橫的")
////            tempView.frame = CGRect(x: 0, y: 0, width: longSide, height: shortSide)
//        case .portrait, .portraitUpsideDown:
//            print("直的")
////            tempView.frame = CGRect(x: 0, y: 0, width: shortSide, height: longSide)
//        default:
//            print("error")
//        }
//        for subs in view.subviews{
//            print(subs)
//        }
//    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //        print(segue.identifier)
        if let vc1 = segue.destination as? LeftMenuViewController {
            LeftMenuViewController = vc1
            LeftMenuViewController?.MenuDelegate = self
        }
        if let vc2 = segue.destination as? SearchViewController {
            SearchViewController = vc2
            SearchViewController?.MenuDelegate = self
            SearchViewController?.ButtonDelegate = self
        }
        if segue.identifier == "CatalogMenuSegue" {
            let vc = segue.destination.children[0] as? CatalogMenuViewController
            CatalogMenuViewController = vc
            vc?.ButtonDelegate = self
        }else if segue.identifier == "UserDataSegue" {
            let vc = segue.destination.children[0] as? UserDataViewController
            vc?.ButtonDelegate = self
        }else if segue.identifier == "AdultSegue" {
            let vc = segue.destination.children[0] as? AdultViewController
            vc?.ButtonDelegate = self
        }else if segue.identifier == "HistorySegue" {
            let vc = segue.destination.children[0] as? HistoryViewController
            vc?.ButtonDelegate = self
        }else if segue.identifier == "RentListSegue" {
            let vc = segue.destination.children[0] as? RentListViewController
            vc?.ButtonDelegate = self
        }else if segue.identifier == "DMSegue" {
            let vc = segue.destination.children[0] as? DevicesManagementViewController
            vc?.ButtonDelegate = self
        }else if segue.identifier == "ParentalControlSegue" {
            let vc = segue.destination.children[0] as? ParentalControlViewController
            vc?.ButtonDelegate = self
        }else if segue.identifier == "ChangePasswordSegue" {
            let vc = segue.destination.children[0] as? ChangePasswordViewController
            vc?.ButtonDelegate = self
        }else if segue.identifier == "QRCodeLoginSTBSegue" {
            let vc = segue.destination.children[0] as? QRCodeLoginSTBViewController
            vc?.ButtonDelegate = self
        }else if segue.identifier == "FaceADSegue" {
            let vc = segue.destination.children[0] as? FaceADViewController
            vc?.ButtonDelegate = self
        }else if segue.identifier == "FavSegue" {
            let vc = segue.destination.children[0] as? FavoriteViewController
            vc?.ButtonDelegate = self
        }else if segue.identifier == "PackDetailPageSegue" {
            let vc = segue.destination.children[0] as? PackDetailViewController
            PackDetailViewController = vc
            vc?.ButtonDelegate = self
        }
    }
}

extension MenuViewController: MenuDelegate {
    func reloadAfterLoad(ProductType: ProductType){
        //        self.toCatalogPage(ProductType: ProductType)
        //        self.viewDidLoad()
//        print("reloadAfterLoad")
        CatalogMenuViewController?.parentProductType = ProductType
        
        let workingView = view.subviews.last
        //0323 point?
        tmpView = CatalogMenuView
//        print("@reloadAfterLoad tmpView.frame 1:\(tmpView.frame)")
        if workingView != tmpView {
//            tmpView.frame = (workingView?.frame)!
            
            self.view.addSubview(tmpView)
            
            tmpView.snp.makeConstraints { (makes) in
                //            makes.edges.equalToSuperview()
                makes.top.left.equalToSuperview()
                
                p1 = makes.height.equalTo(longSide).constraint
                p2 = makes.width.equalTo(shortSide).constraint
                
                l1 = makes.height.equalTo(shortSide).constraint
                l2 = makes.width.equalTo(longSide).constraint
            }
            portraitConstraints.append(p1!)
            portraitConstraints.append(p2!)
            landscapeConstraints.append(l1!)
            landscapeConstraints.append(l2!)
            // 01/19 理論上在這個頁面不用考慮橫置/直立了
            setConstraint()

            workingView?.removeFromSuperview()
            
//            print("@reloadAfterLoad tmpView.frame 2:\(tmpView.frame)")
            //            tmpView.subviews.last?.menu()
        }else{
            
            tmpView.frame = (workingView?.frame)!
            workingView?.removeFromSuperview()
            
            self.view.addSubview(tmpView)
            
            CatalogMenuViewController?.viewDidLoad()
            tmpView.subviews.last?.menu()
        }
        
    }
    
    func changeViedoMenuView(_ viewNum: Int) {
        
        var tmpView: UIView!
        let workingView = view.subviews.last
        switch viewNum {
        case 0:
            tmpView = UserDataView
        case 5:
            tmpView = HistoryView
        case 6:
            tmpView = RentListView
        case 7:
            tmpView = DevicesManageView
        case 8:
            tmpView = ParentalControlView
        case 9:
            tmpView = ChangePasswordView
        case 10:
            tmpView = QRCodeLoginSTBView
        case 11:
            tmpView = FaceADView
        case 12:
            tmpView = FavoriteView
        default:
            return
        }
        
        if workingView != tmpView {
            tmpView.frame = (workingView?.frame)!
            workingView?.removeFromSuperview()
            
            self.view.addSubview(tmpView)
            
            tmpView.subviews.last?.menu()
        }else{
            tmpView.subviews.last?.menu()
        }
    }
    
    func toCatalogPage(ProductType: ProductType){
        print("toCatalogPage")
        CatalogMenuViewController?.parentProductType = ProductType 
        
        var tmpView: UIView!
        let workingView = view.subviews.last
        tmpView = CatalogMenuView
        
        if workingView != tmpView {
            tmpView.frame = (workingView?.frame)!
            workingView?.removeFromSuperview()
            
            self.view.addSubview(tmpView)
            
            tmpView.subviews.last?.menu()
        }else{
            
            tmpView.frame = (workingView?.frame)!
            workingView?.removeFromSuperview()
            
            self.view.addSubview(tmpView)
            
            CatalogMenuViewController?.viewDidLoad()
            tmpView.subviews.last?.menu()
        }
        
    }
    
    func changeUserMenuView(_ viewNum: Int) {
        
        var tmpView: UIView!
        let workingView = view.subviews.last
        if viewNum == 0 {
            tmpView = DeviceView
        } else if viewNum == 1 {
            tmpView = RecommendMovieView
        } else {
            return
        }
        
        if workingView != tmpView {
            tmpView.frame = (workingView?.frame)!
            workingView?.removeFromSuperview()
            
            self.view.addSubview(tmpView)
            
            tmpView.subviews.last?.menu()
        }else{
            tmpView.subviews.last?.menu()
        }
    }
    
    //    func showLeft() {
    //        self.RightMenu.isHidden = true
    //    }
}

extension MenuViewController: ButtonDelegate{
    func clickLeft() {
//        print("clickLeft")
        self.LeftMenu.isHidden = false
        self.RightMenu.isHidden = true
    }
    func clickRight() {
//        print("clickRight")
        self.LeftMenu.isHidden = true
        self.RightMenu.isHidden = false
    }
    func toPackPage(packData: Pack){
        print("toPackPage")
        var tmpView: UIView!
        let workingView = view.subviews.last
        tmpView = PackDetailView
        tmpView.frame = (workingView?.frame)!
        
        backView = workingView!
        workingView?.removeFromSuperview()
        // 01/04 mark 掉，測試暫時沒問題，過一陣子可刪除
//        PackDetailViewController?.packData = packData
        self.view.addSubview(tmpView)
    }
    
    // 0729
    func toPackPageNew(packDataNew: ProductCDetail){
//        print("toPackPageNew")
        var tmpView: UIView!
        let workingView = view.subviews.last
        tmpView = PackDetailView
        
        tmpView.frame = (workingView?.frame)!
        
        backView = workingView!
        workingView?.removeFromSuperview()
//        print("02/17 packDataNew:\(packDataNew)")
        PackDetailViewController?.packDataNew = packDataNew
        self.view.addSubview(tmpView)
    }
    
    func backPage(){
        print("backPage")
        let workingView = view.subviews.last
        backView.frame = (workingView?.frame)!
        workingView?.removeFromSuperview()

        if let contentView = view.viewWithTag(1000) {
            contentView.removeFromSuperview()
        }
        self.view.addSubview(backView)
    }
    
    func setPackDetailPage(setSometext: String){
        PackDetailViewController?.tmpText = setSometext
    }
    
}
