//
//  DevicesManagementViewController.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2019/5/16.
//  Copyright © 2019 蒼月喵. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire
import SwiftyJSON

class DevicesManagementViewController: UIViewController {
    
    weak var ButtonDelegate: ButtonDelegate?
    let titleLabel = UILabel()  //Devices Manage
    let titleLabel2 = UILabel() //Device Registeration
    
    let topIntroText = UITextView()
    let botIntroText = UITextView()
    var deviceDatas : [DeviceData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setViews()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.setActView(openOrClose: true)
        cleanSubViewsBy(tag: 200)
        getRegisterDevice{
            print("getRegisterDevice Done")
//            for dv in self.deviceDatas{
//                print(dv)
//            }
            self.setDeviceDataView()
            self.setActView(openOrClose: false)
        }
    }
    
    /**
     上下方的介紹 textView
     */
    func setViews(){
        view.addSubview(titleLabel)
        titleLabel.text = "Devices Manage"
//        Marker Felt Wide 17.0
        titleLabel.font = UIFont(name: "Marker Felt Wide", size: 17)
        titleLabel.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(10)
            makes.height.equalTo(25)
        }
        
        view.addSubview(topIntroText)
        topIntroText.snp.makeConstraints { (makes) in
            makes.top.equalTo(titleLabel.snp.bottom).offset(10)
            makes.left.equalToSuperview().offset(30)
            makes.right.equalToSuperview().offset(-30)
            makes.height.equalTo(160)
        }
        topIntroText.isSelectable = false
        topIntroText.isScrollEnabled = false
        topIntroText.text = """
                You can register multiple connectes devices (i.e. STB,
                computer, tablet, smart phone, etc.). The maximum number of
                devices is limited to 3 devices. Therefor if you try to connect a
                fourth device, this device will not be able to connect.

                The user will need to disconnect one of the connected devices
                before allowing a new one to be connected.
            """
        
        view.addSubview(titleLabel2)
        titleLabel2.text = "Device Registeration"
        titleLabel2.font = UIFont(name: "Marker Felt Wide", size: 17)
        titleLabel2.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(topIntroText.snp.bottom).offset(10)
            makes.height.equalTo(25)
        }
        
        view.addSubview(botIntroText)
        botIntroText.snp.makeConstraints { (makes) in
            makes.bottom.equalToSuperview().offset(-40)
            makes.left.equalToSuperview().offset(30)
            makes.right.equalToSuperview().offset(-30)
//            makes.centerX.equalToSuperview()
            makes.height.equalTo(60)
        }
        botIntroText.isSelectable = false
        botIntroText.isScrollEnabled = false
        botIntroText.text = """
                * Using Statment:
                Ex: 5 Devices for each Account One time can
                Unregistering per month
            """
    }
    /**
     API 3.1.17 getRegisterDevice
     - Date: 01/27,28  Mike
     */
    func getRegisterDevice(completion : @escaping ()->()) {
        
        let params = [
            "token" : token
        ] as [String : Any]
        
        AF.request("\(serverUrlNew)account/getRegisterDevice", method: .post, parameters: params)
        .responseJSON { (response) in
            if response.value != nil{
                print(response.value)
                let swiftyJsonVar = JSON(response.value!)
                let status: Int = swiftyJsonVar["status"].int!
                if status == 200{
                    let restltJson = JSON(swiftyJsonVar["result"])
                    let devicesData = restltJson["device_register_info"].arrayObject
                    self.deviceDatas = []
                    for deviceData in devicesData!{
//                        print(deviceData)
                        let ddJSON = JSON(deviceData)
                        var tempDD: DeviceData = DeviceData()
                        tempDD.dvName = ddJSON["device_name"].string ?? ""
                        tempDD.dvRegTime = ddJSON["device_register_time"].string ?? ""
                        tempDD.dvSN = ddJSON["device_serial_number"].string ?? ""
                        
                        switch ddJSON["device_type"].string {
                        case nil:
                            print("nil")
                            tempDD.dvType = .unknow
                        case "Smart phone":
                            tempDD.dvType = .phone
                        case "STB":
                            tempDD.dvType = .stb
                        case "Tablet":
                            tempDD.dvType = .tablet
                        default:
                            tempDD.dvType = .unknow
                        }
                        
                        switch ddJSON["device_os"].string {
                        case nil:
                            print("nil")
                            tempDD.dvOS = .unknow
                        case "ios":
                            tempDD.dvOS = .ios
                        case "Android":
                            tempDD.dvOS = .android
                        default:
                            tempDD.dvOS = .unknow
                        }
                        
                        self.deviceDatas.append(tempDD)
                    }
                    print("After API self.deviceDatas.count:\(self.deviceDatas.count)")
                }else{
                    // TODO: status error, handle later
                }
                completion()
            }else{
                print("response.value == nil @getRegisterDevice")
                completion()
            }
        }
    }
    
    /**
     API 3.1.18 UnregisterDevice
     - Date: 02/01  Mike
     */
    func unregisterDevice(index: Int,completion : @escaping ()->()) {
        let params = [
            "device_serial_number" : self.deviceDatas[index].dvSN,
            "token" : token
        ] as [String : Any]
        
        AF.request("\(serverUrlNew)account/UnregisterDevice", method: .post, parameters: params)
        .responseJSON { (response) in
            if response.value != nil{
                print("02/01 unregisterDevice done")
                completion()
            }else{
                print("02/01 unregisterDevice error")
                completion()
            }
        }
    }
    
    /**
     處理中間註冊的 Device Data
     */
    func setDeviceDataView(){
        let height = (fullScreenSize.height - 474)/3
        let dvNum = self.deviceDatas.count
        
//        let dvTypeIcon1 = UIImageView()
//        let dvTypeIcon2 = UIImageView()
//        let dvTypeIcon3 = UIImageView()
//        let dvTypeIcons: [UIImageView] = [dvTypeIcon1,dvTypeIcon2,dvTypeIcon3]
        
        for i in 0..<dvNum{
            let dvTypeIcon = UIImageView()
            self.view.addSubview(dvTypeIcon)
//            dvTypeIcon.backgroundColor = .yellow
            switch self.deviceDatas[i].dvType {
            case .unknow:
                print("?")
            case .web:
                dvTypeIcon.image = UIImage(named: "web")
            case .phone:
                if self.deviceDatas[i].dvOS == .ios{
                    dvTypeIcon.image = UIImage(named: "iPhone")
                }else{
                    dvTypeIcon.image = UIImage(named: "aPhone")
                }
            case .stb:
                dvTypeIcon.image = UIImage(named: "web")
            case .tablet:
                if self.deviceDatas[i].dvOS == .ios{
                    dvTypeIcon.image = UIImage(named: "iPad")
                }else{
                    dvTypeIcon.image = UIImage(named: "aPad")
                }
            }
            dvTypeIcon.snp.makeConstraints { (makes) in
                makes.left.equalToSuperview().offset(30)
                makes.top.equalTo(titleLabel2.snp.bottom).offset(height*CGFloat(i) + CGFloat(i+1)*10)
                makes.height.width.equalTo(height)
            }
            
            let typeLabel = UILabel()
            let osLabel = UILabel()
            let nameLabel = UILabel()
            let regLabel = UILabel()
            
            switch self.deviceDatas[i].dvType {
            case .unknow:
                typeLabel.text = "UnKnow, "
            case .web:
                typeLabel.text = "Web, "
            case .phone:
                typeLabel.text = "Phone, "
            case .stb:
                typeLabel.text = "STB, "
            case .tablet:
                typeLabel.text = "Tablet, "
            }
            
            switch self.deviceDatas[i].dvOS {
            case .unknow:
                osLabel.text = "UnKnow"
            case .ios:
                osLabel.text = "iOS"
            case .android:
                osLabel.text = "Android"
            }
            
            nameLabel.text = self.deviceDatas[i].dvName
            regLabel.text = self.deviceDatas[i].dvRegTime
            
            self.view.addSubview(typeLabel)
            self.view.addSubview(osLabel)
            self.view.addSubview(nameLabel)
            self.view.addSubview(regLabel)
            
            typeLabel.snp.makeConstraints { (makes) in
                makes.left.equalTo(dvTypeIcon.snp.right)
                makes.top.equalTo(dvTypeIcon)
            }
            
            osLabel.snp.makeConstraints { (makes) in
                makes.left.equalTo(typeLabel.snp.right)
                makes.bottom.equalTo(typeLabel)
            }
            
            nameLabel.snp.makeConstraints { (makes) in
                makes.left.equalTo(typeLabel)
                makes.centerY.equalTo(dvTypeIcon)
            }
            
            regLabel.snp.makeConstraints { (makes) in
                makes.left.equalTo(typeLabel)
                makes.bottom.equalTo(dvTypeIcon)
            }
            
            let unRegBtn = UIButton()
            unRegBtn.setImage(UIImage(named: "return"), for: .normal)
            view.addSubview(unRegBtn)
            unRegBtn.snp.makeConstraints { (makes) in
                makes.left.equalTo(regLabel.snp.right).offset(20)
                makes.height.width.equalTo(height/3)
                makes.centerY.equalTo(dvTypeIcon)
            }
            unRegBtn.addTapGestureRecognizer {
                self.setActView(openOrClose: true)
                self.unregisterDevice(index: i) {
                    self.cleanSubViewsBy(tag: 200)
                    self.deviceDatas.remove(at: i)
                    self.setDeviceDataView()
                    self.setActView(openOrClose: false)
                }
            }
            
            
            
            
            // set tag for remove
            dvTypeIcon.tag = 200
            typeLabel.tag = 200
            osLabel.tag = 200
            nameLabel.tag = 200
            regLabel.tag = 200
            unRegBtn.tag = 200
        }
    }
    
    @IBAction func handleEdgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger(location: location, state: sender.state)
        if beganX2 > 0 {
            ButtonDelegate?.clickLeft()
        }
    }
    
    @IBAction func handleEdgePanGesture2(_ sender: UIScreenEdgePanGestureRecognizer) {
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger2(location: location, state: sender.state)
        if beganX2 < 0{
            ButtonDelegate?.clickRight()
        }
    }
    
    @IBAction func handleTapGesture(_ sender: Any) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        if navigationController?.view.superview?.frame.origin.x != 0 {
            navigationController?.view.menu()
        }
    }
    @IBAction func viedoClick(_ sender: Any) {
        navigationController?.view.menu()
        ButtonDelegate?.clickLeft()
    }
    @IBAction func userClick(_ sender: Any) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        navigationController?.view.menu2()
        ButtonDelegate?.clickRight()
    }
    
}
