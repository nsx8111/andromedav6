//
//  HistoryViewController.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2019/5/15.
//  Copyright © 2019 蒼月喵. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SnapKit

class HistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var pageTitleLabel: UILabel!
    
    /** 點擊屏幕用 */
    let contentView = UIView()
    var productTableView = UITableView()
    var productLists: [ProductList] = []
    var historyDatas: [HistoryData] = []
    
    weak var ButtonDelegate: ButtonDelegate?
    
    var scrollView = UIScrollView()
    var containerView = UIView()
    
    //    /** 環形進度圈，用來表示讀取資料中 */
    //    var activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
    //    /** 放著環形進度圈的View，平時為 Hide 狀態，讀取時顯示出來在最上方 */
    //    let actView = UIView()
    
    var tmpPackDetailNew: ProductCDetail = ProductCDetail()
    
    /** 子分類 read from server (對應測試用的forTestCatalogs) */
    var childCatalogNew: [ProductTCCategory] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        switch deviceType {
        //        case .iPhone:
        //            print("no change")
        //            tableViewRowHeight = 160
        //        case .iPad:
        //            print("iPad")
        //            tableViewRowHeight = 320
        ////            self.sizeB = 60
        ////            self.sizeS = 40
        //        }

        setGestures()
        setViews()
        
        //  self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let name = NSNotification.Name("removeContentView")
        NotificationCenter.default.addObserver(self, selector:
            #selector(updateContentViewNoti(noti:)), name: name, object: nil)
        //        self.actView.isHidden = false
        //        let actView = UIView()
        //        setActView(actView: actView)
        self.setActView(openOrClose: true)
        getWatchVideoHistory(){ [weak self] in
            guard let self = self else { return }
//            self.getWatchVideoHistory(){
            print("getWatchVideoHistory done")
            print("self.historyDatas.count:\(self.historyDatas.count)")
            self.productTableView.reloadData()
            //            self.actView.isHidden = true
            //            actView.removeFromSuperview()
            self.setActView(openOrClose: false)
//            }
        }
    }
    
    func setGestures(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTapGesture(_:)))
        let pan: UIScreenEdgePanGestureRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(self.handleEdgePanGesture(_:)))
        let pan2: UIScreenEdgePanGestureRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(self.handleEdgePanGesture2(_:)))
        view.addGestureRecognizer(tap)
        view.addGestureRecognizer(pan)
        view.addGestureRecognizer(pan2)
    }
    
    func setViews(){
        // tableView setting
        productTableView.register(ViewHistoryTableViewCell.self, forCellReuseIdentifier: "vhCell")
        productTableView.delegate = self
        productTableView.dataSource = self
        //        productTableView.allowsSelectionDuringEditing = true
        //        productTableView.allowsSelection = true
        productTableView.rowHeight = tableViewRowHeight
        productTableView.bounces = true
        productTableView.backgroundColor = .buttonColor4
        //        productTableView.separatorInset = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        productTableView.separatorColor = .white
        productTableView.tableFooterView = UIView(frame: CGRect.zero)
        productTableView.tag = 100
        view.addSubview(productTableView)
        productTableView.snp.makeConstraints { (makes) in
            makes.top.equalTo(pageTitleLabel.snp.bottom).offset(offset1)
            makes.left.right.bottom.equalToSuperview()
        }
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        refreshControl.attributedTitle = NSAttributedString(string: "loading...", attributes: attributes)
        refreshControl.tintColor = .white
        productTableView.addSubview(refreshControl)
    }
    
    var canRefresh = true
    var refreshControl: UIRefreshControl!
    /**
     下滑頁面會刷新資料
     - Date: 12/15 Yang
     */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < -offset9 * HeightForScroll { //change 100 to whatever you want
            if canRefresh && !self.refreshControl.isRefreshing {
//                print("下滑")
                self.canRefresh = false
                self.refreshControl.beginRefreshing()
                self.refresh() // your viewController refresh function
            }
            if scrollView.contentOffset.y < -refreshControl.frame.size.height {
                scrollView.isScrollEnabled = false
            }
        }else if scrollView.contentOffset.y >= 0 {
//            print("上滑")
            self.canRefresh = true
            scrollView.isScrollEnabled = true
        }
    }
    
    @objc func refresh() {
        getWatchVideoHistory(){
            self.productTableView.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
    /**
     瀏覽紀錄列表 暫時先不用
     - Date: 09/25 Yang
     */
    func getViewHistoryData(completion : @escaping ()->()) {
        let  params = [
            "token":"\(token)"
            ] as [String : Any]
        
        var tempProductLists: [ProductList] = []
        
        AF.request("\(serverUrlNew)account/getViewProductHistory", method: .post, parameters: params)
            .responseJSON { (response) in
                if response.value != nil {
                    print(response.value!)
                    let swiftyJsonVar = JSON(response.value!)
                    let status: Int = swiftyJsonVar["status"].int!
                    
                    if status == 200{
                        let restltJson = JSON(swiftyJsonVar["result"])
                        let productLists = restltJson["view_product_history"].arrayObject
                        
                        for product in productLists! {
                            let productJson = JSON(product)
                            
                            var tempProduct: ProductList = ProductList()
                            tempProduct.product_id = productJson["product_id"].int!
                            if productJson["product_name"].string != nil{
                                tempProduct.product_name = productJson["product_name"].string!
                            }
                            if productJson["product_description"].string != nil{
                                tempProduct.product_description = productJson["product_description"].string!
                            }else{
                                tempProduct.product_description = ""
                            }
                            
                            let imageDatas = productJson["product_image"].arrayObject
                            for imageData in imageDatas!{
                                let imageDataJson = JSON(imageData)
                                var tempProductImage: Product_image = Product_image()
                                
                                tempProductImage.product_image_id = imageDataJson["product_image_id"].string!
                                tempProductImage.product_image_type_id = imageDataJson["product_image_type_id"].int!
                                //                                    tempProductImage.product_image_type = imageDataJson["product_image_type"].string!
                                tempProductImage.name = imageDataJson["name"].string!
                                if imageDataJson["description"].string != nil{
                                    tempProductImage.description = imageDataJson["description"].string!
                                }
                                tempProductImage.url = imageDataJson["url"].string!
                                
                                if tempProductImage.product_image_type_id == 1{
                                    // Landscape
                                    tempProduct.product_image_L = tempProductImage
                                }else{
                                    // Portrait
                                    tempProduct.product_image_P = tempProductImage
                                }
                                
                            }
                            let ratingJson = JSON(productJson["rating"])
                            //                        print("ratingJson:\(ratingJson)")
                            if ratingJson["avg_rating"].float != nil{
                                tempProduct.rating_avg_rating = ratingJson["avg_rating"].float!
                            }
                            if ratingJson["total_rating"].float != nil{
                                tempProduct.rating_total_rating = ratingJson["total_rating"].float!
                            }
                            
                            tempProductLists.append(tempProduct)
                            // 0722
                            tempProduct = ProductList()
                        }
                        self.productLists = tempProductLists
                        completion()
                        print("productListsCount \(self.productLists.count)")
                    }else{
                        // token error
                        // 11/10
                        self.backToLoginVC(title:"Logout",msg: swiftyJsonVar["message"].string!,okMsg: "Back to LoginPage.")
//                        let alertController = UIAlertController(title: "Logout", message: swiftyJsonVar["message"].string!, preferredStyle: .alert)
//
//                        let okAction = UIAlertAction(
//                            title: "Back to LoginPage.",
//                            style: .default,
//                            handler: {
//                                (action: UIAlertAction!) -> Void in
//                                self.dismiss(animated: true, completion: nil)
//                        }
//                        )
//                        alertController.addAction(okAction)
//
//                        self.present(alertController, animated: true, completion: nil)
                    }
                    completion()
                }else{
                    print("response.result.isFalse @getViewHistoryData")
                    self.productLists = []
                    self.productTableView.reloadData()
                    completion()
                }
                self.setActView(openOrClose: false)
        }
        
    }
    /**
     刪除瀏覽紀錄 暫時先不用
     - Date: 09/25 Yang
     */
    //    func removeViewProductHistory(id :Int,completion : @escaping ()->()) {
    //        let  params = [
    //            "product_id":"\(id)",
    //            "token":"\(token)"
    //        ] as [String : Any]
    //
    //        //        var tempProductLists: [ProductList] = []
    //
    //        AF.request("\(serverUrlNew)account/removeViewProductHistory", method: .post, parameters: params)
    //            .responseJSON { (response) in
    //                if response.value != nil {
    //                    //                print(response.result.value!)
    //                    let swiftyJsonVar = JSON(response.value!)
    //                    let status: Int = swiftyJsonVar["status"].int!
    //                    print("status \(status)")
    //
    //                    if status == 200{
    //                        completion()
    //
    //                    }else if status == 404{
    //                        // token error
    //                        let alertController = UIAlertController(title: "Logout", message: swiftyJsonVar["message"].string!, preferredStyle: .alert)
    //
    //                        let okAction = UIAlertAction(
    //                            title: "Back to LoginPage.",
    //                            style: .default,
    //                            handler: {
    //                                (action: UIAlertAction!) -> Void in
    //                                self.dismiss(animated: true, completion: nil)
    //                            }
    //                        )
    //                        alertController.addAction(okAction)
    //
    //                        self.present(alertController, animated: true, completion: nil)
    //
    //                        completion()
    //                    }else{
    //                        completion()
    //                    }
    //
    //                }else{
    //                    print("response.result.isFalse @removeViewProductHistory")
    //                    completion()
    //                }
    //
    //
    //            }
    //
    //    }
    /**
     取得播放紀錄列表
     - Date: 09/25 Yang
     */
    func getWatchVideoHistory(completion : @escaping ()->()) {
        let params = [
            "token":"\(token)"
            ] as [String : Any]
        
        var tempHistoryDatas: [HistoryData] = []
        let getWatchVideoHistoryAPI = serverUrlNew + "video/getWatchVideoHistory"
        
        AF.request(getWatchVideoHistoryAPI, method: .post, parameters: params)
            .responseJSON{ (response) in
                if response.value != nil{
                    let swiftyJsonVar = JSON(response.value!)
                    let status: Int = swiftyJsonVar["status"].int!
                    if status == 200{
                        let historys = swiftyJsonVar["result"].arrayObject
                        //                        print("historys @ getWatchVideoHistory :\(historys)")
                        for history in historys! {
                            //                        print("history @ getWatchVideoHistory :\(history)")
                            let historyJson = JSON(history)
                            var tempHistoryData: HistoryData = HistoryData()
                            
                            if historyJson["product_id"].int != nil{
                                tempHistoryData.productID = historyJson["product_id"].int!
                                print("productID \(tempHistoryData.productID)")
                            }
                            if historyJson["content_id"].int != nil{
                                tempHistoryData.contentID = historyJson["content_id"].int!
                            }
                            if historyJson["content_description"].int != nil{
                                // tempHistoryData.contentID = historyJson["content_description"].int!
                            }
                            if historyJson["lastest_play_time"].string != nil{
                                tempHistoryData.lastestPlayTime = historyJson["lastest_play_time"].string!
                                if tempHistoryData.productID == 7{ //test
                                    print("lastestPlayTime \(tempHistoryData.lastestPlayTime)")
                                }
                            }
                            if historyJson["start"].string != nil{
                                tempHistoryData.start = historyJson["start"].string!
                            }
                            if historyJson["end"].string != nil{
                                tempHistoryData.end = historyJson["end"].string!
                            }
                            if historyJson["content_name"].string != nil{
                                tempHistoryData.contentName = historyJson["content_name"].string!
                            }
                            if historyJson["content_description"].string != nil{
                                tempHistoryData.contentDescription = historyJson["content_description"].string!
                            }
                            //                            print("contentID \(tempHistoryData.contentID)")
                            //                            print("historyJson[\"content_image\"].arrayObject!.count:\(historyJson["content_image"].arrayObject!.count)")
                            var CIs: [CI] = []
                            if historyJson["content_image"].arrayObject!.count > 0{
                                
                                //  var tempCI: CI = CI()
                                
                                let cis = historyJson["content_image"].arrayObject
                                //  print("ciscis \(cis)")
                                for ci in cis!{
                                    let ciJSON = JSON(ci)
                                    var tempCI: CI = CI()
                                    
                                    if ciJSON["content_image_category_id"].int != nil{
                                        tempCI.contentImageCategoryID = ciJSON["content_image_category_id"].int!
                                    }
                                    if ciJSON["content_image_category"].string != nil{
                                        tempCI.contentImageCategory = ciJSON["content_image_category"].string!
                                    }
                                    if ciJSON["url"].string != nil{
                                        tempCI.url = ciJSON["url"].string!
                                    }
                                    if ciJSON["name"].string != nil{
                                        tempCI.name = ciJSON["name"].string!
                                    }
                                    if ciJSON["description"].string != nil{
                                        tempCI.contentImageDescription = ciJSON["description"].string!
                                    }
                                    CIs.append(tempCI)
                                }
                                tempHistoryData.contentImage = CIs
                            }
                            // print("content_description : \(historyJson["content_description"].string!)")
                            tempHistoryDatas.append(tempHistoryData)
                        }
                        self.historyDatas = tempHistoryDatas
                        completion()
                    }else if status == 204{
                        // 新增狀況處理，status == 204 代表有讀取到 history，不過內容為空
                        print("get watch video history success, but no watch video history.")
                        completion()
                    }else{
                        // token error
                        // get datas error
                        self.backToLoginVC(title:"Logout",msg: swiftyJsonVar["message"].string!,okMsg: "Back to LoginPage.")
//                        let alertController = UIAlertController(title: "Logout", message: swiftyJsonVar["message"].string!, preferredStyle: .alert)
//
//                        let okAction = UIAlertAction(
//                            title: "Back to LoginPage.",
//                            style: .default,
//                            handler: {
//                                (action: UIAlertAction!) -> Void in
//                                self.dismiss(animated: true, completion: nil)
//                        }
//                        )
//                        alertController.addAction(okAction)
//
//                        self.present(alertController, animated: true, completion: nil)
                        completion()
                    }
                }else{
                    print("response.result.isFalse @getWatchVideoHistory")
                    self.historyDatas = []
                    self.productTableView.reloadData()
                    completion()
                }
                
                
        }
        
    }
    /**
     刪除播放紀錄
     - Date: 10/13  Mike
     */
    func deleteWatchVideoHistory(select: Int,completion : @escaping ()->()) {
        let params = [
            "product_id":self.historyDatas[select].productID,
            "content_id":self.historyDatas[select].contentID,
            "token":"\(token)"
            ] as [String : Any]
        
        //        var tempProductLists: [ProductList] = []
        let deleteWatchVideoHistoryAPI = serverUrlNew + "video/deleteWatchVideoHistory"
        
        AF.request(deleteWatchVideoHistoryAPI, method: .post, parameters: params)
            .responseJSON { (response) in
                if response.value != nil {
                    //                print(response.result.value!)
                    let swiftyJsonVar = JSON(response.value!)
                    let status: Int = swiftyJsonVar["status"].int!
                    let message: String = swiftyJsonVar["message"].string!
                    let result = swiftyJsonVar["result"].arrayObject
                    //                    print("status11 \(status)")
                    //                    print("message11 \(message)")
                    //                    print("result \(result)")
                    //                    print("swiftyJsonVar11 \(swiftyJsonVar)")
                    if status == 200{
                        completion()
                        
                    }else if status == 404{
                        // token error
                        // 11/10
                        self.backToLoginVC(title:"Logout",msg: swiftyJsonVar["message"].string!,okMsg: "Back to LoginPage.")
//                        let alertController = UIAlertController(title: "Logout", message: swiftyJsonVar["message"].string!, preferredStyle: .alert)
//                        let okAction = UIAlertAction(
//                            title: "Back to LoginPage.",
//                            style: .default,
//                            handler: {
//                                (action: UIAlertAction!) -> Void in
//                                self.dismiss(animated: true, completion: nil)
//                        })
//                        alertController.addAction(okAction)
//                        self.present(alertController, animated: true, completion: nil)
                        completion()
                    }else{
                        completion()
                    }
                }else{
                    print("response.result.isFalse @removeViewProductHistory")
                    completion()
                }

                
                
        }
                
    }
    
    @objc func clickCell(_ sender:AnyObject?) {
        print("clickCell")
        print(sender?.view.tag)
        //        if let senderView = sender as? ViewHistoryTableViewCell{
        //            print("senderView.tag:\(senderView.tag)")
        //        }
        let index : Int = sender?.view.tag ?? -1
        if index != -1{
            //            self.ButtonDelegate?.toPackPageNew(packDataNew: self.tmpPackDetailNew)
            self.setActView(openOrClose: true)
            self.getPackDetailNew(index: index) {
                //                actView.removeFromSuperview()
                self.setActView(openOrClose: false)
//                print("getPackDetailNewDone")
//                print("tmpPackDetailNew:\(self.tmpPackDetailNew)")
                //                self.actView.isHidden = true
                self.ButtonDelegate?.toPackPageNew(packDataNew: self.tmpPackDetailNew)
                self.setActView(openOrClose: false)
            }
        }
    }
    
    // tableView setting
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.historyDatas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = productTableView.dequeueReusableCell(withIdentifier: "vhCell") as! ViewHistoryTableViewCell
        cell.backgroundColor = .clear
        cell.tag = indexPath.row
        let tap = UITapGestureRecognizer(target: self, action: #selector(clickCell(_:)))
        cell.addGestureRecognizer(tap)
        
        cell.separatorInset = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        cell.nameValueLabel.text = self.historyDatas[indexPath.row].contentName
        cell.lastestValueLabel.text = self.historyDatas[indexPath.row].lastestPlayTime
        cell.startValueLabel.text = self.historyDatas[indexPath.row].start
//        cell.endValueLabel.text = self.historyDatas[indexPath.row].end
        
        if self.historyDatas[indexPath.row].contentImage.count > 1{
            cell.packImage.downloaded(from: self.historyDatas[indexPath.row].contentImage[1].url)
        }else if self.historyDatas[indexPath.row].contentImage.count == 1{
            cell.packImage.downloaded(from: self.historyDatas[indexPath.row].contentImage[0].url)
        }else{
            cell.packImage.image = UIImage(named: "noContentImage")
        }
        
        let bkView : UIView = UIView()
        bkView.backgroundColor = .clear
        cell.selectedBackgroundView = bkView
        return cell
    }
    
    /** 實作TableView方法，自動出現左滑功能 */
    func tableView(_ tableView: UITableView,commit editingStyle: UITableViewCell.EditingStyle,forRowAt indexPath: IndexPath){
        let select:Int = indexPath.row
        //=========
        //操作
        print("select:\(select)")
        print("historyDatasCount:\(self.historyDatas.count)")
        print("historyDatas[\(select)].contentName:\(historyDatas[select].contentName)")
        
        if editingStyle == .delete{
            print("delete")
            deleteWatchVideoHistory(select: select) {
                self.getWatchVideoHistory(){
                    print("historyDatas.count:\(self.historyDatas.count)")
                    self.productTableView.reloadData()
                }
            }
        }
    }
    
    /** 自訂delete的文字為刪除 */
    func tableView(_ tableView: UITableView,titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath)-> String?{
        //        return "刪除"
        return "Delete"
    }
    
    // 不知名原因不能用，解決辦法為 cell + tapGesture
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didSelectRowAt:\(indexPath.row)")
        print(self.historyDatas[indexPath.row].productID)
    }
    /**
     點擊屏幕回彈用
     - Date: 10/20 Yang
     */
    @objc func showContentView(){
        contentView.tag = 1000
        contentView.isUserInteractionEnabled = true
        contentView.backgroundColor = .clear
        view.addSubview(contentView)
        contentView.snp.makeConstraints { (makes) in
            makes.edges.equalTo(view)
        }
    }
    /**
     側邊欄關閉後 就把contentView給移除掉 以便可以觸控此頁面元件
     - Date: 10/20 Yang
     */
    @objc func removeSubview(){
        if let viewWithTag = self.view.viewWithTag(1000) {
            viewWithTag.removeFromSuperview()
        }else{
            print("No!")
        }
    }
    /**
     移除回彈的View
     - Date: 10/20 Yang
     */
    @objc func updateContentViewNoti(noti: Notification) {
        removeSubview()
    }
    
    @IBAction func menuClick(_ sender: Any) {
        navigationController?.view.menu()
        ButtonDelegate?.clickLeft()
        if navigationController?.view.superview?.frame.origin.x != 0 {
            showContentView()
        }else{
            removeSubview()
        }
    }
    
    @IBAction func userClick(_ sender: Any) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        navigationController?.view.menu2()
        ButtonDelegate?.clickRight()
        if navigationController?.view.superview?.frame.origin.x == -navigationOriginX {
            showContentView()
        }else{
            removeSubview()
        }
    }
    
    @IBAction func handleEdgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger(location: location, state: sender.state)
        if beganX2 > 0 {
            ButtonDelegate?.clickLeft()
            showContentView()
        }else{
            contentView.removeFromSuperview()
        }
    }
    
    @IBAction func handleEdgePanGesture2(_ sender: UIScreenEdgePanGestureRecognizer) {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger2(location: location, state: sender.state)
        if beganX2 < 0{
            ButtonDelegate?.clickRight()
            showContentView()
        }else{
            contentView.removeFromSuperview()
        }
    }
    
    @IBAction func handleTapGesture(_ sender: Any) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        print("handleTapGesture @ HistoryVC")
        print(sender)
        if let senderView = sender as? UIView{
            print("senderView.tag:\(senderView.tag)")
        }
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        if navigationController?.view.superview?.frame.origin.x != 0 {
            if navigationController?.view.superview?.frame.origin.x == -navigationOriginX {
                navigationController?.view.menu2()
                contentView.removeFromSuperview()
            }else{
                navigationController?.view.menu()
                contentView.removeFromSuperview()
            }
        }
    }
    
    func getPackDetailNew(index: Int,completion : @escaping ()->()){
        tmpPackDetailNew = ProductCDetail()
        let params = [
            "product_id": self.historyDatas[index].productID,
            "token":"\(token)"
            ] as [String : Any]
        
        AF.request("\(serverUrlNew)product/getProductDetail", method: .post, parameters: params)
            .responseJSON { (response) in
                if response.value != nil {
                    // print(response.result.value!)
                    //1225
                    self.tmpPackDetailNew.product_id = self.historyDatas[index].productID
                    
                    let swiftyJsonVar = JSON(response.value!)
                    let status: Int = swiftyJsonVar["status"].int!
                    //                print("status:\(status)")
                    if status == 200{
                        // token 正確, product_list not null
                        let restltJson = JSON(swiftyJsonVar["result"])
                        //                        print("restltJson in getChildCatalogShowNew :\(restltJson)")
                        let productLists = restltJson["product_content_detail"].arrayObject
                        //                        print("productLists:\(String(describing: productLists))")
                        //                        if productLists != nil{
                        for product in productLists! {
                            //                                print("Hi i : \(i)")
                            let productJson = JSON(product)
                            
                            self.tmpPackDetailNew.content_id = productJson["content_id"].int!
                            self.tmpPackDetailNew.content_name = productJson["content_name"].string!
                            self.tmpPackDetailNew.content_description = productJson["content_description"].string!
                            
                            var tempContentAttributeValueArray: [ContentAttributeValue] = []
                            
                            let contentAttributeValueArrayDatas = productJson["content_attribute_value"].arrayObject
                            for contentAttributeValueArrayData in contentAttributeValueArrayDatas!{
                                let contentAttributeValueDataJson = JSON(contentAttributeValueArrayData)
                                var tempContentAttributeValue: ContentAttributeValue = ContentAttributeValue()
                                
                                tempContentAttributeValue.attribute_id = contentAttributeValueDataJson["attribute_id"].int!
                                tempContentAttributeValue.attribute = contentAttributeValueDataJson["attribute"].string!
                                tempContentAttributeValue.attribute_value_id = contentAttributeValueDataJson["attribute_value_id"].int!
                                tempContentAttributeValue.attribute_value = contentAttributeValueDataJson["attribute_value"].string!
                                
                                tempContentAttributeValueArray.append(tempContentAttributeValue)
                            }
                            self.tmpPackDetailNew.contentAttributeValueArray = tempContentAttributeValueArray
                            
                            var tempStills: [Still] = []
                            
                            let stillsDatas = productJson["Stills"].arrayObject
                            for stillData in stillsDatas!{
                                let stillJson = JSON(stillData)
                                var tempStill: Still = Still()
                                
                                tempStill.assets_id = stillJson["assets_id"].string!
                                tempStill.name = stillJson["name"].string!
                                tempStill.url = stillJson["name"].string!
                                
                                var tempMetaDatas: [MetaData] = []
                                
                                let metaDatas = stillJson["meta_data"].arrayObject
                                
                                if metaDatas != nil{
                                    
                                    for metaData in metaDatas!{
                                        var tempMetaData: MetaData = MetaData()
                                        
                                        let metaDataJson = JSON(metaData)
                                        
                                        tempMetaData.attribute = metaDataJson["attribute"].string!
                                        tempMetaData.value = metaDataJson["value"].string!
                                        
                                        tempMetaDatas.append(tempMetaData)
                                    }
                                    
                                }
                                
                                tempStill.meta_data = tempMetaDatas
                                
                                tempStills.append(tempStill)
                            }
                            
                            self.tmpPackDetailNew.stills = tempStills
                            
                            var tempPosters: [Poster] = []
                            
                            let postersDatas = productJson["Poster"].arrayObject
                            
                            //1223
                            print("In CatalogMenu VC, func getPackDetailNew, postersDatas:\(postersDatas!),postersDatas.count:\(postersDatas!.count)")
                            
                            //1126
                            //                            if postersDatas?.count != 0 {
                            // 10/20
                            if postersDatas?.count == 2 {
                                for posterData in postersDatas!{
                                    
                                    let posterDataJson = JSON(posterData)
                                    var tempPoster: Poster = Poster()
                                    //
                                    tempPoster.assets_id = posterDataJson["assets_id"].string!
                                    tempPoster.name = posterDataJson["name"].string!
                                    tempPoster.url = posterDataJson["url"].string!
                                    
                                    var tempMetaDatas: [MetaData] = []
                                    
                                    let metaDatas = posterDataJson["meta_data"].arrayObject
                                    
                                    if metaDatas != nil{
                                        
                                        for metaData in metaDatas!{
                                            var tempMetaData: MetaData = MetaData()
                                            
                                            let metaDataJson = JSON(metaData)
                                            
                                            tempMetaData.attribute = metaDataJson["attribute"].string!
                                            tempMetaData.value = metaDataJson["value"].string!
                                            
                                            tempMetaDatas.append(tempMetaData)
                                        }
                                        
                                    }
                                    
                                    tempPoster.meta_data = tempMetaDatas
                                    
                                    //                            tempPosters.append(tempPoster)
                                    if metaDatas!.count != 0{
                                        tempPosters.append(tempPoster)
                                    }
                                    
                                }
                                
                            }else{
                                // 10/20
                                print("postersDatas?.count != 2")
                                
                                var tempPoster: Poster = Poster()
                                tempPosters = []
                                if self.historyDatas[index].contentImage.count == 1{
                                    tempPoster.assets_id = ""
                                    tempPoster.name = ""
                                    tempPoster.url = self.historyDatas[index].contentImage[0].url
                                    tempPosters.append(tempPoster)
                                    tempPosters.append(tempPoster)
                                }else{
                                    tempPoster.assets_id = ""
                                    tempPoster.name = ""
                                    for hd in self.historyDatas[index].contentImage {
                                        tempPoster.url = hd.url
                                        tempPosters.append(tempPoster)
                                    }
                                }
                            }
                            //                            else{
                            //                                // 1226 detail no poster
                            //                                //temp in packdetail VC
                            //                                //                            self.testImageView.downloaded(from: packDataNew.posters[0].url)
                            //                                //                            self.viedoImgView.downloaded(from: packDataNew.posters[1].url)
                            //
                            //                                var tempPoster: Poster = Poster()
                            //
                            //                                tempPoster.assets_id = ""
                            //                                tempPoster.name = ""
                            //                                // 10/14
                            ////                                tempPoster.url = self.productLists[i].product_image_L.url
                            ////                                tempPosters.append(tempPoster)
                            ////
                            ////                                tempPoster.url = self.productLists[i].product_image_P.url
                            //
                            //                                tempPosters.append(tempPoster)
                            //
                            //                            }
                            
                            
                            self.tmpPackDetailNew.posters = tempPosters
                            
                            let ratingJson = JSON(productJson["rating"])
                            //                        print("ratingJson:\(ratingJson)")
                            if ratingJson["avg_rating"].float != nil{
                                self.tmpPackDetailNew.rating_avg_rating = ratingJson["avg_rating"].float!
                            }
                            if ratingJson["total_rating"].float != nil{
                                self.tmpPackDetailNew.rating_total_rating = ratingJson["total_rating"].float!
                            }
                            
                            var tempTrailerHLSs: [TrailerHLS] = []
                            
                            let trailerHLSDatas = productJson["Trailer_HLS"].arrayObject
                            for trailerHLSData in trailerHLSDatas!{
                                let trailerHLSDataJson = JSON(trailerHLSData)
                                var tempTrailerHLS: TrailerHLS = TrailerHLS()
                                
                                tempTrailerHLS.assets_id = trailerHLSDataJson["assets_id"].string!
                                tempTrailerHLS.name = trailerHLSDataJson["name"].string!
                                //                            tempPoster.url = trailerHLSDataJson["url"].string!
                                
                                var tempMetaDatas: [MetaData] = []
                                
                                let metaDatas = trailerHLSDataJson["meta_data"].arrayObject
                                
                                if metaDatas != nil{
                                    
                                    for metaData in metaDatas!{
                                        var tempMetaData: MetaData = MetaData()
                                        
                                        let metaDataJson = JSON(metaData)
                                        
                                        tempMetaData.attribute = metaDataJson["attribute"].string!
                                        tempMetaData.value = metaDataJson["value"].string!
                                        
                                        tempMetaDatas.append(tempMetaData)
                                    }
                                    
                                }
                                
                                tempTrailerHLS.meta_data = tempMetaDatas
                                
                                tempTrailerHLSs.append(tempTrailerHLS)
                            }
                            
                            self.tmpPackDetailNew.trailerHLS = tempTrailerHLSs
                            
                            //0726 寫到這
                        }
                    }else if status == 204{
                        // product_list = null
                        //                    completion()
                    }else{
                        // token error
                        // 11/10
                        self.backToLoginVC(title:"Logout",msg: swiftyJsonVar["message"].string!,okMsg: "Back to LoginPage.")
//                        let alertController = UIAlertController(title: "Logout", message: swiftyJsonVar["message"].string!, preferredStyle: .alert)
//
//                        let okAction = UIAlertAction(
//                            title: "Back to LoginPage.",
//                            style: .default,
//                            handler: {
//                                (action: UIAlertAction!) -> Void in
//                                self.dismiss(animated: true, completion: nil)
//                        }
//                        )
//                        alertController.addAction(okAction)
//
//                        self.present(alertController, animated: true, completion: nil)
                    }
                }else{
                    print("HistoryVC.getPackDetailNew Error")
                }
                //            if i == self.childCatalogNew.count - 1{
                completion()
                //            }
                
                
        }
        
    }
    
}
