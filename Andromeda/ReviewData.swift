//
//  ReviewData.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2020/11/6.
//  Copyright © 2020 蒼月喵. All rights reserved.
//

import Foundation

struct ReviewData {
//    "reviews_id": 39,
//    "account_id": 26,
//    "product_id": 2,
//    "comments": "good movie",
//    "rating": 4.5,
//    "created_at": "2020-11-03 23:21:53",
//    "updated_at": "2020-11-03 23:21:53"
    var reviewsId: Int = 0
    var accountId: Int = 0
    var productId: Int = 0
    var comments: String = ""
    // 11/10
    var rating: CGFloat = 0
    var createdAt: String = ""
    var updatedAt: String = ""
}
