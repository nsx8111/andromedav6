//
//  productTCCategory.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2019/7/9.
//  Copyright © 2019 蒼月喵. All rights reserved.
//

import Foundation

struct ProductTCCategory {
    var product_type_id: Int = 0
    var content_category_id: Int = 0
    var name: String = ""
    var description: String = ""
    var productLists: [ProductList] = []
}

//"product_type_id": 1,
//"content_category_id": 1, //******//
//"name": "Action & adventure",
//"description": "Action & adventure Movies"
