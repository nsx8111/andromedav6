//
//  ViewHistoryTableViewCell.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2020/1/21.
//  Copyright © 2020 蒼月喵. All rights reserved.
//

import UIKit
import SnapKit

class ViewHistoryTableViewCell: UITableViewCell {
    
    /** for content image */
    var packImage = UIImageView()
    /** for tapped cell */
    var bkImage = UIImageView()
    
    var nameTitleLabel = UILabel()
    var nameValueLabel = UILabel()
    
    var lastestTitleLabel = UILabel()
    var lastestValueLabel = UILabel()
    
    var typeTitleLabel = UILabel()
    var typeValueLabel = UILabel()
    
    var startTitleLabel = UILabel()
    var startValueLabel = UILabel()
    
    var endTitleLabel = UILabel()
    var endValueLabel = UILabel()
    
    var lineView1 = UIView()
    var lineView2 = UIView()
    var lineView3 = UIView()
    var lineView4 = UIView()
    
    //    //waitting to mark
    //    var descriptionTextView = UITextView()
    
    // 08/13
    /** pageScrol 影片縮圖的高度 */
    //    let pageScrollImageHeight: CGFloat = 292
    let pageScrollImageHeight : CGFloat = {
        var cc : CGFloat = 292
        //        if deviceType == .iPhone {
        //            cc = 292 / 2
        //        }
        cc = tableViewRowHeight
        return cc
    }()
    /** pageScrol 影片縮圖的高度 */
    //    let pageScrollImageWidth: CGFloat = 219
    let pageScrollImageWidth : CGFloat = {
        var cc : CGFloat = 219
        //        if deviceType == .iPhone {
        //            cc = 292 / 2
        //        }
        cc = tableViewRowHeight
        return cc
    }()
    
    let leftOffset : CGFloat = {
        var cc : CGFloat = 30
        if deviceType == .iPhone {
            cc = 30 / 2
        }
        return cc
    }()
    
    let topOrBottomOffset : CGFloat = {
        var cc : CGFloat = 16
        if deviceType == .iPhone {
            cc = 16 / 2
        }
        return cc
    }()
    
    let fontSize : CGFloat = {
        var cc : CGFloat = 28
        if deviceType == .iPhone {
            cc = 28 / 2
        }else{
            cc = 28 / 1.3
        }
        return cc
    }()
    
    let timeFontSize : CGFloat = {
        var cc : CGFloat = 28
        if deviceType == .iPhone {
            cc = 13
        }else{
            cc = 28 / 1.3
        }
        return cc
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        bkImage.backgroundColor = .clear
        contentView.addSubview(bkImage)
        bkImage.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
        }
        
        contentView.addSubview(packImage)
        packImage.snp.makeConstraints { (makes) in
            makes.left.equalToSuperview().offset(leftOffset*1.0)
            makes.centerY.equalToSuperview()
//            if productImageTypeID == 1{
//
//            }else{
//
//            }
            makes.width.equalTo(pageScrollImageWidth/1.2)
            makes.height.equalTo(pageScrollImageHeight/1.2)
        }
        
        let lineViewOffset = pageScrollImageHeight/5
        
        contentView.addSubview(lineView1)
        lineView1.snp.makeConstraints { (makes) in
            makes.left.equalTo(packImage.snp.right).offset(0)
            makes.right.equalToSuperview()
            makes.top.equalToSuperview().offset(lineViewOffset)
            makes.height.equalTo(1)
        }
        
        contentView.addSubview(lineView2)
        lineView2.snp.makeConstraints { (makes) in
            makes.left.equalTo(packImage.snp.right).offset(0)
            makes.right.equalToSuperview()
            makes.top.equalToSuperview().offset(lineViewOffset*2)
            makes.height.equalTo(1)
        }
        
        contentView.addSubview(lineView3)
        lineView3.snp.makeConstraints { (makes) in
            makes.left.equalTo(packImage.snp.right).offset(0)
            makes.right.equalToSuperview()
            makes.top.equalToSuperview().offset(lineViewOffset*3)
            makes.height.equalTo(1)
        }
        
        contentView.addSubview(lineView4)
        lineView4.snp.makeConstraints { (makes) in
            makes.left.equalTo(packImage.snp.right).offset(0)
            makes.right.equalToSuperview()
            makes.top.equalToSuperview().offset(lineViewOffset*4)
            makes.height.equalTo(1)
        }
        
        lastestTitleLabel.text = "Lastest : "
        lastestTitleLabel.textColor = .white
        lastestTitleLabel.font = UIFont(name: "Georgia-Bold", size: fontSize)
        contentView.addSubview(lastestTitleLabel)
        lastestTitleLabel.snp.makeConstraints { (makes) in
            //            makes.centerY.equalTo(packImage.snp.top).offset(pageScrollImageHeight/3)
            //            makes.top.equalTo(packImage.snp.top).offset(pageScrollImageHeight*2/5 - topOrBottomOffset)
            makes.left.equalTo(packImage.snp.right).offset(leftOffset)
            makes.centerY.equalTo(lineView3)
        }
        
        lastestValueLabel.numberOfLines = 1
        lastestValueLabel.text = ""
        lastestValueLabel.textColor = .white
        lastestValueLabel.font = UIFont(name: "Georgia-Bold", size: fontSize)
        contentView.addSubview(lastestValueLabel)
        lastestValueLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(lineView3)
            makes.left.equalTo(lastestTitleLabel.snp.right)
            //            makes.right.equalTo(lineView1.snp.right).offset(spacing1)
        }
        
        nameValueLabel.numberOfLines = 2
        nameValueLabel.text = ""
        nameValueLabel.textColor = .white
        nameValueLabel.font = UIFont(name: "Georgia-Bold", size: fontSize)
        contentView.addSubview(nameValueLabel)
        nameValueLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(lineView1).offset(lineViewOffset/3)
            makes.left.equalTo(lastestValueLabel.snp.left)
            makes.right.lessThanOrEqualToSuperview().offset(-offset1)
        }
        
        nameTitleLabel.text = "Name : "
        nameTitleLabel.textColor = .white
        nameTitleLabel.font = UIFont(name: "Georgia-Bold", size: fontSize)
        contentView.addSubview(nameTitleLabel)
        nameTitleLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(lineView1).offset(lineViewOffset/3)
            makes.right.equalTo(lastestTitleLabel)
        }
        
        startTitleLabel.text = "Start : "
        startTitleLabel.textColor = .white
        startTitleLabel.font = UIFont(name: "Georgia-Bold", size: fontSize)
        contentView.addSubview(startTitleLabel)
        startTitleLabel.snp.makeConstraints { (makes) in
            //            makes.centerY.equalTo(packImage.snp.bottom).offset(-pageScrollImageHeight/3)
            //            makes.top.equalTo(packImage.snp.top).offset(pageScrollImageHeight*3/5 - topOrBottomOffset)
            //            makes.top.greaterThanOrEqualTo(lastestTitleLabel.snp.bottom).offset(1)
            makes.centerY.equalTo(lineView4)
            makes.right.equalTo(lastestTitleLabel)
        }
        
        startValueLabel.numberOfLines = 0
        startValueLabel.text = ""
        startValueLabel.textColor = .white
        startValueLabel.font = UIFont(name: "Georgia-Bold", size: timeFontSize)
        contentView.addSubview(startValueLabel)
        startValueLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(lineView4)
            makes.left.equalTo(startTitleLabel.snp.right)
        }
        
//        endTitleLabel.text = "End : "
//        endTitleLabel.textColor = .white
//        endTitleLabel.font = UIFont(name: "Georgia-Bold", size: fontSize)
//        contentView.addSubview(endTitleLabel)
//        endTitleLabel.snp.makeConstraints { (makes) in
//            //            makes.bottom.equalTo(packImage)
//            //            makes.top.equalTo(packImage.snp.top).offset(pageScrollImageHeight*4/5 - topOrBottomOffset)
//            //            makes.top.greaterThanOrEqualTo(startTitleLabel.snp.bottom).offset(topOrBottomOffset)
//            makes.centerY.equalTo(lineView4)
//            makes.right.equalTo(lastestTitleLabel)
//        }
//
//        endValueLabel.numberOfLines = 0
//        endValueLabel.text = ""
//        endValueLabel.textColor = .white
//        endValueLabel.font = UIFont(name: "Georgia-Bold", size: timeFontSize)
//        contentView.addSubview(endValueLabel)
//        endValueLabel.snp.makeConstraints { (makes) in
//            makes.centerY.equalTo(lineView4)
//            makes.left.equalTo(endTitleLabel.snp.right)
//            //            makes.right.equalToSuperview().offset(spacing1)
//        }

        
        //yang 0904
        //        switch UIDevice.current.userInterfaceIdiom {
        //        case .pad:
        //            self.pageScrollImageHeight = 292
        //            self.pageScrollImageWidth = 219
        //            lastestTitleLabel.font = UIFont(name: "Georgia-Bold", size: 28)
        //            lastestValueLabel.font = UIFont(name: "Georgia-Bold", size: 28)
        //            nameTitleLabel.font = UIFont(name: "Georgia-Bold", size: 28)
        //            nameValueLabel.font = UIFont(name: "Georgia-Bold", size: 28)
        //            startTitleLabel.font = UIFont(name: "Georgia-Bold", size: 28)
        //            startValueLabel.font = UIFont(name: "Georgia-Bold", size: 28)
        //            endTitleLabel.font = UIFont(name: "Georgia-Bold", size: 28)
        //            endValueLabel.font = UIFont(name: "Georgia-Bold", size: 28)
        //        case .phone:
        //            self.pageScrollImageHeight = 190
        //            self.pageScrollImageWidth = 190 / 1.5
        //            lastestTitleLabel.font = UIFont(name: "Georgia-Bold", size: 16)
        //            lastestValueLabel.font = UIFont(name: "Georgia-Bold", size: 16)
        //            nameTitleLabel.font = UIFont(name: "Georgia-Bold", size: 16)
        //            nameValueLabel.font = UIFont(name: "Georgia-Bold", size: 16)
        //            startTitleLabel.font = UIFont(name: "Georgia-Bold", size: 16)
        //            startValueLabel.font = UIFont(name: "Georgia-Bold", size: 16)
        //            endTitleLabel.font = UIFont(name: "Georgia-Bold", size: 16)
        //            endValueLabel.font = UIFont(name: "Georgia-Bold", size: 16)
        //        default:
        //            print("3 Device Error")
        
        
        //        //waitting to mark
        //        contentView.addSubview(descriptionTextView)
        //        descriptionTextView.text = ""
        //        descriptionTextView.font = UIFont(name: "Georgia-Bold", size: 30)
        //        descriptionTextView.textColor = .white
        //        descriptionTextView.backgroundColor = .clear
        //        descriptionTextView.isScrollEnabled = true
        //        descriptionTextView.isEditable = false
        //        descriptionTextView.isSelectable = false
        //
        //        descriptionTextView.snp.makeConstraints { (makes) in
        //            makes.left.equalTo(nameTitleLabel)
        //            makes.right.equalToSuperview().offset(-30)
        //            makes.top.equalTo(nameTitleLabel.snp.bottom).offset(30)
        //            makes.height.equalTo(pageScrollImageHeight*3/5)
        //        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //        packImage.downloaded(from: url)
        //        self.contentView.addSubview(packImage)
        //        packImage.snp.makeConstraints { (makes) in
        //            makes.top.left.equalToSuperview().offset(10)
        //            makes.bottom.equalToSuperview().offset(-10)
        //        }
        //
        //        nameTitleLabel.text = "name"
        //        self.contentView.addSubview(nameTitleLabel)
        //        nameTitleLabel.snp.makeConstraints { (makes) in
        //            makes.center.equalToSuperview()
        //        }
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
//                print("selected")
        // Configure the view for the selected state
    }
    
//    override func select(_ sender: Any?) {
//        print("override func select(_ sender: Any?)")
//    }
    
    //    override func select
    
}
