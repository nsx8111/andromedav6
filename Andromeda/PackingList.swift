//
//  PackingList.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2019/5/21.
//  Copyright © 2019 蒼月喵. All rights reserved.
//

import Foundation

/** 存放 pack Dalta */
struct Pack {
    /** viedo ID */
    var packingId: Int = 0
    /** viedo Mode ??? */
    var packingMode: Int = 0
    /** viedo name */
    var name: String = ""
    /** 縮圖 small */
    var smallUrl: String = ""
    /** 縮圖 medium */
    var mediumUrl: String = ""
    /** 縮圖 large */
    var largeUrl: String = ""
    
    /** 縮圖 horizontal small */
    var horizontalSmallUrl: String = ""
    /** 縮圖 horizontal medium */
    var horizontalMediumUrl: String = ""
    /** 縮圖 horizontal large */
    var horizontalLargeUrl: String = ""
    
    
    var packDetail: PackDetail? = nil
    
    
}

/** 存放 pack detail */
struct PackDetail {
//
//    /** 簽發國家 ??? */
//    var issueCountry: String
//    /** 簽發日期 ??? */
//    var issueDate: String
    
//    /** viedo language */
//    var language: String
//    /** 副標題 */
//    var subTitle: String
    /** 影片介紹 */
    var description: String = ""
    /** viedo length */
    var viedoLength: Int = 0
    /** 導演 */
    var director: String = ""
    /** 作者 */
    var writer: String = ""
    /** 演員 */
    var hero: String = ""
    /**
     Rating
     example "1,G"
     */
    var rating: String = ""
// ??
    //    "film_num": "0",
// is_free" 0 免費 1 付費
    //    "is_free": "0",
// 似乎用不到
    //    "preview_url": "default_F.jpg",
// is_visible ?
    //    "is_visible": "1",
// 似乎用不到
    //    "visible_from": "2018-08-01 00:00:00",
// 似乎用不到
    //    "visible_to": null,
// 這個似乎都 null = =?
    //    "sub_title": "",
// use
    //    "description": "When a successful, but tired Tokyo-based playwright who has sworn off easy women and casual encounters takes refuge in the countryside, his plans are disrupted by a horny woman who pedals fast into his life and is unrelenting.",
// 似乎用不到
    //    "issue_date": "2017-11-17 00:00:00",
// 似乎用不到
    //    "issue_country": "Japan",
// 似乎用不到
    //    "language": "Japanese",
// use
    //    "video_length": "77",
// use
    //    "director": "",
// use
    //    "hero": "",
// 似乎用不到
    //    "heroine": null,
    //    "keyword": "",
    //    "promote_txt": "",
    //    "issuer": "",
    //    "PackingPrice": null,
    //    "SellPrice": null,
    //    "FavourablePrice": null,
    //    "MemberPrice": null,
    //    "BillType": null,
    //    "packing_mode": "3",
    //    "MainCatalog": "103,成人",
    //    "SubCatalog": "131,亞洲成人",
// use
    //    "Rating": "1,G",
    //    "RatingSystem": "1,MPAA",
    //    "content_source_id": "10",
    //    "content_id": "13",
// use
    //    "writer": "",
    //    "isPurchase": 0,
    //    "hasTrailer": 0

    
    
    //    "PackingPrice": "1",
    //    "visible_from": "2019-05-01 00:00:00",
    //    "visible_to": null,
    //    "order": "1",
    //    "catalog_id": "101",
    //    "parent": "10",
    //    "hls_url": "http:\/\/andromeda.tti.tv\/AndromedaMedia\/103\/HLS\/103-8c1e53a9-b209-404c-be30-004afc11db04.m3u8"
    
}
//    Example
//    "packing_id": "87",
//    "packing_mode": "3",
//    "name": "Nous Finirons Ensemble - Bande-annonce officielle HD",
//    "issue_country": "",
//    "issue_date": "2019-05-15 00:00:00",
//    "video_length": "123",
//    "language": "English",
//    "sub_title": "",
//    "director": "",
//    "hero": "",
//    "small_url": "59S.jpg",
//    "medium_url": "59M.jpg",
//    "large_url": "59L.jpg",
//    "horizontal_small_url": "59HS.jpg",
//    "horizontal_medium_url": "59HM.jpg",
//    "horizontal_large_url": "59HL.jpg",
//    "PackingPrice": "1",
//    "visible_from": "2019-05-01 00:00:00",
//    "visible_to": null,
//    "order": "1",
//    "catalog_id": "101",
//    "parent": "10",
//    "hls_url": "http:\/\/andromeda.tti.tv\/AndromedaMedia\/103\/HLS\/103-8c1e53a9-b209-404c-be30-004afc11db04.m3u8"

func howManyPeople(title:String,serverString:String)->String{
    var ss = serverString
    if serverString.isEmpty {
        return ""
    }else{
        ss = "\(title) : \(serverString)"
    }
    if ss.contains(","){
        return ss.replace(target: ",", withString: ",\n　　   ")
    }else{
        return ss
    }
}
