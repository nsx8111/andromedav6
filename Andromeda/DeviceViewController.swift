//
//  DeviceViewController.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2018/10/4.
//  Copyright © 2018年 蒼月喵. All rights reserved.
//

import UIKit

class DeviceViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func handleEdgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger(location: location, state: sender.state)
    }

    @IBAction func handleEdgePanGesture2(_ sender: UIScreenEdgePanGestureRecognizer) {
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger2(location: location, state: sender.state)
    }
    
    @IBAction func handleTapGesture(_ sender: Any) {
        if navigationController?.view.superview?.frame.origin.x != 0 {
            navigationController?.view.menu()
        }
    }
    @IBAction func viedoClick(_ sender: Any) {
        navigationController?.view.menu()
    }
    @IBAction func userClick(_ sender: Any) {
        navigationController?.view.menu2()
    }
    
}
