//
//  productList.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2019/7/9.
//  Copyright © 2019 蒼月喵. All rights reserved.
//

import Foundation

struct PurchaseOptionList {
    var productID: Int = 0
    var contentID: Int = 0
    var price: String = ""
    var currency: String = ""
    var priceType: String = ""
    var price_start_time: String = ""
    var price_end_time: String = ""
}

struct PurchaseList {
    var productID: Int = 0
    var productName: String = ""
    var purchaseTime: String = ""
    var productType: String = ""
    var productImage = [Product_image2]()
    // Landscape
    var product_image_L: Product_image = Product_image()
    // Portrait
    var product_image_P: Product_image = Product_image()
}

struct RentList {
    var productID: Int = 0
    var productName: String = ""
    var rentEndTime: String = ""
    var productType: String = ""
    var productImage = [Product_image2]()
    // Landscape
    var product_image_L: Product_image = Product_image()
    // Portrait
    var product_image_P: Product_image = Product_image()
}

struct FavoriteList {
    var productID: Int = 0
    var categoryID: Int = 0
    var productName: String = ""
    var favoriteCategory: String = ""
    var updated_at: String = ""
    var created_at: String = ""
    var productType: String = ""
    var productImage = [Product_image2]()
    // Landscape
    var product_image_L: Product_image = Product_image()
    // Portrait
    var product_image_P: Product_image = Product_image()
}

struct Product_image2 {
    var product_image_type_id: Int = 0
    var product_image_id: String = ""
    var product_image_type: String = ""
    var url: String = ""
    var name: String = ""
    var description: String = ""
}

struct ProductList {
    var product_id: Int = 0
    var product_name: String = ""
    var product_description: String = ""
    var product_type: String = ""
    var start_time: String = ""
    var end_time: String? = nil
    // Landscape
    var product_image_L: Product_image = Product_image()
    // Portrait
    var product_image_P: Product_image = Product_image()
    var rating_avg_rating: Float = 0.0
    var rating_total_rating: Float = 0.0
}

struct Product_image {
    var product_image_id: String = ""
    var product_image_type_id: Int = 0
    var name: String = ""
    var description: String = ""
    var url: String = ""
    
//    這個應該用不到 有 typ_id 就可以判斷直向橫向了
//    var product_image_type: String = ""
}


//"product_id": 2, //******//
//"product_name": "Shazam!",
//"product_description": "Shazam!",
//"product_type": "movie",
//"start_time": "2019-04-24 00:00:00",
//"end_time": null,
//"product_image": [
//{
//    "product_image_id": "product_2_1.jpg",
//    "product_image_type_id": 1,
//    "product_image_type": "Landscape thumbnail",
//    "name": "test",
//    "description": "test",
//    "url": "http://rddev.andromeda.tti.tv/AndromedaProduct/image/2/product_2_1.jpg"
//},
//{
//    "product_image_id": "product_2_2.jpg",
//    "product_image_type_id": 2,
//    "product_image_type": "Portrait thumbnail",
//    "name": "test",
//    "description": "test",
//    "url": "http://rddev.andromeda.tti.tv/AndromedaProduct/image/2/product_2_2.jpg"
//}
//],
//"rating": {
//    "avg_rating": 3.5,
//    "total_rating": 3
//}
