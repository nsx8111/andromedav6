//
//  RentTableViewCell.swift
//  Andromeda
//
//  Created by 洋洋 on 2020/12/23.
//  Copyright © 2020 蒼月喵. All rights reserved.
//

import UIKit
import SnapKit

class RentTableViewCell: UITableViewCell {
    /** for content image */
    var packImage = UIImageView()
    /** for tapped cell */
    var bkImage = UIImageView()
    
    var typeTitleLabel = UILabel()
    var typeValueLabel = UILabel()
    
    var nameTitleLabel = UILabel()
    var nameValueLabel = UILabel()
    
    var timeTitleLabel = UILabel()
    var timeValueLabel = UILabel()
   
    //    //waitting to mark
    //    var descriptionTextView = UITextView()
    
    /** pageScrol 影片縮圖的高度 */
    //    let pageScrollImageHeight: CGFloat = 292
    let pageScrollImageHeight : CGFloat = {
        var cc : CGFloat = 292
        //        if deviceType == .iPhone {
        //            cc = 292 / 2
        //        }
        cc = tableViewRowHeight
        return cc
    }()
    /** pageScrol 影片縮圖的高度 */
    //    let pageScrollImageWidth: CGFloat = 219
    let pageScrollImageWidth : CGFloat = {
        var cc : CGFloat = 219
        //        if deviceType == .iPhone {
        //            cc = 292 / 2
        //        }
        cc = tableViewRowHeight
        return cc
    }()
    
    let leftOffset : CGFloat = {
        var cc : CGFloat = 30
        if deviceType == .iPhone {
            cc = 30 / 2
        }
        return cc
    }()
    
    let topOrBottomOffset : CGFloat = {
        var cc : CGFloat = 16
        if deviceType == .iPhone {
            cc = 16 / 2
        }
        return cc
    }()
    
    let fontSize : CGFloat = {
        var cc : CGFloat = 28
        if deviceType == .iPhone {
            cc = 28 / 2
        }else{
            cc = 28 / 1.5
        }
        return cc
    }()
    
    let timeFontSize : CGFloat = {
        var cc : CGFloat = 28
        if deviceType == .iPhone {
            cc = 13
        }else{
            cc = 28 / 1.5
        }
        return cc
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        bkImage.backgroundColor = .clear
        contentView.addSubview(bkImage)
        bkImage.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
        }
        
        contentView.addSubview(packImage)
        packImage.snp.makeConstraints { (makes) in
            makes.left.equalToSuperview().offset(leftOffset*1.0)
            makes.centerY.equalToSuperview()
            if productImageTypeID == 2{
                makes.width.equalTo(pageScrollImageWidth/1)
                makes.height.equalTo(pageScrollImageHeight/1)
            }else{
                makes.width.equalTo(pageScrollImageWidth/1.2)
                makes.height.equalTo(pageScrollImageHeight/1.2)
            }
        }
        
//        typeTitleLabel.text = "Type : "
//        typeTitleLabel.textColor = .white
//        typeTitleLabel.font = UIFont(name: "Georgia-Bold", size: fontSize*1.1)
//        contentView.addSubview(typeTitleLabel)
//        typeTitleLabel.snp.makeConstraints { (makes) in
//            makes.top.equalTo(packImage).offset(offset0)
//            makes.left.equalTo(packImage.snp.right).offset(offset1)
//        }
        
        typeValueLabel.numberOfLines = 1
        typeValueLabel.text = ""
        typeValueLabel.textColor = .white
        typeValueLabel.font = UIFont(name: "Georgia-Bold", size: fontSize*1.1)
        contentView.addSubview(typeValueLabel)
        typeValueLabel.snp.makeConstraints { (makes) in
//            makes.centerY.equalTo(typeTitleLabel)
//            makes.left.equalTo(typeTitleLabel.snp.right)
            makes.top.equalTo(packImage.snp.top).offset(offset1)
            makes.left.equalTo(packImage.snp.right).offset(offset1)
        }
        
//        nameTitleLabel.text = "Name : "
//        nameTitleLabel.textColor = .white
//        nameTitleLabel.font = UIFont(name: "Georgia-Bold", size: fontSize*1.3)
//        contentView.addSubview(nameTitleLabel)
//        nameTitleLabel.snp.makeConstraints { (makes) in
//            makes.left.equalTo(packImage.snp.right).offset(offset1)
//            makes.top.equalTo(typeTitleLabel).offset(offset7)
//        }
        
        nameValueLabel.numberOfLines = 2
        nameValueLabel.text = ""
        nameValueLabel.textColor = .white
        nameValueLabel.font = .boldSystemFont(ofSize: fontSize*1.2)
        contentView.addSubview(nameValueLabel)
        nameValueLabel.snp.makeConstraints { (makes) in
//            makes.centerY.equalTo(nameTitleLabel)
//            makes.left.equalTo(nameTitleLabel.snp.right)
            makes.centerY.equalTo(packImage)
//            makes.top.equalTo(typeValueLabel.snp.bottom).offset(offset1)
            makes.left.equalTo(packImage.snp.right).offset(offset1)
            makes.right.lessThanOrEqualToSuperview().offset(-offset1)
        }
        
        timeTitleLabel.text = "Purchase Time : "
        timeTitleLabel.textColor = .white
        timeTitleLabel.font = UIFont(name: "Georgia-Bold", size: fontSize*1.0)
        contentView.addSubview(timeTitleLabel)
        timeTitleLabel.snp.makeConstraints { (makes) in
            makes.bottom.equalTo(packImage.snp.bottom).offset(-offset1)
            makes.left.equalTo(packImage.snp.right).offset(offset1)
        }
        
        timeValueLabel.numberOfLines = 1
        timeValueLabel.text = ""
        timeValueLabel.textColor = .white
        timeValueLabel.font = UIFont(name: "Georgia-Bold", size: fontSize*1.0)
        contentView.addSubview(timeValueLabel)
        timeValueLabel.snp.makeConstraints { (makes) in
            makes.bottom.equalTo(packImage.snp.bottom).offset(-offset1)
            makes.right.lessThanOrEqualToSuperview().offset(-1)
            makes.left.greaterThanOrEqualTo(timeTitleLabel.snp.right).offset(0)
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
  
}
