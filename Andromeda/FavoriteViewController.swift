//
//  FavoriteViewController.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2020/9/21.
//  Copyright © 2020 蒼月喵. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SnapKit

class FavoriteViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favoriteLists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = favoriteTableView.dequeueReusableCell(withIdentifier: "favCell") as! FavoriteTableViewCell
        let bkView : UIView = UIView()
        cell.selectedBackgroundView = bkView
        cell.backgroundColor = .clear
        cell.separatorInset = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        cell.nameValueLabel.text = favoriteLists[indexPath.row].productName
        cell.categoryValueLabel.text = favoriteLists[indexPath.row].favoriteCategory
        cell.typeValueLabel.text = favoriteLists[indexPath.row].productType
        cell.creationValueLabel.text = favoriteLists[indexPath.row].created_at
        cell.updateValueLabel.text = favoriteLists[indexPath.row].updated_at
        
        if favoriteLists[indexPath.row].productImage.count > 1 {
            cell.packImage.downloaded(from: favoriteLists[indexPath.row].productImage[0].url)
        }else if favoriteLists[indexPath.row].productImage.count == 1 {
            cell.packImage.downloaded(from: favoriteLists[indexPath.row].productImage[0].url)
        }else{
            cell.packImage.image = UIImage(named: "noContentImage")
        }
        
        cell.tag = indexPath.row
        let tap = UITapGestureRecognizer(target: self, action: #selector(clickCell(_:)))
        cell.addGestureRecognizer(tap)
        return cell
    }
    /** 實作TableView方法，自動出現左滑功能 */
    func tableView(_ tableView: UITableView,commit editingStyle: UITableViewCell.EditingStyle,forRowAt indexPath: IndexPath){
        let select:Int = indexPath.row
        //        self.setActView(openOrClose: true)
        if editingStyle == .delete{
            self.modifyFavoriteStatus(select: select,bool: false){
                self.getMyFavoriteData(){
                    self.favoriteTableView.reloadData()
                    //                    self.setActView(openOrClose: false)
//                    print("favCount5 \(favoriteLists.count)")
                }
            }
        }
    }
    /** 自訂delete的文字為刪除 */
    func tableView(_ tableView: UITableView,titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath)-> String?{
        return "Delete"
    }
    // 不知名原因不能用，解決辦法為 cell + tapGesture
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didSelectRowAt:\(indexPath.row)")
        // print(self.historyDatas[indexPath.row].productID)
    }
    
    @objc func clickCell(_ sender:AnyObject?) {
        print(sender?.view.tag as Any)
        let index : Int = sender?.view.tag ?? -1
        if index != -1 {
            self.setActView(openOrClose: true)
            self.getPackDetailNew(index: index) {
                self.setActView(openOrClose: false)
                self.ButtonDelegate?.toPackPageNew(packDataNew: self.tmpPackDetailNew)
                self.setActView(openOrClose: false)
            }
        }
    }
    
    /** 點擊屏幕用 */
    let contentView = UIView()
    var favoriteTableView = UITableView()

    @IBOutlet weak var pageTitleLabel: UILabel!
    weak var ButtonDelegate: ButtonDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        setGestures()
        setViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let name = NSNotification.Name("removeContentView")
        NotificationCenter.default.addObserver(self, selector:
            #selector(updateContentViewNoti(noti:)), name: name, object: nil)
//        print("isFavoriteChanged \(isFavoriteChanged)")
        self.setActView(openOrClose: true)
        if isFavoriteChanged == false{
            getMyFavoriteData(){ [weak self] in
                guard let self = self else { return }
//                print("favCount1 \(favoriteLists.count)")
                self.favoriteTableView.reloadData()
                self.setActView(openOrClose: false)
            }
        }else{
            let name = Notification.Name("getFavoriteStatus")
            NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
//            print("favCount2 \(favoriteLists.count)")
            DispatchQueue.global().async {
                self.getMyFavoriteData(){ [weak self] in
                    guard let self = self else { return }
//                    print("favCount3 \(favoriteLists.count)")
                    DispatchQueue.main.async {
                        self.favoriteTableView.reloadData()
                        self.setActView(openOrClose: false)
                        isFavoriteChanged = false
                    }
                }
            }
        }
    }
    
    func setGestures(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTapGesture(_:)))
        let pan: UIScreenEdgePanGestureRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(self.handleEdgePanGesture(_:)))
        let pan2: UIScreenEdgePanGestureRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(self.handleEdgePanGesture2(_:)))
        view.addGestureRecognizer(tap)
        view.addGestureRecognizer(pan)
        view.addGestureRecognizer(pan2)
    }
    
    func setViews(){
        favoriteTableView.register(FavoriteTableViewCell.self, forCellReuseIdentifier: "favCell")
        favoriteTableView.delegate = self
        favoriteTableView.dataSource = self
        favoriteTableView.bounces = true
        favoriteTableView.rowHeight = tableViewRowHeight
        favoriteTableView.backgroundColor = .buttonColor3
        favoriteTableView.separatorColor = .white
        favoriteTableView.tableFooterView = UIView(frame: CGRect.zero)
        view.addSubview(favoriteTableView)
        favoriteTableView.snp.makeConstraints { (makes) in
            makes.top.equalTo(pageTitleLabel.snp.bottom).offset(offset1)
            makes.left.right.bottom.equalToSuperview()
        }
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        refreshControl.attributedTitle = NSAttributedString(string: "loading...", attributes: attributes)
        refreshControl.tintColor = .white
        favoriteTableView.addSubview(refreshControl)
    }
    
    var canRefresh = true
    var refreshControl: UIRefreshControl!
    /**
     下滑頁面會刷新資料
     - Date: 12/15 Yang
     */
//    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
//    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < -offset7 * HeightForScroll {
            if canRefresh && !self.refreshControl.isRefreshing {
                self.canRefresh = false
                self.refreshControl.beginRefreshing()
                self.refresh()
            }
            if scrollView.contentOffset.y < -refreshControl.frame.size.height{
                scrollView.isScrollEnabled = false
            }
        }else if scrollView.contentOffset.y >= 0 {
            self.canRefresh = true
            scrollView.isScrollEnabled = true
        }
    }
    
    @objc func refresh() {
        getMyFavoriteData(){
            self.favoriteTableView.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
    /**
     刪除我的最愛
     - Date: 10/15 Yang
     */
    func modifyFavoriteStatus(select: Int,bool:Bool,completion : @escaping ()->()){
        var addOrDelete : String = ""
        if bool{
            //新增到最愛
            addOrDelete = "addMyFavorite"
        }else{
            //從最愛移除
            addOrDelete = "deleteMyFavorite"
        }
//        print(favoriteLists[select].productID)
        let params = [
            "product_id": favoriteLists[select].productID,
            "token": "\(token)"
            ] as [String : Any]
        
        AF.request("\(serverUrlNew)product/\(addOrDelete)", method: .post, parameters: params)
            .responseJSON { (response) in
                if response.value != nil {
                    //                    print(response.result.value!)
                    let swiftyJsonVar = JSON(response.value!)
                    let status: Int = swiftyJsonVar["status"].int!
                    print("status:\(status)")
                    if status == 404{
                        // token error
                        // 11/10
                        self.backToLoginVC(title:"Logout",msg: swiftyJsonVar["message"].string!,okMsg: "Back to LoginPage.")
//                        let alertController = UIAlertController(title: "Logout", message: swiftyJsonVar["message"].string!, preferredStyle: .alert)
//                        let okAction = UIAlertAction(
//                            title: "Back to LoginPage.",
//                            style: .default,
//                            handler: {
//                                (action: UIAlertAction!) -> Void in
//                                self.dismiss(animated: true, completion: nil)
//                        })
//                        alertController.addAction(okAction)
//                        self.present(alertController, animated: true, completion: nil)
                    }else{
                        // token 正確
                        completion()
                    }
                }else{
                    print("modifyFavoriteStatus Error")
                    completion()
                }
                
                
        }
        
    }
    /**
     取得我的最愛類別
     - Date: 09/28 Yang
     */
    func getMyFavoriteData(completion : @escaping ()->()) {
        let params = [
            // "my_favorite_category_id": nil,
            "token":"\(token)"
            ] as [String : Any]
        
        var tempFavoriteLists: [FavoriteList] = []
        let getMyFavoriteAPI = serverUrlNew + "account/getMyFavorite"

        AF.request(getMyFavoriteAPI, method: .post, parameters: params)
            .responseJSON{ (response) in
                if response.value != nil{
                    let swiftyJsonVar = JSON(response.value!)
                    let status: Int = swiftyJsonVar["status"].int!

                    if status == 200 { ///包含類別
                        if let resultObject = swiftyJsonVar["result"].dictionaryObject, let myFavoriteArray = resultObject["my_favorite"] {
                            print("AnyObject \((myFavoriteArray as AnyObject).count!)")
//                            print("resultObject \(resultObject)")
                            let swiftyJsonVar2 = JSON(myFavoriteArray)
                            if let myFavoriteArray2 = swiftyJsonVar2.arrayObject {
                                let reversedProductArray : [Any] = Array(myFavoriteArray2.reversed())

                                for myFavData in reversedProductArray {
                                    let favoriteJson = JSON(myFavData)
                                    var tempFavData: FavoriteList = FavoriteList()
                                    
                                    if favoriteJson["product_id"].int != nil{
                                        tempFavData.productID = favoriteJson["product_id"].int!
                                        print("tempProductID \(tempFavData.productID)")
                                    }
                                    if favoriteJson["product_name"].string != nil{
                                        tempFavData.productName = favoriteJson["product_name"].string!
                                    }
                                    if favoriteJson["created_at"].string != nil{
                                        tempFavData.created_at = favoriteJson["created_at"].string!
//                                        print("created_at \(tempFavData.created_at)")
                                    }
                                    if favoriteJson["updated_at"].string != nil{
                                        tempFavData.updated_at = favoriteJson["updated_at"].string!
                                        print("updated_at \(tempFavData.updated_at)")
                                    }
                                    if favoriteJson["my_favorite_category"].string != nil{
                                        tempFavData.favoriteCategory = favoriteJson["my_favorite_category"].string!
                                        //                                        print("favoriteCategory \(tempFavData.favoriteCategory)")
                                    }else{
                                        tempFavData.favoriteCategory = "null"
                                    }
                                    if favoriteJson["product_type"].string != nil{
                                        tempFavData.productType = favoriteJson["product_type"].string!
                                        //                                        print("productType \(tempFavData.productType)")
                                    }
                                    
                                    var CIs: [Product_image2] = []
                                    if favoriteJson["product_image"].count > 0 {
                                        let cis = favoriteJson["product_image"].arrayObject
                                        for ci in cis!{
                                            let ciJSON = JSON(ci)
                                            var tempCI: Product_image2 = Product_image2()
                                            
                                            if ciJSON["product_image_type_id"].int != nil{
                                                tempCI.product_image_type_id = ciJSON["product_image_type_id"].int!
                                                productImageTypeID = ciJSON["product_image_type_id"].int!
                                                print("productImageTypeID_Fav \(productImageTypeID)")
                                            }
                                            if ciJSON["product_image_id"].string != nil{
                                                tempCI.product_image_id = ciJSON["product_image_id"].string!
                                            }
                                            if ciJSON["product_image_type"].string != nil{
                                                tempCI.product_image_type = ciJSON["product_image_type"].string!
                                            }
                                            if ciJSON["url"].string != nil{
                                                tempCI.url = ciJSON["url"].string!
                                            }
                                            if ciJSON["name"].string != nil{
                                                tempCI.name = ciJSON["name"].string!
                                            }
                                            if ciJSON["description"].string != nil{
                                                tempCI.description = ciJSON["description"].string!
                                            }
                                            CIs.append(tempCI)
                                        }
                                        tempFavData.productImage = CIs
                                    }
                                    tempFavoriteLists.append(tempFavData)
                                }
                            }
                        }
                        favoriteLists = tempFavoriteLists
                        completion()
                    }else if status == 204 { ///沒有類別
                        print("get my favorite category success, but my favorite category is empty")
                        
                        if let resultObject = swiftyJsonVar["result"].dictionaryObject, let myFavoriteArray = resultObject["my_favorite"] {
                            print((myFavoriteArray as AnyObject).count!)
                            print("resultObject200:\(resultObject)")
                            print("favCategoryArray200:\(myFavoriteArray)")
                        }
                        completion()
                    }else{
                        print("token ＆ get datas error")
                        self.backToLoginVC(title:"Logout",msg: swiftyJsonVar["message"].string!,okMsg: "Back to LoginPage.")
                        
                        //                        let alertController = UIAlertController(title: "Logout", message: swiftyJsonVar["message"].string!, preferredStyle: .alert)
                        //                        let okAction = UIAlertAction(
                        //                            title: "Back to LoginPage.",
                        //                            style: .default,
                        //                            handler: {
                        //                                (action: UIAlertAction!) -> Void in
                        //                                self.dismiss(animated: true, completion: nil)
                        //                        })
                        //                        alertController.addAction(okAction)
                        //                        self.present(alertController, animated: true, completion: nil)
                        completion()
                    }
                }else{
                    print("response.result.isFalse @getMyFavoriteCategory")
                    favoriteLists = []
                    self.favoriteTableView.reloadData()
                    completion()
                }
                
                
                
        }
        
    }
    
    
    @IBAction func handleEdgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger(location: location, state: sender.state)
        if beganX2 > 0 {
            ButtonDelegate?.clickLeft()
            showContentView()
        }else{
            contentView.removeFromSuperview()
        }
    }
    
    @IBAction func handleEdgePanGesture2(_ sender: UIScreenEdgePanGestureRecognizer) {
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger2(location: location, state: sender.state)
        if beganX2 < 0{
            ButtonDelegate?.clickRight()
            showContentView()
        }else{
            contentView.removeFromSuperview()
        }
    }
    
    @IBAction func handleTapGesture(_ sender: Any) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        print("FavriteOriginX \(navigationController?.view.superview?.frame.origin.x as Any)")
        if navigationController?.view.superview?.frame.origin.x != 0 {
            if navigationController?.view.superview?.frame.origin.x == -navigationOriginX {
                navigationController?.view.menu2()
                contentView.removeFromSuperview()
            }else{
                navigationController?.view.menu()
                contentView.removeFromSuperview()
            }
        }
    }
    /**
     點擊屏幕回彈用
     - Date: 10/20 Yang
     */
    @objc func showContentView(){
        contentView.tag = 1000
        contentView.isUserInteractionEnabled = true
        contentView.backgroundColor = .clear
        view.addSubview(contentView)
        contentView.snp.makeConstraints { (makes) in
            makes.edges.equalTo(view)
        }
    }
    /**
     側邊欄關閉後 就把contentView給移除掉 以便可以觸控此頁面元件
     - Date: 10/20 Yang
     */
    @objc func removeSubview(){
        if let viewWithTag = self.view.viewWithTag(1000) {
            viewWithTag.removeFromSuperview()
        }else{
            print("No!")
        }
    }
    /**
     移除回彈的View
     - Date: 10/20 Yang
     */
    @objc func updateContentViewNoti(noti: Notification) {
        removeSubview()
    }
    
    @IBAction func viedoClick(_ sender: Any) {
        navigationController?.view.menu()
        ButtonDelegate?.clickLeft()
        if navigationController?.view.superview?.frame.origin.x != 0 {
            showContentView()
        }else{
            removeSubview()
        }
    }
    
    @IBAction func userClick(_ sender: Any) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        navigationController?.view.menu2()
        ButtonDelegate?.clickRight()
        if navigationController?.view.superview?.frame.origin.x == -navigationOriginX {
            showContentView()
        }else{
            removeSubview()
        }
    }
    
    var tmpPackDetailNew: ProductCDetail = ProductCDetail()
    
    func getPackDetailNew(index: Int,completion : @escaping ()->()){
        tmpPackDetailNew = ProductCDetail()
        let params = [
            "product_id": favoriteLists[index].productID,
            "token":"\(token)"
            ] as [String : Any]
        
        AF.request("\(serverUrlNew)product/getProductDetail", method: .post, parameters: params)
            .responseJSON { (response) in
                if response.value != nil {
                    //   print(response.result.value!)
                    self.tmpPackDetailNew.product_id = favoriteLists[index].productID
                    
                    let swiftyJsonVar = JSON(response.value!)
                    let status: Int = swiftyJsonVar["status"].int!
                    //                print("status:\(status)")
                    if status == 200{
                        // token 正確, product_list not null
                        let restltJson = JSON(swiftyJsonVar["result"])
                        //                        print("restltJson in getChildCatalogShowNew :\(restltJson)")
                        let productLists = restltJson["product_content_detail"].arrayObject
                        //                        print("productLists:\(String(describing: productLists))")
                        //                        if productLists != nil{
                        for product in productLists! {
                            //                                print("Hi i : \(i)")
                            let productJson = JSON(product)
                            
                            self.tmpPackDetailNew.content_id = productJson["content_id"].int!
                            self.tmpPackDetailNew.content_name = productJson["content_name"].string!
                            self.tmpPackDetailNew.content_description = productJson["content_description"].string!
                            
                            var tempContentAttributeValueArray: [ContentAttributeValue] = []
                            
                            let contentAttributeValueArrayDatas = productJson["content_attribute_value"].arrayObject
                            for contentAttributeValueArrayData in contentAttributeValueArrayDatas!{
                                let contentAttributeValueDataJson = JSON(contentAttributeValueArrayData)
                                var tempContentAttributeValue: ContentAttributeValue = ContentAttributeValue()
                                
                                tempContentAttributeValue.attribute_id = contentAttributeValueDataJson["attribute_id"].int!
                                tempContentAttributeValue.attribute = contentAttributeValueDataJson["attribute"].string!
                                tempContentAttributeValue.attribute_value_id = contentAttributeValueDataJson["attribute_value_id"].int!
                                tempContentAttributeValue.attribute_value = contentAttributeValueDataJson["attribute_value"].string!
                                
                                tempContentAttributeValueArray.append(tempContentAttributeValue)
                            }
                            self.tmpPackDetailNew.contentAttributeValueArray = tempContentAttributeValueArray
                            
                            var tempStills: [Still] = []
                            
                            let stillsDatas = productJson["Stills"].arrayObject
                            for stillData in stillsDatas!{
                                let stillJson = JSON(stillData)
                                var tempStill: Still = Still()
                                
                                tempStill.assets_id = stillJson["assets_id"].string!
                                tempStill.name = stillJson["name"].string!
                                tempStill.url = stillJson["name"].string!
                                
                                var tempMetaDatas: [MetaData] = []
                                
                                let metaDatas = stillJson["meta_data"].arrayObject
                                
                                if metaDatas != nil{
                                    
                                    for metaData in metaDatas!{
                                        var tempMetaData: MetaData = MetaData()
                                        
                                        let metaDataJson = JSON(metaData)
                                        
                                        tempMetaData.attribute = metaDataJson["attribute"].string!
                                        tempMetaData.value = metaDataJson["value"].string!
                                        
                                        tempMetaDatas.append(tempMetaData)
                                    }
                                    
                                }
                                
                                tempStill.meta_data = tempMetaDatas
                                
                                tempStills.append(tempStill)
                            }
                            
                            self.tmpPackDetailNew.stills = tempStills
                            
                            var tempPosters: [Poster] = []
                            
                            let postersDatas = productJson["Poster"].arrayObject
                            
                            /**
                             error : 有時候顯示圖片為前一個
                             check log : In CatalogMenu VC, func getPackDetailNew, postersDatas:[["meta_data": [["attribute": poster type, "value": Landscape]], "name": Test file, "asset_category_id": 4, "asset_format_id": 12, "url": http://rddev.andromeda.tti.tv/AndromedaContent/Poster/1/poster_1_1.jpg, "assets_id": poster_1_1.jpg]],postersDatas.count:1
                             只有 Landscape 圖，沒有抓到 portrait 的圖
                             */
                            print("In History VC, func getPackDetailNew, postersDatas:\(postersDatas!),postersDatas.count:\(postersDatas!.count)")
                            
                            //  if postersDatas?.count != 0 {
                            if postersDatas?.count == 2 {
                                for posterData in postersDatas!{
                                    
                                    let posterDataJson = JSON(posterData)
                                    var tempPoster: Poster = Poster()
                                    //
                                    tempPoster.assets_id = posterDataJson["assets_id"].string!
                                    tempPoster.name = posterDataJson["name"].string!
                                    tempPoster.url = posterDataJson["url"].string!
                                    
                                    var tempMetaDatas: [MetaData] = []
                                    
                                    let metaDatas = posterDataJson["meta_data"].arrayObject
                                    
                                    if metaDatas != nil{
                                        
                                        for metaData in metaDatas!{
                                            var tempMetaData: MetaData = MetaData()
                                            
                                            let metaDataJson = JSON(metaData)
                                            
                                            tempMetaData.attribute = metaDataJson["attribute"].string!
                                            tempMetaData.value = metaDataJson["value"].string!
                                            
                                            tempMetaDatas.append(tempMetaData)
                                        }
                                        
                                    }
                                    
                                    tempPoster.meta_data = tempMetaDatas
                                    
                                    //                            tempPosters.append(tempPoster)
                                    if metaDatas!.count != 0{
                                        tempPosters.append(tempPoster)
                                    }
                                    
                                }
                            }else{
                                print("postersDatas?.count != 2")
                                //                                print("favoriteLists[index].productImage:\(favoriteLists[index].productImage)")
                                //                                print("favoriteLists[index].productImage.count:\(favoriteLists[index].productImage.count)")
                                // 目前只遇到只有一個 ( Lucy )
                                // 或是兩個的
                                var tempPoster: Poster = Poster()
                                tempPosters = []
                                if favoriteLists[index].productImage.count == 1{
                                    tempPoster.assets_id = ""
                                    tempPoster.name = ""
                                    tempPoster.url = favoriteLists[index].productImage[0].url
                                    tempPosters.append(tempPoster)
                                    tempPosters.append(tempPoster)
                                }else{
                                    tempPoster.assets_id = ""
                                    tempPoster.name = ""
                                    for pi in favoriteLists[index].productImage{
                                        tempPoster.url = pi.url
                                        tempPosters.append(tempPoster)
                                    }
                                }
                            }
                            //                            }else{
                            //                                var tempPoster: Poster = Poster()
                            //                                tempPoster.assets_id = ""
                            //                                tempPoster.name = ""
                            //                                tempPosters.append(tempPoster)
                            //                            }
                            
                            self.tmpPackDetailNew.posters = tempPosters
                            
                            let ratingJson = JSON(productJson["rating"])
                            //                        print("ratingJson:\(ratingJson)")
                            if ratingJson["avg_rating"].float != nil{
                                self.tmpPackDetailNew.rating_avg_rating = ratingJson["avg_rating"].float!
                            }
                            if ratingJson["total_rating"].float != nil{
                                self.tmpPackDetailNew.rating_total_rating = ratingJson["total_rating"].float!
                            }
                            
                            var tempTrailerHLSs: [TrailerHLS] = []
                            
                            let trailerHLSDatas = productJson["Trailer_HLS"].arrayObject
                            for trailerHLSData in trailerHLSDatas!{
                                let trailerHLSDataJson = JSON(trailerHLSData)
                                var tempTrailerHLS: TrailerHLS = TrailerHLS()
                                
                                tempTrailerHLS.assets_id = trailerHLSDataJson["assets_id"].string!
                                tempTrailerHLS.name = trailerHLSDataJson["name"].string!
                                //                            tempPoster.url = trailerHLSDataJson["url"].string!
                                
                                var tempMetaDatas: [MetaData] = []
                                
                                let metaDatas = trailerHLSDataJson["meta_data"].arrayObject
                                
                                if metaDatas != nil{
                                    
                                    for metaData in metaDatas!{
                                        var tempMetaData: MetaData = MetaData()
                                        
                                        let metaDataJson = JSON(metaData)
                                        
                                        tempMetaData.attribute = metaDataJson["attribute"].string!
                                        tempMetaData.value = metaDataJson["value"].string!
                                        
                                        tempMetaDatas.append(tempMetaData)
                                    }
                                    
                                }
                                
                                tempTrailerHLS.meta_data = tempMetaDatas
                                
                                tempTrailerHLSs.append(tempTrailerHLS)
                            }
                            
                            self.tmpPackDetailNew.trailerHLS = tempTrailerHLSs
                            
                            //寫到這
                        }
                    }else if status == 204{
                        // product_list = null
                        //                    completion()
                    }else{
                        // token error
                        self.backToLoginVC(title:"Logout",msg: swiftyJsonVar["message"].string!,okMsg: "Back to LoginPage.")
//                        let alertController = UIAlertController(title: "Logout", message: swiftyJsonVar["message"].string!, preferredStyle: .alert)
//
//                        let okAction = UIAlertAction(
//                            title: "Back to LoginPage.",
//                            style: .default,
//                            handler: {
//                                (action: UIAlertAction!) -> Void in
//                                self.dismiss(animated: true, completion: nil)
//                            }
//                        )
//                        alertController.addAction(okAction)
//
//                        self.present(alertController, animated: true, completion: nil)
                    }
                }else{
                    print("FavoriteVC.getPackDetailNew Error")
                }
                //            if i == self.childCatalogNew.count - 1{
                completion()
                //            }
                
        }
        
    }

}
