//
//  clickLabel.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2019/6/26.
//  Copyright © 2019 蒼月喵. All rights reserved.
//

import Foundation
import UIKit

public typealias SimpleClosure2 = (() -> ())
private var tappableKey : UInt8 = 0
private var actionKey : UInt8 = 1

extension UILabel {
    
    @objc var callback: SimpleClosure2 {
        get {
            return objc_getAssociatedObject(self, &actionKey) as! SimpleClosure2
        }
        set {
            objc_setAssociatedObject(self, &actionKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    var gesture: UITapGestureRecognizer {
        get {
            return UITapGestureRecognizer(target: self, action: #selector(tapped))
        }
    }
    
    var tappable: Bool! {
        get {
            return objc_getAssociatedObject(self, &tappableKey) as? Bool
        }
        set(newValue) {
            objc_setAssociatedObject(self, &tappableKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            self.addTapGesture()
        }
    }
    
    fileprivate func addTapGesture() {
        if (self.tappable) {
            self.gesture.numberOfTapsRequired = 1
            self.isUserInteractionEnabled = true
            self.addGestureRecognizer(gesture)
        }
    }
    
    @objc private func tapped() {
        callback()
    }
    
}
