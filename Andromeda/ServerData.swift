//
//  ServerData.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2020/8/14.
//  Copyright © 2020 蒼月喵. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
/// Server Url
///外網
//var serverUrlNew = "http://andromeda.tti.tv/AndromedaBeta/api/"
///內網
//var serverUrlNew = "http://rddev.andromeda.tti.tv/AndromedaBeta/api/"
var serverUrlNew = "http://10.118.210.217/AndromedaBeta/api/"

/**
 紀錄親子鎖密碼用

 - TODO: 之後可能要修正（等API
 - Date: 09/02  Mike
 */
//var pcPwd : String? = nil
// 01/15 配合 FaceID or TouchID 解鎖，以後這個密碼改為會存放在本機備參   Mike
var pcPwd : String = "0000"

/**
 For pcStatus，有兩種 case
 
 - Date: 09/02  Mike
 */
enum pcEnum : String {
    case on = "ON"
    case off = "OFF"
}
/**
 紀錄目前親子鎖狀態
 預設為 off
 
 - Date: 09/02  Mike
 */
var pcStatus : pcEnum = .off


func getPcStatus(completion : @escaping ()->()){
    
    let params = [
    "lock_type_id": 1,
    "token":"\(token)"
    ] as [String : Any]
    
    // 11/05
    let getLockStatusAPI = serverUrlNew + "account/getLockStatus"
    
    AF.request(getLockStatusAPI, method: .post, parameters: params)
    .responseJSON{ (response) in
        if response.value != nil{
            print(response.value)
            let swiftyJsonVar = JSON(response.value!)
            let status: Int = swiftyJsonVar["status"].int!
            if status == 200{
                let result = JSON(swiftyJsonVar["result"])
                let lockArray = result["lock"].arrayObject
                for lock in lockArray!{
                    let lockJson = JSON(lock)
                    print(lockJson["lock_status"])
                    if lockJson["lock_status"] == "OFF"{
                        pcStatus = .off
                    }else{
                        pcStatus = .on
                    }
                }
                completion()
            }
        }
    }
}


// Old data
//let serverUrl = "http://10.118.215.86/Andromeda/"
//let andromedaServer: String = "http://andromeda.tti.tv/Andromeda/API/ClientMgr"

//let serverUrlNew = "http://rddev.andromeda.tti.tv/AndromedaBeta/api/"
//let serverUrlNew = "http://10.118.210.210/AndromedaBeta/api/"

///// Server Url (Old)
//let serverUrl = "http://andromeda.tti.tv/Andromeda/"

///** 圖片網址開頭 */
//let imageUrl = serverUrl + "ConsoleAux/images/"
//
///** 查詢影片 Url (QueryContentByRank) */
//let queryContentByRankUrl = serverUrl +  "API/ClientMgr/QueryContentByRank?ContentIndex=0&NumPerPage=100&portal_lang=zh-tw&Type="
//
///** getCatalogs */
//let queryCatalogUrl = serverUrl + "API/ClientMgr/QueryCatalog?"
///** QueryPackingByCatalog */
//let queryPackingByCatalog = serverUrl + "API/ClientMgr/QueryPackingByCatalog?"
//
///** 查詢指定 ID 影片 Url */
//let queryPackingByIdUrl = serverUrl + "API/ClientMgr/QueryPackingById?"
