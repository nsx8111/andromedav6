//
//  productCDetail.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2019/7/9.
//  Copyright © 2019 蒼月喵. All rights reserved.
//

import Foundation

struct ProductCDetail {
    var product_id: Int = 0
    var content_id: Int = 0
    var content_name: String = ""
    var content_description: String = ""
    
    var contentAttributeValueArray: [ContentAttributeValue] = []
    
    var stills: [Still] = []
    
    var posters: [Poster] = []
    
    var rating_avg_rating: Float = 0.0
    var rating_total_rating: Float = 0.0
    
    var trailerHLS: [TrailerHLS] = []
    
    func getAttribute() -> String{
        var outPutString: String = "演員："
        
        for cav in contentAttributeValueArray{
            if cav.attribute_id == 1{
                outPutString += cav.attribute_value
            }
        }
        
        return outPutString
    }
    
    func getValueArray(attribute_id: Int) -> [ContentAttributeValue]{
        var outputArray: [ContentAttributeValue] = []
//        var num: Int = 0
        
        for cav in contentAttributeValueArray{
            if cav.attribute_id == attribute_id{
                outputArray.append(cav)
            }
        }
        
        
        return outputArray
    }
    
}

// 屬性
struct ContentAttributeValue {
    var attribute_id: Int = 0
    var attribute: String = ""
    var attribute_value_id: Int = 0
    var attribute_value: String = ""
}

// 劇照
struct Still {
    var assets_id: String = ""
    var name: String = ""
    var url: String = ""
    // 這個不知道要幹嘛 先設成 String Array
    // 寫到 Poster ，看起來是圖片的額外屬性
    var meta_data: [MetaData] = []
}

// 海報
struct Poster {
    var assets_id: String = ""
    var name: String = ""
    var url: String = ""
    var meta_data: [MetaData] = []
}

struct MetaData {
    var attribute: String = ""
    var value: String = ""
}

struct TrailerHLS {
    var assets_id: String = ""
    var name: String = ""
    var meta_data: [MetaData] = []
}

//"content_id": 1, //******//
//"content_name": "Shazam!",
//"content_description": "We all have a superhero inside us, it just takes a bit of magic to bring it out. In Billy Batson's case, by shouting out one word - SHAZAM. - this streetwise fourteen-year-old foster kid can turn into the grown-up superhero Shazam.",
//"content_attribute_value": [
//{
//"attribute_id": 1,
//"attribute": "starring",
//"attribute_value_id": 1,
//"attribute_value": "Zachary Levi"
//},
//{
//"attribute_id": 1,
//"attribute": "starring",
//"attribute_value_id": 2,
//"attribute_value": "boysMichelle Borth"
//},
//{
//"attribute_id": 1,
//"attribute": "starring",
//"attribute_value_id": 15,
//"attribute_value": "Zacharv bovsMichelle Borth"
//},
//{
//"attribute_id": 2,
//"attribute": "Genres",
//"attribute_value_id": 4,
//"attribute_value": "Action"
//},
//{
//"attribute_id": 2,
//"attribute": "Genres",
//"attribute_value_id": 5,
//"attribute_value": "Adventure"
//},
//{
//"attribute_id": 2,
//"attribute": "Genres",
//"attribute_value_id": 6,
//"attribute_value": "Comedy"
//},
//{
//"attribute_id": 3,
//"attribute": "Director",
//"attribute_value_id": 8,
//"attribute_value": "Anthony Russo"
//},
//{
//"attribute_id": 5,
//"attribute": "MPAA rating",
//"attribute_value_id": 19,
//"attribute_value": "G"
//},
//{
//"attribute_id": 17,
//"attribute": "Release date",
//"attribute_value_id": 29,
//"attribute_value": "2019-04-03"
//},
//{
//"attribute_id": 18,
//"attribute": "Main catalog",
//"attribute_value_id": 26,
//"attribute_value": "Comedy"
//}
//],
//"Stills": [
//{
//"assets_id": "still_1_1558077227_12.jpg",
//"name": "test",
//"url": "http://rddev.andromeda.tti.tv/AndromedaContent/Still/1/still_1_1558077227_12.jpg",
//"meta_data": []
//},
//{
//"assets_id": "still_1_1558077310_27.jpg",
//"name": "test",
//"url": "http://rddev.andromeda.tti.tv/AndromedaContent/Still/1/still_1_1558077310_27.jpg",
//"meta_data": []
//},
//{
//"assets_id": "still_1_1558077405_14.jpg",
//"name": "test",
//"url": "http://rddev.andromeda.tti.tv/AndromedaContent/Still/1/still_1_1558077405_14.jpg",
//"meta_data": []
//},
//{
//"assets_id": "still_1_1558077551_17.jpg",
//"name": "test",
//"url": "http://rddev.andromeda.tti.tv/AndromedaContent/Still/1/still_1_1558077551_17.jpg",
//"meta_data": []
//}
//],
//"Poster": [
//{
//"assets_id": "poster_1_1.jpg",
//"name": "test",
//"url": "http://rddev.andromeda.tti.tv/AndromedaContent/Poster/1/poster_1_1.jpg",
//"meta_data": [
//{
//"attribute": "poster type",
//"value": "Landscape"
//}
//]
//},
//{
//"assets_id": "poster_1_2.jpg",
//"name": "test",
//"url": "http://rddev.andromeda.tti.tv/AndromedaContent/Poster/1/poster_1_2.jpg",
//"meta_data": [
//{
//"attribute": "poster type",
//"value": "Portrait"
//}
//]
//}
//],
//"rating": {
//    "avg_rating": 3.5,
//    "total_rating": 3
//},
//"Trailer_HLS": [
//{
//"assets_id": "La La Land .m3u8",
//"name": "La La Land .m3u8",
//"meta_data": []
//},
//{
//"assets_id": "trailer_hls_01_156084056_77.m3u8",
//"name": "trailer hls",
//"meta_data": []
//}
//]
