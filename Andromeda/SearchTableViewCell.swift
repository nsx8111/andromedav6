//
//  ViewHistoryTableViewCell.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2020/1/21.
//  Copyright © 2020 蒼月喵. All rights reserved.
//

import UIKit
import SnapKit

class SearchTableViewCell: UITableViewCell {
    
    /** for content image */
    var packImage = UIImageView()
    /** for tapped cell */
    var bkImage = UIImageView()
    
    var nameTitleLabel = UILabel()
    var nameValueLabel = UILabel()
        
    var typeTitleLabel = UILabel()
    var typeValueLabel = UILabel()
    
    var startTitleLabel = UILabel()
    var startValueLabel = UILabel()
    
    var endTitleLabel = UILabel()
    var endValueLabel = UILabel()
    
    var lineView1 = UIView()
    var lineView2 = UIView()
    var lineView3 = UIView()
    var lineView4 = UIView()
    
    //    //waitting to mark
    //    var descriptionTextView = UITextView()
    
    // 08/13
    /** pageScrol 影片縮圖的高度 */
    //    let pageScrollImageHeight: CGFloat = 292
    let pageScrollImageHeight : CGFloat = {
        var cc : CGFloat = 292
        //        if deviceType == .iPhone {
        //            cc = 292 / 2
        //        }
        cc = tableViewRowHeight
        return cc
    }()
    /** pageScrol 影片縮圖的高度 */
    //    let pageScrollImageWidth: CGFloat = 219
    let pageScrollImageWidth : CGFloat = {
        var cc : CGFloat = 219
        //        if deviceType == .iPhone {
        //            cc = 292 / 2
        //        }
        cc = tableViewRowHeight
        return cc
    }()
    
    let leftOffset : CGFloat = {
        var cc : CGFloat = 30
        if deviceType == .iPhone {
            cc = 30 / 2
        }
        return cc
    }()
    
    let topOrBottomOffset : CGFloat = {
        var cc : CGFloat = 16
        if deviceType == .iPhone {
            cc = 16 / 2
        }
        return cc
    }()
    
    let fontSize : CGFloat = {
        var cc : CGFloat = 28
        if deviceType == .iPhone {
            cc = 28 / 2
        }else{
            cc = 28 / 1.7
        }
        return cc
    }()
    
    let timeFontSize : CGFloat = {
        var cc : CGFloat = 28
        if deviceType == .iPhone {
            cc = 13
        }else{
            cc = 28 / 1.7
        }
        return cc
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        bkImage.backgroundColor = .cellBgColor
        contentView.addSubview(bkImage)
        bkImage.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
        }
        
        contentView.addSubview(packImage)
        packImage.snp.makeConstraints { (makes) in
            makes.left.equalToSuperview().offset(leftOffset*1.0)
            makes.centerY.equalToSuperview()
            makes.width.equalTo(pageScrollImageWidth/1.2)
            makes.height.equalTo(pageScrollImageHeight/1.2)
        }
        
        let lineViewOffset = pageScrollImageHeight/5
        
        contentView.addSubview(lineView1)
        lineView1.snp.makeConstraints { (makes) in
            makes.left.equalTo(packImage.snp.right).offset(0)
            makes.right.equalToSuperview()
            makes.top.equalToSuperview().offset(lineViewOffset)
            makes.height.equalTo(1)
        }
        
        contentView.addSubview(lineView2)
        lineView2.snp.makeConstraints { (makes) in
            makes.left.equalTo(packImage.snp.right).offset(0)
            makes.right.equalToSuperview()
            makes.top.equalToSuperview().offset(lineViewOffset*2)
            makes.height.equalTo(1)
        }
        
        contentView.addSubview(lineView3)
        lineView3.snp.makeConstraints { (makes) in
            makes.left.equalTo(packImage.snp.right).offset(0)
            makes.right.equalToSuperview()
            makes.top.equalToSuperview().offset(lineViewOffset*3)
            makes.height.equalTo(1)
        }
        
        contentView.addSubview(lineView4)
        lineView4.snp.makeConstraints { (makes) in
            makes.left.equalTo(packImage.snp.right).offset(0)
            makes.right.equalToSuperview()
            makes.top.equalToSuperview().offset(lineViewOffset*4)
            makes.height.equalTo(1)
        }
        
        startTitleLabel.text = "Start Time : "
        startTitleLabel.textColor = .white
        startTitleLabel.font = UIFont(name: "Georgia-Bold", size: fontSize)
        contentView.addSubview(startTitleLabel)
        startTitleLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(lineView3)
            makes.left.equalTo(packImage.snp.right).offset(leftOffset)
        }
        
        startValueLabel.numberOfLines = 0
        startValueLabel.text = ""
        startValueLabel.textColor = .white
        startValueLabel.font = UIFont(name: "Georgia-Bold", size: timeFontSize)
        contentView.addSubview(startValueLabel)
        startValueLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(lineView3)
            makes.left.equalTo(startTitleLabel.snp.right)
//            makes.right.lessThanOrEqualToSuperview().offset(-spacing1)
        }
        
        endTitleLabel.text = "End Time : "
        endTitleLabel.textColor = .white
        endTitleLabel.font = UIFont(name: "Georgia-Bold", size: fontSize)
        contentView.addSubview(endTitleLabel)
        endTitleLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(lineView4)
            makes.right.equalTo(startTitleLabel)
        }
        
        endValueLabel.numberOfLines = 0
        endValueLabel.text = ""
        endValueLabel.textColor = .white
        endValueLabel.font = UIFont(name: "Georgia-Bold", size: timeFontSize)
        contentView.addSubview(endValueLabel)
        endValueLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(lineView4)
            makes.left.equalTo(endTitleLabel.snp.right)
//            makes.right.lessThanOrEqualToSuperview().offset(-spacing1)
        }
        
        typeValueLabel.numberOfLines = 1
        typeValueLabel.text = ""
        typeValueLabel.textColor = .white
        typeValueLabel.font = UIFont(name: "Georgia-Bold", size: fontSize)
        contentView.addSubview(typeValueLabel)
        typeValueLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(lineView2)
            makes.left.equalTo(startValueLabel.snp.left)
        }
        
        typeTitleLabel.text = "Type : "
        typeTitleLabel.textColor = .white
        typeTitleLabel.font = UIFont(name: "Georgia-Bold", size: fontSize)
        contentView.addSubview(typeTitleLabel)
        typeTitleLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(lineView2)
            makes.right.equalTo(startTitleLabel)
        }
        
        nameValueLabel.numberOfLines = 2
        nameValueLabel.text = ""
        nameValueLabel.textColor = .white
        nameValueLabel.font = UIFont(name: "Georgia-Bold", size: fontSize)
        contentView.addSubview(nameValueLabel)
        nameValueLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(lineView1)
            makes.left.equalTo(startValueLabel.snp.left)
            makes.right.lessThanOrEqualToSuperview().offset(-offset1)
        }
        
        nameTitleLabel.text = "Name : "
        nameTitleLabel.textColor = .white
        nameTitleLabel.font = UIFont(name: "Georgia-Bold", size: fontSize)
        contentView.addSubview(nameTitleLabel)
        nameTitleLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(lineView1)
            makes.right.equalTo(startTitleLabel)
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
