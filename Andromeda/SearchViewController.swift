//
//  RightSearchViewController.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2019/5/17.
//  Copyright © 2019 蒼月喵. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire
import SwiftyJSON

class SearchViewController: UIViewController, UITextFieldDelegate {
    
    weak var MenuDelegate: MenuDelegate?
    weak var ButtonDelegate: ButtonDelegate?

    var searchImageView = UIImageView()
    var searchTableView = UITableView()
    let inputTextField = UITextField()
    let clearTextButton = UIButton()

    var searchDatas: [SearchData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackGroundImage()
//        setAboutSearch()
        setViews()
        setTableView()
        hideKeyboardWhenTappedAround()
    }

    override func viewDidAppear(_ animated: Bool) {
        let name = NSNotification.Name("SearchTextFieldResign")
        NotificationCenter.default.addObserver(self, selector:
        #selector(searchTextFieldResignNoti(noti:)), name: name, object: nil)
//        print("Search_OriginX \(navigationController?.view.superview?.frame.origin.x as Any)")
    }
    
    /** set BackGround Image */
    func setBackGroundImage(){
        view.backgroundColor = .buttonColor
        //Add BackGround
        let bkgImageView = UIImageView()
        bkgImageView.frame = CGRect(x: 0, y: 0, width: fullScreenSize.width, height: fullScreenSize.height)
        bkgImageView.backgroundColor = .labelTextColor3
//        bkgImageView.backgroundColor = .systemPurple
        self.view.addSubview(bkgImageView)
    }
    
    func setViews(){
        searchImageView = UIImageView(image: UIImage(named: "Search"))
        searchImageView.tappable = true
        searchImageView.callback = {
            print("tapped search")
            print(self.inputTextField.text as Any)
            self.textFieldResignFirstResponder()
            self.setActView(openOrClose: true)
            self.searchKeyword(keyword: self.inputTextField.text!){
                self.searchTableView.reloadData()
            }
        }
        view.addSubview(searchImageView)
        searchImageView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(statusBarHeight+(44-29)/2)
//            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
            make.width.height.equalTo(29)
        }
        
        clearTextButton.isHidden = true
        clearTextButton.isEnabled = true
        clearTextButton.setImage(UIImage(named: "playerClose"), for: .normal)
        clearTextButton.addTarget(self,action: #selector(self.clickClearText),for: .touchUpInside)
        view.addSubview(clearTextButton)
        clearTextButton.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(searchImageView)
            makes.width.height.equalTo(offset2)
            makes.right.equalTo(searchImageView.snp.left).offset(-offset2)
        }
        
        inputTextField.delegate = self
        inputTextField.textColor = .white
//        inputTextField.backgroundColor = .buttonColor
        inputTextField.placeholder = "Search Video"
        view.addSubview(inputTextField)
        inputTextField.snp.makeConstraints { (makes) in
            makes.right.equalTo(clearTextButton.snp.left).offset(-offset0)
            makes.left.equalToSuperview().offset(offset0)
            makes.top.equalToSuperview().offset(statusBarHeight)
            makes.height.equalTo(44)
        }
        
        let lineView = UIView()
        lineView.backgroundColor = .white
        view.addSubview(lineView)
        lineView.snp.makeConstraints { (makes) in
            makes.top.equalTo(searchImageView.snp.bottom).offset((44-29)/2)
            makes.width.equalToSuperview()
            makes.height.equalTo(1)
        }
        
    }
    
    // searchTableView.d
    func setTableView(){
        searchTableView.register(SearchTableViewCell.self, forCellReuseIdentifier: "SearchCell")
        searchTableView.tableFooterView = UIView(frame: CGRect.zero)
        searchTableView.delegate = self
        searchTableView.dataSource = self
        searchTableView.rowHeight = tableViewRowHeight
        searchTableView.bounces = true
        searchTableView.backgroundColor = .cellBgColor
        searchTableView.separatorColor = .white
        view.addSubview(searchTableView)
        searchTableView.snp.makeConstraints { (makes) in
            makes.left.right.bottom.equalToSuperview()
            makes.top.equalTo(inputTextField.snp.bottom).offset(1)
        }
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        refreshControl.attributedTitle = NSAttributedString(string: "loading...", attributes: attributes)
        refreshControl.tintColor = .white
        refreshControl.backgroundColor = .cellBgColor
        searchTableView.addSubview(refreshControl)
//        print("refreshFrame \(refreshControl.frame)")
    }
    
    var canRefresh = true
    var refreshControl: UIRefreshControl!
    /**
     下滑頁面會刷新資料
     - Date: 12/15 Yang
     */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < -offset7 * HeightForScroll {
            print("contentOffsetY \(scrollView.contentOffset.y)")
            if scrollView.contentOffset.y < -refreshControl.frame.size.height{
                scrollView.isScrollEnabled = false
                if canRefresh && !self.refreshControl.isRefreshing {
                    canRefresh = false
                    refreshControl.beginRefreshing()
                    refresh()
                }
            }
        }else if scrollView.contentOffset.y >= 0 {
            canRefresh = true
            scrollView.isScrollEnabled = true
        }
    }
    
    @objc func refresh() {
        self.searchKeyword(keyword: self.inputTextField.text!){
            self.searchTableView.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
    
    /**
    關鍵字搜尋影片
    - Date: 10/23 Yang
    */
    func searchKeyword(keyword:String, completion : @escaping ()->()){
        let params = [
            "keyword": keyword,
            "token": token
            ] as [String : Any]
        var tempSearchDatas: [SearchData] = []
        let searchAPI = serverUrlNew + "search/search"

        AF.request(searchAPI, method: .post, parameters: params)
            .responseJSON { (response) in
                if response.value != nil{
                    let swiftyJsonVar = JSON(response.value!)
                    let status: Int = swiftyJsonVar["status"].int!
                    print("currentStatus \(status)")
                    if status == 200{
                        if let resultObject = swiftyJsonVar["result"].dictionaryObject , let productList = resultObject["product_list"] {
//                            print("productList \(productList)")
                            let swiftyJsonVar2 = JSON(productList)
                            if let searchArray = swiftyJsonVar2.arrayObject {
                                for searchData in searchArray {
                                    let searchJson = JSON(searchData)
                                    var tempSearchData: SearchData = SearchData()
                                    
                                    if searchJson["product_id"].int != nil{
                                        tempSearchData.productID = searchJson["product_id"].int!
//                                        print("productID \(tempSearchData.productID)")
                                    }
                                    if searchJson["product_name"].string != nil{
                                        tempSearchData.productName = searchJson["product_name"].string!
//                                        print("productName \(tempSearchData.productName)")
                                    }
                                    if searchJson["product_type"].string != nil{
                                        tempSearchData.productType = searchJson["product_type"].string!
//                                        print("productType \(tempSearchData.productType)")
                                    }
                                    if searchJson["start_time"].string != nil{
                                        tempSearchData.startTime = searchJson["start_time"].string!
//                                        print("startTime \(tempSearchData.startTime)")
                                    }
                                    if searchJson["end_time"].string != nil{
                                        tempSearchData.endTime = searchJson["end_time"].string!
//                                        print("endTime \(String(describing: tempSearchData.endTime))")
                                    }
                                    
                                    var PIs: [PI] = []
                                    if searchJson["product_image"].arrayObject!.count > 0{
                                        let pis = searchJson["product_image"].arrayObject
                                        for pi in pis!{
                                            let piJSON = JSON(pi)
                                            var tempPI: PI = PI()
                                            
                                            if piJSON["product_image_id"].string != nil{
                                                tempPI.productImageId = piJSON["product_image_id"].string!
                                            }
                                            if piJSON["product_image_type_id"].int != nil{
                                                tempPI.productImageTypeId = piJSON["product_image_type_id"].int!
                                            }
                                            if piJSON["url"].string != nil{
                                                tempPI.url = piJSON["url"].string!
                                            }
                                            if piJSON["name"].string != nil{
                                                tempPI.name = piJSON["name"].string!
                                            }
                                            if piJSON["description"].string != nil{
                                                tempPI.description = piJSON["description"].string!
                                            }
                                            PIs.append(tempPI)
                                        }
                                        tempSearchData.productImage = PIs
                                    }
                                    tempSearchDatas.append(tempSearchData)
                                }
                            }
                        }
                        self.searchDatas = tempSearchDatas
//                        print("searchDatas \(self.searchDatas)")
                        self.setActView(openOrClose: false)
                        completion()
                    }else{
                        completion()
                        print("search error")
                        self.alertMsg3(title: "Not Found", msg: "Please search again or check your connection")
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.refreshControl.endRefreshing()
                            self.setActView(openOrClose: false)
                        }
                    }
                }else{
                    print("response value nil")
                    self.alertMsg3(title: "Error Connection", msg: "Please check your connection")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.refreshControl.endRefreshing()
                        self.setActView(openOrClose: false)
                    }
                }
                
        }
    }
    
    var tmpPackDetailNew: ProductCDetail = ProductCDetail()
    /**
     點擊cell進入Detail頁
     - Date: 11/03 Yang
     */
    func getPackDetailNew(index: Int,completion : @escaping ()->()){
        tmpPackDetailNew = ProductCDetail()
        let params = [
            "product_id": searchDatas[index].productID,
            "token":"\(token)"
            ] as [String : Any]
        
        AF.request("\(serverUrlNew)product/getProductDetail", method: .post, parameters: params)
            .responseJSON { [self] (response) in
                if response.value != nil {
                    self.tmpPackDetailNew.product_id = self.searchDatas[index].productID
                    
                    let swiftyJsonVar = JSON(response.value!)
                    let status: Int = swiftyJsonVar["status"].int!
                    if status == 200{
//                        print("status200")
                        // token 正確, product_list not null
                        let restltJson = JSON(swiftyJsonVar["result"])
                        //                        print("restltJson in getChildCatalogShowNew :\(restltJson)")
                        let productLists = restltJson["product_content_detail"].arrayObject
                        //                        print("productLists:\(String(describing: productLists))")
                        //                        if productLists != nil{
                        for product in productLists! {
                            //                                print("Hi i : \(i)")
                            let productJson = JSON(product)
                            
                            self.tmpPackDetailNew.content_id = productJson["content_id"].int!
                            self.tmpPackDetailNew.content_name = productJson["content_name"].string!
                            self.tmpPackDetailNew.content_description = productJson["content_description"].string!
                            
                            var tempContentAttributeValueArray: [ContentAttributeValue] = []
                            
                            let contentAttributeValueArrayDatas = productJson["content_attribute_value"].arrayObject
                            for contentAttributeValueArrayData in contentAttributeValueArrayDatas!{
                                let contentAttributeValueDataJson = JSON(contentAttributeValueArrayData)
                                var tempContentAttributeValue: ContentAttributeValue = ContentAttributeValue()
                                
                                tempContentAttributeValue.attribute_id = contentAttributeValueDataJson["attribute_id"].int!
                                tempContentAttributeValue.attribute = contentAttributeValueDataJson["attribute"].string!
                                tempContentAttributeValue.attribute_value_id = contentAttributeValueDataJson["attribute_value_id"].int!
                                tempContentAttributeValue.attribute_value = contentAttributeValueDataJson["attribute_value"].string!
                                
                                tempContentAttributeValueArray.append(tempContentAttributeValue)
                            }
                            self.tmpPackDetailNew.contentAttributeValueArray = tempContentAttributeValueArray
                            
                            var tempStills: [Still] = []
                            
                            let stillsDatas = productJson["Stills"].arrayObject
                            for stillData in stillsDatas!{
                                let stillJson = JSON(stillData)
                                var tempStill: Still = Still()
                                
                                tempStill.assets_id = stillJson["assets_id"].string!
                                tempStill.name = stillJson["name"].string!
                                tempStill.url = stillJson["name"].string!
                                
                                var tempMetaDatas: [MetaData] = []
                                
                                let metaDatas = stillJson["meta_data"].arrayObject
                                
                                if metaDatas != nil{
                                    
                                    for metaData in metaDatas!{
                                        var tempMetaData: MetaData = MetaData()
                                        
                                        let metaDataJson = JSON(metaData)
                                        
                                        tempMetaData.attribute = metaDataJson["attribute"].string!
                                        tempMetaData.value = metaDataJson["value"].string!
                                        
                                        tempMetaDatas.append(tempMetaData)
                                    }
                                    
                                }
                                
                                tempStill.meta_data = tempMetaDatas
                                
                                tempStills.append(tempStill)
                            }
                            
                            self.tmpPackDetailNew.stills = tempStills
                            
                            var tempPosters: [Poster] = []
                            
                            let postersDatas = productJson["Poster"].arrayObject
                            
                            //1223
                            /**
                             10/16 error : 有時候顯示圖片為前一個
                             check log : In CatalogMenu VC, func getPackDetailNew, postersDatas:[["meta_data": [["attribute": poster type, "value": Landscape]], "name": Test file, "asset_category_id": 4, "asset_format_id": 12, "url": http://rddev.andromeda.tti.tv/AndromedaContent/Poster/1/poster_1_1.jpg, "assets_id": poster_1_1.jpg]],postersDatas.count:1
                             只有 Landscape 圖，沒有抓到 portrait 的圖
                             */
//                            print("In History VC, func getPackDetailNew, postersDatas:\(postersDatas!),postersDatas.count:\(postersDatas!.count)")
                            
                            if postersDatas?.count == 2 {
                                for posterData in postersDatas!{
                                    
                                    let posterDataJson = JSON(posterData)
                                    var tempPoster: Poster = Poster()
                                    //
                                    tempPoster.assets_id = posterDataJson["assets_id"].string!
                                    tempPoster.name = posterDataJson["name"].string!
                                    tempPoster.url = posterDataJson["url"].string!
                                    
                                    var tempMetaDatas: [MetaData] = []
                                    
                                    let metaDatas = posterDataJson["meta_data"].arrayObject
                                    
                                    if metaDatas != nil{
                                        
                                        for metaData in metaDatas!{
                                            var tempMetaData: MetaData = MetaData()
                                            
                                            let metaDataJson = JSON(metaData)
                                            
                                            tempMetaData.attribute = metaDataJson["attribute"].string!
                                            tempMetaData.value = metaDataJson["value"].string!
                                            
                                            tempMetaDatas.append(tempMetaData)
                                        }
                                        
                                    }
                                    
                                    tempPoster.meta_data = tempMetaDatas
                                    
                                    //                            tempPosters.append(tempPoster)
                                    if metaDatas!.count != 0{
                                        tempPosters.append(tempPoster)
                                    }
                                    
                                }
                            }else{
                                print("postersDatas?.count != 2")
                                // 目前只遇到只有一個 ( Lucy )
                                // 或是兩個的
                                var tempPoster: Poster = Poster()
                                tempPosters = []
                                if self.searchDatas[index].productImage.count == 1{
                                    tempPoster.assets_id = ""
                                    tempPoster.name = ""
                                    tempPoster.url = self.searchDatas[index].productImage[0].url
                                    tempPosters.append(tempPoster)
                                    tempPosters.append(tempPoster)
                                }else{
                                    tempPoster.assets_id = ""
                                    tempPoster.name = ""
                                    for pi in self.searchDatas[index].productImage{
                                        tempPoster.url = pi.url
                                        tempPosters.append(tempPoster)
                                    }
                                }
                            }
                            
                            // 10/20 mark
                            //                            }else{
                            //                                var tempPoster: Poster = Poster()
                            //                                tempPoster.assets_id = ""
                            //                                tempPoster.name = ""
                            //                                tempPosters.append(tempPoster)
                            //                            }
                            
                            self.tmpPackDetailNew.posters = tempPosters
                            
                            let ratingJson = JSON(productJson["rating"])
                            //                        print("ratingJson:\(ratingJson)")
                            if ratingJson["avg_rating"].float != nil{
                                self.tmpPackDetailNew.rating_avg_rating = ratingJson["avg_rating"].float!
                            }
                            if ratingJson["total_rating"].float != nil{
                                self.tmpPackDetailNew.rating_total_rating = ratingJson["total_rating"].float!
                            }
                            
                            var tempTrailerHLSs: [TrailerHLS] = []
                            
                            let trailerHLSDatas = productJson["Trailer_HLS"].arrayObject
                            for trailerHLSData in trailerHLSDatas!{
                                let trailerHLSDataJson = JSON(trailerHLSData)
                                var tempTrailerHLS: TrailerHLS = TrailerHLS()
                                
                                tempTrailerHLS.assets_id = trailerHLSDataJson["assets_id"].string!
                                tempTrailerHLS.name = trailerHLSDataJson["name"].string!
                                //                            tempPoster.url = trailerHLSDataJson["url"].string!
                                
                                var tempMetaDatas: [MetaData] = []
                                
                                let metaDatas = trailerHLSDataJson["meta_data"].arrayObject
                                
                                if metaDatas != nil{
                                    
                                    for metaData in metaDatas!{
                                        var tempMetaData: MetaData = MetaData()
                                        
                                        let metaDataJson = JSON(metaData)
                                        
                                        tempMetaData.attribute = metaDataJson["attribute"].string!
                                        tempMetaData.value = metaDataJson["value"].string!
                                        
                                        tempMetaDatas.append(tempMetaData)
                                    }
                                    
                                }
                                
                                tempTrailerHLS.meta_data = tempMetaDatas
                                
                                tempTrailerHLSs.append(tempTrailerHLS)
                            }
                            
                            self.tmpPackDetailNew.trailerHLS = tempTrailerHLSs
                            
                        }
                    }else if status == 204{
//                        print("status204")
                        // product_list = null
                        //                    completion()
                    }else{
                        // token error
                        // 11/10
                        self.backToLoginVC(title:"Logout",msg: swiftyJsonVar["message"].string!,okMsg: "Back to LoginPage.")
//                        let alertController = UIAlertController(title: "Logout", message: swiftyJsonVar["message"].string!, preferredStyle: .alert)
//
//                        let okAction = UIAlertAction(
//                            title: "Back to LoginPage.",
//                            style: .default,
//                            handler: {
//                                (action: UIAlertAction!) -> Void in
//                                self.dismiss(animated: true, completion: nil)
//                        }
//                        )
//                        alertController.addAction(okAction)
//
//                        self.present(alertController, animated: true, completion: nil)
                    }
                }else{
                    print("SearchVC.getPackDetailNew Error")
                }
                //            if i == self.childCatalogNew.count - 1{
                completion()
                //            }
                
        }
        
    }
    /**
     點擊Cell進入PackDetail
     - Date: 11/03 Yang
     */
    @objc func clickCell(_ sender:AnyObject?) {
        let isResign = inputTextField.resignFirstResponder()
        print(inputTextField.resignFirstResponder())
        if isResign == true{
            inputTextField.resignFirstResponder()
        }else{
            print(sender?.view.tag as Any)
            let index : Int = sender?.view.tag ?? -1
            if index != -1{
                self.setActView(openOrClose: true)
                self.getPackDetailNew(index: index) {
                    self.setActView(openOrClose: false)
                    self.ButtonDelegate?.toPackPageNew(packDataNew: self.tmpPackDetailNew)
                    self.setActView(openOrClose: false)
                    let name = Notification.Name("SearchToDetail")
                    NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
                }
            }
        }
    }
    /**
     清除輸入文字
     - Date: 10/28 Yang
     */
    @objc func clickClearText(){
        if inputTextField.text != "" {
            inputTextField.text = ""
            clearTextButton.isHidden = true
        }
    }
    /**
    
    - Date: 10/29 Yang
    */
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let countOfWords = textField.text!.count - range.length + string.count
        print("countOfWords \(countOfWords)")
        if countOfWords == 0 {
            clearTextButton.isHidden = true
        }else{
            clearTextButton.isHidden = false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.textFieldResignFirstResponder()
        self.setActView(openOrClose: true)
        self.searchKeyword(keyword: self.inputTextField.text!){
            self.searchTableView.reloadData()
        }
        return true
    }
    
    @objc func searchTextFieldResignNoti(noti: Notification) {
        textFieldResignFirstResponder()
    }
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        searchTableView.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    func textFieldResignFirstResponder() {
        inputTextField.resignFirstResponder()
    }
    
}


extension SearchViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchDatas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = searchTableView.dequeueReusableCell(withIdentifier: "SearchCell") as! SearchTableViewCell
//        cell.backgroundColor = .cellBgColor
//        cell.separatorInset = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        cell.tag = indexPath.row
        let tap = UITapGestureRecognizer(target: self, action: #selector(clickCell(_:)))
        cell.addGestureRecognizer(tap)
        
        cell.nameValueLabel.text = self.searchDatas[indexPath.row].productName
        cell.typeValueLabel.text = self.searchDatas[indexPath.row].productType
        cell.startValueLabel.text = self.searchDatas[indexPath.row].startTime
        cell.endValueLabel.text = self.searchDatas[indexPath.row].endTime
        
        if self.searchDatas[indexPath.row].productImage.count > 1{
            cell.packImage.downloaded(from: self.searchDatas[indexPath.row].productImage[1].url)
        }else if self.searchDatas[indexPath.row].productImage.count == 1{
            cell.packImage.downloaded(from: self.searchDatas[indexPath.row].productImage[0].url)
        }else{
            cell.packImage.image = UIImage(named: "noContentImage")
        }
        return cell
    }
    
}
