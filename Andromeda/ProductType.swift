//
//  productType.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2019/7/9.
//  Copyright © 2019 蒼月喵. All rights reserved.
//

import Foundation

struct ProductType{
    var product_type_id: Int = 0
    var name: String = ""
    // 下面幾個暫時看不出來用在哪
    var product_type_status_id: Int = 0
    var description: String? = nil
    var created_at: String = ""
    var updated_at: String = ""
    
}

//"product_type_id": 1,
//"name": "movie",
//"description": null,
//"created_at": "2019-04-24 15:30:05",
//"updated_at": "2019-04-25 14:37:28",
//"product_type_status_id": 1
