//
//  UserMenuTableViewController.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2018/10/4.
//  Copyright © 2018年 蒼月喵. All rights reserved.
//

import UIKit

class UserMenuTableViewController: UITableViewController {
    weak var MenuDelegate: MenuDelegate?
    
    let list = ["Device", "Transactions", "Coupon", "Play History"]
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return list.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserMenuCell", for: indexPath)
        cell.textLabel?.text = list[indexPath.row]
        cell.textLabel?.textAlignment = .center
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        MenuDelegate?.changeUserMenuView(indexPath.row)
//        MenuDelegate?.showLeft()
        // 取消 cell 的選取狀態
        tableView.deselectRow(at: indexPath, animated: true)
        
        
    }
   

}
