//
//  AppDelegate.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2018/10/1.
//  Copyright © 2018年 蒼月喵. All rights reserved.
//

import UIKit
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
//    var blockRotation: Bool = false
//    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
//        if self.blockRotation{
//            return UIInterfaceOrientationMask.all
//        } else {
//            return UIInterfaceOrientationMask.portrait
//        }
//
//    }
    
    // 宣告螢幕能夠的旋轉方向
    var orientationLock = UIInterfaceOrientationMask.portrait
    
    //螢幕旋轉後最終會調用的function
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
     return orientationLock
    }
     
    //0306 橫置設定（iPhoneu 有效，iPad 無效）
    var isForceLandscape:Bool = false
    var isForcePortrait:Bool = true
    var isForceAllDerictions:Bool = false //支持所有方向
    /// 设置屏幕支持的方向
//    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
//        if isForceAllDerictions == true {
//            return .all
//        } else if isForceLandscape == true {
//            return .landscape
//        } else if isForcePortrait == true {
//            return .portrait
//        }
//        return .portrait
//    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //設定statusbar為白色
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
}
/**
 for ChangePassword
 客製化輸入框 for隱藏密碼
 - Date: 09/11 Yang
*/
class CustomTextField: UITextField {
    override func layoutSubviews() {
        super.layoutSubviews()
        for view in subviews {
            if let button = view as? UIButton {
                button.setImage(button.image(for: .normal)?.withRenderingMode(.alwaysTemplate), for: .normal)
                button.frame = CGRect(x: textFieldWidth-rightBtnSize-offset1*WidthForDevices, y: rightBtnSize/2, width: rightBtnSize, height: rightBtnSize)
                button.tintColor = .buttonColor
            }
        }
    }
}
///for Login
class CustomTextField2: UITextField {
    override func layoutSubviews() {
        super.layoutSubviews()
        for view in subviews {
            if let button = view as? UIButton {
                button.setImage(button.image(for: .normal)?.withRenderingMode(.alwaysTemplate), for: .normal)
                button.frame = CGRect(x: textFieldWidth2-rightBtnSize2-offset0*WidthForDevices, y: rightBtnSize2/2, width: rightBtnSize2, height: rightBtnSize2)
                button.tintColor = .viewBgColor
            }
        }
    }
}
/**
 過濾掉陣列中所有重複的element
 - Date: 01/15 Yang
*/
func uniq<S : Sequence, T : Hashable>(source: S) -> [T] where S.Iterator.Element == T {
    var buffer = [T]()
    var added = Set<T>()
    for elem in source {
        if !added.contains(elem) {
            buffer.append(elem)
            added.insert(elem)
        }
    }
    return buffer
}
/**
 改變icon尺寸 目前專案中大部分為壓縮圖片用
 - Date: 09/07 Yang
*/
func resizeImage(image: UIImage, width: CGFloat) -> UIImage {
        let size = CGSize(width: width, height:
            image.size.height * width / image.size.width)
        let renderer = UIGraphicsImageRenderer(size: size)
        let newImage = renderer.image { (context) in
            image.draw(in: renderer.format.bounds)
        }
        return newImage
}
/**
 取得Storyboard上的ViewController
 - Date: 08/26 Yang
*/
public func getVC(storyboardName:String,identifier:String)->UIViewController{
    return UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(withIdentifier: identifier)
}

extension UIColor {
    ///深藍色
    static var tableColor: UIColor { return UIColor(red: 0/255.0, green: 20/255.0, blue: 44/255.0, alpha: 1.0) }
    static var tableColor2: UIColor { return UIColor(red: 0/255.0, green: 20/255.0, blue: 44/255.0, alpha: 0.01) }
    static var viewBgColor: UIColor  { return UIColor(red: 0/255.0, green: 24/255.0, blue: 48/255.0, alpha: 1.0) }
    static var viewBgColor2: UIColor  { return UIColor(red: 0/255.0, green: 24/255.0, blue: 48/255.0, alpha: 0.01) }
    ///藍色
    static var blueBgColor: UIColor { return UIColor(red: 0/255.0, green: 33/255.0, blue: 66/255.0, alpha: 1.0) }
    static var blueBgColor2: UIColor { return UIColor(red: 0/255.0, green: 33/255.0, blue: 66/255.0, alpha: 0.2) }
    static var buttonColor: UIColor { return UIColor(red: 0/255.0, green: 72/255.0, blue: 144/255.0, alpha: 1.0) }
    static var buttonColor3: UIColor { return UIColor(red: 0/255.0, green: 72/255.0, blue: 144/255.0, alpha: 0.25) }
    static var buttonColor4: UIColor { return UIColor(red: 0/255.0, green: 72/255.0, blue: 144/255.0, alpha: 0.45) }
    ///淺藍色
    static var labelTextColor: UIColor { return UIColor(red: 120/255.0, green: 198/255.0, blue: 237/255.0, alpha: 1.0) }
    static var labelTextColor2: UIColor { return UIColor(red: 120/255.0, green: 198/255.0, blue: 237/255.0, alpha: 0.04) }
    static var labelTextColor3: UIColor { return UIColor(red: 120/255.0, green: 198/255.0, blue: 237/255.0, alpha: 0.45) }
    static var labelTextColor4: UIColor { return UIColor(red: 120/255.0, green: 198/255.0, blue: 237/255.0, alpha: 0.4) }
    /** cell 的背景顏色 */
    static var cellBgColor: UIColor { return UIColor(red: 7/255.0, green: 15/255.0, blue: 102/255.0, alpha: 1.0) }
    static var cellBgColor2: UIColor { return UIColor(red: 7/255.0, green: 15/255.0, blue: 102/255.0, alpha: 0.2) }
    ///黃色
    static var buttonColor2: UIColor { return UIColor(red: 217/255.0, green: 150/255.0, blue: 0/255.0, alpha: 1.0) }
    ///白色
    static var textFieldColor: UIColor { return UIColor(red: 0, green: 0, blue: 0, alpha: 0.3) }
    static var textFieldColor2: UIColor { return UIColor(red: 255, green: 255, blue: 255, alpha: 0.1) }
    static var actViewColor: UIColor { return UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.4) }
    
}
/**
 根據不同裝置來調整UI佈局尺寸
 - Date: 09/07 Yang
*/
extension UIDevice {
    ///iPhone後面數字單位為吋 例如iPhone55為5.5吋手機
    enum DeviceType {
        case iPhone35
        case iPhone40
        case iPhone47
        case iPhone55
        case iPhone58
        case iPhone61
        case iPad
        case TV
        case unspecified
        case carPlay

        var isPhone: Bool {
            return [ .iPhone35, .iPhone40, .iPhone47, .iPhone55, .iPhone58, .iPhone61 ].contains(self)
        }
    }
    
    var deviceType: DeviceType? {
        ///計算不同裝置的屏幕長度 例如5.5吋的height為736
        let screenSize = UIScreen.main.bounds.size
        let height = max(screenSize.width, screenSize.height)
//        print("deviceHeight \(height)")
        
        switch UIDevice.current.userInterfaceIdiom {
        case .tv:
            return .TV
        case .pad:
            switch height {
            case 1024:
                return .iPad
            case 1366:
                return .iPad
            default:
                return .iPad
            }
        case .phone:
            switch height {
            case 480:  //iPhone3
                return .iPhone35
            case 568:  //iPhoneSE
                return .iPhone40
            case 667:  //iPhone8
                return .iPhone47
            case 736:  //iPhone6S PLUS
                return .iPhone55
            case 812:  //iPhone11 Pro
                return .iPhone58
            case 896:  //iPhoneXR
                return .iPhone61
            default:
                return .iPhone58
            }
        case .unspecified:
            return .unspecified
        case .carPlay:
            return .carPlay
        case .mac:
            return nil
        @unknown default:
            return nil
        }
    }
}
/**
 客製化狀態列背景色樣式
 - Date: 09/07 Yang
*/
extension UIApplication {
    var statusBarView: UIView? {
        if #available(iOS 13.0, *) {
            let tag = 3848245
            let keyWindow = UIApplication.shared.connectedScenes
                .map({$0 as? UIWindowScene})
                .compactMap({$0})
                .first?.windows.first
            if let statusBar = keyWindow?.viewWithTag(tag) {
                return statusBar
            } else {
                let height = keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? .zero
                let statusBarView = UIView(frame: height)
                statusBarView.tag = tag
                statusBarView.layer.zPosition = 999999
                keyWindow?.addSubview(statusBarView)
                return statusBarView
            }
        } else {
            if responds(to: Selector(("statusBar"))) {
                return value(forKey: "statusBar") as? UIView
            }
        }
        return nil
    }
}

extension String{
    func replace(target: String, withString: String) -> String
    {
        return self.replacingOccurrences(of: target, with: withString, options: NSString.CompareOptions.literal, range: nil)
    }
}

extension UITextField{
    func moveLeft(){
        self.leftView = UIView.init(frame:CGRect(x: 0,y: 0,width: 5,height: 0))
        self.leftViewMode = .always
    }
}

extension UIImageView {
    /** 從網路下載圖片給 ImageView 使用 */
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
        }.resume()
    }
    /** 從網路下載圖片給 ImageView 使用 */
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}

extension UIViewController: UIGestureRecognizerDelegate {
    func alertMsg(msg: String) {
        let alertController = UIAlertController(title: "Error", message: msg, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(defaultAction)
        self.present(alertController, animated: true, completion: nil)
    }
    //For SignUp Done
    func alertMsg2(msg: String) {
        let alertController = UIAlertController(title: "Done", message: msg, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(defaultAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func alertMsg3(title: String , msg: String) {
        let controller = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        controller.addAction(cancelAction)
        present(controller, animated: true, completion: nil)
    }
    
    func allGestureRecognizer(){
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
        tap.cancelsTouchesInView = false
        tap.delegate = self
        self.view.addGestureRecognizer(tap)

        let tap2: UITapGestureRecognizer = UITapGestureRecognizer(target: self,action:#selector(self.handleTapGesture2))
        tap2.cancelsTouchesInView = false
        tap2.delegate = self
        self.view.addGestureRecognizer(tap2)
        
//        let pan: UIScreenEdgePanGestureRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(self.handleEdgePanGesture3))
//        let pan2: UIScreenEdgePanGestureRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(self.handleEdgePanGesture4))
//        view.addGestureRecognizer(pan)
//        view.addGestureRecognizer(pan2)
    }

    /** extract all the textfield from view **/
    func getTextfield(view: UIView) -> [UITextField] {
        var results = [UITextField]()
        for subview in view.subviews as [UIView] {
            if let textField = subview as? UITextField {
                results += [textField]
            } else {
                results += getTextfield(view: subview)
            }
        }
        return results
    }
    
    func resignTextfield(){
        let allTextField = getTextfield(view: self.view)
        for txtField in allTextField {
            txtField.resignFirstResponder()
        }
    }
    
    @objc func showContentView2(){
        let contentView = UIView()
        contentView.tag = 1000
        contentView.isUserInteractionEnabled = true
        contentView.backgroundColor = .clear
        view.addSubview(contentView)
        contentView.snp.makeConstraints { (makes) in
            makes.edges.equalTo(view)
        }
    }
    
    @objc func handleTapGesture2(){
        print("TapOriginY  \(self.view.frame.origin.y)")
        print("extensionOriginX \(navigationController?.view.superview?.frame.origin.x as Any)")
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        if self.navigationController?.view.superview?.frame.origin.x != 0 {
            if self.navigationController?.view.superview?.frame.origin.x == -navigationOriginX {
                self.navigationController?.view.menu2()
                if let viewWithTag = self.view.viewWithTag(1000) {
                    viewWithTag.removeFromSuperview()
                }
            }else{
                self.navigationController?.view.menu()
                if let viewWithTag = self.view.viewWithTag(1000) {
                    viewWithTag.removeFromSuperview()
                }
            }
        }
    }
}

extension UIView {
    
    func menu() {
        var frame = self.superview?.frame
        if frame?.origin.x == 0{
            frame?.origin.x = navigationOriginX
            //            frame?.origin.x = 200
        } else{
            frame?.origin.x = 0
        }
        UIView.animate(withDuration: 0.4) {
            self.superview?.frame = frame!
        }
    }
    
    func menu2() {
        var frame = self.superview?.frame
        if frame?.origin.x == 0{
            frame?.origin.x = -navigationOriginX
        } else{
            frame?.origin.x = 0
        }
        UIView.animate(withDuration: 0.4) {
            self.superview?.frame = frame!
        }
    }
    ///右滑
    func slideByFinger(location: CGPoint, state: UIGestureRecognizer.State) {
        
        struct FirstTouch {
            static var location: CGPoint? = nil
        }
        
        var frame = (self.superview?.frame)!
        var beganX = Int(frame.origin.x)
               beganX2 = beganX
        switch state {
        case .began:
            FirstTouch.location = location
            
        case .changed:
            frame.origin.x = location.x - FirstTouch.location!.x
            if frame.origin.x < 0 || beganX < 0{
                           UIView.animate(withDuration: 0.3) {
                                self.superview?.frame.origin.x = 0
                            }
                        } else if frame.origin.x > fullScreenSize.width*0.2 {
           frame.origin.x = navigationOriginX
                            UIView.animate(withDuration: 0.3) {
                                self.superview?.frame = frame
                            }
                         }
        default:
            print("default")
//            if frame.origin.x > 40 {
//                frame.origin.x = navigationOriginX
//            } else {
//                frame.origin.x = 0
//            }
//
//            UIView.animate(withDuration: 0.2) {
//                self.superview?.frame = frame
//            }
        }
    }
    ///左滑
    func slideByFinger2(location: CGPoint, state: UIGestureRecognizer.State) {
        
        struct FirstTouch {
            static var location: CGPoint? = nil
        }
        
        var frame = (self.superview?.frame)!
        var beganX = Int(frame.origin.x)
               beganX2 = beganX
        
        switch state {
        case .began:
            FirstTouch.location = location
            
        case .changed:
            frame.origin.x = location.x - FirstTouch.location!.x
            if frame.origin.x > 0 || beganX > 0{
                UIView.animate(withDuration: 0.3) {
                    self.superview?.frame.origin.x = 0
                }
            } else if frame.origin.x < -fullScreenSize.width*0.2 {
                frame.origin.x = -navigationOriginX
                UIView.animate(withDuration: 0.3) {
                    self.superview?.frame = frame
                }
            }
            
//            if frame.origin.x > 0 {
//                frame.origin.x = 0
//            } else if frame.origin.x < -fullScreenSize.width*0.5 {
//                frame.origin.x = -navigationOriginX
//            }
//            self.superview?.frame = frame
        default:
            print("default")
//            if frame.origin.x < -40 {
//                frame.origin.x = -navigationOriginX
//            } else {
//                frame.origin.x = 0
//            }
//
//            UIView.animate(withDuration: 0.2) {
//                self.superview?.frame = frame
//            }
        }
    }
}

extension UILabel {
    
    func setLineSpacing(lineSpacing: CGFloat = 0.0, lineHeightMultiple: CGFloat = 0.0) {
        
        guard let labelText = self.text else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        
        let attributedString:NSMutableAttributedString
        if let labelattributedText = self.attributedText {
            attributedString = NSMutableAttributedString(attributedString: labelattributedText)
        } else {
            attributedString = NSMutableAttributedString(string: labelText)
        }
        
        // (Swift 4.2 and above) Line spacing attribute
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        
        //        // (Swift 4.1 and 4.0) Line spacing attribute
        //        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        self.attributedText = attributedString
    }
    
    /**  改变字间距  */
    func changeWordSpace(space:CGFloat) {
        if self.text == nil || self.text == "" {
            return
        }
        let text = self.text
        let attributedString = NSMutableAttributedString.init(string: text!, attributes: [NSAttributedString.Key.kern:space])
        let paragraphStyle = NSMutableParagraphStyle()
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: .init(location: 0, length: (text?.characters.count)!))
        self.attributedText = attributedString
        self.sizeToFit()
    }
}


