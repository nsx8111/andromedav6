//
//  CreateAccPageViewController.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2019/1/2.
//  Copyright © 2019 蒼月喵. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SnapKit

class SignUpViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    
    var scrollView = UIScrollView()
    var containerView = UIView()
    
    /** 輸入 account 的 text field */
    var accountTextField = UITextField()
    /** 輸入 password 的 text field */
    var passwordTextField = UITextField()
    /** 重新輸入 password 的 text field */
    var confirmPasswordTextField = UITextField()
    /** 輸入 e-mail 的 text field */
    var emailTextField = UITextField()
    /** 紀錄 birthday 的 text field */
    var birthdayTextField = UITextField()
    /** 日期選擇 for birthday */
    var datePicker = UIDatePicker()
    /** 輸入 First name 的 text field */
    var firstNameTextField = UITextField()
    /** 輸入 Last name 的 text field */
    var lastNameTextField = UITextField()
    /** 輸入 手機號碼(國碼) 的 text field */
    var mobileNumTextField = UITextField()
    /** 輸入 手機號碼(後九碼) 的 text field */
    var mobileNumTextField2 = UITextField()
    /** 輸入 id 的 text field */
    var idTextField = UITextField()
    /** 紀錄 性別 的 text field */
    var genderTextField = UITextField()
    /** 性別選項 array */
    let genders = ["Male","Female"]
    /** 性別 picker */
    let genderPickerView = UIPickerView()
    /** 國碼選項 array */
    let countryNum = ["+886","+86","+852"]
    /** 國碼 picker */
    let countryNumPickerView = UIPickerView()
    
    //0313
    let firstNameLabel = UILabel()
    let lastNameLabel = UILabel()
    let mobileNumLabel = UILabel()
    let genderLabel = UILabel()
    let leftButton = UIButton()
    let rightButton = UIButton()
    
    //0316
    var longConstraint: Constraint?
    var shortConstraint: Constraint?
    
    /** size Big */
    var sizeB: CGFloat = 30
    /** size Small */
    var sizeS: CGFloat = 20
    /** Label 間的距離 */
    var offset: CGFloat = 10
    /** 進入 Sign Up Page 時的動作 */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 08/13
        switch deviceType {
        case .iPhone:
            print("no change")
        case .iPad:
            self.sizeB = 60
            self.sizeS = 40
        }
        
        setBackGroundImage()
        setAccToBirthday()
        setNameToGender()
        setButtons()
        
        //reset
        clickMidButton()
    }
    /**
     測試用func，不用每次都要一格一格填寫資料
     - Note: #上架
     */
    func forTest_SetData(){
        self.accountTextField.text = "ttt01"
        self.passwordTextField.text = "abcd12"
        self.confirmPasswordTextField.text = "abcd12"
        self.emailTextField.text = "dasda@sdsd.cda"
        self.birthdayTextField.text = "1983-05-08"
        self.firstNameTextField.text = "AAAA"
        self.lastNameTextField.text = "BBBB"
        self.mobileNumTextField.text = "+886"
        self.mobileNumTextField2.text = "912345678"
        self.idTextField.text = ""
        self.genderTextField.text = "Male"
    }
    /** set BackGround Image */
    func setBackGroundImage(){
        //Add BackGround
        let bkgImageView = UIImageView()
        bkgImageView.backgroundColor = .viewBgColor
        
        self.view.addSubview(bkgImageView)
        
        bkgImageView.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
        }
        
        //0109
        view.addSubview(scrollView)
        scrollView.backgroundColor = .clear
        scrollView.bounces = false
        
        scrollView.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
        }
        // add containerView
        scrollView.addSubview(containerView)
        containerView.backgroundColor = .clear
        
        containerView.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
            longConstraint = makes.width.equalTo(longSide).constraint
            shortConstraint = makes.width.equalTo(shortSide).constraint
        }
        
        if UIDevice.current.orientation.isPortrait{
            shortConstraint?.activate()
            longConstraint?.deactivate()
        }else if UIDevice.current.orientation.isLandscape{
            shortConstraint?.deactivate()
            longConstraint?.activate()
        }else{
            print("isPortrait or isLandscape error @setBackGroundImage()")
            shortConstraint?.activate()
            longConstraint?.deactivate()
        }
        
    }
    /**
     set Account to Birthday Objs
     1. Add title Label ("Create Account")
     2. Add Account Label & textfield
     3. Add Password Label & textfield
     4. Add Confirm Password Label & textfield
     5. Add E-mail Label & textfield
     6. Add Birthday Label & DatePicker
     */
    func setAccToBirthday(){
        //Add Account Label
        let titleLabel = UILabel()
        print(self.sizeB)
        titleLabel.backgroundColor = .clear
        
        titleLabel.text = "Create Account"
        titleLabel.font = UIFont(name: "Georgia-Bold", size: self.sizeB)
        titleLabel.textColor = .white
        titleLabel.numberOfLines = 2
        titleLabel.setLineSpacing(lineSpacing: 2.0, lineHeightMultiple: 1.2)
        
        titleLabel.tappable = true
        titleLabel.callback = {
            self.forTest_SetData()
        }
        
        self.containerView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(15)
            makes.left.equalTo(30)
        }
        
        //Add Account Label
        let accountLabel = UILabel()
        accountLabel.backgroundColor = .clear
        
        accountLabel.text = "Account"
        accountLabel.font = UIFont(name: "Georgia-Bold", size: self.sizeS)
        accountLabel.textColor = .white
        accountLabel.numberOfLines = 2
        accountLabel.setLineSpacing(lineSpacing: 2.0, lineHeightMultiple: 1.2)
        
        self.containerView.addSubview(accountLabel)
        accountLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(titleLabel.snp.bottom).offset(offset)
            makes.left.equalTo(titleLabel)
            makes.height.equalTo(self.sizeS)
        }
        
        //Add Account TextField
        accountTextField.placeholder = "   Enter Account here"
        accountTextField.clearButtonMode = .whileEditing
        accountTextField.returnKeyType = .done
        accountTextField.textColor = .black
        accountTextField.backgroundColor = .lightGray
        accountTextField.alpha = 0.8
        accountTextField.layer.cornerRadius = 12.0
        accountTextField.delegate = self
        accountTextField.moveLeft()
        
        self.containerView.addSubview(accountTextField)
        accountTextField.snp.makeConstraints { (makes) in
            makes.top.equalTo(accountLabel.snp.bottom).offset(offset)
            makes.left.equalTo(titleLabel)
            makes.width.equalTo(shortSide*0.7)
            makes.height.equalTo(self.sizeB)
        }
        
        //Add Password Label
        let passwordLabel = UILabel()
        
        passwordLabel.backgroundColor = .clear
        passwordLabel.text = "Password"
        passwordLabel.font = UIFont(name: "Georgia-Bold", size: self.sizeS)
        passwordLabel.textColor = .white
        passwordLabel.numberOfLines = 2
        passwordLabel.setLineSpacing(lineSpacing: 2.0, lineHeightMultiple: 1.2)
        
        self.containerView.addSubview(passwordLabel)
        passwordLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(accountTextField.snp.bottom).offset(offset)
            makes.left.equalTo(titleLabel)
            makes.height.equalTo(self.sizeS)
        }
        
        //Add Password TextField
        passwordTextField.placeholder = "   Enter Password here"
        passwordTextField.clearButtonMode = .whileEditing
        passwordTextField.returnKeyType = .done
        passwordTextField.textColor = .black
        passwordTextField.backgroundColor = .lightGray
        passwordTextField.alpha = 0.8
        passwordTextField.layer.cornerRadius = 12.0
        passwordTextField.isSecureTextEntry = true
        passwordTextField.delegate = self
        passwordTextField.moveLeft()
        
        self.containerView.addSubview(passwordTextField)
        passwordTextField.snp.makeConstraints { (makes) in
            makes.top.equalTo(passwordLabel.snp.bottom).offset(offset)
            makes.left.equalTo(titleLabel)
            makes.width.equalTo(shortSide*0.7)
            makes.height.equalTo(self.sizeB)
        }
        
        //Add ConfirmPassword Label
        let confirmPasswordLabel = UILabel()
        
        confirmPasswordLabel.backgroundColor = .clear
        
        confirmPasswordLabel.text = "Conirm Password*"
        confirmPasswordLabel.font = UIFont(name: "Georgia-Bold", size: self.sizeS)
        confirmPasswordLabel.textColor = .white
        confirmPasswordLabel.numberOfLines = 2
        confirmPasswordLabel.setLineSpacing(lineSpacing: 2.0, lineHeightMultiple: 1.2)
        
        self.containerView.addSubview(confirmPasswordLabel)
        confirmPasswordLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(passwordTextField.snp.bottom).offset(offset)
            makes.left.equalTo(titleLabel)
            makes.height.equalTo(self.sizeS)
        }
        
        //Add ConfirmPassword TextField
        confirmPasswordTextField.placeholder = "   Enter Confirm Password here"
        confirmPasswordTextField.clearButtonMode = .whileEditing
        confirmPasswordTextField.returnKeyType = .done
        confirmPasswordTextField.textColor = .black
        confirmPasswordTextField.backgroundColor = .lightGray
        confirmPasswordTextField.alpha = 0.8
        confirmPasswordTextField.layer.cornerRadius = 12.0
        confirmPasswordTextField.isSecureTextEntry = true
        confirmPasswordTextField.delegate = self
        confirmPasswordTextField.moveLeft()
        
        self.containerView.addSubview(confirmPasswordTextField)
        confirmPasswordTextField.snp.makeConstraints { (makes) in
            makes.top.equalTo(confirmPasswordLabel.snp.bottom).offset(offset)
            makes.left.equalTo(titleLabel)
            makes.width.equalTo(shortSide*0.7)
            makes.height.equalTo(self.sizeB)
        }
        
        //Add email Label
        let emailLabel = UILabel(frame: CGRect(x: fullScreenSize.width*0.08 , y: fullScreenSize.height*0.35, width: fullScreenSize.width, height: self.sizeS))
        
        emailLabel.backgroundColor = .clear
        emailLabel.text = "E-mail"
        emailLabel.font = UIFont(name: "Georgia-Bold", size: self.sizeS)
        emailLabel.textColor = .white
        emailLabel.numberOfLines = 2
        emailLabel.setLineSpacing(lineSpacing: 2.0, lineHeightMultiple: 1.2)
        
        self.containerView.addSubview(emailLabel)
        emailLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(confirmPasswordTextField.snp.bottom).offset(offset)
            makes.left.equalTo(titleLabel)
            makes.height.equalTo(self.sizeS)
        }
        
        //Add email TextField
        emailTextField = UITextField(frame: CGRect(x: fullScreenSize.width*0.08 , y: fullScreenSize.height*0.39, width: textFieldWidth2, height: self.sizeB))
        emailTextField.placeholder = "   Enter E-mail here"
        emailTextField.clearButtonMode = .whileEditing
        emailTextField.returnKeyType = .done
        emailTextField.textColor = .black
        emailTextField.backgroundColor = .lightGray
        emailTextField.alpha = 0.8
        emailTextField.layer.cornerRadius = 12.0
        emailTextField.delegate = self
        emailTextField.moveLeft()
        
        self.containerView.addSubview(emailTextField)
        emailTextField.snp.makeConstraints { (makes) in
            makes.top.equalTo(emailLabel.snp.bottom).offset(offset)
            makes.left.equalTo(titleLabel)
            makes.width.equalTo(shortSide*0.7)
            makes.height.equalTo(self.sizeB)
        }
        
        //Add Birthday Label
        let birthdayLabel = UILabel()
        
        birthdayLabel.backgroundColor = .clear
        
        birthdayLabel.text = "Birthday"
        birthdayLabel.font = UIFont(name: "Georgia-Bold", size: self.sizeS)
        birthdayLabel.textColor = .white
        birthdayLabel.numberOfLines = 2
        birthdayLabel.setLineSpacing(lineSpacing: 2.0, lineHeightMultiple: 1.2)
        
        self.containerView.addSubview(birthdayLabel)
        birthdayLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(emailTextField.snp.bottom).offset(offset)
            makes.left.equalTo(titleLabel)
            makes.height.equalTo(self.sizeS)
        }
        
        //Add Birthday TextField
        birthdayTextField.placeholder = "   Pick a Day"
        birthdayTextField.clearButtonMode = .never
        birthdayTextField.returnKeyType = .done
        birthdayTextField.textColor = .black
        birthdayTextField.backgroundColor = .lightGray
        birthdayTextField.alpha = 0.8
        birthdayTextField.layer.cornerRadius = 12.0
        birthdayTextField.moveLeft()
        birthdayTextField.delegate = self
        
        datePicker.datePickerMode = .date
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.viewTapped(gestureRecognizer:)))
        view.addGestureRecognizer(tapGesture)
        //0109
        containerView.addGestureRecognizer(tapGesture)
        
        birthdayTextField.inputView = datePicker
        
        self.containerView.addSubview(birthdayTextField)
        birthdayTextField.snp.makeConstraints { (makes) in
            makes.top.equalTo(birthdayLabel.snp.bottom).offset(offset)
            makes.left.equalTo(titleLabel)
            makes.width.equalTo(shortSide*0.7)
            makes.height.equalTo(self.sizeB)
        }
        
    }
    /** UIDatePicker 設定 */
    @objc func dateChanged(datePicker: UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        birthdayTextField.text = dateFormatter.string(from: datePicker.date)
    }
    /** 點擊空白處的時候的處理，主要是 end Editing，另外就是判斷生日是否為一個小時內 -> error */
    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        if(datePicker.date.timeIntervalSinceNow > -3600){
            DispatchQueue.main.async {
                self.birthdayTextField.text = ""
            }
        }else{
            birthdayTextField.text = dateFormatter.string(from: datePicker.date)
        }
        view.endEditing(true)
        containerView.endEditing(true)
    }
    /**
     set Name to gender Objs
     1. Add First name Label & textfield
     2. Add Last name Label & textfield
     3. Add Mobild nums Label & textfield
     4. Add Id Label & textfield
     5. Add gender Label & textfield
     */
    func setNameToGender(){
        
        //Add FirstName Label
        firstNameLabel.tag = 200
        firstNameLabel.backgroundColor = .clear
        firstNameLabel.text = "First Name"
        firstNameLabel.font = UIFont(name: "Georgia-Bold", size: self.sizeS)
        firstNameLabel.textColor = .white
        firstNameLabel.numberOfLines = 2
        firstNameLabel.setLineSpacing(lineSpacing: 2.0, lineHeightMultiple: 1.2)
        
        self.containerView.addSubview(firstNameLabel)
        firstNameLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(birthdayTextField.snp.bottom).offset(offset)
            makes.left.equalTo(birthdayTextField)
            makes.height.equalTo(self.sizeS)
        }
        
        
        //Add FirstName TextField
        firstNameTextField.tag = 200
        firstNameTextField.placeholder = "   First Name"
        firstNameTextField.clearButtonMode = .whileEditing
        firstNameTextField.returnKeyType = .done
        firstNameTextField.textColor = .black
        firstNameTextField.backgroundColor = .lightGray
        firstNameTextField.alpha = 0.8
        firstNameTextField.layer.cornerRadius = 12.0
        firstNameTextField.delegate = self
        firstNameTextField.moveLeft()
        
        self.containerView.addSubview(firstNameTextField)
        firstNameTextField.snp.makeConstraints { (makes) in
            makes.top.equalTo(firstNameLabel.snp.bottom).offset(offset)
            makes.left.equalTo(birthdayTextField)
            makes.width.equalTo(shortSide*0.4)
            makes.height.equalTo(self.sizeB)
        }
        
        //Add LastName Label
        lastNameLabel.tag = 200
        lastNameLabel.backgroundColor = .clear
        lastNameLabel.text = "Last Name"
        lastNameLabel.font = UIFont(name: "Georgia-Bold", size: self.sizeS)
        lastNameLabel.textColor = .white
        lastNameLabel.numberOfLines = 2
        lastNameLabel.setLineSpacing(lineSpacing: 2.0, lineHeightMultiple: 1.2)
        
        self.containerView.addSubview(lastNameLabel)
        lastNameLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(birthdayTextField.snp.bottom).offset(offset)
            makes.left.equalToSuperview().offset(shortSide*0.56)
            makes.height.equalTo(self.sizeS)
        }
        
        //Add LastName TextField
        lastNameTextField.tag = 200
        lastNameTextField.placeholder = "   Last Name"
        lastNameTextField.clearButtonMode = .whileEditing
        lastNameTextField.returnKeyType = .done
        lastNameTextField.textColor = .black
        lastNameTextField.backgroundColor = .lightGray
        lastNameTextField.alpha = 0.8
        lastNameTextField.layer.cornerRadius = 12.0
        lastNameTextField.delegate = self
        lastNameTextField.moveLeft()
        
        self.containerView.addSubview(lastNameTextField)
        lastNameTextField.snp.makeConstraints { (makes) in
            makes.top.equalTo(firstNameLabel.snp.bottom).offset(offset)
            makes.left.equalTo(shortSide*0.56)
            makes.width.equalTo(shortSide*0.4)
            makes.height.equalTo(self.sizeB)
        }
        
        //Add MobileNum Label
        mobileNumLabel.tag = 200
        mobileNumLabel.backgroundColor = .clear
        mobileNumLabel.text = "Mobile Number"
        mobileNumLabel.font = UIFont(name: "Georgia-Bold", size: self.sizeS)
        mobileNumLabel.textColor = .white
        mobileNumLabel.numberOfLines = 2
        mobileNumLabel.setLineSpacing(lineSpacing: 2.0, lineHeightMultiple: 1.2)
        
        self.containerView.addSubview(mobileNumLabel)
        mobileNumLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(firstNameTextField.snp.bottom).offset(offset)
            makes.left.equalTo(birthdayTextField)
            makes.height.equalTo(self.sizeS)
        }
        
        //Add MobileNum TextField (county num, select by countryNumPickerView)"
        mobileNumTextField.tag = 200
        mobileNumTextField.text = countryNum[0]
        mobileNumTextField.clearButtonMode = .never
        mobileNumTextField.returnKeyType = .done
        mobileNumTextField.textColor = .black
        mobileNumTextField.backgroundColor = .lightGray
        mobileNumTextField.alpha = 0.8
        mobileNumTextField.layer.cornerRadius = 12.0
        mobileNumTextField.delegate = self
        mobileNumTextField.moveLeft()
        
        // 設定 UIPickerView 的 delegate 及 dataSource
        countryNumPickerView.delegate = self
        countryNumPickerView.dataSource = self
        countryNumPickerView.frame = CGRect(x: 0, y: 0, width: fullScreenSize.width, height: fullScreenSize.height*0.15)
        
        mobileNumTextField.inputView = countryNumPickerView
        
        self.containerView.addSubview(mobileNumTextField)
        mobileNumTextField.snp.makeConstraints { (makes) in
            makes.top.equalTo(mobileNumLabel.snp.bottom).offset(offset)
            makes.left.equalTo(birthdayTextField)
            makes.width.equalTo(shortSide*0.16)
            makes.height.equalTo(self.sizeB)
        }
        
        
        //Add MobileNum TextField (後面九碼）
        mobileNumTextField2.tag = 200
        mobileNumTextField2.placeholder = "   9xxxxxxxx"
        mobileNumTextField2.clearButtonMode = .whileEditing
        mobileNumTextField2.returnKeyType = .done
        mobileNumTextField2.textColor = .black
        mobileNumTextField2.backgroundColor = .lightGray
        mobileNumTextField2.alpha = 0.8
        mobileNumTextField2.layer.cornerRadius = 12.0
        mobileNumTextField2.delegate = self
        mobileNumTextField2.moveLeft()
        
        self.containerView.addSubview(mobileNumTextField2)
        mobileNumTextField2.snp.makeConstraints { (makes) in
            makes.top.equalTo(mobileNumLabel.snp.bottom).offset(offset)
            makes.left.equalTo(shortSide*0.32)
            makes.width.equalTo(shortSide*0.38)
            makes.height.equalTo(self.sizeB)
        }
        
        //Add Gender Label
        genderLabel.tag = 200
        genderLabel.backgroundColor = .clear
        genderLabel.text = "Gender"
        genderLabel.font = UIFont(name: "Georgia-Bold", size: self.sizeS)
        genderLabel.textColor = .white
        genderLabel.numberOfLines = 2
        genderLabel.setLineSpacing(lineSpacing: 2.0, lineHeightMultiple: 1.2)
        
        self.containerView.addSubview(genderLabel)
        genderLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(mobileNumTextField.snp.bottom).offset(offset)
            makes.left.equalTo(mobileNumTextField)
            makes.height.equalTo(self.sizeS)
        }
        
        //Add Gender TextField
        genderTextField.tag = 200
        genderTextField.text = genders[0]
        genderTextField.clearButtonMode = .never
        genderTextField.returnKeyType = .done
        genderTextField.textColor = .black
        genderTextField.backgroundColor = .lightGray
        genderTextField.alpha = 0.8
        genderTextField.layer.cornerRadius = 12.0
        genderTextField.moveLeft()
        
        genderTextField.delegate = self
        genderPickerView.delegate = self
        genderPickerView.dataSource = self
        
        genderTextField.inputView = genderPickerView
        
        self.containerView.addSubview(genderTextField)
        genderTextField.snp.makeConstraints { (makes) in
            makes.top.equalTo(genderLabel.snp.bottom).offset(offset)
            makes.left.equalTo(mobileNumTextField)
            makes.width.equalTo(shortSide*0.7)
            makes.height.equalTo(self.sizeB)
        }
    }
    
    // 02/04 later
    func setAfterName(){
        cleanSubViewsBy(tag: 200)
        setNameToGender()
        setButtons()
    }
    
    /** pickview 有多少 Components */
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    /** 返回陣列 genders 的成員數量 */
    func pickerView(
        _ pickerView: UIPickerView,
        numberOfRowsInComponent component: Int) -> Int {
        if pickerView == genderPickerView{
            return genders.count
        }else{
            return countryNum.count
        }
    }
    /** 返回陣列 genders 成員的標題 */
    func pickerView(_ pickerView: UIPickerView,
                    titleForRow row: Int,
                    forComponent component: Int) -> String? {
        if pickerView == genderPickerView{
            return genders[row]
        }else{
            return countryNum[row]
        }
    }
    /** 將 UITextField 的值更新為陣列 genders 的第 row 項資料 */
    func pickerView(_ pickerView: UIPickerView,
                    didSelectRow row: Int, inComponent component: Int) {
        if pickerView == genderPickerView{
            genderTextField.text = genders[row]
        }else{
            mobileNumTextField.text = countryNum[row]
        }
        
    }
    /**
     set 3 button
     1. Submit Button - clickLeftButton()
     2. Reset Button - clickMidButton()
     3. Back Button - clickRightButton()
     - SeeAlso: `clickLeftButton(...)` , `clickMidButton(...)` , `clickRightButton(...)`
     */
    func setButtons(){
        
        //Add Left Button
        leftButton.tag = 200
        leftButton.backgroundColor = UIColor(red: 253/255.0, green: 198/255.0, blue: 46/255.0, alpha: 1.0)
        leftButton.layer.cornerRadius = 18.0
        leftButton.layer.shadowColor = UIColor.white.cgColor
        leftButton.layer.shadowRadius = 3
        leftButton.layer.shadowOpacity = 0.8
        leftButton.setTitle("Submit", for: [])
        leftButton.titleLabel?.font = UIFont(name: "Georgia-Bold", size: 22)
        leftButton.titleLabel?.textColor = .white
        leftButton.isEnabled = true
        leftButton.addTarget(self,action: #selector(self.clickLeftButton),for: .touchUpInside)
        self.containerView.addSubview(leftButton)
        leftButton.snp.makeConstraints { (makes) in
            makes.top.equalTo(genderTextField.snp.bottom).offset(offset*3)
            makes.left.right.equalTo(birthdayTextField)
            makes.height.equalTo(sizeB)
        }
        
        //Add Right Button
        rightButton.tag = 200
        rightButton.backgroundColor = UIColor(red: 121/255.0, green: 185/255.0, blue: 73/255.0, alpha: 1.0)
        rightButton.layer.cornerRadius = 18.0
        rightButton.layer.shadowColor = UIColor.white.cgColor
        rightButton.layer.shadowRadius = 3
        rightButton.layer.shadowOpacity = 0.8
        rightButton.setTitle("Back", for: [])
        rightButton.titleLabel?.font = UIFont(name: "Georgia-Bold", size: 22)
        rightButton.titleLabel?.textColor = .white
        rightButton.titleLabel?.textAlignment = .center
        rightButton.isEnabled = true
        rightButton.addTarget(self,action: #selector(self.clickRightButton),for: .touchUpInside)
        
        self.containerView.addSubview(rightButton)
        rightButton.snp.makeConstraints { (makes) in
            makes.top.equalTo(leftButton.snp.bottom).offset(offset*3)
            makes.left.right.equalTo(birthdayTextField)
            makes.bottom.equalToSuperview().offset(-30)
            makes.height.equalTo(sizeB)
        }
    }
    /**
     check all field 的格式，若是正確則呼叫 alertMsgForSendRegData (...)
     - SeeAlso: `alertMsgForSendRegData(...)`
     */
    @objc func clickLeftButton(){
        //Check Data
        
        //Check Account
        let accInput: String = accountTextField.text!
        if accInput.count < 4 || accInput.count > 12{
            alertMsg(msg: "Account must have 4 to 12 characters !")
            return
        }else if !isValidAcc(testStr: accInput){
            alertMsg(msg: "Account must have English and digital combination.")
            return
        }
        
        //Check Password
        let passInput: String = passwordTextField.text!
        if passInput.count < 6 || passInput.count > 12{
            alertMsg(msg: "Password must have 6 to 12 characters !")
            return
        }else if !isValidAcc(testStr: passInput){
            alertMsg(msg: "Password must have English and digital combination.")
            return
        }
        //Check ConfirmPasswordTextField
        else if passInput != confirmPasswordTextField.text!{
            alertMsg(msg: "Passwords are different")
            return
        }
        
        //Check E-mail
        let emailInput: String = emailTextField.text!
        if !isValidEmail(testStr: emailInput){
            alertMsg(msg: "The e-mail format is incorrect.")
            return
        }
        
        //Get Input Birthday
        let birthdayInput: String = birthdayTextField.text!
        if birthdayInput == ""{
            alertMsg(msg: "Birthday is required")
        }
        
        
        //Check Name
        let fNameInput: String = firstNameTextField.text!
        let lNameInput: String = lastNameTextField.text!
        if fNameInput.count == 0 || lNameInput.count == 0 {
            alertMsg(msg: "Name can't be null.")
            return
        }else if !isValidName(testStr: fNameInput) || !isValidName(testStr: lNameInput){
            alertMsg(msg: "Name format is incorrect.")
            return
        }
        
        //Check phone nums
        
        let countyyNum: String = mobileNumTextField.text!
        let lastNum: String = mobileNumTextField2.text!
        //        let mobileNumInput: String = "+886_981234567"
        if !isValidPhone(testStr: countyyNum){
            //            alertMsg(msg: "Mobile Number format is incorrect.")
            alertMsg(msg: "Mobile Number format is incorrect.(+886_9xxxxxxxx)")
            return
        }
        if !isValidPhone2(testStr: lastNum){
            alertMsg(msg: "Mobile Number format is incorrect.(+countryNum_xxxxxxxxx)")
            return
        }
        
        let mobileNumInput: String = "\(countyyNum)_\(lastNum)"
        
        //Check gender
        let genderInput: String = genderTextField.text!
        
        let idInput: String = idTextField.text!
        
        //No return -> data 格式正確
        alertMsgForSendRegData(accInput: accInput,passInput: passInput,emailInput: emailInput,birthdayInput: birthdayInput,fNameInput: fNameInput,lNameInput: lNameInput,mobileNumInput: mobileNumInput,genderInput: genderInput,idInput: idInput,msg: "* Please be sure to enter the correct information, in order to provide it when forgetting the account or password.")
    }
    /** reset all field */
    @objc func clickMidButton(){
        //Reset
        accountTextField.text = ""
        passwordTextField.text = ""
        confirmPasswordTextField.text = ""
        emailTextField.text = ""
        birthdayTextField.text = ""
        firstNameTextField.text = ""
        lastNameTextField.text = ""
        mobileNumTextField.text = countryNum[0]
        mobileNumTextField2.text = ""
        genderTextField.text = genders[0]
    }
    /** 返回登入頁面 */
    @objc func clickRightButton(){
        print("clickRightButton")
        self.dismiss(animated: false, completion: nil)
    }
    /** 檢查Account格式是否正確 */
    func isValidAcc(testStr:String) -> Bool {
        let accRegEx = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{4,12}$"
        
        let accTest = NSPredicate(format:"SELF MATCHES %@", accRegEx)
        return accTest.evaluate(with: testStr)
    }
    /** 檢查E-mail格式是否正確 */
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    /** 檢查Name格式是否正確 */
    func isValidName(testStr:String) -> Bool {
        let emailRegEx = "^[A-Za-z]+"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    /** 檢查Phone格式(國碼）是否正確 */
    func isValidPhone(testStr:String) -> Bool {
        let emailRegEx = "^[+][0-9]{2,3}$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    /** 檢查Phone格式(後九碼）是否正確 */
    func isValidPhone2(testStr:String) -> Bool {
        let emailRegEx = "[0-9]{9}$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    /**
     提示框，確認則呼叫 signUp() 送資料給 server，取消就繼續輸入
     - SeeAlso: `signUp(...)`
     */
    func alertMsgForSendRegData(accInput: String,passInput: String,emailInput: String,birthdayInput: String,fNameInput: String,lNameInput: String,mobileNumInput: String,genderInput: String,idInput: String,msg: String) {
        
        let alertController = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        
        // 建立[Yes]按鈕
        let okAction = UIAlertAction(
            title: "Yes, submit",
            style: .destructive,    /* 以紅色顯示按鈕 提醒用戶 */
            handler: {
                (action: UIAlertAction!) -> Void in
                self.signUp(accInput: accInput, passInput: passInput, emailInput: emailInput, birthdayInput: birthdayInput, fNameInput: fNameInput, lNameInput: lNameInput, mobileNumInput: mobileNumInput, genderInput: genderInput, idInput: idInput)}
        )
        alertController.addAction(okAction)
        
        /*
         建立[取消]按鈕
         注意 style .cancel 的按鈕在 「雙按鈕」提示框時，依造Apple的習慣 都會固定在「左方」
         (windows系統是習慣 Cancel按鈕在右方)
         即使這段程式碼是在 後方
         */
        let cancelAction = UIAlertAction(
            title: "No, recheck",
            style: .cancel,
            handler: nil)
        
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    /**
     傳送 Sign Up 資料給 server，根據 server 回傳資料決定動作
     1. error -> alert error msg -> keep check 輸入資料
     2. success -> 提示去 e-mail 收信確認 -> 跳回登入頁面
     */
    func signUp(accInput: String,passInput: String,emailInput: String,birthdayInput: String,fNameInput: String,lNameInput: String,mobileNumInput: String,genderInput: String,idInput: String){
        
        var sex: Int = 9
        if genderInput == "Male"{
            sex = 1
        }else if genderInput == "Female"{
            sex = 2
        }else{
            sex = 0
        }
        
        let params = [
            "user_name": accInput,
            "password": passInput,
            "first_name": fNameInput,
            "last_name": lNameInput,
            "sex": sex,
            "email": emailInput,
            "birthday": birthdayInput,
            
            "phone": mobileNumInput,
            "civil_id": idInput
        ] as [String : Any]
        
        self.setActView(openOrClose: true)
        
        AF.request("\(serverUrlNew)account/createNewAccount", method: .post, parameters: params)
            .responseJSON { (response) in
                self.setActView(openOrClose: false)
                if response.value != nil {
                    let swiftyJsonVar = JSON(response.value!)
                    
                    let status: Int = swiftyJsonVar["status"].int!
                    if status == 200{
                        let alertController = UIAlertController(title: "Sign Up", message: swiftyJsonVar["message"].string!, preferredStyle: .alert)
                        
                        let okAction = UIAlertAction(
                            title: "Back to LoginPage.",
                            style: .default,
                            handler: {
                                (action: UIAlertAction!) -> Void in
                                self.dismiss(animated: true, completion: nil)
                            }
                        )
                        alertController.addAction(okAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                    }else{
                        let alertController = UIAlertController(title: "Sign Up Error", message: swiftyJsonVar["message"].string!, preferredStyle: .alert)
                        
                        let okAction = UIAlertAction(
                            title: "Check datas.",
                            style: .default,
                            handler: {
                                (action: UIAlertAction!) -> Void in
                                self.clickMidButton()
                            }
                        )
                        alertController.addAction(okAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                }else{
                    print("createNewAccount Error")
                }
            }
    }
    
    /** 讓輸入 textfield 的鍵盤，按下 Done 的時候，可以縮起來 */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == mobileNumTextField || textField == genderTextField || textField == birthdayTextField {
            
            return false
        }else{
            return true
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        print("========================================")
        print("解析度為 \(size.width) x \(size.height)")
        switch UIDevice.current.orientation {
        case .landscapeLeft, .landscapeRight:
            print("橫向")
            shortConstraint?.deactivate()
            longConstraint?.activate()
            setAfterName()
            
            
            print("fullScreenSize:\(fullScreenSize),fullScreenSize.width:\(fullScreenSize.width)")
            print("UIScreen.main.bounds.size:\(UIScreen.main.bounds.size),UIScreen.main.bounds.size.width:\(UIScreen.main.bounds.size.width)")
            print("self.view.bounds:\(self.view.bounds),self.view.bounds.width:\(self.view.bounds.width)")
            print("longSide:\(longSide),shortSide:\(shortSide)")
            
        //                print()
        case .portrait, .portraitUpsideDown:
            print("裝置直向")
            shortConstraint?.activate()
            longConstraint?.deactivate()
            setAfterName()
            print("fullScreenSize:\(fullScreenSize),fullScreenSize.width:\(fullScreenSize.width)")
            print("UIScreen.main.bounds.size:\(UIScreen.main.bounds.size),UIScreen.main.bounds.size.width:\(UIScreen.main.bounds.size.width)")
            print("self.view.bounds:\(self.view.bounds),self.view.bounds.width:\(self.view.bounds.width)")
            print("longSide:\(longSide),shortSide:\(shortSide)")
        case .unknown:
            print("error")
        default:
            print("load error")
        }
        
        print("========================================")
        
        
        
    }
}
