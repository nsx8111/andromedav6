//
//  LoginPageViewController.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2018/12/26.
//  Copyright © 2018 蒼月喵. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SnapKit
import Network
import SystemConfiguration.CaptiveNetwork

/** 登入頁面 */
class LoginPageViewController: UIViewController, UITextFieldDelegate {
    /** 輸入 account 的 text field */
    var accountTextField = UITextField()
    //svn test
    var accountTextField333 = UITextField()
    /** 輸入 password 的 text field */
    var passwordTextField = CustomTextField2()
    /** 輸入框切換隱藏密碼按鈕 */
    let rightViewBtn = UIButton(type: .custom)
    /** 字體大小 */
    var fontSize: CGFloat = 20
    /** 下方signup , forget password Label Font Size*/
    var labelFontSize: CGFloat = 18
    /** placeholder 的顏色 */
    let placeholderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
    //    /** 環形進度圈，用來表示讀取資料中 */
    //    var activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
    //    /** 放著環形進度圈的View，平時為 Hide 狀態，讀取時顯示出來在最上方 */
    //    let actView = UIView()
    //    // new API type datas
    //    var ProductTypes: [ProductType] = []
    var logoImageView = UIImageView()
    let welcomeLabel = UILabel()
    let enjoyLabel = UILabel()
    let bg1TextField = UITextField()
    let bg2TextField = UITextField()
    let loginButton = UIButton()
    let forgetLabel = UILabel()
    let signUpLabel = UILabel()
    
    /**
     設定 welcomeLabel 的 font size，暫時先用寫死的方式
     只區分 iPhone 跟 iPad 的大小，之後嚴格一點應該要用 func 來算該設定為多少 size
     有一篇類似的 func 討論在此：https://ithelp.ithome.com.tw/articles/10219651
     
     - TODO: 之後改為 func 算 size
     - Date: 08/25  Mike
     */
    var welcomeFontSize:CGFloat = 80
    
    
    //0306
    //    override var shouldAutorotate: Bool
    
    /** 進入 Login Page 時的動作 */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            // 08/13
            deviceType = .iPad
            self.fontSize = 50
            self.labelFontSize = 50 / 1.5
            
            self.welcomeFontSize = 100
        case .phone:
            print("fontSize no change")
        default:
            print("3 Device Error")
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.viewTapped(gestureRecognizer:)))
        view.addGestureRecognizer(tapGesture)
        
        setBackGroundImage()
//        removeViews(){
//            self.setLogoImage()
//        }
        setLogoImage()
        setMidObjs()
        setTransPage()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //初始化
    }
    
    func cleanData(){
        accountTextField.text = ""
        passwordTextField.text = ""
        accountTextField.backgroundColor = .textFieldColor
        passwordTextField.backgroundColor = .textFieldColor
        
        accountTextField.attributedPlaceholder = NSAttributedString(string: "User Name", attributes: [
            .foregroundColor: placeholderColor,
            .font: UIFont.init(name: "Helvetica-Light", size: self.fontSize)!
        ])
        //        accountTextField.font = UIFont(name: "Helvetica-Light", size: self.fontSize)
        
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [
            .foregroundColor: placeholderColor,
            .font: UIFont.init(name: "Helvetica-Light", size: self.fontSize)!
        ])
        //        passwordTextField.font = UIFont(name: "Helvetica-Light", size: self.fontSize)
        view.endEditing(true)
    }
    
    // move to enjoyLabel.addTapGestureRecognizer {...} #deleteLater
    //    /** 方便測試APP */
    //    func inputDataForTest(){
    //        // old Api
    //        //        self.accountTextField.text = "Mike04"
    //        //        self.passwordTextField.text = "mm"
    //        self.accountTextField.text = "mike01"
    //        self.passwordTextField.text = "Abcd1234"
    //    }
    
    /** 點擊 View 空白處的時候，end Editing 狀態及 check textField 狀況，若是空白則重回原始設定 */
    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer){
        accountTextField.backgroundColor = .textFieldColor
        passwordTextField.backgroundColor = .textFieldColor
        if accountTextField.text == ""{
            print("viewTapped no acc input")
            
            accountTextField.attributedPlaceholder = NSAttributedString(string: "User Name", attributes: [
                .foregroundColor: placeholderColor,
                .font: UIFont.init(name: "Helvetica-Light", size: self.fontSize)!
            ])
            accountTextField.font = UIFont(name: "Helvetica-Light", size: self.fontSize)
        }
        if passwordTextField.text == ""{
            print("viewTapped no pass input")
            passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [
                .foregroundColor: placeholderColor,
                .font: UIFont.init(name: "Helvetica-Light", size: self.fontSize)!
            ])
            passwordTextField.font = UIFont(name: "Helvetica-Light", size: self.fontSize)
        }
        view.endEditing(true)
    }
    
    /** set BackGround Image */
    func setBackGroundImage(){
        let bkgImageView = UIImageView()
        bkgImageView.backgroundColor = .viewBgColor
        self.view.addSubview(bkgImageView)
        bkgImageView.snp.makeConstraints{ (makes) in
            makes.edges.equalToSuperview()
        }
    }
    
    /** set Logo Image */
    func setLogoImage(){
        //Add Logo
        /**
         這個 tag 不知道要幹嘛用的，唯一呼叫在 removeViews 裡面
         目前已經停用呼叫 removeViews，如果過一陣子都沒問題，再刪除
         - Note: 01/26 Mike #deleteLater
         */
        logoImageView.tag = 200
        logoImageView = UIImageView(image: UIImage(named: "AndromedaLogo"))
        self.view.addSubview(logoImageView)
        logoImageView.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(UIScreen.main.bounds.size.height*0.08)
            makes.width.equalTo(UIScreen.main.bounds.size.width*0.75)
            makes.height.equalTo(UIScreen.main.bounds.size.height*0.08)
        }
        
        /**
         切換內網/外網用 For test
         - Note: #上架 時要註解掉
         */
        logoImageView.addTapGestureRecognizer {
            print("tapLogo")
            let checkServerView = UIView()
            checkServerView.backgroundColor = .black
            checkServerView.alpha = 0.4
            checkServerView.tag = 300
            
            checkServerView.isUserInteractionEnabled = true
            
            checkServerView.addTapGestureRecognizer {
//                for sv in self.view.subviews{
//                    if sv.tag == 300{
//                        sv.removeFromSuperview()
//                    }
//                }
                self.cleanSubViewsBy(tag: 300)
            }
            
            self.view.addSubview(checkServerView)
            checkServerView.snp.makeConstraints { (makes) in
                makes.edges.equalToSuperview()
            }
            
            let urlLabel = UILabel()
            urlLabel.textColor = .white
            urlLabel.text = serverUrlNew
            
            checkServerView.addSubview(urlLabel)
            urlLabel.snp.makeConstraints { (makes) in
                makes.bottom.equalToSuperview().offset(-60)
                makes.centerX.equalToSuperview()
                makes.height.equalTo(20)
            }
            
            let changeServerBtn = UIButton()
            changeServerBtn.backgroundColor = .white
            changeServerBtn.tintColor = .black
            changeServerBtn.setTitleColor(.black, for: .normal)
            changeServerBtn.setTitle("Change Server", for: .normal)
            
            checkServerView.addSubview(changeServerBtn)
            changeServerBtn.snp.makeConstraints { (makes) in
                makes.bottom.equalToSuperview().offset(-10)
                makes.centerX.equalToSuperview()
                makes.width.equalTo(fullScreenSize.width/3)
            }
            
            changeServerBtn.addTapGestureRecognizer {
                if serverUrlNew == "http://10.118.210.217/AndromedaBeta/api/"{
                    serverUrlNew = "http://rddev.andromeda.tti.tv/AndromedaBeta/api/"
                    urlLabel.text = "http://rddev.andromeda.tti.tv/AndromedaBeta/api/"
                }else{
                    serverUrlNew = "http://10.118.210.217/AndromedaBeta/api/"
                    urlLabel.text = "http://10.118.210.217/AndromedaBeta/api/"
                }
            }
            
//            changeServerBtn.addTapGestureRecognizer {
//                if serverUrlNew == "http://rddev.andromeda.tti.tv/AndromedaBeta/api/"{
//                    serverUrlNew = "http://andromeda.tti.tv/AndromedaBeta/api/"
//                    urlLabel.text = "http://andromeda.tti.tv/AndromedaBeta/api/"
//                }else{
//                    serverUrlNew = "http://rddev.andromeda.tti.tv/AndromedaBeta/api/"
//                    urlLabel.text = "http://rddev.andromeda.tti.tv/AndromedaBeta/api/"
//                }
//            }
        }
    }
    /**
     set Mid Objs
     1. Add Welcome Label
     2. Add Enjoy Label
     3. Add Account TextField
     4. Add Password TextField
     */
    func setMidObjs(){
        //Add Welcome Label
        //        let welcomeLabel = UILabel(frame: CGRect(x: fullScreenSize.width*0.18 , y: fullScreenSize.height*0.2, width: fullScreenSize.width*0.64, height: fullScreenSize.height*0.11))
        //        let welcomeLabel = UILabel()
        
        welcomeLabel.backgroundColor = .clear
        welcomeLabel.text = "Welcome"
        // 08/25
        welcomeLabel.textColor = .labelTextColor
        welcomeLabel.adjustsFontSizeToFitWidth = true
        self.view.addSubview(welcomeLabel)
        welcomeLabel.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(UIScreen.main.bounds.size.height*0.18)
            //            makes.height.equalTo(UIScreen.main.bounds.size.height*0.11)
        }
        switch currentDevice.deviceType {
        case .iPhone40:
            welcomeLabel.font = UIFont(name: "Helvetica-Light", size: welcomeFontSize/1.2)
        default:
            welcomeLabel.font = UIFont(name: "Helvetica-Light", size: welcomeFontSize)
        }
        
        //Add Enjoy Label
        //        let enjoyLabel = UILabel(frame: CGRect(x: fullScreenSize.width*0.18 , y: fullScreenSize.height*0.24, width: fullScreenSize.width*0.64, height: fullScreenSize.height*0.12))
        //        let enjoyLabel = UILabel()
        
        enjoyLabel.backgroundColor = .clear
        enjoyLabel.text = "Enjoy Your Viewing Time!"
        enjoyLabel.font = UIFont(name: "Helvetica-Light", size: 100)
        enjoyLabel.textColor = .labelTextColor
        enjoyLabel.adjustsFontSizeToFitWidth = true
        self.view.addSubview(enjoyLabel)
        enjoyLabel.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(UIScreen.main.bounds.size.height*0.24)
            makes.width.equalTo(UIScreen.main.bounds.size.width*0.64)
            //            makes.height.equalTo(UIScreen.main.bounds.size.height*0.12)
        }
        
        //方便測試用
        enjoyLabel.isUserInteractionEnabled = true
        /**
         方便輸入帳號密碼測試APP
         - Note: #上架 時要註解掉
         */
        enjoyLabel.addTapGestureRecognizer {
            self.accountTextField.text = "mike01"
            self.passwordTextField.text = "Abcd1234"
        }
        // move to enjoyLabel.addTapGestureRecognizer {...} #deleteLater
        //        let tapEnjoyLabel = UITapGestureRecognizer(target: self, action: #selector(self.tapEnjoyLabel))
        //        enjoyLabel.addGestureRecognizer(tapEnjoyLabel)
        
        //Add Account TextField BG
        bg1TextField.backgroundColor = UIColor.white
        bg1TextField.layer.cornerRadius = 5.0
        self.view.addSubview(bg1TextField)
        bg1TextField.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(UIScreen.main.bounds.size.height*0.45)
            makes.width.equalTo(UIScreen.main.bounds.size.width*0.72)
            makes.height.equalTo(UIScreen.main.bounds.size.height*0.06)
        }
        
        //Add Account TextField
        accountTextField.attributedPlaceholder = NSAttributedString(string: "User Name", attributes: [
            .foregroundColor: placeholderColor,
            .font: UIFont.init(name: "Helvetica-Light", size: self.fontSize)!
        ])
        //        accountTextField.font = UIFont(name: "Helvetica-Light", size: self.fontSize)
        accountTextField.textAlignment = .center
        accountTextField.clearButtonMode = .whileEditing
        accountTextField.returnKeyType = .done
        accountTextField.textColor = .black
        accountTextField.backgroundColor = .textFieldColor
        accountTextField.layer.cornerRadius = 5.0
        accountTextField.addTarget(self, action: #selector(tapAccTextField), for: UIControl.Event.touchDown)
        accountTextField.delegate = self
        self.view.addSubview(accountTextField)
        accountTextField.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(UIScreen.main.bounds.size.height*0.45)
            makes.width.equalTo(UIScreen.main.bounds.size.width*0.72)
            makes.height.equalTo(UIScreen.main.bounds.size.height*0.06)
        }
        
        
        //Add Password TextField BG
        bg2TextField.backgroundColor = UIColor.white
        bg2TextField.layer.cornerRadius = 5.0
        self.view.addSubview(bg2TextField)
        bg2TextField.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(UIScreen.main.bounds.size.height*0.55)
            makes.width.equalTo(UIScreen.main.bounds.size.width*0.72)
            makes.height.equalTo(UIScreen.main.bounds.size.height*0.06)
        }
        
        //Add Password TextField
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [
            .foregroundColor: placeholderColor,
            .font: UIFont.init(name: "Helvetica-Light", size: self.fontSize)!
        ])
        passwordTextField.font = UIFont(name: "Helvetica-Light", size: self.fontSize)
        passwordTextField.textAlignment = .center
        passwordTextField.clearButtonMode = .whileEditing
        passwordTextField.returnKeyType = .done
        passwordTextField.textColor = .black
        passwordTextField.backgroundColor = .textFieldColor
        passwordTextField.layer.cornerRadius = 5.0
        passwordTextField.addTarget(self, action: #selector(tapPassTextField), for: UIControl.Event.touchDown)
        passwordTextField.delegate = self
        passwordTextField.isSecureTextEntry = true
        self.view.addSubview(passwordTextField)
        passwordTextField.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(UIScreen.main.bounds.size.height*0.55)
            makes.width.equalTo(UIScreen.main.bounds.size.width*0.72)
            makes.height.equalTo(UIScreen.main.bounds.size.height*0.06)
        }
        
        if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysOriginal) {
            let smallImage = resizeImage(image: image, width: rightBtnSize2)
            rightViewBtn.addTarget(self,action: #selector(self.securityClick),for: .touchUpInside)
            rightViewBtn.setImage(smallImage, for: .normal)
            passwordTextField.rightView = rightViewBtn
            passwordTextField.rightViewMode = .always
        }
        
        accountTextField.text = accountForLogin
        passwordTextField.text = passwordForLogin
    }
    /**
     - 密碼輸入框切換隱藏密碼icon變換功能
     - Date: 09/11 Yang
     */
    @objc func securityClick() {
        //        print(rightViewBtn.isSelected)
        if passwordTextField.isSecureTextEntry == true{
            if let image = UIImage(named: "eye-b")?.withRenderingMode(.alwaysOriginal) {
                let smallImage = resizeImage(image: image, width: rightBtnSize2)
                rightViewBtn.setImage(smallImage, for: .normal)
            }
            passwordTextField.isSecureTextEntry = false
        }else{
            if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysOriginal) {
                let smallImage = resizeImage(image: image, width: rightBtnSize2)
                rightViewBtn.setImage(smallImage, for: .normal)
            }
            passwordTextField.isSecureTextEntry = true
        }
    }
    // move to enjoyLabel.addTapGestureRecognizer {...} #deleteLater
    //    /** 測試方便用，點擊 enjoyLabel，直接登入 */
    //    @objc func tapEnjoyLabel(sender:UITapGestureRecognizer) {
    ////        print("tapEnjoyLabel")
    //        //        self.loginSuccess()
    //        //0117
    //        inputDataForTest()
    //
    //    }
    
    //    /**
    //     測試用途，點擊可以知道目前 server 為哪個，並且可以做切換的動作
    //     - Date: 11/04  Mike
    //     - 用不到 01/26    Mike    #deleteLater
    //     */
    //    @objc func tapLogo(sender:UITapGestureRecognizer) {
    //        print("tapLogo")
    //        let checkServerView = UIView()
    //        checkServerView.backgroundColor = .black
    //        checkServerView.alpha = 0.4
    //        checkServerView.tag = 300
    //
    //        checkServerView.isUserInteractionEnabled = true
    //
    //        view.addSubview(checkServerView)
    //        checkServerView.snp.makeConstraints { (makes) in
    //            makes.edges.equalToSuperview()
    //        }
    //    }
    /** 點擊 password textField 的處理 */
    @objc func tapPassTextField(textField: UITextField) {
        
        if accountTextField.text == ""{
            print("no acc input")
            
            accountTextField.attributedPlaceholder = NSAttributedString(string: "User Name", attributes: [
                .foregroundColor: placeholderColor,
                .font: UIFont.init(name: "Helvetica-Light", size: self.fontSize)!
            ])
            accountTextField.font = UIFont(name: "Helvetica-Light", size: self.fontSize)
        }
        
        textField.placeholder = ""
        passwordTextField.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1)
        accountTextField.backgroundColor = .textFieldColor
    }
    /** 點擊 account textField 的處理 */
    @objc func tapAccTextField(textField: UITextField) {
        
        if passwordTextField.text == ""{
            print("no pass input")
            
            passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [
                .foregroundColor: placeholderColor,
                .font: UIFont.init(name: "Helvetica-Light", size: self.fontSize)!
            ])
            passwordTextField.font = UIFont(name: "Helvetica-Light", size: self.fontSize)
        }
        
        textField.placeholder = ""
        accountTextField.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1)
        passwordTextField.backgroundColor = .textFieldColor
    }
    /**
     設定跳轉頁面的 objs
     1. Login Button
     2. Forgot Label
     3. Sign up Label
     */
    func setTransPage(){
        //Add Login Button
        loginButton.backgroundColor = .buttonColor2
        loginButton.layer.cornerRadius = 5.0
        loginButton.setTitle("Login", for: [])
        loginButton.titleLabel?.font = UIFont(name: "Helvetica-Light", size: self.fontSize)
        
        loginButton.titleLabel?.textColor = .white
        loginButton.isEnabled = true
        loginButton.addTarget(self,action: #selector(self.clickLoginButton),for: .touchUpInside)
        self.view.addSubview(loginButton)
        
        loginButton.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(UIScreen.main.bounds.size.height*0.65)
            makes.width.equalTo(UIScreen.main.bounds.size.width*0.72)
            makes.height.equalTo(UIScreen.main.bounds.size.height*0.06)
        }
        
        
        //Add Forgot Label
        forgetLabel.backgroundColor = .clear
        forgetLabel.attributedText = NSAttributedString(string: "Forget Password", attributes:
                                                            [.underlineStyle: NSUnderlineStyle.single.rawValue])
        forgetLabel.font = UIFont(name: "Helvetica-Light", size: labelFontSize)
        forgetLabel.textColor = .labelTextColor
        forgetLabel.isUserInteractionEnabled = true
        forgetLabel.textAlignment = .right
        forgetLabel.layer.masksToBounds = false
        forgetLabel.layer.shadowColor = UIColor.gray.cgColor
        forgetLabel.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        forgetLabel.layer.shadowOpacity = 1.0
        forgetLabel.layer.shadowRadius = 0.0
        forgetLabel.addTapGestureRecognizer {
            self.jumpVcBy(name: "ForgotPage")
        }
        // #deleteLater
        //        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapForgetLabel))
        //        forgetLabel.addGestureRecognizer(tap)
        
        self.view.addSubview(forgetLabel)
        forgetLabel.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(UIScreen.main.bounds.size.height*0.74)
            makes.right.equalTo(loginButton)
        }
        
        //Add Sign Up Label
        signUpLabel.backgroundColor = .clear
        signUpLabel.attributedText = NSAttributedString(string: "Sign Up", attributes:
                                                            [.underlineStyle: NSUnderlineStyle.single.rawValue])
        
        signUpLabel.font = UIFont(name: "Helvetica-Light", size: labelFontSize)
        
        signUpLabel.textColor = .labelTextColor
        signUpLabel.isUserInteractionEnabled = true
        signUpLabel.layer.masksToBounds = false
        signUpLabel.layer.shadowColor = UIColor.gray.cgColor
        signUpLabel.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        signUpLabel.layer.shadowOpacity = 1.0
        signUpLabel.layer.shadowRadius = 0.0
        
        signUpLabel.addTapGestureRecognizer {
            self.jumpVcBy(name: "SignUpPage")
        }
        // #deleteLater
        //        let tapSignUpLabel = UITapGestureRecognizer(target: self, action: #selector(self.tapSignUpLabel))
        //        signUpLabel.addGestureRecognizer(tapSignUpLabel)
        
        self.view.addSubview(signUpLabel)
        signUpLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(UIScreen.main.bounds.size.height*0.74)
            makes.left.equalTo(loginButton)
            makes.right.greaterThanOrEqualTo(forgetLabel.snp.left).offset(5)
        }
        
    }
    // #deleteLater
    //    /** 點擊 Forget Label 會跳轉到 ForgotPage */
    //    @objc func tapForgetLabel(sender:UITapGestureRecognizer) {
    //        print("tapForgetLabel")
    //        self.cleanData()
    //        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPage")
    //        vc!.modalPresentationStyle = .fullScreen
    //        self.present(vc!, animated: true, completion: nil)
    //    }
    //    /** 點擊 Sign Up Label 會跳轉到 SignUpPage */
    //    @objc func tapSignUpLabel(sender:UITapGestureRecognizer) {
    //        print("tapSignUpLabel")
    //        self.cleanData()
    //        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpPage")
    //        vc!.modalPresentationStyle = .fullScreen
    //        self.present(vc!, animated: true, completion: nil)
    //    }
    /**
     以 name 來跳轉 VC
     - Date: 01/26  Mike
     */
    func jumpVcBy(name: String){
        self.cleanData()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: name)
        vc!.modalPresentationStyle = .fullScreen
        self.present(vc!, animated: true, completion: nil)
    }
    /**
     點擊 Login Button 以後的處理
     1. 判斷輸入是否為空
     2. 呼叫 loginTest()判斷是否成功登入
     - SeeAlso: `loginTest(...)`
     */
    @objc func clickLoginButton(){
        if self.accountTextField.text == "" || self.passwordTextField.text == "" {
            alertMsg(msg: "Please input account and password")
        } else {
            self.setActView(openOrClose: true)
            loginTest(uid: self.accountTextField.text!,pwd: self.passwordTextField.text!){
                accountForLogin = self.accountTextField.text!
                passwordForLogin = self.passwordTextField.text!
                self.setActView(openOrClose: false)
            }
        }
    }
    /**
     判斷是否登入成功，成功的話呼叫 loginSuccess()
     更新：改為呼叫 jumpVcBy(name: "MenuView") 01/26   Mike
     失敗的話，跳出錯誤提示框
     - Parameters:
     - uid: 登入ID
     - pwd: 登入密碼
     - SeeAlso: `loginSuccess(...)` , `UIViewController.alertMsg(...)`
     
     */
    func loginTest(uid: String,pwd: String,completion : @escaping ()->()) {
        // 01/22 use uuid, systemVersion, 01/23 dvName
        let params = [
            "user_name": "\(uid)",
            "password":"\(pwd)",
            "device_name":dvName,
            "device_os":"ios",
            "device_os_version":systemVersion,
            "device_mac": uuid,
            "device_serial_number": uuid,
            "device_type_id":3
        ] as [String : Any]
        
        print("loginTest_params \(params)")
        
        AF.request("\(serverUrlNew)account/login", method: .post, parameters: params)
            .responseJSON { (response) in
                if response.value != nil {
                    let swiftyJsonVar = JSON(response.value!)
                    
                    if serverUrlNew == "http://10.118.210.217/AndromedaBeta/api/"{
                        let status: Int = swiftyJsonVar["status"].int!
                        if status == 200{
                            let restltJson = JSON(swiftyJsonVar["result"])
                            token = restltJson["token"].string ?? ""
                            print("token \(token)")
                            self.loadUserData(token: token, completion: {
                                self.jumpVcBy(name: "MenuView")
                                completion()
                            })
                        }else{
                            let loginFailMsg: String = swiftyJsonVar["message"].string!
                            self.alertMsg(msg: loginFailMsg)
                            completion()
                        }
                    }else{
                        /**
                         change server to 10.118.210.217 & api return's format
                         this is old format
                         更正，目前 10.118.210.217 還不穩定，暫時先以 serverUrlNew 來判斷要如何處理登入
                         - NOTE: 上架時，或是一陣子以後，確定不會更動回傳格式即可刪除 #上架
                         - Date: 02/02  Mike
                         */
                        if let testToken = swiftyJsonVar["token"].string{
                            token = testToken
                            print("token \(token)")
                            self.loadUserData(token: token, completion: {
                                // #deleteLater
    //                            self.loginSuccess()
                                self.jumpVcBy(name: "MenuView")
                                completion()
                            })
                        }else{
                            let loginFailMsg: String = swiftyJsonVar["message"].string!
                            self.alertMsg(msg: loginFailMsg)
                            completion()
                        }
                    }
                    
                    
                }else{
                    print(response)
                    print("login Error")
                    completion()
                }
            }
    }
    
    
    func loadUserData (token: String,completion : @escaping ()->()) {
        
        let params = [
            "token":"\(token)"
        ] as [String : Any]
        
        AF.request("\(serverUrlNew)account/getAccountInfo", method: .post, parameters: params)
            .responseJSON { (response) in
                if response.value != nil {
                    let swiftyJsonVar = JSON(response.value!)
                    
                    let restltJson = JSON(swiftyJsonVar["result"])
                    
                    if let id: Int = restltJson["id"].int{
                        accountInfoNew.id = id
                    }else{
                        print("error in restltJson[\"id\"]")
                    }
                    accountInfoNew.user_name = restltJson["user_name"].string ?? ""
                    accountInfoNew.first_name = restltJson["first_name"].string ?? ""
                    accountInfoNew.last_name = restltJson["last_name"].string ?? ""
                    accountInfoNew.sex_id = restltJson["sex_id"].int ?? 0
                    accountInfoNew.birthday = restltJson["birthday"].string ?? ""
                    accountInfoNew.email = restltJson["email"].string ?? ""
                    accountInfoNew.phone = restltJson["phone"].string ?? ""
                    accountInfoNew.account_type_id = restltJson["account_type_id"].int ?? 0
                }else{
                    print("loadUserData Error")
                }
                completion()
                
            }
    }
    
    // #deleteLater
//    /** 若是 loginTest() 判斷登入成功，即跳轉到 MenuView 頁面 */
//    func loginSuccess() {
////        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuView")
////        vc!.modalPresentationStyle = .fullScreen
////        self.present(vc!, animated: true, completion: nil)
////        self.cleanData()
//
//        self.jumpVcBy(name: "MenuView")
//    }
    
    /** 讓輸入 textfield 的鍵盤，按下 Done 的時候，可以縮起來 */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return(true)
    }
    
    /**
     這個 func 不知道要幹嘛用的，唯一呼叫在 viewDidLoad 裡面
     目前已經停用呼叫，如果過一陣子都沒問題，再刪除
     - Note: 01/26 Mike #deleteLater
     */
    func removeViews(completion : @escaping ()->()){
        for subView in self.view.subviews{
            if subView.tag == 200 {
                print("remove subViews:\(subView)")
                subView.removeFromSuperview()
            }
        }
        completion()
    }
    
    func reSetSubViews(){
        logoImageView.removeFromSuperview()
        setLogoImage()
        welcomeLabel.removeFromSuperview()
        enjoyLabel.removeFromSuperview()
        bg1TextField.removeFromSuperview()
        bg2TextField.removeFromSuperview()
        accountTextField.removeFromSuperview()
        passwordTextField.removeFromSuperview()
        setMidObjs()
        loginButton.removeFromSuperview()
        forgetLabel.removeFromSuperview()
        signUpLabel.removeFromSuperview()
        setTransPage()
    }
    
    // #deleteLater
//    //0310
//    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
//        super.viewWillTransition(to: size, with: coordinator)
//        reSetSubViews()
//
//    }
    
    // 01/22 get wifi addr
    // 這邊暫時用不到，留存
    func getIPAddress() -> String {
        var address: String?
        var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next }
                
                guard let interface = ptr?.pointee else { return "" }
                let addrFamily = interface.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                    
                    // wifi = ["en0"]
                    // wired = ["en2", "en3", "en4"]
                    // cellular = ["pdp_ip0","pdp_ip1","pdp_ip2","pdp_ip3"]
                    
                    let name: String = String(cString: (interface.ifa_name))
                    if  name == "en0" || name == "en2" || name == "en3" || name == "en4" || name == "pdp_ip0" || name == "pdp_ip1" || name == "pdp_ip2" || name == "pdp_ip3" {
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface.ifa_addr, socklen_t((interface.ifa_addr.pointee.sa_len)), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                        address = String(cString: hostname)
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        return address ?? ""
    }
    
    //    func GetMACAddressFromIPv6(ip: String) -> String{
    //        print(ip)
    //        let IPStruct = IPv6Address(ip)
    //        if(IPStruct == nil){
    //            print("IPStruct == nil")
    //            let test = IPv4Address(ip)
    //            print(test)
    //            return ""
    //        }
    //        let extractedMAC = [
    //            (IPStruct?.rawValue[8])! ^ 0b00000010,
    //            IPStruct?.rawValue[9],
    //            IPStruct?.rawValue[10],
    //            IPStruct?.rawValue[13],
    //            IPStruct?.rawValue[14],
    //            IPStruct?.rawValue[15]
    //        ]
    //        return String(format: "%02x:%02x:%02x:%02x:%02x:%02x",
    //            extractedMAC[0] ?? 00,
    //            extractedMAC[1] ?? 00,
    //            extractedMAC[2] ?? 00,
    //            extractedMAC[3] ?? 00,
    //            extractedMAC[4] ?? 00,
    //            extractedMAC[5] ?? 00)
    //    }
    
    //    func getSSID() -> String{
    //
    //        var currentSSID = ""
    //
    //            let interfaces = CNCopySupportedInterfaces()
    //
    //            if interfaces != nil {
    //                var currentSSID = ""
    //                for i in 0..<CFArrayGetCount(interfaces) {
    //                    let interfaceName: UnsafeRawPointer = CFArrayGetValueAtIndex(interfaces, i)
    //                    let rec = unsafeBitCast(interfaceName, to: AnyObject.self)
    //                    let unsafeInterfaceData = CNCopyCurrentNetworkInfo("\(rec)" as CFString)
    //                                if unsafeInterfaceData != nil {
    //                                    let interfaceData = unsafeInterfaceData! as Dictionary
    //
    //                                    currentSSID = interfaceData["SSID"] as! String
    //                                }
    //                            }
    //
    //                let interfacesArray = interfaces.takeRetainedValue() as! [String]
    //
    //                if interfacesArray.count > 0 {
    //
    //                    let interfaceName = interfacesArray[0] as String
    //
    //                    let unsafeInterfaceData = CNCopyCurrentNetworkInfo(interfaceName)
    //
    //                    if unsafeInterfaceData != nil {
    //
    //                        let interfaceData = unsafeInterfaceData.takeRetainedValue() as Dictionary!
    //
    //                        currentSSID = interfaceData[kCNNetworkInfoKeySSID] as! String
    //
    //                        let ssiddata = NSString(data:interfaceData[kCNNetworkInfoKeySSIDData]! as! NSData, encoding:NSUTF8StringEncoding) as! String
    //
    //                        //ssid data from hex
    //                        print(ssiddata)
    //                    }
    //                }
    //
    //            }
    //
    //            return currentSSID
    //
    //        }
    //    func getSSIDs() -> [String] {
    //        guard let interfaceNames = CNCopySupportedInterfaces() as? [String] else {
    //            return []
    //        }
    //
    //        return interfaceNames.compactMap { name in
    //            guard let info = CNCopyCurrentNetworkInfo(name as CFString) as? [String: AnyObject] else {
    //                return nil
    //            }
    //            guard let ssid = info[kCNNetworkInfoKeySSID as String] as? String else {
    //                return nil
    //            }
    //
    //            return ssid
    //        }
    //    }
}
