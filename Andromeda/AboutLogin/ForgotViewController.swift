//
//  ForgotViewController.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2018/12/26.
//  Copyright © 2018 蒼月喵. All rights reserved.
//

import UIKit
import SnapKit
/** 忘記密碼的頁面 */
class ForgotViewController: UIViewController, UITextFieldDelegate {
    
    /** 輸入 account 的 text field */
    var accountTextField = UITextField()
    /** 輸入 e-mail 的 text field */
    var emailTextField = UITextField()
    
    /** size Big */
    var sizeB: CGFloat = 40
    /** sise Smaill */
    var sizeS: CGFloat = 20
    
    let titleLabel = UILabel()
    let introLabel = UILabel()
    let accountLabel = UILabel()
    let emailLabel = UILabel()
    let leftButton = UIButton()
    let rightButton = UIButton()
    
    /** 進入 Forgot Page 時的動作 */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 08/13
        switch deviceType {
        case .iPhone:
            print("no change")
        case .iPad:
            self.sizeB = 70
            self.sizeS = 40
        }
        
        setBackGroundImage()
        setMidObjs()
        setButtons()
    }
    /** set BackGround Image */
    func setBackGroundImage(){
        //Add BackGround
        let bkgImageView = UIImageView()
        //        bkgImageView.frame = CGRect(x: 0, y: 0, width: fullScreenSize.width, height: fullScreenSize.height)
        bkgImageView.backgroundColor = .viewBgColor
        
        self.view.addSubview(bkgImageView)
        
        bkgImageView.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
        }
    }
    /**
     set Mid Objs
     1. Add Title Label ("Forgot Password")
     2. Add Account Label & TextField
     3. Add E-mail Label & TextField
     4. Add intro Label
     */
    func setMidObjs(){
        titleLabel.backgroundColor = .clear
        
        titleLabel.text = "Forgot Password"
        titleLabel.font = UIFont(name: "Georgia-Bold", size: self.sizeB)
        titleLabel.textColor = .white
//        titleLabel.numberOfLines = 2
//        titleLabel.setLineSpacing(lineSpacing: 2.0, lineHeightMultiple: 1.2)
        
        self.view.addSubview(titleLabel)
                
        titleLabel.snp.makeConstraints { (makes) in
            makes.left.equalToSuperview().offset(shortSide*0.08)
            makes.top.equalToSuperview().offset(longSide*0.15)
            makes.height.equalTo(self.sizeB*2)
//            makes.width.equalTo(fullScreenSize.width*0.8)
            makes.right.equalToSuperview().offset(-shortSide*0.08)
        }
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.minimumScaleFactor = 0.4
        
        accountLabel.backgroundColor = .clear
        accountLabel.text = "Account"
        accountLabel.font = UIFont(name: "Georgia-Bold", size: self.sizeS)
        accountLabel.textColor = .white
        accountLabel.numberOfLines = 2
        accountLabel.setLineSpacing(lineSpacing: 2.0, lineHeightMultiple: 1.2)
        
        self.view.addSubview(accountLabel)
        
        accountLabel.snp.makeConstraints { (makes) in
            makes.left.equalToSuperview().offset(shortSide*0.08)
            makes.top.equalToSuperview().offset(longSide*0.32)
            makes.height.equalTo(self.sizeS)
        }
        
        accountTextField.placeholder = "   Enter Account here"
        accountTextField.clearButtonMode = .whileEditing
        accountTextField.returnKeyType = .done
        accountTextField.textColor = .black
        accountTextField.backgroundColor = .lightGray
        accountTextField.alpha = 0.8
        accountTextField.layer.cornerRadius = 12.0
        accountTextField.delegate = self
        
        self.view.addSubview(accountTextField)
        
        accountTextField.snp.makeConstraints { (makes) in
            makes.left.equalToSuperview().offset(shortSide*0.08)
            makes.top.equalToSuperview().offset(longSide*0.37)
            makes.width.equalTo(shortSide*0.7)
            makes.height.equalTo(self.sizeB)
        }
        
        emailLabel.backgroundColor = .clear
        emailLabel.text = "E-mail"
        emailLabel.font = UIFont(name: "Georgia-Bold", size: self.sizeS)
        emailLabel.textColor = .white
        emailLabel.numberOfLines = 2
        emailLabel.setLineSpacing(lineSpacing: 2.0, lineHeightMultiple: 1.2)
        
        self.view.addSubview(emailLabel)
        
        emailLabel.snp.makeConstraints { (makes) in
            makes.left.equalToSuperview().offset(shortSide*0.08)
            makes.top.equalToSuperview().offset(longSide*0.47)
            makes.height.equalTo(self.sizeS)
        }
        
        //Add Password TextField
        emailTextField = UITextField(frame: CGRect(x: fullScreenSize.width*0.08 , y: fullScreenSize.height*0.52, width: textFieldWidth2, height: self.sizeB))
        emailTextField.placeholder = "   Enter Password here"
        emailTextField.clearButtonMode = .whileEditing
        emailTextField.returnKeyType = .done
        emailTextField.textColor = .black
        emailTextField.backgroundColor = .lightGray
        emailTextField.alpha = 0.8
        emailTextField.layer.cornerRadius = 12.0
        emailTextField.delegate = self
        
        self.view.addSubview(emailTextField)
        
        emailTextField.snp.makeConstraints { (makes) in
            makes.left.equalToSuperview().offset(shortSide*0.08)
            makes.top.equalToSuperview().offset(longSide*0.52)
            makes.width.equalTo(shortSide*0.7)
            makes.height.equalTo(self.sizeB)
        }
        
        introLabel.backgroundColor = .clear
        introLabel.text = "The system will send a Temporary Password to your email inbox. Then you can go back to the login age and try to log in."
        introLabel.font = UIFont(name: "Georgia-Italic", size: 14.5)
        introLabel.textColor = .white
        introLabel.numberOfLines = 4
        introLabel.setLineSpacing(lineSpacing: 4.0, lineHeightMultiple: 1.0)
        introLabel.isUserInteractionEnabled = true
        
        self.view.addSubview(introLabel)
        
        introLabel.snp.makeConstraints { (makes) in
            makes.left.equalToSuperview().offset(shortSide*0.08)
            makes.top.equalToSuperview().offset(longSide*0.62)
            makes.width.equalTo(shortSide*0.72)
            makes.height.equalTo(self.sizeB)
        }
    }
    
    /**
     set 2 button
     1. Submit Button - clickLeftButton()
     2. Back Button - clickRightButton()
     - SeeAlso: `clickLeftButton(...)` , `clickRightButton(...)`
     */
    func setButtons(){
        //Add Login Button
        //        let leftButton = UIButton(frame: CGRect(x: fullScreenSize.width*0.05, y: fullScreenSize.height*0.85, width: fullScreenSize.width*0.42, height: fullScreenSize.height*0.12))
        leftButton.backgroundColor = UIColor(red: 253/255.0, green: 198/255.0, blue: 46/255.0, alpha: 1.0)
        leftButton.layer.cornerRadius = 18.0
        leftButton.layer.shadowColor = UIColor.white.cgColor
        leftButton.layer.shadowRadius = 3
        leftButton.layer.shadowOpacity = 0.8
        leftButton.setTitle("Submit", for: [])
        leftButton.titleLabel?.font = UIFont(name: "Georgia-Bold", size: 35)
        leftButton.titleLabel?.textColor = .white
        leftButton.isEnabled = true
        leftButton.addTarget(self,action: #selector(self.clickLeftButton),for: .touchUpInside)
        self.view.addSubview(leftButton)
        
        leftButton.snp.makeConstraints { (makes) in
            makes.left.equalToSuperview().offset(shortSide*0.05)
            makes.top.equalToSuperview().offset(longSide*0.85)
            makes.width.equalTo(shortSide*0.42)
            makes.height.equalTo(longSide*0.12)
        }
        
        rightButton.backgroundColor = UIColor(red: 121/255.0, green: 185/255.0, blue: 73/255.0, alpha: 1.0)
        rightButton.layer.cornerRadius = 18.0
        rightButton.layer.shadowColor = UIColor.white.cgColor
        rightButton.layer.shadowRadius = 3
        rightButton.layer.shadowOpacity = 0.8
        rightButton.setTitle("Back", for: [])
        rightButton.titleLabel?.font = UIFont(name: "Georgia-Bold", size: 32)
        rightButton.titleLabel?.numberOfLines = 2
        rightButton.titleLabel?.setLineSpacing(lineSpacing: 2.0, lineHeightMultiple: 0.8)
        rightButton.titleLabel?.textColor = .white
        rightButton.titleLabel?.textAlignment = .center
        rightButton.isEnabled = true
        rightButton.addTarget(self,action: #selector(self.clickRightButton),for: .touchUpInside)
        
        self.view.addSubview(rightButton)
        
        rightButton.snp.makeConstraints { (makes) in
            makes.left.equalToSuperview().offset(shortSide*0.53)
            makes.top.equalToSuperview().offset(longSide*0.85)
            makes.width.equalTo(shortSide*0.42)
            makes.height.equalTo(longSide*0.12)
        }
    }
    /**
     點擊左邊 Submit Button 的處理
     1. 檢查輸入格式是否正確 - checkData()
     2. 送資料給 server - forgot()
     - SeeAlso: `checkData(...)` , `forgot(...)`
     */
    @objc func clickLeftButton(){
        self.checkData()
        self.forgot()
        print("Left")
    }
    /** 點擊右邊的 Back Button 的處理 -> 回上一頁 */
    @objc func clickRightButton(){
        print("Right")
        self.dismiss(animated: false, completion: nil)
    }
    /** 檢查輸入格式是否正確，否的話跳提示訊息，且break */
    func checkData(){
        let accInput: String = accountTextField.text!
        if accInput.count < 4 || accInput.count > 12{
            alertMsg(msg: "Account must have 4 to 12 characters !")
            return
        }else if !isValidAcc(testStr: self.accountTextField.text!){
            alertMsg(msg: "Account must have English and digital combination.")
            return
        }
        
        if !isValidEmail(testStr: self.emailTextField.text!){
            alertMsg(msg: "The e-mail format is incorrect.")
            return
        }
        
    }
    /**
     資料格式正確以後，送資料給 server
     1. 若是資料內容錯誤，提示錯誤訊息，清空輸入框
     2. 若是資料無誤，提示 Send to E-mail，並且返回登入頁面 (呼叫 sendOkAlert())
     - SeeAlso: `sendOkAlert(...)`
     */
    func forgot(){
//        self.actView.isHidden = false
        
        // 08/14 這邊有問題 ForgotPassword API 不見了
        // TODO
        let andromedaEndpoint: String = "\(serverUrlNew)API/ClientMgr/ForgotPassword?uid=\(self.accountTextField.text!)&email=\(self.emailTextField.text!)"
        //        print(andromedaEndpoint)
//        let actView = UIView()
//        self.setActView(actView: actView)
        self.setActView(openOrClose: true)
        guard let url = URL(string: andromedaEndpoint) else {
            print("Error: cannot create URL")
            DispatchQueue.main.async {
//                self.actView.isHidden = true
//                actView.removeFromSuperview()
                self.setActView(openOrClose: false)
                self.alertMsg(msg: "Forgot Fail 1")
                self.accountTextField.text = ""
                self.emailTextField.text = ""
            }
            return
        }
        let urlRequest = URLRequest(url: url)
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            // check for any errors
            guard error == nil else {
                print("error calling GET in forgot")
                print(error!)
//                self.actView.isHidden = true
//                actView.removeFromSuperview()
                self.setActView(openOrClose: false)
                return
            }
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data in forgot")
//                self.actView.isHidden = true
//                actView.removeFromSuperview()
                self.setActView(openOrClose: false)
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                guard let loginData = try JSONSerialization.jsonObject(with: responseData, options: [])
                        as? [String: Any] else {
                    print("error trying to convert data to JSON")
                    //                        self.actView.isHidden = true
//                    actView.removeFromSuperview()
                    self.setActView(openOrClose: false)
                    return
                }
                dump(loginData)
                let errorMsg = loginData["Message"] as! String
                let code = loginData["Code"] as! Int
                if code == 0 {
                    DispatchQueue.main.async {
//                        self.actView.isHidden = true
//                        actView.removeFromSuperview()
                        self.setActView(openOrClose: false)
                        
                        self.sendOkAlert()
                    }
                } else if code == 1 {
                    DispatchQueue.main.async {
//                        self.actView.isHidden = true
//                        actView.removeFromSuperview()
                        self.setActView(openOrClose: false)
                        self.alertMsg(msg: errorMsg)
                    }
                }
            } catch  {
                DispatchQueue.main.async {
                    self.alertMsg(msg: "Server Error!")
//                    self.actView.isHidden = true
//                    actView.removeFromSuperview()
                    self.setActView(openOrClose: false)
                }
                print("error trying to convert data to JSON")
                return
            }
        }
        task.resume()
    }
    
    /** 如果送出的資料正確，提示Done，並且返回登入頁面 */
    func sendOkAlert(){
        let alertController = UIAlertController(title: "Done", message: "Send mail successfully.", preferredStyle: .alert)
        
        let okAction = UIAlertAction(
            title: "Back to LoginPage.",
            style: .default,
            handler: {
                (action: UIAlertAction!) -> Void in
                self.dismiss(animated: true, completion: nil)
        }
        )
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    /** 檢查Account格式是否正確 */
    func isValidAcc(testStr:String) -> Bool {
        let accRegEx = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{4,12}$"
        
        let accTest = NSPredicate(format:"SELF MATCHES %@", accRegEx)
        return accTest.evaluate(with: testStr)
    }
    /** 檢查E-mail格式是否正確 */
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    /** 讓輸入 textfield 的鍵盤，按下 Done 的時候，可以縮起來 */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return(true)
    }
}
