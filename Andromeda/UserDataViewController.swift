//
//  HistoryViewController.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2019/5/15.
//  Copyright © 2019 蒼月喵. All rights reserved.
//

import UIKit
import Alamofire
import SnapKit
import SwiftyJSON

class UserDataViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // yang test
        let cell = scrollView2.dequeueReusableCell(withIdentifier: "ScrollCell2") as! RentTableViewCell
        return cell
    }
    
    weak var ButtonDelegate: ButtonDelegate?
    var scrollView2 = UITableView()
    var scrollView = UIScrollView()
    var containerView = UIView()
    
    /** 輸入 account 的 text field */
    var accountTextField = UITextField()
    /** 輸入 password 的 text field */
    var passwordTextField = UITextField()
    /** 重新輸入 password 的 text field */
    var confirmPasswordTextField = UITextField()
    /** 輸入 e-mail 的 text field */
    var emailTextField = UITextField()
    /** 紀錄 birthday 的 text field */
    var birthdayTextField = UITextField()
    /** 日期選擇 for birthday */
    var datePicker = UIDatePicker()
    /** 輸入 First name 的 text field */
    var firstNameTextField = UITextField()
    /** 輸入 Last name 的 text field */
    var lastNameTextField = UITextField()
    /** 輸入 手機號碼(國碼) 的 text field */
    var mobileNumTextField = UITextField()
    /** 輸入 手機號碼(後九碼) 的 text field */
    var mobileNumTextField2 = UITextField()
    /** 輸入 id 的 text field */
    var idTextField = UITextField()
    /** 紀錄 性別 的 text field */
    var genderTextField = UITextField()
    /** 性別選項 array */
    let genders = ["Male","Female"]
    /** 性別 picker */
    let genderPickerView = UIPickerView()
    /** 國碼選項 array */
    let countryNum = ["+886","+86","+852"]
    /** 國碼 picker */
    let countryNumPickerView = UIPickerView()
    /** 點擊屏幕用 */
    let contentView = UIView()
    /**  */
    let bgView = UIView()
    
    let titleLabel = UILabel()
    let genderLabel = UILabel()
    let accountLabel = UILabel()

    var keyboardHeight = CGFloat()
    /** size Big */
    var sizeB: CGFloat = 30
    /** size Small */
    var sizeS: CGFloat = 20
    /** Label 間的距離 */
    var offset: CGFloat = 10
//    /** 環形進度圈，用來表示讀取資料中 */
//    var activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
//    /** 放著環形進度圈的View，平時為 Hide 狀態，讀取時顯示出來在最上方 */
//    let actView = UIView()
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navBarHeight = statusBarHeight + self.navigationController!.navigationBar.frame.height
        print("navBarHeight \(navBarHeight)")

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardEvent), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardEvent), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        allGestureRecognizer()
        
//        switch UIDevice.current.userInterfaceIdiom {
//        case .pad:
//            self.sizeB = 60
//            self.sizeS = 40
//        case .phone:
//            print("no change")
//        default:
//            print("3 Device Error")
//        }
        
        switch deviceType {
        case .iPhone:
            print("no change")
        case .iPad:
            self.sizeB = 60
            self.sizeS = 40
        }
        
        setBackGroundImage()
        setAccToBirthday()
        setNameToGender()
        setButtons()
        isTextFieldsCanEdit(true)

//        forTest_SetData()
        
//        activityIndicator.hidesWhenStopped = true;
//        activityIndicator.style  = UIActivityIndicatorView.Style.gray;
//        activityIndicator.center = view.center;
//
//        actView.frame = CGRect(x: 0, y: 0, width: fullScreenSize.width, height: fullScreenSize.height)
//        actView.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.4)
//
//        view.addSubview(actView)
//        actView.addSubview(activityIndicator)
//        activityIndicator.startAnimating()
//
//        actView.isHidden = true
//        setDatas()
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        let pan: UIScreenEdgePanGestureRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(self.handleEdgePanGesture(_:)))
//        let pan2: UIScreenEdgePanGestureRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(self.handleEdgePanGesture2(_:)))
//        if let viewWithTag = self.view.viewWithTag(1000) {
//            viewWithTag.addGestureRecognizer(pan)
//            viewWithTag.addGestureRecognizer(pan2)
//        }
        
        let name = NSNotification.Name("removeContentView")
        NotificationCenter.default.addObserver(self, selector:
        #selector(updateContentViewNoti(noti:)), name: name, object: nil)
        setDatas()
    }
    
    // when user select a textfield, this method will be called
    func textFieldDidBeginEditing(_ textField: UITextField) {
        // set the activeTextField to the selected textfield
        activeTextField = textField
    }
    
    // when user click 'done' or dismiss the keyboard
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeTextField = nil
    }
    
    @objc func keyboardEvent(_ notification: Notification) {
        guard let keyboardSize = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
        // 取得鍵盤的 Frame
        let keyboardScreenEndFrame: CGRect = keyboardSize.cgRectValue
        // 取得鍵盤轉換後的 Frame 是相對於 UIWindow 的位置並非是 UIView，因 UIWindow 是用來接收鍵盤事件的
//        let keyboardViewEndFrame: CGRect = view.convert(keyboardScreenEndFrame, to: view.window)

        // contentInset 可增加可 Scroll 的高度
        // 當鍵盤收合時，將 contentInset 設為 0，反之，增加可 Scroll 的高度
        if notification.name == UIResponder.keyboardWillHideNotification {
            bgView.snp.updateConstraints {(makes) in
                makes.top.equalTo(view).offset(0)
                makes.bottom.equalTo(view).offset(0)
            }
        } else {

            var shouldMoveViewUp = false
            var bottomOfTextField: CGFloat = 0
            var topOfKeyboard: CGFloat = 0

            // if active text field is not nil
            if let activeTextField = activeTextField {
                bottomOfTextField = activeTextField.convert(activeTextField.bounds, to: self.scrollView).maxY
                topOfKeyboard = self.view.frame.height - keyboardScreenEndFrame.height
//                bottomOfTextField = activeTextField.convert(activeTextField.bounds, to: self.view.window).maxY
//                topOfKeyboard = self.view.frame.height - keyboardViewEndFrame.height
                keyboardHeight = keyboardScreenEndFrame.height
                print("keyboardHeight \(keyboardHeight)")
                print("topOfKeyboard \(topOfKeyboard)")
                print("bottomOfTextField \(bottomOfTextField)")
                // if the bottom of Textfield is below the top of keyboard, move up
                if bottomOfTextField > topOfKeyboard {
                    shouldMoveViewUp = true
                }
            }
            UIViewPropertyAnimator.runningPropertyAnimator(withDuration: 0.25, delay: 0, animations: {
                if(shouldMoveViewUp) {
                    self.bgView.snp.updateConstraints {(makes) in
                        makes.top.equalTo(self.view).offset(topOfKeyboard - bottomOfTextField)
                        makes.bottom.equalTo(self.view).offset(self.keyboardHeight)
                    }
                }else{
                    self.bgView.snp.updateConstraints {(makes) in
                        makes.top.equalTo(self.view).offset(0)
                        makes.bottom.equalTo(self.view).offset(0)
                    }
                }
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    /** 切換 textField 可否編輯 */
    func isTextFieldsCanEdit(_ bool: Bool){
        for subview in containerView.subviews{
            if subview.tag == 200{
                let textField = subview as! UITextField
                textField.isEnabled = bool
            }
        }
    }
    
    /** 測試用func，不用每次都要一格一格填寫資料 */
    func forTest_SetData(){
        self.accountTextField.text = "ttt01"
        self.passwordTextField.text = "abcd12"
        self.confirmPasswordTextField.text = "abcd12"
        self.emailTextField.text = "dasda@sdsd.cda"
        self.birthdayTextField.text = "1983-05-08"
        self.firstNameTextField.text = "AAAA"
        self.lastNameTextField.text = "BBBB"
        self.mobileNumTextField.text = "+886"
        self.mobileNumTextField2.text = "912345678"
        //        self.idTextField.text = "L123456789"
        self.idTextField.text = ""
        self.genderTextField.text = "Male"
    }
    
    func setDatas(){
        self.accountTextField.text = accountInfoNew.user_name
//        print("in setDatas accountInfo.AccountName:\(accountInfo.AccountName)")
        self.passwordTextField.text = "xxxxx"
        self.confirmPasswordTextField.text = "xxxxx"
        self.emailTextField.text = accountInfoNew.email
        self.birthdayTextField.text = accountInfoNew.birthday
        self.firstNameTextField.text = accountInfoNew.first_name
        self.lastNameTextField.text = accountInfoNew.last_name
        self.mobileNumTextField.text = "+886"
        self.mobileNumTextField2.text = accountInfoNew.phone
        //        self.idTextField.text = "L123456789"
        self.idTextField.text = ""
        if accountInfoNew.sex_id == 0{
            self.genderTextField.text = "Male"
        }else{
            self.genderTextField.text = "Female"
        }
    }
    
    /** set BackGround Image */
    func setBackGroundImage(){
        //Add BackGround
//        let bkgImageView = UIImageView()
//        bkgImageView.frame = CGRect(x: 0, y: 0, width: fullScreenSize.width, height: fullScreenSize.height)
//        bkgImageView.backgroundColor = UIColor(red: 0/255.0, green: 24/255.0, blue: 48/255.0, alpha: 1.0)
//        self.view.addSubview(bkgImageView)
        
        view.addSubview(bgView)
        bgView.backgroundColor = .viewBgColor
        bgView.snp.makeConstraints {(makes) in
            makes.centerX.equalTo(view)
            makes.top.equalTo(view).offset(0)
            makes.bottom.equalTo(view).offset(0)
            makes.width.equalTo(view.frame.width)
        }
        
        bgView.addSubview(scrollView)
//        scrollView.register(RentTableViewCell.self, forCellReuseIdentifier: "ScrollCell2")
//        scrollView.backgroundColor = .viewBgColor
//        scrollView.separatorColor = .clear
//        scrollView.dataSource = self
        scrollView.delegate = self
        scrollView.bounces = false
        scrollView.snp.makeConstraints { (makes) in
            makes.edges.equalTo(bgView)
        }
        // add containerView
        scrollView.addSubview(containerView)
        containerView.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
            makes.width.equalTo(view.bounds.width)
        }
    }
    /**
     set Account to Birthday Objs
     1. Add title Label ("Create Account")
     2. Add Account Label & textfield
     3. Add Password Label & textfield
     4. Add Confirm Password Label & textfield
     5. Add E-mail Label & textfield
     6. Add Birthday Label & DatePicker
     */
    func setAccToBirthday(){
        //Add Account Label
        //        let titleLabel = UILabel(frame: CGRect(x: fullScreenSize.width*0.08 , y: fullScreenSize.height*0.02, width: fullScreenSize.width, height: self.sizeB))
        print(self.sizeB)
        titleLabel.backgroundColor = .clear
        titleLabel.text = "User Data"
        titleLabel.font = UIFont(name: "Georgia-Bold", size: self.sizeB)
        titleLabel.textColor = .white
        titleLabel.numberOfLines = 2
        titleLabel.setLineSpacing(lineSpacing: 2.0, lineHeightMultiple: 1.2)
        self.containerView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(15)
            makes.left.equalToSuperview().offset(30)
//            makes.left.equalTo(30)
        }
        
        //Add Account Label
        //        let accountLabel = UILabel(frame: CGRect(x: fullScreenSize.width*0.08 , y: fullScreenSize.height*0.08, width: fullScreenSize.width, height: self.sizeS))
        accountLabel.backgroundColor = .clear
        accountLabel.text = "Account"
        accountLabel.font = UIFont(name: "Georgia-Bold", size: self.sizeS)
        accountLabel.textColor = .white
        accountLabel.numberOfLines = 2
        accountLabel.setLineSpacing(lineSpacing: 2.0, lineHeightMultiple: 1.2)
        self.containerView.addSubview(accountLabel)
        accountLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(titleLabel.snp.bottom).offset(offset)
            makes.left.equalTo(titleLabel)
            makes.height.equalTo(self.sizeS)
        }
        
        //Add Account TextField
        //        accountTextField = UITextField(frame: CGRect(x: fullScreenSize.width*0.08 , y: fullScreenSize.height*0.12, width: fullScreenSize.width*0.7, height: self.sizeB))
        accountTextField.placeholder = "   Enter Account here"
        //            accountTextField.isEnabled = false
        accountTextField.tag = 200
        //        accountTextField.placeholder.
        accountTextField.clearButtonMode = .whileEditing
        accountTextField.returnKeyType = .done
        accountTextField.textColor = .black
        accountTextField.backgroundColor = .lightGray
        accountTextField.alpha = 0.8
        accountTextField.layer.cornerRadius = 12.0
        accountTextField.delegate = self
        //        accountTextField.leftView = UIView.init(frame:CGRect(x: 0,y: 0,width: 5,height: 0))
        //        accountTextField.leftViewMode = .always
        accountTextField.moveLeft()
        self.containerView.addSubview(accountTextField)
        accountTextField.snp.makeConstraints { (makes) in
            makes.top.equalTo(accountLabel.snp.bottom).offset(offset)
            makes.left.equalTo(titleLabel)
            makes.width.equalTo(textFieldWidth2)
            makes.height.equalTo(self.sizeB)
        }
        
        //Add Password Label
        let passwordLabel = UILabel()
        passwordLabel.backgroundColor = .clear
        passwordLabel.text = "Password"
        passwordLabel.font = UIFont(name: "Georgia-Bold", size: self.sizeS)
        passwordLabel.textColor = .white
        passwordLabel.numberOfLines = 2
        passwordLabel.setLineSpacing(lineSpacing: 2.0, lineHeightMultiple: 1.2)
        self.containerView.addSubview(passwordLabel)
        passwordLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(accountTextField.snp.bottom).offset(offset)
            makes.left.equalTo(titleLabel)
            makes.height.equalTo(self.sizeS)
        }
        
        //Add Password TextField
        //        passwordTextField = UITextField(frame: CGRect(x: fullScreenSize.width*0.08 , y: fullScreenSize.height*0.21, width: fullScreenSize.width*0.7, height: self.sizeB))
        passwordTextField.placeholder = "   Enter Password here"
        passwordTextField.clearButtonMode = .whileEditing
        passwordTextField.tag = 200
        passwordTextField.returnKeyType = .done
        passwordTextField.textColor = .black
        passwordTextField.backgroundColor = .lightGray
        passwordTextField.alpha = 0.8
        passwordTextField.layer.cornerRadius = 12.0
        passwordTextField.isSecureTextEntry = true
        passwordTextField.delegate = self
        passwordTextField.moveLeft()
        self.containerView.addSubview(passwordTextField)
        passwordTextField.snp.makeConstraints { (makes) in
            makes.top.equalTo(passwordLabel.snp.bottom).offset(offset)
            makes.left.equalTo(titleLabel)
            makes.width.equalTo(textFieldWidth2)
            makes.height.equalTo(self.sizeB)
        }
        
        //Add ConfirmPassword Label
        let confirmPasswordLabel = UILabel()
        confirmPasswordLabel.backgroundColor = .clear
        confirmPasswordLabel.text = "Conirm Password*"
        confirmPasswordLabel.font = UIFont(name: "Georgia-Bold", size: self.sizeS)
        confirmPasswordLabel.textColor = .white
        confirmPasswordLabel.numberOfLines = 2
        confirmPasswordLabel.setLineSpacing(lineSpacing: 2.0, lineHeightMultiple: 1.2)
        self.containerView.addSubview(confirmPasswordLabel)
        confirmPasswordLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(passwordTextField.snp.bottom).offset(offset)
            makes.left.equalTo(titleLabel)
            makes.height.equalTo(self.sizeS)
        }
        
        //Add ConfirmPassword TextField
        //        confirmPasswordTextField = UITextField(frame: CGRect(x: fullScreenSize.width*0.08 , y: fullScreenSize.height*0.30, width: fullScreenSize.width*0.7, height: self.sizeB))
        confirmPasswordTextField.placeholder = "   Enter Confirm Password here"
        confirmPasswordTextField.clearButtonMode = .whileEditing
        confirmPasswordTextField.returnKeyType = .done
        confirmPasswordTextField.textColor = .black
        confirmPasswordTextField.backgroundColor = .lightGray
        confirmPasswordTextField.alpha = 0.8
        confirmPasswordTextField.layer.cornerRadius = 12.0
        confirmPasswordTextField.isSecureTextEntry = true
        confirmPasswordTextField.delegate = self
        confirmPasswordTextField.moveLeft()
        confirmPasswordTextField.tag = 200
        self.containerView.addSubview(confirmPasswordTextField)
        confirmPasswordTextField.snp.makeConstraints { (makes) in
            makes.top.equalTo(confirmPasswordLabel.snp.bottom).offset(offset)
            makes.left.equalTo(titleLabel)
            makes.width.equalTo(textFieldWidth2)
            makes.height.equalTo(self.sizeB)
        }
        
        //Add email Label
        let emailLabel = UILabel(frame: CGRect(x: fullScreenSize.width*0.08 , y: fullScreenSize.height*0.35, width: fullScreenSize.width, height: self.sizeS))
        emailLabel.backgroundColor = .clear
        emailLabel.text = "E-mail"
        emailLabel.font = UIFont(name: "Georgia-Bold", size: self.sizeS)
        emailLabel.textColor = .white
        emailLabel.numberOfLines = 2
        emailLabel.setLineSpacing(lineSpacing: 2.0, lineHeightMultiple: 1.2)
        self.containerView.addSubview(emailLabel)
        emailLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(confirmPasswordTextField.snp.bottom).offset(offset)
            makes.left.equalTo(titleLabel)
            makes.height.equalTo(self.sizeS)
        }
        
        //Add email TextField
        emailTextField = UITextField(frame: CGRect(x: fullScreenSize.width*0.08 , y: fullScreenSize.height*0.39, width: textFieldWidth2, height: self.sizeB))
        emailTextField.placeholder = "   Enter E-mail here"
        emailTextField.clearButtonMode = .whileEditing
        emailTextField.returnKeyType = .done
        emailTextField.textColor = .black
        emailTextField.backgroundColor = .lightGray
        emailTextField.alpha = 0.8
        emailTextField.layer.cornerRadius = 12.0
        emailTextField.delegate = self
        emailTextField.moveLeft()
        emailTextField.tag = 200
        self.containerView.addSubview(emailTextField)
        emailTextField.snp.makeConstraints { (makes) in
            makes.top.equalTo(emailLabel.snp.bottom).offset(offset)
            makes.left.equalTo(titleLabel)
            makes.width.equalTo(textFieldWidth2)
            makes.height.equalTo(self.sizeB)
        }
        
        //Add Birthday Label
        let birthdayLabel = UILabel()
        birthdayLabel.backgroundColor = .clear
        birthdayLabel.text = "Birthday"
        birthdayLabel.font = UIFont(name: "Georgia-Bold", size: self.sizeS)
        birthdayLabel.textColor = .white
        birthdayLabel.numberOfLines = 2
        birthdayLabel.setLineSpacing(lineSpacing: 2.0, lineHeightMultiple: 1.2)
        self.containerView.addSubview(birthdayLabel)
        birthdayLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(emailTextField.snp.bottom).offset(offset)
            makes.left.equalTo(titleLabel)
//            makes.height.equalTo(self.sizeS)
        }
        
        //Add Birthday TextField
        //        birthdayTextField = UITextField(frame: CGRect(x: fullScreenSize.width*0.08 , y: fullScreenSize.height*0.48, width: fullScreenSize.width*0.7, height: self.sizeB))
        birthdayTextField.placeholder = "   Pick a Day"
        birthdayTextField.clearButtonMode = .never
        birthdayTextField.returnKeyType = .done
        birthdayTextField.textColor = .black
        birthdayTextField.backgroundColor = .lightGray
        birthdayTextField.alpha = 0.8
        birthdayTextField.layer.cornerRadius = 12.0
        birthdayTextField.moveLeft()
        birthdayTextField.tag = 200
        birthdayTextField.delegate = self
        datePicker.datePickerMode = .date
        //        datePicker.addTarget(self, action: #selector(self.dateChanged(datePicker:)), for: .valueChanged)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.viewTapped(gestureRecognizer:)))
        view.addGestureRecognizer(tapGesture)
        containerView.addGestureRecognizer(tapGesture)
        
        birthdayTextField.inputView = datePicker
        self.containerView.addSubview(birthdayTextField)
        birthdayTextField.snp.makeConstraints { (makes) in
            makes.top.equalTo(birthdayLabel.snp.bottom).offset(offset)
            makes.left.equalTo(titleLabel)
            makes.width.equalTo(textFieldWidth2)
            makes.height.equalTo(self.sizeB)
        }
        
    }
    /** UIDatePicker 設定 */
    @objc func dateChanged(datePicker: UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        birthdayTextField.text = dateFormatter.string(from: datePicker.date)
        //        view.endEditing(true)
    }
    /** 點擊空白處的時候的處理，主要是 end Editing，另外就是判斷生日是否為一個小時內 -> error */
    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer){
        let dateFormatter = DateFormatter()
        //        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
//        print("@viewTapped birthdayTextField.text:\(birthdayTextField.text)")
        //0113
        if birthdayTextField.text == nil{
            if(datePicker.date.timeIntervalSinceNow > -3600){
                //            print("YY")
                DispatchQueue.main.async {
                    //                self.alertMsg(msg: "Check BirthDay Date")
                    self.birthdayTextField.text = ""
                }
            }else{
                birthdayTextField.text = dateFormatter.string(from: datePicker.date)
            }
        }
        view.endEditing(true)
        //0109
        containerView.endEditing(true)
    }
    /**
     set Name to gender Objs
     1. Add First name Label & textfield
     2. Add Last name Label & textfield
     3. Add Mobild nums Label & textfield
     4. Add Id Label & textfield
     5. Add gender Label & textfield
     */
    func setNameToGender(){
        //Add FirstName Label
        let firstNameLabel = UILabel()
        firstNameLabel.backgroundColor = .clear
        firstNameLabel.text = "First Name"
        firstNameLabel.font = UIFont(name: "Georgia-Bold", size: self.sizeS)
        firstNameLabel.textColor = .white
        firstNameLabel.numberOfLines = 2
        firstNameLabel.setLineSpacing(lineSpacing: 2.0, lineHeightMultiple: 1.2)
        self.containerView.addSubview(firstNameLabel)
        firstNameLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(birthdayTextField.snp.bottom).offset(offset)
            makes.left.equalTo(birthdayTextField)
            makes.height.equalTo(self.sizeS)
        }
        
        //Add FirstName TextField
        //        firstNameTextField = UITextField(frame: CGRect(x: fullScreenSize.width*0.08 , y: fullScreenSize.height*0.57, width: fullScreenSize.width*0.4, height: self.sizeB))
        firstNameTextField.placeholder = "   First Name"
        firstNameTextField.clearButtonMode = .whileEditing
        firstNameTextField.returnKeyType = .done
        firstNameTextField.textColor = .black
        firstNameTextField.backgroundColor = .lightGray
        firstNameTextField.alpha = 0.8
        firstNameTextField.layer.cornerRadius = 12.0
        firstNameTextField.delegate = self
        firstNameTextField.moveLeft()
        firstNameTextField.tag = 200
        self.containerView.addSubview(firstNameTextField)
        firstNameTextField.snp.makeConstraints { (makes) in
            makes.top.equalTo(firstNameLabel.snp.bottom).offset(offset)
            makes.left.equalTo(birthdayTextField)
            makes.width.equalTo(fullScreenSize.width*0.4)
            makes.height.equalTo(self.sizeB)
        }
        
        //Add LastName Label
        let lastNameLabel = UILabel()
        lastNameLabel.backgroundColor = .clear
        lastNameLabel.text = "Last Name"
        lastNameLabel.font = UIFont(name: "Georgia-Bold", size: self.sizeS)
        lastNameLabel.textColor = .white
        lastNameLabel.numberOfLines = 2
        lastNameLabel.setLineSpacing(lineSpacing: 2.0, lineHeightMultiple: 1.2)
        self.containerView.addSubview(lastNameLabel)
        lastNameLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(birthdayTextField.snp.bottom).offset(offset)
            makes.left.equalTo(fullScreenSize.width*0.56)
            makes.height.equalTo(self.sizeS)
        }
        
        //Add LastName TextField
        //        lastNameTextField = UITextField(frame: CGRect(x: fullScreenSize.width*0.56 , y: fullScreenSize.height*0.57, width: fullScreenSize.width*0.4, height: self.sizeB))
        lastNameTextField.placeholder = "   Last Name"
        lastNameTextField.clearButtonMode = .whileEditing
        lastNameTextField.returnKeyType = .done
        lastNameTextField.textColor = .black
        lastNameTextField.backgroundColor = .lightGray
        lastNameTextField.alpha = 0.8
        lastNameTextField.layer.cornerRadius = 12.0
        lastNameTextField.delegate = self
        lastNameTextField.moveLeft()
        lastNameTextField.tag = 200
        self.containerView.addSubview(lastNameTextField)
        lastNameTextField.snp.makeConstraints { (makes) in
            makes.top.equalTo(firstNameLabel.snp.bottom).offset(offset)
            makes.left.equalTo(fullScreenSize.width*0.56)
            makes.width.equalTo(fullScreenSize.width*0.4)
            makes.height.equalTo(self.sizeB)
        }
        
        //Add MobileNum Label
        let mobileNumLabel = UILabel()
        mobileNumLabel.backgroundColor = .clear
        mobileNumLabel.text = "Mobile Number"
        mobileNumLabel.font = UIFont(name: "Georgia-Bold", size: self.sizeS)
        mobileNumLabel.textColor = .white
        mobileNumLabel.numberOfLines = 2
        mobileNumLabel.setLineSpacing(lineSpacing: 2.0, lineHeightMultiple: 1.2)
        self.containerView.addSubview(mobileNumLabel)
        mobileNumLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(firstNameTextField.snp.bottom).offset(offset)
            makes.left.equalTo(birthdayTextField)
            makes.height.equalTo(self.sizeS)
        }
        
        //Add MobileNum TextField (county num, select by countryNumPickerView)
        //        mobileNumTextField = UITextField(frame: CGRect(x: fullScreenSize.width*0.08 , y: fullScreenSize.height*0.66, width: fullScreenSize.width*0.16, height: self.sizeB))
        //        mobileNumTextField.placeholder = "   Mobile Number"
        //        mobileNumTextField.placeholder = "   +886_9xxxxxxxx"
        mobileNumTextField.text = countryNum[0]
        mobileNumTextField.clearButtonMode = .never
        mobileNumTextField.returnKeyType = .done
        mobileNumTextField.textColor = .black
        mobileNumTextField.backgroundColor = .lightGray
        mobileNumTextField.alpha = 0.8
        mobileNumTextField.layer.cornerRadius = 12.0
        mobileNumTextField.delegate = self
        mobileNumTextField.moveLeft()
        mobileNumTextField.tag = 200
        
        // 設定 UIPickerView 的 delegate 及 dataSource
        countryNumPickerView.delegate = self
        countryNumPickerView.dataSource = self
        
        //        print(countryNumPickerView.frame)
        countryNumPickerView.frame = CGRect(x: 0, y: 0, width: fullScreenSize.width, height: fullScreenSize.height*0.15)
        mobileNumTextField.inputView = countryNumPickerView
        self.containerView.addSubview(mobileNumTextField)
        mobileNumTextField.snp.makeConstraints { (makes) in
            makes.top.equalTo(mobileNumLabel.snp.bottom).offset(offset)
            makes.left.equalTo(birthdayTextField)
            makes.width.equalTo(fullScreenSize.width*0.16)
            makes.height.equalTo(self.sizeB)
        }
        
        //Add MobileNum TextField (後面九碼）
        //        mobileNumTextField2 = UITextField(frame: CGRect(x: fullScreenSize.width*0.32 , y: fullScreenSize.height*0.66, width: fullScreenSize.width*0.38, height: self.sizeB))
        //        mobileNumTextField.placeholder = "   Mobile Number"
        mobileNumTextField2.placeholder = "   9xxxxxxxx"
        mobileNumTextField2.clearButtonMode = .whileEditing
        mobileNumTextField2.returnKeyType = .done
        mobileNumTextField2.textColor = .black
        mobileNumTextField2.backgroundColor = .lightGray
        mobileNumTextField2.alpha = 0.8
        mobileNumTextField2.layer.cornerRadius = 12.0
        mobileNumTextField2.delegate = self
        mobileNumTextField2.moveLeft()
        mobileNumTextField2.tag = 200
        self.containerView.addSubview(mobileNumTextField2)
        mobileNumTextField2.snp.makeConstraints { (makes) in
            makes.top.equalTo(mobileNumLabel.snp.bottom).offset(offset)
            makes.left.equalTo(fullScreenSize.width*0.32)
            makes.width.equalTo(fullScreenSize.width*0.38)
            makes.height.equalTo(self.sizeB)
        }
        
        //Add Gender Label
        //        let genderLabel = UILabel(frame: CGRect(x: fullScreenSize.width*0.08 , y: fullScreenSize.height*0.80, width: fullScreenSize.width, height: self.sizeS))
        genderLabel.backgroundColor = .clear
        genderLabel.text = "Gender"
        genderLabel.font = UIFont(name: "Georgia-Bold", size: self.sizeS)
        genderLabel.textColor = .white
        genderLabel.numberOfLines = 2
        genderLabel.setLineSpacing(lineSpacing: 2.0, lineHeightMultiple: 1.2)
        self.containerView.addSubview(genderLabel)
        genderLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(mobileNumTextField.snp.bottom).offset(offset)
            makes.left.equalTo(mobileNumTextField)
            makes.height.equalTo(self.sizeS)
        }
        
        //Add Gender TextField
        //        genderTextField = UITextField(frame: CGRect(x: fullScreenSize.width*0.08 , y: fullScreenSize.height*0.84, width: fullScreenSize.width*0.7, height: self.sizeB))
        //        genderTextField = UITextField(frame: CGRect(x: fullScreenSize.width*0.08 , y: fullScreenSize.height*0.75, width: fullScreenSize.width*0.7, height: self.sizeB))
        
        
        //  genderTextField.placeholder = "   Click to choise Gender"
        genderTextField.text = genders[0]
        genderTextField.clearButtonMode = .never
        genderTextField.returnKeyType = .done
        genderTextField.textColor = .black
        genderTextField.backgroundColor = .lightGray
        genderTextField.alpha = 0.8
        genderTextField.layer.cornerRadius = 12.0
        genderTextField.moveLeft()
        genderTextField.tag = 200
        genderTextField.delegate = self
        // 設定 UIPickerView 的 delegate 及 dataSource
        genderPickerView.delegate = self
        genderPickerView.dataSource = self
        //        print(genderPickerView.frame)
        genderPickerView.frame = CGRect(x: 0, y: 0, width: fullScreenSize.width, height: fullScreenSize.height*0.15)
        genderTextField.inputView = genderPickerView
        self.containerView.addSubview(genderTextField)
        genderTextField.snp.makeConstraints { (makes) in
            makes.top.equalTo(genderLabel.snp.bottom).offset(offset)
            makes.left.equalTo(mobileNumTextField)
            makes.width.equalTo(textFieldWidth2)
            makes.height.equalTo(self.sizeB)
        }
    }
    /** pickview 有多少 Components */
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    /** 返回陣列 genders 的成員數量 */
    func pickerView(
        _ pickerView: UIPickerView,
        numberOfRowsInComponent component: Int) -> Int {
        if pickerView == genderPickerView{
            return genders.count
        }else{
            return countryNum.count
        }
    }
    /** 返回陣列 genders 成員的標題 */
    func pickerView(_ pickerView: UIPickerView,
                    titleForRow row: Int,
                    forComponent component: Int) -> String? {
        if pickerView == genderPickerView{
            return genders[row]
        }else{
            return countryNum[row]
        }
    }
    /** 將 UITextField 的值更新為陣列 genders 的第 row 項資料 */
    func pickerView(_ pickerView: UIPickerView,
                    didSelectRow row: Int, inComponent component: Int) {
        if pickerView == genderPickerView{
            genderTextField.text = genders[row]
        }else{
            mobileNumTextField.text = countryNum[row]
        }
    }
    /**
     set 3 button
     1. Submit Button - clickLeftButton()
     2. Reset Button - clickMidButton()
     3. Back Button - clickRightButton()
     - SeeAlso: `clickLeftButton(...)` , `clickMidButton(...)` , `clickRightButton(...)`
     */
    func setButtons(){
        //Add Left Button
        //let leftButton = UIButton(frame: CGRect(x: fullScreenSize.width*0.05, y: fullScreenSize.height*0.91, width: fullScreenSize.width*0.26, height: fullScreenSize.height*0.06))
        let leftButton = UIButton()
        leftButton.backgroundColor = UIColor(red: 253/255.0, green: 198/255.0, blue: 46/255.0, alpha: 1.0)
        leftButton.layer.cornerRadius = 18.0
        leftButton.layer.shadowColor = UIColor.white.cgColor
        leftButton.layer.shadowRadius = 3
        leftButton.layer.shadowOpacity = 0.8
        leftButton.setTitle("Modify", for: [])
        leftButton.titleLabel?.font = UIFont(name: "Georgia-Bold", size: 22)
        leftButton.titleLabel?.textColor = .white
        leftButton.isEnabled = true
        leftButton.addTarget(self,action: #selector(self.clickLeftButton),for: .touchUpInside)
        self.containerView.addSubview(leftButton)
        leftButton.snp.makeConstraints { (makes) in
            makes.top.equalTo(genderTextField.snp.bottom).offset(offset*4.0)
            makes.left.equalTo(genderTextField)
            makes.right.equalToSuperview().offset(-15)
            makes.bottom.equalToSuperview().offset(-offset7)
            makes.height.equalTo(sizeB*1.3)
        }
        
        //        //Add Mid Button
        //        let midButton = UIButton(frame: CGRect(x: fullScreenSize.width*0.37, y: fullScreenSize.height*0.91, width: fullScreenSize.width*0.26, height: fullScreenSize.height*0.06))
        //        midButton.backgroundColor = UIColor(red: 192/255.0, green: 192/255.0, blue: 192/255.0, alpha: 1.0)
        //        midButton.layer.cornerRadius = 18.0
        //        midButton.layer.shadowColor = UIColor.white.cgColor
        //        midButton.layer.shadowRadius = 3
        //        midButton.layer.shadowOpacity = 0.8
        //        midButton.setTitle("Reset", for: [])
        //        midButton.titleLabel?.font = UIFont(name: "Georgia-Bold", size: 22)
        //        midButton.titleLabel?.textColor = .white
        //        midButton.isEnabled = true
        //        midButton.addTarget(self,action: #selector(self.clickMidButton),for: .touchUpInside)
        //        self.view.addSubview(midButton)
        
        
        //            //Add Right Button
        //            let rightButton = UIButton()
        //            rightButton.backgroundColor = UIColor(red: 121/255.0, green: 185/255.0, blue: 73/255.0, alpha: 1.0)
        //            rightButton.layer.cornerRadius = 18.0
        //            rightButton.layer.shadowColor = UIColor.white.cgColor
        //            rightButton.layer.shadowRadius = 3
        //            rightButton.layer.shadowOpacity = 0.8
        //            rightButton.setTitle("Back", for: [])
        //            rightButton.titleLabel?.font = UIFont(name: "Georgia-Bold", size: 22)
        //            //        rightButton.titleLabel?.numberOfLines = 2
        //            //        rightButton.titleLabel?.setLineSpacing(lineSpacing: 2.0, lineHeightMultiple: 0.8)
        //            rightButton.titleLabel?.textColor = .white
        //            rightButton.titleLabel?.textAlignment = .center
        //            rightButton.isEnabled = true
        //            rightButton.addTarget(self,action: #selector(self.clickRightButton),for: .touchUpInside)
        //
        //            self.containerView.addSubview(rightButton)
        //            rightButton.snp.makeConstraints { (makes) in
        //                makes.top.equalTo(leftButton.snp.bottom).offset(offset)
        //                makes.left.equalTo(genderTextField)
        //                makes.right.equalToSuperview().offset(-15)
        //                makes.bottom.equalToSuperview().offset(-30)
        //            }
        
    }
    /**
     check all field 的格式，若是正確則呼叫 alertMsgForSendRegData (...)
     - SeeAlso: `alertMsgForSendRegData(...)`
     */
    @objc func clickLeftButton(){
        //0120此API尚未完成
        alertMsg(msg: "此API尚未完成!")
        return
        
        //Check Data
        //Check Account
        let accInput: String = accountTextField.text!
        if accInput.count < 4 || accInput.count > 12{
            alertMsg(msg: "Account must have 4 to 12 characters !")
            return
        }else if !isValidAcc(testStr: accInput){
            alertMsg(msg: "Account must have English and digital combination.")
            return
        }
        
        //Check Password
        let passInput: String = passwordTextField.text!
        if passInput.count < 6 || passInput.count > 12{
            alertMsg(msg: "Password must have 6 to 12 characters !")
            return
        }else if !isValidAcc(testStr: passInput){
            alertMsg(msg: "Password must have English and digital combination.")
            return
        }
            //Check ConfirmPasswordTextField
        else if passInput != confirmPasswordTextField.text!{
            alertMsg(msg: "Passwords are different")
            return
        }
        
        //Check E-mail
        let emailInput: String = emailTextField.text!
        if !isValidEmail(testStr: emailInput){
            alertMsg(msg: "The e-mail format is incorrect.")
            return
        }
        
        //Get Input Birthday
        //        let dateInputFormatter = DateFormatter()
        ////        dateInputFormatter.dateFormat = "yyyyMMdd"
        //        dateInputFormatter.dateFormat = "yyyy-MM-dd"
        //        let birthdayInput: String = dateInputFormatter.string(from: datePicker.date)
        let birthdayInput: String = birthdayTextField.text!
        if birthdayInput == ""{
            alertMsg(msg: "Birthday is required")
        }
        
        //Check Name
        let fNameInput: String = firstNameTextField.text!
        let lNameInput: String = lastNameTextField.text!
        if fNameInput.count == 0 || lNameInput.count == 0 {
            alertMsg(msg: "Name can't be null.")
            return
        }else if !isValidName(testStr: fNameInput) || !isValidName(testStr: lNameInput){
            alertMsg(msg: "Name format is incorrect.")
            return
        }
        
        //Check phone nums
        
        let countyyNum: String = mobileNumTextField.text!
        let lastNum: String = mobileNumTextField2.text!
        //        let mobileNumInput: String = "+886_981234567"
        if !isValidPhone(testStr: countyyNum){
            //            alertMsg(msg: "Mobile Number format is incorrect.")
            alertMsg(msg: "Mobile Number format is incorrect.(+886_9xxxxxxxx)")
            return
        }
        if !isValidPhone2(testStr: lastNum){
            alertMsg(msg: "Mobile Number format is incorrect.(+countryNum_xxxxxxxxx)")
            return
        }
        
        let mobileNumInput: String = "\(countyyNum)_\(lastNum)"
        
        
        //Check gender
        let genderInput: String = genderTextField.text!
        //        if genderInput.count == 0{
        //            alertMsg(msg: "Select Gender.")
        //            return
        //        }else{
        //            print(genderInput)
        //        }
        
        
        let idInput: String = idTextField.text!
        //select Gender has some problem
        
        
        //for check
        //        alertMsg(msg: birthdayInput)
        
        //No return -> data 格式正確
        alertMsgForSendRegData(accInput: accInput,passInput: passInput,emailInput: emailInput,birthdayInput: birthdayInput,fNameInput: fNameInput,lNameInput: lNameInput,mobileNumInput: mobileNumInput,genderInput: genderInput,idInput: idInput,msg: "* Please be sure to enter the correct information, in order to provide it when forgetting the account or password.")
        
    }
    /** reset all field */
    @objc func clickMidButton(){
        //Reset
        accountTextField.text = ""
        passwordTextField.text = ""
        confirmPasswordTextField.text = ""
        emailTextField.text = ""
        birthdayTextField.text = ""
        firstNameTextField.text = ""
        lastNameTextField.text = ""
        mobileNumTextField.text = countryNum[0]
        mobileNumTextField2.text = ""
        //        idTextField.text = ""
        genderTextField.text = genders[0]
    }
    /** 返回登入頁面 */
    @objc func clickRightButton(){
        print("clickRightButton")
        self.dismiss(animated: false, completion: nil)
    }
    /** 檢查Account格式是否正確 */
    func isValidAcc(testStr:String) -> Bool {
        let accRegEx = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{4,12}$"
        
        let accTest = NSPredicate(format:"SELF MATCHES %@", accRegEx)
        return accTest.evaluate(with: testStr)
    }
    /** 檢查E-mail格式是否正確 */
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    /** 檢查Name格式是否正確 */
    func isValidName(testStr:String) -> Bool {
        let emailRegEx = "^[A-Za-z]+"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    /** 檢查Phone格式(國碼）是否正確 */
    func isValidPhone(testStr:String) -> Bool {
        //        let emailRegEx = "^09[0-9]{8}$"
        //        let emailRegEx = "^[+]886_9[0-9]{8}$"
        let emailRegEx = "^[+][0-9]{2,3}$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    /** 檢查Phone格式(後九碼）是否正確 */
    func isValidPhone2(testStr:String) -> Bool {
        //        let emailRegEx = "^09[0-9]{8}$"
        let emailRegEx = "[0-9]{9}$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    /**
     提示框，確認則呼叫 signUp() 送資料給 server，取消就繼續輸入
     - SeeAlso: `signUp(...)`
     */
    func alertMsgForSendRegData(accInput: String,passInput: String,emailInput: String,birthdayInput: String,fNameInput: String,lNameInput: String,mobileNumInput: String,genderInput: String,idInput: String,msg: String) {
        //        print("birthdayInput in alertMsgForSendRegData : \(birthdayInput)")
        
        let alertController = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        
        //        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        //        alertController.addAction(defaultAction)
        
        // 建立[Yes]按鈕
        let okAction = UIAlertAction(
            title: "Yes, submit",
            style: .destructive,    /* 以紅色顯示按鈕 提醒用戶 */
            handler: {
                (action: UIAlertAction!) -> Void in
                //print("按「Yes」，執行clouser")}
                self.signUp(accInput: accInput, passInput: passInput, emailInput: emailInput, birthdayInput: birthdayInput, fNameInput: fNameInput, lNameInput: lNameInput, mobileNumInput: mobileNumInput, genderInput: genderInput, idInput: idInput)}
        )
        alertController.addAction(okAction)
        
        /*
         建立[取消]按鈕
         注意 style .cancel 的按鈕在 「雙按鈕」提示框時，依造Apple的習慣 都會固定在「左方」
         (windows系統是習慣 Cancel按鈕在右方)
         即使這段程式碼是在 後方
         */
        let cancelAction = UIAlertAction(
            title: "No, recheck",
            style: .cancel,
            handler: nil)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    /**
     傳送 Sign Up 資料給 server，根據 server 回傳資料決定動作
     1. error -> alert error msg -> keep check 輸入資料
     2. success -> 提示去 e-mail 收信確認 -> 跳回登入頁面
     */
    func signUp(accInput: String,passInput: String,emailInput: String,birthdayInput: String,fNameInput: String,lNameInput: String,mobileNumInput: String,genderInput: String,idInput: String){
        
        //        self.actView.isHidden = false
        //        print("birthdayInput in signUp : \(birthdayInput)")
        
        var sex: Int = 9
        if genderInput == "Male"{
            sex = 1
        }else if genderInput == "Female"{
            sex = 2
        }else{
            sex = 0
        }
        
        let params = [
            "user_name": accInput,
            "password": passInput,
            "first_name": fNameInput,
            "last_name": lNameInput,
            "sex": sex,
            "email": emailInput,
            "birthday": birthdayInput,
            
            "phone": mobileNumInput,
            "civil_id": idInput
            ] as [String : Any]
        //        params["sex"] = sex
        
        print(params)
        
        let actView = UIView()
        self.setActView(actView: actView)
        
        AF.request("\(serverUrlNew)account/createNewAccount", method: .post, parameters: params)
            .responseJSON { (response) in
                actView.removeFromSuperview()
                if response.value != nil {
                    print(response.value!)
                }else{
                    print("createNewAccount Error")
                }
        }
        
//        Alamofire.request("\(serverUrlNew)account/createNewAccount", method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON { (response) in
//            if response.result.isSuccess {
//                print(response.result.value!)
//            }else{
//                print("Error")
//            }
//        }
        
    }
    
    /** 讓輸入 textfield 的鍵盤，按下 Done 的時候，可以縮起來 */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == mobileNumTextField || textField == genderTextField || textField == birthdayTextField {
            return false
        }else{
            return true
        }
    }
    /**
     側邊欄關閉後 就把contentView給移除掉 以便可以觸控此頁面元件
     - Date: 10/20 Yang
     */
    @objc func removeSubview(){
        if let viewWithTag = self.view.viewWithTag(1000) {
            viewWithTag.removeFromSuperview()
        }else{
            print("No!")
        }
    }
    /**
     移除回彈的View
     - Date: 10/20 Yang
     */
    @objc func updateContentViewNoti(noti: Notification) {
        removeSubview()
    }
    
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var navButton: UIButton!
    
    @IBAction func menuClick(_ sender: Any) {
        resignTextfield()
        navigationController?.view.menu()
        ButtonDelegate?.clickLeft()
        if navigationController?.view.superview?.frame.origin.x != 0 {
            showContentView2()
        }else{
            removeSubview()
        }
    }
    
    @IBAction func userClick(_ sender: Any) {
        resignTextfield()
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        navigationController?.view.menu2()
        ButtonDelegate?.clickRight()
        if navigationController?.view.superview?.frame.origin.x == -navigationOriginX{
            showContentView2()
        }else{
            removeSubview()
        }
    }
    
    @IBAction func handleEdgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
        print("EdgePan \(navigationController?.view.superview?.frame.origin.x as Any)")
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger(location: location, state: sender.state)
        if beganX2 > 0 {
            ButtonDelegate?.clickLeft()
            showContentView2()
        }else{
            contentView.removeFromSuperview()
        }
    }

    @IBAction func handleEdgePanGesture2(_ sender: UIScreenEdgePanGestureRecognizer) {
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger2(location: location, state: sender.state)
        if beganX2 < 0{
            print("UserDataPan")
            ButtonDelegate?.clickRight()
            showContentView2()
        }else{
            contentView.removeFromSuperview()
        }
    }
    
    @IBAction func handleTapGesture(_ sender: Any) {
        
    }
}
