//
//  CatalogMenuViewController.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2019/6/10.
//  Copyright © 2019 蒼月喵. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MarqueeLabel

class CatalogMenuViewController: UIViewController, UIScrollViewDelegate {
    weak var ButtonDelegate: ButtonDelegate?
    /** 頁面標題 */
    @IBOutlet weak var pageTitle: UILabel!
    
    /** 點擊屏幕用 */
    let contentView = UIView()
    
    //0226
    var catalogProuctNum: Int = 10
    
    /** LeftMenuViewController 傳值過來的 parentData */
    var parentProductType: ProductType = ProductType()
    
    /** 子分類 read from server (對應測試用的forTestCatalogs) */
    var childCatalogNew: [ProductTCCategory] = []
    
    
    var tmpPackDetailNew: ProductCDetail = ProductCDetail()
    
    /** pageScrol 中間 items 的間距 */
    let pageItemSpacing: CGFloat = 5
    /** pageScrol 標題的高度 */
    let pageScrollTittleHeight: CGFloat = 40
    /** pageScrol 影片縮圖的高度 */
    let pageScrollImageHeight: CGFloat = 192
    /** pageScrol 影片縮圖的高度 */
    let pageScrollImageWidth: CGFloat = 144
    /** pageScrol 片名的高度 */
    let pageScrollPackNameHeight: CGFloat = 20
    
    /** pageScrol 的高度 */
    lazy var pageScrollHeight: CGFloat = {
        
        var height = CGFloat()
        height = pageItemSpacing*CGFloat(4) + pageScrollTittleHeight + pageScrollImageHeight + pageScrollPackNameHeight
        
        return height
    }()
    
    /** pageScroll 間的間距 */
    let pageScrollSpacing: CGFloat = 30
    
    /** scrollView 裝頁面內容（標題除外）*/
    var scrollView = UIScrollView()
    /** ContainerView for scrollView */
    var containerView = UIView()
    
    /** 放 pageView 圖片輪轉用 */
    let pageScrollView = UIScrollView()
    /** ContainerView for pageScrollView */
    var pageContainerView = UIView()
    // TODO: 上方 pageView 裡面的 imageView 先用寫死的，之後再從 server 撈
    let page1 = UIImageView(image: UIImage(named: "page1"))
    let page2 = UIImageView(image: UIImage(named: "page2"))
    let page3 = UIImageView(image: UIImage(named: "page3"))
    let page4 = UIImageView(image: UIImage(named: "page4"))
    let pageControl = UIPageControl()
    
//    /** 環形進度圈，用來表示讀取資料中 */
//    var activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
//    /** 放著環形進度圈的View，平時為 Hide 狀態，讀取時顯示出來在最上方 */
//    let actView = UIView()
    //0115
    var timer: Timer?
    
    
    override func viewDidLoad() {
        if self.timer != nil {
//            print("timer != nil")
            self.timer?.invalidate()
        }
        self.timer = Timer.scheduledTimer(timeInterval: 8, target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
        
        //        print("self.parentProductType : \(self.parentProductType)")
        
        //        print("self.scrollView.subviews.count:\(self.scrollView.subviews.count)")
        
        for subView in self.scrollView.subviews{
            if subView.tag != 100{
                subView.removeFromSuperview()
            }
        }
        self.setPageTitle()
        self.setBackground()
        
//        activityIndicator.hidesWhenStopped = true;
//        activityIndicator.style  = UIActivityIndicatorView.Style.whiteLarge;
//        activityIndicator.center = view.center;
//
//        //        actView.frame = CGRect(x: 0, y: 0, width: fullScreenSize.width, height: fullScreenSize.height)
//        actView.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.4)
//
//        view.addSubview(actView)
//
//        actView.snp.makeConstraints { (makes) in
//            makes.edges.equalToSuperview()
//        }
//        actView.addSubview(activityIndicator)
//        activityIndicator.startAnimating()
//
//        actView.isHidden = false
//        let actView = UIView()
//        self.setActView(actView: actView)
        self.setActView(openOrClose: true)
        // 0724
        getChildCatalogNew(){
            //            print("getChildCatalogNew done")
            //            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
            
//            self.actView.isHidden = true
//            actView.removeFromSuperview()
            self.setActView(openOrClose: false)
            self.setPageView()
            
            self.setCatalogsNew(catalogs: self.childCatalogNew)
            //            }
        }
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTapGesture(_:)))
        let pan: UIScreenEdgePanGestureRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(self.handleEdgePanGesture(_:)))
        let pan2: UIScreenEdgePanGestureRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(self.handleEdgePanGesture2(_:)))
        view.addGestureRecognizer(tap)
        view.addGestureRecognizer(pan)
        view.addGestureRecognizer(pan2)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let name = NSNotification.Name("removeContentView")
        NotificationCenter.default.addObserver(self, selector:
        #selector(updateContentViewNoti(noti:)), name: name, object: nil)
        //        if self.timer != nil {
        //            print("timer != nil")
        //            self.timer?.invalidate()
        //        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        // 將timer的執行緒停止
//        print("viewDidDisappear @ CatalogMenuViewController")
        if self.timer != nil {
//            print("timer != nil")
            self.timer?.invalidate()
        }
    }
    /**
     點擊屏幕回彈用
     - Date: 10/20 Yang
     */
    @objc func showContentView(){
        contentView.tag = 1000
        contentView.isUserInteractionEnabled = true
        contentView.backgroundColor = .clear
        view.addSubview(contentView)
        contentView.snp.makeConstraints { (makes) in
            makes.edges.equalTo(view)
        }
    }
    /**
     側邊欄關閉後 就把contentView給移除掉 以便可以觸控此頁面元件
     - Date: 10/20 Yang
     */
    @objc func removeSubview(){
        if let viewWithTag = self.view.viewWithTag(1000) {
            viewWithTag.removeFromSuperview()
        }else{
            print("No!")
        }
    }
    /**
     移除回彈的View
     - Date: 10/20 Yang
     */
    @objc func updateContentViewNoti(noti: Notification) {
        removeSubview()
    }
    
    func setPageTitle(){
        //        pageTitle.text = parentCatalog.name
        //        print("setPageTitle")
        pageTitle.text = parentProductType.name
    }
    
    func setBackground(){
        pageTitle.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(10)
            make.centerX.equalToSuperview()
        }
        
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        // TODO: Color for test
        scrollView.backgroundColor = .black
        scrollView.bounces = false      // 滑動超過單張圖範圍時是否使用彈回效果
        //        scrollView.isPagingEnabled = true    // 以一頁為單位滑動
        //        scrollView.delegate = self
        
        view.addSubview(scrollView)
        scrollView.snp.makeConstraints { (makes) in
            makes.bottom.left.right.equalToSuperview()
            makes.top.equalTo(pageTitle.snp.bottom).offset(10)
        }
        
        scrollView.addSubview(containerView)
        containerView.tag = 100
        
        containerView.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
            makes.width.equalTo(fullScreenSize.width)
        }
        
    }
    
    func setPageView(){
//        print("setPageView")
        super.viewDidLoad()
        //        self.timer = Timer.scheduledTimer(timeInterval: 8, target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
        
        pageScrollView.showsHorizontalScrollIndicator = false
        pageScrollView.showsVerticalScrollIndicator = false
        
        pageScrollView.bounces = false      // 滑動超過單張圖範圍時是否使用彈回效果
        pageScrollView.isPagingEnabled = true    // 以一頁為單位滑動
        pageScrollView.delegate = self
        pageScrollView.tag = 100
        
        scrollView.addSubview(pageScrollView)
        pageScrollView.snp.makeConstraints { (makes) in
            //035
            //            makes.top.left.right.equalToSuperview()
            makes.top.equalToSuperview()
            makes.width.equalTo(shortSide)
            makes.centerX.equalToSuperview()
            //            makes.top.equalTo(pageTitle.snp.bottom).offset(10)
            makes.height.equalTo(shortSide/16*9)
        }
        
        pageScrollView.addSubview(pageContainerView)
        
        pageContainerView.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
            makes.height.equalTo(shortSide/16*9)
        }
        
        pageContainerView.addSubview(page1)
        
        page1.snp.makeConstraints { (makes) in
            makes.top.left.bottom.equalToSuperview()
            makes.width.equalTo(shortSide)
        }
        
        pageContainerView.addSubview(page2)
        
        page2.snp.makeConstraints { (makes) in
            makes.top.bottom.equalToSuperview()
            makes.left.equalTo(page1.snp.right)
            makes.width.equalTo(shortSide)
        }
        
        pageContainerView.addSubview(page3)
        
        page3.snp.makeConstraints { (makes) in
            makes.top.bottom.equalToSuperview()
            makes.left.equalTo(page2.snp.right)
            makes.width.equalTo(shortSide)
        }
        
        pageContainerView.addSubview(page4)
        
        page4.snp.makeConstraints { (makes) in
            makes.top.bottom.right.equalToSuperview()
            makes.left.equalTo(page3.snp.right)
            makes.width.equalTo(shortSide)
        }
        
        pageControl.numberOfPages = 4
        pageControl.currentPage = 0 // 起始頁面
        pageControl.currentPageIndicatorTintColor = UIColor.black // 目前所在頁數的點點顏色
        pageControl.pageIndicatorTintColor = UIColor.lightGray // 其餘頁數的點點顏色
        pageControl.addTarget(self,action: #selector(self.pageChanged),for: .valueChanged)
        pageControl.tag = 100
        scrollView.addSubview(pageControl)
        pageControl.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.centerY.equalTo(pageScrollView.snp.bottom).offset(-10)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        // Update page number when user slide
        var page = Int(scrollView.contentOffset.x / scrollView.frame.size.width)
//        print("page \(page)")
//        if page == 4{
//            page = 0
//            scrollView.contentOffset.x = 0
//        }
        pageControl.currentPage = page
    }
    
    @IBAction func pageChanged(_ sender: UIPageControl) {
        //        print("pageChanged")
        let currentPageNumber = sender.currentPage
        let width = pageScrollView.frame.size.width
        let offset = CGPoint(x: width * CGFloat(currentPageNumber), y: 0)
        // 讓ScrollView隨著PageControl到達我們要的位置
        pageScrollView.setContentOffset(offset, animated: true)
        print("currentPageNumber \(currentPageNumber)")
    }
    
    @objc func moveToNextPage (){
        let pageWidth:CGFloat = self.pageScrollView.frame.width
        let maxWidth:CGFloat = pageWidth * 4
        let contentOffset:CGFloat = self.pageScrollView.contentOffset.x
        var slideToX = contentOffset + pageWidth
        if  contentOffset + pageWidth == maxWidth {
            slideToX = 0
            //            pageControl.currentPage = 0
        }
        //0113
        //        print("@moveToNextPage() pageWidth:\(pageWidth)")
        if pageWidth != 0{
            pageControl.currentPage = Int(slideToX/pageWidth)
        }
        //        pageControl.currentPage = Int(slideToX/pageWidth)
        
        self.pageScrollView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.pageScrollView.frame.height), animated: true)
    }
    
    // New API
    func setCatalogsNew(catalogs: [ProductTCCategory]){
        for index in 0..<catalogs.count {
            let catalogScrollView = UIScrollView()
            let catalogContainerView = UIView()
            
            catalogScrollView.showsHorizontalScrollIndicator = false
            catalogScrollView.showsVerticalScrollIndicator = false
            catalogScrollView.bounces = false      // 滑動超過單張圖範圍時是否使用彈回效果
            catalogScrollView.delegate = self
            catalogScrollView.backgroundColor = .clear
            
            self.scrollView.addSubview(catalogScrollView)
            
            catalogScrollView.snp.makeConstraints { (makes) in
                //0325
                //                makes.left.right.equalToSuperview()
                makes.width.equalTo(shortSide)
                makes.centerX.equalToSuperview()
                
                makes.top.equalTo(self.pageScrollView.snp.bottom).offset(
                    pageScrollSpacing*CGFloat(index+1) + pageScrollHeight*CGFloat(index) )
                
                if index == (catalogs.count - 1) && catalogs.count > 1 {
//                    print("set bottom")
                    makes.bottom.equalToSuperview().offset(-10)
                }
                makes.height.equalTo(pageScrollHeight)
            }
            
            catalogScrollView.addSubview(catalogContainerView)
            
            catalogContainerView.snp.makeConstraints { (makes) in
                makes.edges.equalToSuperview()
                makes.height.equalTo(pageScrollHeight)
            }
            
            let catalogTitle = UILabel()
            catalogTitle.font = UIFont(name: "Helvetica-Bold", size: 25)
            // FIX
            //            catalogTitle.text = self.childCatalog[index].name
            //            catalogTitle.text = self.childCatalogNew[index].name
            catalogTitle.text = catalogs[index].name
            catalogTitle.textColor = .labelTextColor
            
            self.scrollView.addSubview(catalogTitle)
            
            catalogTitle.snp.makeConstraints { (makes) in
                makes.top.equalTo(catalogContainerView.snp.top).offset(self.pageItemSpacing)
                //                makes.left.equalToSuperview().offset(10)
                makes.left.equalTo(pageScrollView).offset(10)
                makes.height.equalTo(self.pageScrollTittleHeight)
            }
            // FIX
            //            if let packNum: Int = self.childCatalogNew[index].productLists.count {
            let packNum: Int = self.childCatalogNew[index].productLists.count
            //            print("In New API self.childCatalogNew[\(index)].productLists.count : \(self.childCatalogNew[index].productLists.count)")
            if packNum != 0{
                // show more 選項
                //                if packNum == 6 {
                if packNum > catalogProuctNum {
                    let moreLabel = UILabel()
                    moreLabel.font = UIFont(name: "Helvetica-Lighr", size: 23)
                    moreLabel.text = "More"
                    moreLabel.textColor = .labelTextColor
                    
                    moreLabel.tappable = true
                    moreLabel.callback = {
                        
                        print("=== Click more Label ===")
                        print("Parent Catalog's name:\(self.parentProductType.name) id:\(self.parentProductType.product_type_id)")
                        // FIX
                        //                        print("Child catalog which need more packs name:\(self.childCatalog[index].name) id:\(self.childCatalog[index].catalog_id)")
                    }
                    
                    catalogContainerView.addSubview(moreLabel)
                    
                    moreLabel.snp.makeConstraints { (makes) in
                        makes.right.equalToSuperview().offset(-self.pageItemSpacing)
                        makes.centerY.equalTo(catalogTitle)
                    }
                }
                
                
                for i in 0..<packNum {
                    // 如果超過 5 個就 break (顯示最多五個的意思)
                    if i == catalogProuctNum {
                        break
                    }
                    
                    let packImgView = UIImageView(image: UIImage(named: "noContentImage"))
                    // FIX
                    packImgView.downloaded(from: catalogs[index].productLists[i].product_image_P.url)
                    
                    
                    // 0722 test print
                    //                    print("In setCatalogsNew when set image, index:\(index), i:\(i)")
                    //                    print("catalogs[\(index)]:\(catalogs[index])")
                    
                    catalogContainerView.addSubview(packImgView)
                    
                    //for test click imageView
                    packImgView.tappable = true
                    packImgView.callback = {
                        //                        let for0204HistoryVCTestString =
                        //                        """
                        //                        for0204HistoryVCTestString
                        //                        catalogs[index].productLists[i].product_id : \(catalogs[index].productLists[i].product_id)
                        //                        index : \(index)
                        //                        i : \(i)
                        //                        """
                        //                        print(for0204HistoryVCTestString)
                        // 0726
//                        self.actView.isHidden = false
//                        let actView = UIView()
//                        self.setActView(actView: actView)
                        self.setActView(openOrClose: true)
                        self.getPackDetailNew(product_id: catalogs[index].productLists[i].product_id,Index: index,i: i) {
//                            actView.removeFromSuperview()
                            self.setActView(openOrClose: false)
//                            print("getPackDetailNewDone")
//                            print("tmpPackDetailNew:\(self.tmpPackDetailNew)")
//                            self.actView.isHidden = true
                            self.ButtonDelegate?.toPackPageNew(packDataNew: self.tmpPackDetailNew)
                        }
                        
                        //                        // TODO: getPack Detail
                        //                        self.getPackDetail(packingId: self.childCatalog[index].childShowPack![i].packingId) {
                        //                            print("Done")
                        //                            self.childCatalog[index].childShowPack![i].packDetail = self.tmpPackDetail
                        //                            self.ButtonDelegate?.toPackPage(packData: self.childCatalog[index].childShowPack![i])
                        //                        }
                    }
                    
                    packImgView.snp.makeConstraints { (makes) in
                        //                        makes.top.equalTo(catalogTitle.snp.bottom).offset(self.pageItemSpacing)
                        makes.top.equalToSuperview().offset(self.pageItemSpacing * 2 + self.pageScrollTittleHeight)
                        //                    makes.bottom.equalToSuperview().offset(-5)
                        
                        makes.left.equalToSuperview().offset(10 + 154 * i)
                        makes.height.equalTo(self.pageScrollImageHeight)
                        makes.width.equalTo(self.pageScrollImageWidth)
                        //                    makes.left.equalToSuperview().offset(10)
                        
                        if i == packNum - 1 || i == (catalogProuctNum - 1) {
//                            print("set right")
                            makes.right.equalToSuperview().offset(-10)
                        }
                    }
                    
                    let packName = MarqueeLabel.init(frame: CGRect(x: 0, y: 20, width: fullScreenSize.width, height: self.pageScrollPackNameHeight), duration: 8.0, fadeLength: 1.0)
                    //                packName.text = "long for test 跑馬燈.....!!!QQQaaa      "
                    // FIX
                    //                    packName.text = self.childCatalog[index].childShowPack![i].name
                    packName.text = self.childCatalogNew[index].productLists[i].product_name
                    packName.textColor = .white
                    if packName.intrinsicContentSize.width > 144 {
                        // FIX
                        //                        packName.text = self.childCatalog[index].childShowPack![i].name + "      "
                        packName.text = self.childCatalogNew[index].productLists[i].product_name + "      "
                    }
                    
                    catalogContainerView.addSubview(packName)
                    
                    packName.snp.makeConstraints { (makes) in
                        //                    makes.left.right.equalTo(testImgView)
                        makes.width.centerX.equalTo(packImgView)
                        makes.height.equalTo(self.pageScrollPackNameHeight)
                        makes.top.equalTo(packImgView.snp.bottom).offset(pageItemSpacing)
                        makes.bottom.equalToSuperview().offset(-pageItemSpacing)
                    }
                }
                
            }else{
                //                    print("\(index) is null")
                // child catalog no pack
                // set a noViedosYet image
                let packImgView = UIImageView(image: UIImage(named: "noViedosYet"))
                catalogContainerView.addSubview(packImgView)
                
                packImgView.snp.makeConstraints { (makes) in
                    makes.top.equalToSuperview().offset(self.pageItemSpacing * 2 + self.pageScrollTittleHeight)
                    makes.left.equalToSuperview().offset(10)
                    makes.height.equalTo(self.pageScrollImageHeight)
                    makes.width.equalTo(self.pageScrollImageWidth)
                }
                
                let packName = MarqueeLabel.init(frame: CGRect(x: 0, y: 20, width: fullScreenSize.width, height: self.pageScrollPackNameHeight), duration: 8.0, fadeLength: 1.0)
                //                packName.text = "long for test 跑馬燈.....!!!QQQaaa      "
                packName.text = "No Viedos Yet"
                packName.textColor = .white
                
                catalogContainerView.addSubview(packName)
                
                packName.snp.makeConstraints { (makes) in
                    //                    makes.left.right.equalTo(testImgView)
                    makes.width.centerX.equalTo(packImgView)
                    makes.height.equalTo(self.pageScrollPackNameHeight)
                    makes.top.equalTo(packImgView.snp.bottom).offset(pageItemSpacing)
                    makes.bottom.equalToSuperview().offset(-pageItemSpacing)
                }
                
            }
            
        }
        
    }
    
    // New API
    func getChildCatalogNew (completion : @escaping ()->()) {
        self.childCatalogNew = []
        let params = [
            "product_type_id": self.parentProductType.product_type_id,
            "token":"\(token)"
            ] as [String : Any]
        
        
        AF.request("\(serverUrlNew)product/getProductTypeContentCategory", method: .post, parameters: params)
            .responseJSON { response in
                if response.value != nil {
                    //                print(response.result.value!)
                    let swiftyJsonVar = JSON(response.value!)
                    let status: Int = swiftyJsonVar["status"].int!
                    //                print("getChildCatalogNew status:\(status)")
//                    print("status \(status)")
//                    print("swiftyJsonVar@getProductTypeContentCategory:\(swiftyJsonVar)")
                    if status == 200{
                        // token 正確
                        let restltJson = JSON(swiftyJsonVar["result"])
                        //                    print("getChildCatalogNew restltJson:\(restltJson)")
                        let productTypes = restltJson["product_type_content_category"].arrayObject
                        
//                        print("restltJson:\(restltJson)")
//                        print("productTypes?.count:\(productTypes?.count)")
                        // 0724
                        var c: Int = 0
                        for index in 0 ..< productTypes!.count{
                            let typeJson = JSON(productTypes![index])
                            
                            var tempCatalog: ProductTCCategory = ProductTCCategory()
                            tempCatalog.name = typeJson["name"].string!
                            tempCatalog.product_type_id = self.parentProductType.product_type_id
                            tempCatalog.content_category_id = typeJson["content_category_id"].int!
                            tempCatalog.description = typeJson["description"].string!
                            
                            self.childCatalogNew.append(tempCatalog)
                            
                            let params = [
                                "product_type_id": self.parentProductType.product_type_id,
                                "content_category_id": tempCatalog.content_category_id,
                                "token":"\(token)"
                                ] as [String : Any]
                            self.getOneChildProductList(index: index, params: params){
                                //                            if index ==
//                                print("index:\(index) done,c:\(c)")
                                c += 1
                                
                                if c == productTypes!.count{
//                                    print("getChildCatalogNew load all catalogs, c:\(c), productTypes!.count:\(productTypes!.count)")
                                    completion()
                                }
                            }
                            
                        }
                        //                    }
                        //                    completion()
                        
                    }else if status == 204{
                        // list == null
                        print("no catalog")
                        // 09/08 處理這邊!
//                        let restltJson = JSON(swiftyJsonVar["result"])
                        //                        let productTypes = restltJson["product_type_content_category"].arrayObject
                        print("self.parentProductType:\(self.parentProductType)")
                        
                        var tempCatalog: ProductTCCategory = ProductTCCategory()
                        tempCatalog.name = self.parentProductType.name
                        tempCatalog.product_type_id = self.parentProductType.product_type_id
                        self.childCatalogNew.append(tempCatalog)
                        
                        let params = [
                            "product_type_id": self.parentProductType.product_type_id,
                            "token":"\(token)"
                            ] as [String : Any]
                        
                        self.getOneChildProductList(index: -1, params: params){
//                            print("getChildCatalogNew load done")
//                            self.childCatalogNew.append(tempCatalog)
                            completion()
                        }
                        
//                        print("before self.childCatalogNew.append(tempCatalog)")
//                        print("params:\(params)")
                        
//                        self.childCatalogNew.append(tempCatalog)
//                        completion()
                    }else{
                        // token error
                        // 11/10
                        self.backToLoginVC(title:"Logout",msg: swiftyJsonVar["message"].string!,okMsg:"Back to LoginPage.")
//                        let alertController = UIAlertController(title: "Logout", message: swiftyJsonVar["message"].string!, preferredStyle: .alert)
//
//                        let okAction = UIAlertAction(
//                            title: "Back to LoginPage.",
//                            style: .default,
//                            handler: {
//                                (action: UIAlertAction!) -> Void in
//                                self.dismiss(animated: true, completion: nil)
//                        }
//                        )
//                        alertController.addAction(okAction)
//
//                        self.present(alertController, animated: true, completion: nil)
                        completion()
                    }
                    //                completion()
                }else{
                    print("getChildCatalogNew Error")
                    completion()
                }
                
        }
    }
    
    //    }
    
    // New API 0722 change for test
    func getChildCatalogShowNew (completion : @escaping ()->()) {
        
        var paramsArray: [[String:Any]] = []
        
        for i in 0..<self.childCatalogNew.count{
            let params = [
                "product_type_id": self.parentProductType.product_type_id,
                "content_category_id": self.childCatalogNew[i].content_category_id,
                "token":"\(token)"
                ] as [String : Any]
            paramsArray.append(params)
        }
        
        //        print("In getChildCatalogShowNew paramsArray.count : \(paramsArray.count)")
        //        for index in 0..<paramsArray.count{
        //            print("paramsArray[\(index)]:\(paramsArray[index])")
        //        }
        
        if paramsArray.count > 0{
            self.getOneChildProductList(index: 0, params: paramsArray[0]) {
                if paramsArray.count > 1{
                    self.getOneChildProductList(index: 1, params: paramsArray[1]) {
                        self.getOneChildProductList(index: 2, params: paramsArray[2]) {
                            self.getOneChildProductList(index: 3, params: paramsArray[3]) {
                                //                        self.getOneChildProductList(index: 4, params: paramsArray[4]) {
                                completion()
                                //                        }
                            }
                        }
                    }
                }else{
                    completion()
                }
                
            }
        }else{
            completion()
        }
        
    }
    
    
    // try
    func getOneChildProductList(index: Int, params: [String : Any], completion : @escaping ()->()){
        var tempProductLists: [ProductList] = []
        let getProductListAPI = serverUrlNew + "product/getProductList"

        AF.request(getProductListAPI, method: .post, parameters: params)
            .responseJSON { (response) in
                if response.value != nil {
                    //                    print(response.result.value!)
                    let swiftyJsonVar = JSON(response.value!)
                    let status: Int = swiftyJsonVar["status"].int!
                    //                print("status:\(status)")
//                    print("swiftyJsonVar@getOneChildProductList:\(swiftyJsonVar)")
                    if status == 200{
                        // token 正確, product_list not null
                        let restltJson = JSON(swiftyJsonVar["result"])
                        //                        print("restltJson in getChildCatalogShowNew :\(restltJson)")
                        let productLists = restltJson["product_list"].arrayObject
                        //                        print("productLists:\(String(describing: productLists))")
                        //                        if productLists != nil{
                        for product in productLists! {
                            //                                print("Hi i : \(i)")
                            let productJson = JSON(product)
                            
                            var tempProduct: ProductList = ProductList()
                            tempProduct.product_id = productJson["product_id"].int!
                            tempProduct.product_name = productJson["product_name"].string!
                            //1219
                            if productJson["product_description"].string != nil{
                                tempProduct.product_description = productJson["product_description"].string!
                            }else{
                                tempProduct.product_description = ""
                            }
                            
                            tempProduct.start_time = productJson["start_time"].string!
                            if productJson["end_time"].string != nil{
                                tempProduct.end_time = productJson["end_time"].string!
                            }
                            
                            let imageDatas = productJson["product_image"].arrayObject
                            
                            //                        print("imageDatas.count:\(imageDatas?.count)")
                            for imageData in imageDatas!{
                                let imageDataJson = JSON(imageData)
                                var tempProductImage: Product_image = Product_image()
                                // id 不影響抓取圖片功能，暫時無用處，先不理會（02/02新API改版）  02/03   Mike
//                                tempProductImage.product_image_id = imageDataJson["product_image_id"].string!
                                tempProductImage.product_image_type_id = imageDataJson["product_image_type_id"].int!
//                                print("tempProductImage_id \(tempProductImage.product_image_type_id)")
                                //                                    tempProductImage.product_image_type = imageDataJson["product_image_type"].string!
                                tempProductImage.name = imageDataJson["name"].string!
                                //                            tempProductImage.description = imageDataJson["description"].string!
                                tempProductImage.url = imageDataJson["url"].string!
                                
                                if tempProductImage.product_image_type_id == 1{
                                    // Landscape
                                    tempProduct.product_image_L = tempProductImage
                                }else{
                                    // Portrait
                                    tempProduct.product_image_P = tempProductImage
                                }
                                
                            }
                            let ratingJson = JSON(productJson["rating"])
                            //                        print("ratingJson:\(ratingJson)")
                            if ratingJson["avg_rating"].float != nil{
                                tempProduct.rating_avg_rating = ratingJson["avg_rating"].float!
                            }
                            if ratingJson["total_rating"].float != nil{
                                tempProduct.rating_total_rating = ratingJson["total_rating"].float!
                            }
                            tempProductLists.append(tempProduct)
                            tempProduct = ProductList()
                        }
//                        print("tempProductLists:\(tempProductLists)")
                        
                        if  index == -1{
                            self.childCatalogNew[0].productLists = tempProductLists //01/06當掉
                        }else if self.childCatalogNew.count > index {
                            self.childCatalogNew[index].productLists = tempProductLists
                        }
                        
                        //                    print("self.childCatalogNew[\(index)].productLists : \(self.childCatalogNew[index].productLists)")
                        completion()
                    }else if status == 204{
                        // product_list = null
                        completion()
                    }else{
                        // token error
                        let alertController = UIAlertController(title: "Logout", message: swiftyJsonVar["message"].string!, preferredStyle: .alert)
                        
                        let okAction = UIAlertAction(
                            title: "Back to LoginPage.",
                            style: .default,
                            handler: {
                                (action: UIAlertAction!) -> Void in
                                self.dismiss(animated: true, completion: nil)
                        }
                        )
                        alertController.addAction(okAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        completion()
                    }
                }else{
                    print("getOneChildProductList Error")
                    completion()
                }
                
                
        }
    }
    
    // API 3.2.4 getProductDetail
    // 改傳入參數 加上 index & i
    func getPackDetailNew(product_id:Int,Index:Int,i:Int,completion : @escaping ()->()){
        tmpPackDetailNew = ProductCDetail()
        print("02/03 product_id:\(product_id)")
        let params = [
            "product_id": product_id,
            "token":"\(token)"
            ] as [String : Any]
        
//        print("02/17 params:\(params)")
        
        AF.request("\(serverUrlNew)product/getProductDetail", method: .post, parameters: params)
            .responseJSON { response in
                //                if response.result.isSuccess {
                if response.value != nil{
                    //1225
                    self.tmpPackDetailNew.product_id = product_id
                    
                    //                    let swiftyJsonVar = JSON(response.result.value!)
                    let swiftyJsonVar = JSON(response.value!)
                    let status: Int = swiftyJsonVar["status"].int!
                    //                print("status:\(status)")
                    if status == 200{
                        // token 正確, product_list not null
                        let restltJson = JSON(swiftyJsonVar["result"])
                        //                        print("restltJson in getChildCatalogShowNew :\(restltJson)")
                        let productLists = restltJson["product_content_detail"].arrayObject
                        //                        print("productLists:\(String(describing: productLists))")
                        //                        if productLists != nil{
                        for product in productLists! {
                            //                                print("Hi i : \(i)")
                            let productJson = JSON(product)
                            
                            self.tmpPackDetailNew.content_id = productJson["content_id"].int!
                            self.tmpPackDetailNew.content_name = productJson["content_name"].string!
                            self.tmpPackDetailNew.content_description = productJson["content_description"].string!
                            
                            var tempContentAttributeValueArray: [ContentAttributeValue] = []
                            
                            let contentAttributeValueArrayDatas = productJson["content_attribute_value"].arrayObject
                            for contentAttributeValueArrayData in contentAttributeValueArrayDatas!{
                                let contentAttributeValueDataJson = JSON(contentAttributeValueArrayData)
                                var tempContentAttributeValue: ContentAttributeValue = ContentAttributeValue()
                                
                                tempContentAttributeValue.attribute_id = contentAttributeValueDataJson["attribute_id"].int!
                                tempContentAttributeValue.attribute = contentAttributeValueDataJson["attribute"].string!
                                tempContentAttributeValue.attribute_value_id = contentAttributeValueDataJson["attribute_value_id"].int!
                                tempContentAttributeValue.attribute_value = contentAttributeValueDataJson["attribute_value"].string!
                                
                                tempContentAttributeValueArray.append(tempContentAttributeValue)
                            }
                            self.tmpPackDetailNew.contentAttributeValueArray = tempContentAttributeValueArray
                            
                            
                            var tempStills: [Still] = []
                            
                            let stillsDatas = productJson["Stills"].arrayObject
                            for stillData in stillsDatas!{
                                let stillJson = JSON(stillData)
                                var tempStill: Still = Still()
                                
                                // assets_id 不影響抓取圖片功能，暫時無用處，先不理會（02/02新API改版）  02/03   Mike
//                                tempStill.assets_id = stillJson["assets_id"].string!
                                tempStill.name = stillJson["name"].string!
                                tempStill.url = stillJson["name"].string!
                                
                                var tempMetaDatas: [MetaData] = []
                                
                                let metaDatas = stillJson["meta_data"].arrayObject
                                
                                if metaDatas != nil{
                                    
                                    for metaData in metaDatas!{
                                        var tempMetaData: MetaData = MetaData()
                                        
                                        let metaDataJson = JSON(metaData)
                                        
                                        tempMetaData.attribute = metaDataJson["attribute"].string!
                                        tempMetaData.value = metaDataJson["value"].string!
                                        
                                        tempMetaDatas.append(tempMetaData)
                                    }
                                    
                                }
                                
                                tempStill.meta_data = tempMetaDatas
                                
                                tempStills.append(tempStill)
                            }
                            
                            self.tmpPackDetailNew.stills = tempStills
                            
                            
                            var tempPosters: [Poster] = []
                            
                            let postersDatas = productJson["Poster"].arrayObject
                            
                            //1223
//                            print("In CatalogMenu VC, func getPackDetailNew, postersDatas:\(postersDatas!),postersDatas.count:\(postersDatas!.count)")
                            
                            //1126
//                            if postersDatas?.count != 0 {
                            if postersDatas?.count == 2 {
                                print("postersDatas?.count == 2")
                                for posterData in postersDatas!{
                                    
                                    let posterDataJson = JSON(posterData)
                                    var tempPoster: Poster = Poster()
                                    //
                                    tempPoster.assets_id = posterDataJson["assets_id"].string!
                                    tempPoster.name = posterDataJson["name"].string!
                                    tempPoster.url = posterDataJson["url"].string!
                                    
                                    var tempMetaDatas: [MetaData] = []
                                    
                                    let metaDatas = posterDataJson["meta_data"].arrayObject
                                    
                                    if metaDatas != nil{
                                        
                                        for metaData in metaDatas!{
                                            var tempMetaData: MetaData = MetaData()
                                            
                                            let metaDataJson = JSON(metaData)
                                            
                                            tempMetaData.attribute = metaDataJson["attribute"].string!
                                            tempMetaData.value = metaDataJson["value"].string!
                                            
                                            tempMetaDatas.append(tempMetaData)
                                        }
                                        
                                    }
                                    
                                    tempPoster.meta_data = tempMetaDatas
                                    
                                    //                            tempPosters.append(tempPoster)
                                    if metaDatas!.count != 0{
                                        tempPosters.append(tempPoster)
                                    }
                                    
                                }
                                //                            print("tempPosters.conut:\(self.tempPosters.conut)")
                                
                            }
                            else if postersDatas?.count == 1{
//                                print("postersDatas?.count == 1")
                                // 1226 detail no poster
                                //temp in packdetail VC
                                var tempPoster: Poster = Poster()

                                tempPoster.assets_id = ""
                                tempPoster.name = ""
                                //01/06當掉
                                tempPoster.url = self.childCatalogNew[Index].productLists[i].product_image_L.url
                                
                                tempPosters.append(tempPoster)

                                tempPoster.url = self.childCatalogNew[Index].productLists[i].product_image_P.url

                                tempPosters.append(tempPoster)
//                                print("tempPosters \(tempPosters)")

                            }else if postersDatas?.count == 0{
                                // 10/19 yang
                                var tempPoster: Poster = Poster()
                                tempPoster.assets_id = ""
                                tempPoster.name = ""
                                tempPoster.url = self.childCatalogNew[Index].productLists[i].product_image_L.url
                                tempPosters.append(tempPoster)
                                tempPoster.url = self.childCatalogNew[Index].productLists[i].product_image_P.url
                                tempPosters.append(tempPoster)
                                // 01/19 mark
//                                print("postersDatas?.count == 0")
                            }
                            
                            self.tmpPackDetailNew.posters = tempPosters
                            //                        print("0219 postersDatas?.count:\(postersDatas?.count), self.tmpPackDetailNew.posters.count:\(self.tmpPackDetailNew.posters.count)")
                            
                            let ratingJson = JSON(productJson["rating"])
                            //                        print("ratingJson:\(ratingJson)")
                            if ratingJson["avg_rating"].float != nil{
                                self.tmpPackDetailNew.rating_avg_rating = ratingJson["avg_rating"].float!
                            }
                            if ratingJson["total_rating"].float != nil{
                                self.tmpPackDetailNew.rating_total_rating = ratingJson["total_rating"].float!
                            }
                            
                            var tempTrailerHLSs: [TrailerHLS] = []
                            
                            let trailerHLSDatas = productJson["Trailer_HLS"].arrayObject
                            for trailerHLSData in trailerHLSDatas!{
                                let trailerHLSDataJson = JSON(trailerHLSData)
                                var tempTrailerHLS: TrailerHLS = TrailerHLS()
                                
                                // assets_id 不影響抓取圖片功能，暫時無用處，先不理會（02/02新API改版）  02/03   Mike
//                                tempTrailerHLS.assets_id = trailerHLSDataJson["assets_id"].string!
                                tempTrailerHLS.name = trailerHLSDataJson["name"].string!
                                //                            tempPoster.url = trailerHLSDataJson["url"].string!
                                
                                var tempMetaDatas: [MetaData] = []
                                
                                let metaDatas = trailerHLSDataJson["meta_data"].arrayObject
                                
                                if metaDatas != nil{
                                    
                                    for metaData in metaDatas!{
                                        var tempMetaData: MetaData = MetaData()
                                        
                                        let metaDataJson = JSON(metaData)
                                        
                                        tempMetaData.attribute = metaDataJson["attribute"].string!
                                        tempMetaData.value = metaDataJson["value"].string!
                                        
                                        tempMetaDatas.append(tempMetaData)
                                    }
                                    
                                }
                                
                                tempTrailerHLS.meta_data = tempMetaDatas
                                
                                tempTrailerHLSs.append(tempTrailerHLS)
                            }
                            
                            self.tmpPackDetailNew.trailerHLS = tempTrailerHLSs
                            
                            //0726 寫到這
                        }
                        //                    self.childCatalogNew[index].productLists = tempProductLists
                        //                    print("self.childCatalogNew[\(index)].productLists : \(self.childCatalogNew[index].productLists)")
                    }else if status == 204{
                        // product_list = null
                        //                    completion()
                    }else{
                        // token error
                        let alertController = UIAlertController(title: "Logout", message: swiftyJsonVar["message"].string!, preferredStyle: .alert)
                        
                        let okAction = UIAlertAction(
                            title: "Back to LoginPage.",
                            style: .default,
                            handler: {
                                (action: UIAlertAction!) -> Void in
                                self.dismiss(animated: true, completion: nil)
                        }
                        )
                        alertController.addAction(okAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
                }else{
                    print("Catalog.getPackDetailNew Error")
                }
                //            if i == self.childCatalogNew.count - 1{
                completion()
                //            }
                
        }
    }
    
    
    @IBAction func menuClick(_ sender: Any) {
        navigationController?.view.menu()
        ButtonDelegate?.clickLeft()
        if navigationController?.view.superview?.frame.origin.x != 0 {
            showContentView()
        }else{
            removeSubview()
        }
    }
    
    @IBAction func userClick(_ sender: Any) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        navigationController?.view.menu2()
        ButtonDelegate?.clickRight()
        if navigationController?.view.superview?.frame.origin.x == -navigationOriginX {
            showContentView()
        }else{
            removeSubview()
        }
    }
    
    @IBAction func handleEdgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger(location: location, state: sender.state)
        if beganX2 > 0 {
            ButtonDelegate?.clickLeft()
            showContentView()
        }else{
            contentView.removeFromSuperview()
        }
    }
    
    @IBAction func handleEdgePanGesture2(_ sender: UIScreenEdgePanGestureRecognizer) {
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger2(location: location, state: sender.state)
        if beganX2 < 0{
            ButtonDelegate?.clickRight()
            showContentView()
        }else{
            contentView.removeFromSuperview()
        }
    }
    
    @IBAction func handleTapGesture(_ sender: Any) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        if navigationController?.view.superview?.frame.origin.x != 0 {
            if navigationController?.view.superview?.frame.origin.x == -navigationOriginX {
                navigationController?.view.menu2()
                contentView.removeFromSuperview()
            }else{
                navigationController?.view.menu()
                contentView.removeFromSuperview()
            }
            
        }
        
    }
    
}




