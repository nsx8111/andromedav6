//
//  ButtonDelegate.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2019/5/21.
//  Copyright © 2019 蒼月喵. All rights reserved.
//

import Foundation

protocol ButtonDelegate:class {
    func clickLeft()
    func clickRight()
//    func toPackPage()
    func toPackPage(packData: Pack)
    // 0729
    func toPackPageNew(packDataNew: ProductCDetail)
    func backPage()
    func setPackDetailPage(setSometext: String)
}
