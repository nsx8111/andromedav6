//
//  UIUtils.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2020/9/29.
//  Copyright © 2020 蒼月喵. All rights reserved.
//

import Foundation

class UIUtils {
    //設置能夠旋轉的方向
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = orientation
        }
    }
    //
   static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
        self.lockOrientation(orientation)
        //強制改變裝置的方向
        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
    }
}
