//
//  DeviceData.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2021/1/28.
//  Copyright © 2021 蒼月喵. All rights reserved.
//

import Foundation

struct DeviceData {
    enum dvType {
        case unknow
        case web
        case phone
        case stb
        case tablet
    }
    enum dvOS {
        case unknow
        case ios
        case android
    }
    
    var dvType: dvType = .unknow
    var dvName: String = ""
    var dvRegTime: String = ""
    var dvOS: dvOS = .unknow
    var dvSN: String = ""
}


