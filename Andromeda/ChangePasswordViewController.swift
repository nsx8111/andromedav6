//
//  ChangePasswordViewController.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2019/5/16.
//  Copyright © 2019 蒼月喵. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SnapKit

class ChangePasswordViewController: UIViewController, UITextFieldDelegate{
    weak var ButtonDelegate: ButtonDelegate?
    
    @IBOutlet weak var titleLabel: UILabel!
    let navBarLabel = UILabel()
    let navigationBarView = UIImageView()
    let bkgImageView = UIImageView()
    let passwordLabel = UILabel()
    let passwordTextField = CustomTextField()
    let passwordLabel2 = UILabel()
    let passwordTextField2 = CustomTextField()
    let passwordLabel3 = UILabel()
    let passwordTextField3 = CustomTextField()
    let applyButton = UIButton()
    let rightViewBtn = UIButton(type: .custom)
    let rightViewBtn2 = UIButton(type: .custom)
    let rightViewBtn3 = UIButton(type: .custom)
    let bkView = UIView()
    let bkView2 = UIView()
    let contentView = UIView()
    let actView = UIView()
    var activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
    ///存放新密碼用
    var tempPassword = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        hideKeyboardWhenTappedAround()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let name = NSNotification.Name("removeContentView")
        NotificationCenter.default.addObserver(self, selector:
        #selector(updateContentViewNoti(noti:)), name: name, object: nil)
    }
    
    func setViews(){
//        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTapGesture(_:)))
        let pan: UIScreenEdgePanGestureRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(self.handleEdgePanGesture(_:)))
        let pan2: UIScreenEdgePanGestureRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(self.handleEdgePanGesture2(_:)))
        view.addGestureRecognizer(tap)
        view.addGestureRecognizer(pan)
        view.addGestureRecognizer(pan2)
        
        passwordLabel3.backgroundColor = .clear
        passwordLabel3.text = "Enter Old Password"
        passwordLabel3.font = UIFont.systemFont(ofSize: 16)
        passwordLabel3.textColor = .cyan
        passwordLabel3.textAlignment = .left
        self.view.addSubview(passwordLabel3)
        passwordLabel3.snp.makeConstraints { (makes) in
            makes.top.equalTo(titleLabel.snp.bottom).offset(offset2)
            makes.left.right.equalToSuperview().inset((fullScreenSize.width-textFieldWidth)/2)
        }
        
        passwordTextField3.isSecureTextEntry = true
        passwordTextField3.font = UIFont(name: "Helvetica-Light", size: 22)
        passwordTextField3.textAlignment = .left
        passwordTextField3.clearButtonMode = .whileEditing
        passwordTextField3.returnKeyType = .done
        passwordTextField3.textColor = .black
        passwordTextField3.backgroundColor = .textFieldColor2
        passwordTextField3.layer.cornerRadius = 5.0
        passwordTextField3.keyboardType = .namePhonePad
        passwordTextField3.delegate = self
        self.view.addSubview(passwordTextField3)
        passwordTextField3.snp.makeConstraints { (makes) in
            makes.top.equalTo(passwordLabel3.snp.bottom).offset(offset1)
            makes.left.equalTo(passwordLabel3)
            makes.centerX.equalToSuperview()
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        
        passwordLabel.backgroundColor = .clear
//        passwordLabel.text = localString(key: "NEW_LOGIN_PASSWORD", table: .inputTitle)
        passwordLabel.text = "New Login Password"
        passwordLabel.font = UIFont.systemFont(ofSize: 16)
        passwordLabel.textColor = .cyan
        passwordLabel.textAlignment = .left
        self.view.addSubview(passwordLabel)
        passwordLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(passwordTextField3.snp.bottom).offset(offset3)
            makes.left.right.equalToSuperview().inset((fullScreenSize.width-textFieldWidth)/2)
        }
        
        passwordTextField.isSecureTextEntry = true
        passwordTextField.font = UIFont(name: "Helvetica-Light", size: 22)
        passwordTextField.textAlignment = .left
        passwordTextField.clearButtonMode = .whileEditing
        passwordTextField.returnKeyType = .done
        passwordTextField.textColor = .black
        passwordTextField.backgroundColor = .textFieldColor2
        passwordTextField.layer.cornerRadius = 5.0
        passwordTextField.keyboardType = .namePhonePad
        passwordTextField.delegate = self
        self.view.addSubview(passwordTextField)
        passwordTextField.snp.makeConstraints { (makes) in
            makes.top.equalTo(passwordLabel.snp.bottom).offset(offset1)
            makes.left.equalTo(passwordLabel)
            makes.centerX.equalToSuperview()
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        
        passwordLabel2.backgroundColor = .clear
//        passwordLabel2.text = localString(key: "ENTER_PASSWORD_AGAIN", table: .inputTitle)
        passwordLabel2.text = "Enter Password Again"
        passwordLabel2.font = UIFont.systemFont(ofSize: 16)
        passwordLabel2.textColor = .cyan
        passwordLabel2.textAlignment = .left
        self.view.addSubview(passwordLabel2)
        passwordLabel2.snp.makeConstraints { (makes) in
            makes.top.equalTo(passwordTextField.snp.bottom).offset(offset3)
            makes.width.left.equalTo(passwordLabel)
        }
        
        passwordTextField2.isSecureTextEntry = true
        passwordTextField2.font = UIFont(name: "Helvetica-Light", size: 22)
        passwordTextField2.textAlignment = .left
        passwordTextField2.clearButtonMode = .whileEditing
        passwordTextField2.returnKeyType = .done
        passwordTextField2.textColor = .black
        passwordTextField2.backgroundColor = .textFieldColor2
        passwordTextField2.layer.cornerRadius = 5.0
        passwordTextField2.keyboardType = .namePhonePad
        passwordTextField2.delegate = self
        self.view.addSubview(passwordTextField2)
        passwordTextField2.snp.makeConstraints { (makes) in
            makes.top.equalTo(passwordLabel2.snp.bottom).offset(offset1)
            makes.left.equalTo(passwordLabel2)
            makes.centerX.equalToSuperview()
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        
        applyButton.backgroundColor = .buttonColor2
        applyButton.layer.cornerRadius = 7
//        applyButton.setTitle(localString(key: "APPLY", table: .button), for: [])
        applyButton.setTitle("Apply", for: [])
        applyButton.titleLabel?.font = UIFont(name: "Helvetica-Light", size: 20)
        applyButton.titleLabel?.textColor = .white
        applyButton.isEnabled = true
        applyButton.addTarget(self,action: #selector(self.clickChangePassword),for: .touchUpInside)
        self.view.addSubview(applyButton)
        applyButton.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(passwordTextField2.snp.bottom).offset(offset9)
            makes.width.equalTo(buttonWidth)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        ///密碼輸入框右側切換是否隱藏密碼icon
        if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysOriginal) {
            let smallImage = resizeImage(image: image, width: rightBtnSize)
            rightViewBtn.addTarget(self,action: #selector(self.securityClick),for: .touchUpInside)
            rightViewBtn.setImage(smallImage, for: .normal)
            passwordTextField.rightView = rightViewBtn
            passwordTextField.rightViewMode = .always
        }
        
        if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysOriginal) {
            let smallImage = resizeImage(image: image, width: rightBtnSize)
            rightViewBtn2.addTarget(self,action: #selector(self.securityClick2),for: .touchUpInside)
            rightViewBtn2.setImage(smallImage, for: .normal)
            passwordTextField2.rightView = rightViewBtn2
            passwordTextField2.rightViewMode = .always
        }
        
        if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysOriginal) {
            let smallImage = resizeImage(image: image, width: rightBtnSize)
            rightViewBtn3.addTarget(self,action: #selector(self.securityClick3),for: .touchUpInside)
            rightViewBtn3.setImage(smallImage, for: .normal)
            passwordTextField3.rightView = rightViewBtn3
            passwordTextField3.rightViewMode = .always
        }
        
//        switch currentDevice.deviceType {
//        case .iPhone35:
//            applyButton.layer.cornerRadius = 17
//        case .iPhone40:
//            applyButton.layer.cornerRadius = 17
//        case .iPhone47:
//            applyButton.layer.cornerRadius = 20
//        case .iPhone55:
//            applyButton.layer.cornerRadius = 22
//        case .iPad:
//            applyButton.layer.cornerRadius = 30
//        default:
//            applyButton.layer.cornerRadius = 24
//        }
        
    }
    /**
    - 密碼輸入框切換隱藏密碼icon變換功能
    - Date: 09/11 Yang
     */
    @objc func securityClick() {
        if passwordTextField.isSecureTextEntry == true{
            if let image = UIImage(named: "eye-b")?.withRenderingMode(.alwaysOriginal) {
                let smallImage = resizeImage(image: image, width: rightBtnSize)
                rightViewBtn.setImage(smallImage, for: .normal)
            }
            passwordTextField.isSecureTextEntry = false
        }else{
            if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysOriginal) {
                let smallImage = resizeImage(image: image, width: rightBtnSize)
                rightViewBtn.setImage(smallImage, for: .normal)
            }
            passwordTextField.isSecureTextEntry = true
        }
        
    }
    
    @objc func securityClick2() {
        if passwordTextField2.isSecureTextEntry == true{
            if let image = UIImage(named: "eye-b")?.withRenderingMode(.alwaysOriginal) {
                let smallImage = resizeImage(image: image, width: rightBtnSize)
                rightViewBtn2.setImage(smallImage, for: .normal)
            }
            passwordTextField2.isSecureTextEntry = false
        }else{
            if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysOriginal) {
                let smallImage = resizeImage(image: image, width: rightBtnSize)
                rightViewBtn2.setImage(smallImage, for: .normal)
            }
            passwordTextField2.isSecureTextEntry = true
        }
        
    }
    
    @objc func securityClick3() {
        if passwordTextField3.isSecureTextEntry == true{
            if let image = UIImage(named: "eye-b")?.withRenderingMode(.alwaysOriginal) {
                let smallImage = resizeImage(image: image, width: rightBtnSize)
                rightViewBtn3.setImage(smallImage, for: .normal)
            }
            passwordTextField3.isSecureTextEntry = false
        }else{
            if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysOriginal) {
                let smallImage = resizeImage(image: image, width: rightBtnSize)
                rightViewBtn3.setImage(smallImage, for: .normal)
            }
            passwordTextField3.isSecureTextEntry = true
        }
        
    }
    
    /**
     執行變更密碼
     - Date: 09/14 Yang
     */
    @objc func clickChangePassword(){
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
        activityIndicator.center = view.center
        activityIndicator.startAnimating()
        
        actView.isHidden = false
        actView.backgroundColor = .actViewColor
        actView.addSubview(activityIndicator)
        self.view.addSubview(actView)
        actView.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
        }
        
        textFieldResignFirstResponder()
        self.sendData{
            self.actView.isHidden = true
        }
    }
    /**
     送出新密碼
     - Date:09/14
    */
    func sendData(completion : @escaping ()->()){
        
        if passwordTextField3.text != passwordForLogin{
            alertMsg3(title: "INVALID", msg: "Old password incorrect")
            self.actView.isHidden = true
            return
        }
        if passwordTextField.text == nil || passwordTextField.text == "" || passwordTextField2.text == nil || passwordTextField2.text == "" {
//            alertMsg(title: localString(key: "INVALID", table: .alertTitle), msg: localString(key: "LOGIN_PASSWORD_EMPTY", table: .alertMsg))
            alertMsg3(title: "INVALID", msg: "Login password cannot be empty")
            self.actView.isHidden = true
            return
        }
        if passwordTextField.text != passwordTextField2.text{
//            alertMsg(title: localString(key: "INVALID", table: .alertTitle), msg: localString(key: "LOGIN_PASSWORD_DIFFERENT", table: .alertMsg))
            alertMsg3(title: "INVALID", msg: "The passwords entered twice are different")
            self.actView.isHidden = true
            return
        }
        tempPassword = self.passwordTextField2.text!
        
//        let headers: HTTPHeaders = [.authorization(username: user, password: password)]
//        let requestStr : String = "\(formPasswordSetupStr)?userName=\(user)&newPass=\(tempPassword)"
        
        let params = [
        "old_password" : passwordForLogin,
        "new_password" : tempPassword,
        "confirm_new_password" : tempPassword,
        "token":"\(token)"
            ] as [String : Any]
        
        let changePasswordAPI = serverUrlNew + "account/changePassword"
                
        AF.request(changePasswordAPI, method: .post, parameters: params)
            .responseJSON { (response) in
                passwordForLogin = self.tempPassword
                
                if response.value != nil {
                    let swiftyJsonVar = JSON(response.value!)
                    print("swiftyJsonVar: \(swiftyJsonVar)")
                    let status: Int = swiftyJsonVar["status"].int!
//                    let message = swiftyJsonVar["message"]
//                    print("status: \(status)")
//                    print("message: \(message)")
                
                    ///changed success
                    if status == 200{
//                        let restltJson = JSON(swiftyJsonVar["result"])
//                        print("restltJson:\(restltJson)")
                        let alertController = UIAlertController(title: "Password changed", message: "SUCCESS", preferredStyle: .alert)
                        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                        alertController.addAction(defaultAction)
                        self.present(alertController, animated: true, completion: nil)
                    ///changed failure
                    }else{
                        let alertController = UIAlertController(title: "Password changed", message: "ERROR", preferredStyle: .alert)
                        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                        alertController.addAction(defaultAction)
                        self.present(alertController, animated: true, completion: nil)
                        print("error")
                    }
                    completion()
                }else{
                    print("送API出現意外問題")
                    completion()
                }
                
                
                
        }
            
    }
    
    /** 處理點擊 View */
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        print("touchesBegan")
//        self.view.endEditing(true)
//    }
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        removeSubview()
        if navigationController?.view.superview?.frame.origin.x != 0 {
        navigationController?.view.menu()
        }
        view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textFieldResignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    func textFieldResignFirstResponder() {
        passwordTextField.resignFirstResponder()
        passwordTextField2.resignFirstResponder()
        passwordTextField3.resignFirstResponder()
    }
    /**
     點擊屏幕回彈用
     - Date: 08/26 Yang
    */
    @objc func showContentView(){
        contentView.tag = 1000
        contentView.isUserInteractionEnabled = true
        contentView.backgroundColor = .clear
        view.addSubview(contentView)
        contentView.snp.makeConstraints { (makes) in
            makes.edges.equalTo(view)
        }
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTapGesture(_:)))
        contentView.addGestureRecognizer(tap)
    }
    /**
     側邊欄關閉後 就把contentView給移除掉 以便可以觸控此頁面元件
     - Date: 08/26 Yang
    */
    @objc func removeSubview(){
        if let viewWithTag = self.view.viewWithTag(1000) {
            viewWithTag.removeFromSuperview()
        }else{
            print("No!")
        }
    }
    /**
     
     - Date: 09/17 Yang
    */
    @objc func updateContentViewNoti(noti: Notification) {
        removeSubview()
    }
    
    @IBAction func menuClick(_ sender: Any) {
        textFieldResignFirstResponder()
        navigationController?.view.menu()
        ButtonDelegate?.clickLeft()
        if navigationController?.view.superview?.frame.origin.x != 0 {
            showContentView()
        }else{
            removeSubview()
        }
    }
    
    @IBAction func userClick(_ sender: Any) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        textFieldResignFirstResponder()
        navigationController?.view.menu2()
        ButtonDelegate?.clickRight()
        if navigationController?.view.superview?.frame.origin.x == -navigationOriginX {
            showContentView()
        }else{
            removeSubview()
        }
    }
    
    @IBAction func handleEdgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
        textFieldResignFirstResponder()
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger(location: location, state: sender.state)
        if beganX2 > 0 {
            ButtonDelegate?.clickLeft()
            showContentView()
        }else{
            contentView.removeFromSuperview()
        }
    }
    
    @IBAction func handleEdgePanGesture2(_ sender: UIScreenEdgePanGestureRecognizer) {
        textFieldResignFirstResponder()
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger2(location: location, state: sender.state)
        if beganX2 < 0 {
            ButtonDelegate?.clickRight()
            showContentView()
        }else{
            contentView.removeFromSuperview()
        }
    }
    
    @IBAction func handleTapGesture(_ sender: Any) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        textFieldResignFirstResponder()
        print("OriginX \(navigationController?.view.superview?.frame.origin.x as Any)")
        if navigationController?.view.superview?.frame.origin.x != 0 {
            if navigationController?.view.superview?.frame.origin.x == -navigationOriginX {
                navigationController?.view.menu2()
                contentView.removeFromSuperview()
            }else{
                navigationController?.view.menu()
                contentView.removeFromSuperview()
            }
        }
    }
}
