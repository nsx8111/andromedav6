//
//  ParentalControlViewController.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2019/5/16.
//  Copyright © 2019 蒼月喵. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SnapKit

class ParentalControlViewController: UIViewController {
    weak var ButtonDelegate: ButtonDelegate?
    
    let pcStatusLabel = UILabel()
    let pcOnOffBtn = UIButton()
    let pcChangePwdBtn = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setViews()
        self.setActView(openOrClose: true)
        getPcStatus {
            print("getPcStatus done")
            print("pcStatus : \(pcStatus.rawValue)")
//            self.setViews()
            if pcStatus == .off {
                self.pcOnOffBtn.setTitle("Open Parent Control", for: .normal)
            }else{
                self.pcOnOffBtn.setTitle("Close Parent Control", for: .normal)
            }
            self.pcStatusLabel.text = pcStatus.rawValue
            self.setActView(openOrClose: false)
        }
        
    }
    
    
    func setViews(){
        let pcLabel = UILabel()
        pcLabel.text = "Parent Control Status : "
        pcLabel.font = UIFont(name: "Helvetica-Light", size: 20)
        pcLabel.textColor = .white
        view.addSubview(pcLabel)
        
        pcLabel.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview().offset(-20)
            makes.top.equalToSuperview().offset(fullScreenSize.height/5)
        }
        
        // 09/25
//        pcStatusLabel.text = pcStatus.rawValue
        pcStatusLabel.text = "??"
        pcStatusLabel.font = UIFont(name: "Helvetica-Light", size: 20)
        pcStatusLabel.textColor = .white
        view.addSubview(pcStatusLabel)
        
        pcStatusLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(pcLabel)
            makes.left.equalTo(pcLabel.snp.right)
        }
        
        // 09/25
//        if pcStatus == .off {
//            pcOnOffBtn.setTitle("Open Parent Control", for: .normal)
//        }else{
//            pcOnOffBtn.setTitle("Close Parent Control", for: .normal)
//        }
        pcOnOffBtn.setTitle("Waiting Server", for: .normal)
        
        pcOnOffBtn.backgroundColor = .white
        pcOnOffBtn.setTitleColor(.cellBgColor, for: .normal)
        pcOnOffBtn.addTarget(self, action: #selector(tapPcOnOffBtn), for: .touchDown)
        view.addSubview(pcOnOffBtn)
        pcOnOffBtn.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(fullScreenSize.height * 0.3)
            makes.width.equalTo(fullScreenSize.width/2)
            makes.height.equalTo(fullScreenSize.height/10)
        }
        
        pcChangePwdBtn.setTitle("Change Password", for: .normal)
        pcChangePwdBtn.backgroundColor = .white
        pcChangePwdBtn.setTitleColor(.cellBgColor, for: .normal)
        pcChangePwdBtn.addTarget(self, action: #selector(tapPcChangePwdBtn), for: .touchDown)
        view.addSubview(pcChangePwdBtn)
        pcChangePwdBtn.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(fullScreenSize.height * 0.45)
            makes.width.equalTo(fullScreenSize.width/2)
            makes.height.equalTo(fullScreenSize.height/10)
        }
        
//        If you forget your password, please contact the service provider
        let reminderMsg = UILabel()
        reminderMsg.text = "If you forget your password, \nplease contact the service provider"
        reminderMsg.lineBreakMode = NSLineBreakMode.byWordWrapping
        reminderMsg.numberOfLines = 0
        reminderMsg.textAlignment = .center
        reminderMsg.font = UIFont(name: "Helvetica-Light", size: 20)
        reminderMsg.textColor = .white
        view.addSubview(reminderMsg)
        reminderMsg.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(fullScreenSize.height * 0.6)
        }
        
    }
    
    @objc func tapPcChangePwdBtn(){
        print("tapPcChangePwdBtn")
        
        let controller = UIAlertController(title: "更改密碼", message: "請輸入親子鎖密碼", preferredStyle: .alert)
        controller.addTextField { (textField) in
            textField.placeholder = "舊密碼"
            textField.keyboardType = UIKeyboardType.phonePad
        }
        
        controller.addTextField { (textField) in
            textField.placeholder = "新密碼"
            textField.keyboardType = UIKeyboardType.phonePad
        }
        
        controller.addTextField { (textField) in
            textField.placeholder = "重複新密碼"
            textField.keyboardType = UIKeyboardType.phonePad
        }
        
        let okAction = UIAlertAction(title: "OK", style: .default) { (_) in
            let oldPwd = controller.textFields?[0].text
            let newPwd = controller.textFields?[1].text
            let newPwd2 = controller.textFields?[2].text
            
            if self.checkInput(oldPwd: oldPwd!, newPwd: newPwd!, newPwd2: newPwd2!) {
//                print(oldPwd, newPwd, newPwd2)
                self.changePwd(oldPwd: oldPwd!, newPwd: newPwd!) {
                    print("changePwd done")
                }
            }
        }
        controller.addAction(okAction)
        let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        controller.addAction(cancelAction)
        present(controller, animated: true, completion: nil)
    }
    
    func checkInput(oldPwd:String,newPwd:String,newPwd2:String) -> Bool{
        if oldPwd == "" || newPwd == "" || newPwd2 == ""{
            return false
        }
        if newPwd != newPwd2{
            return false
        }
        return true
    }
    
    func changePwd(oldPwd:String, newPwd:String, completion : @escaping ()->()){
        /// - TODO: "password"部分先寫死測試
        let params = [
            "lock_type_id": 1,
            "password": oldPwd,
            "new_password": newPwd,
            "confirm_password": newPwd,
            "token": token
        ] as [String : Any]
        
//        let actView : UIView = UIView()
//        self.setActView(actView: actView)
        self.setActView(openOrClose: true)
        
        // 11/05
        let changeLockPasswordAPI = serverUrlNew + "account/changeLockPassword"
        
        AF.request(changeLockPasswordAPI, method: .post, parameters: params)
        .responseJSON{ (response) in
            if response.value != nil{
//                print(response.value)
                let swiftyJsonVar = JSON(response.value!)
                let status: Int = swiftyJsonVar["status"].int!
                if status == 200{
                    self.alertMsg2(msg: "change done")
//                    actView.removeFromSuperview()
                    self.setActView(openOrClose: false)
                    completion()
                }else{
                    print("change error")
                    self.alertMsg2(msg: swiftyJsonVar["message"].string!)
//                    actView.removeFromSuperview()
                    self.setActView(openOrClose: false)
                    completion()
                }
            }
        }
    }
    
    @objc func tapPcOnOffBtn() {
        print("tapPcOnOffBtn")
        
        
        let controller = UIAlertController(title: "輸入密碼", message: "請輸入親子鎖密碼", preferredStyle: .alert)
        controller.addTextField { (textField) in
            textField.placeholder = "密碼"
            textField.keyboardType = UIKeyboardType.phonePad
        }
        
        let okAction = UIAlertAction(title: "OK", style: .default) { (_) in
            let pwd = controller.textFields?[0].text
            
//            let actView : UIView = UIView()
//            self.setActView(actView: actView)
            self.setActView(openOrClose: true)
            self.changePcOnOff(pwd: pwd!) {
                print("changePcOnOff Done")
//                actView.removeFromSuperview()
                self.setActView(openOrClose: false)
            }
        }
        controller.addAction(okAction)
        let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        controller.addAction(cancelAction)
        present(controller, animated: true, completion: nil)
        
        
    }
    
    func changePcOnOff(pwd:String, completion : @escaping ()->()){
        let params = [
            "lock_type_id": 1,
            "password": pwd,
            "token": token
        ] as [String : Any]
        
        // 11/05
//        var requestAPI : String = setLockAPI
        var requestAPI : String = serverUrlNew + "account/setLock"
        
        if pcStatus == .off {
            requestAPI += "On"
        }else{
            requestAPI += "Off"
        }
        
        AF.request(requestAPI, method: .post, parameters: params)
        .responseJSON{ (response) in
            if response.value != nil{
                print(response.value)
                let swiftyJsonVar = JSON(response.value!)
                let status: Int = swiftyJsonVar["status"].int!
                if status == 200{
                    if pcStatus == .off {
                        self.pcOnOffBtn.setTitle("Close Parent Control", for: .normal)
                        pcStatus = .on
                    }else{
                        self.pcOnOffBtn.setTitle("Open Parent Control", for: .normal)
                        pcStatus = .off
                    }
                    self.pcStatusLabel.text = pcStatus.rawValue
                    self.alertMsg2(msg: "Done")
                    completion()
                }else{
                    print("change error")
                    self.alertMsg2(msg: swiftyJsonVar["message"].string!)
                    completion()
                }
            }
        }
    }
    
    @IBAction func menuClick(_ sender: Any) {
        navigationController?.view.menu()
        ButtonDelegate?.clickLeft()
    }
    @IBAction func userClick(_ sender: Any) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        navigationController?.view.menu2()
        ButtonDelegate?.clickRight()
    }
    
    
    @IBAction func handleEdgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger(location: location, state: sender.state)
        if beganX2 > 0 {
            ButtonDelegate?.clickLeft()
        }
    }
    
    @IBAction func handleEdgePanGesture2(_ sender: UIScreenEdgePanGestureRecognizer) {
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger2(location: location, state: sender.state)
        if beganX2 < 0{
            ButtonDelegate?.clickRight()
        }
    }
    
    @IBAction func handleTapGesture(_ sender: Any) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        if navigationController?.view.superview?.frame.origin.x != 0 {
            navigationController?.view.menu()
        }
    }
    
    
}


