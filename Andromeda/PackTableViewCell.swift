//
//  PackTableViewCell.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2019/5/21.
//  Copyright © 2019 蒼月喵. All rights reserved.
//
// 10/14 這檔案測試過後應該是沒有用到，暫時留存
// .xib 同樣沒用到

import UIKit
import SnapKit

class PackTableViewCell: UITableViewCell {
    @IBOutlet weak var packImage: UIImageView!
    @IBOutlet weak var packNameTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setConstraints()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
//        let name = String(self.packNameTextView.text)
//        print("\(name) setSelected")
        
        
        // Configure the view for the selected state
    }
    
    func setConstraints(){
        self.backgroundColor = .cellBgColor
//        packImage.downloaded(from: <#T##URL#>)
        
        packImage.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(self.bounds.height*0.12)
            make.left.equalToSuperview().offset(10)
            make.bottom.equalToSuperview().offset(-self.bounds.height*0.12)
            
//            make.height.equalTo(self.bounds.height*0.76)
//            make.width.equalTo(self.bounds.width*0.34782609)
        }
        
        packNameTextView.textColor = .white
        packNameTextView.backgroundColor = .cellBgColor
        packNameTextView.isSelectable = false
            
        /** 讓 textView 的高度隨文字行數變化 */
        packNameTextView.translatesAutoresizingMaskIntoConstraints = true
        packNameTextView.sizeToFit()
        packNameTextView.isScrollEnabled = false
        
        packNameTextView.snp.makeConstraints { (make) in
            make.left.equalTo(packImage.snp.right).offset(self.bounds.width*0.03)
            make.right.equalToSuperview().offset(-20)
            make.centerY.equalTo(packImage.snp.centerY)
        }
        
    }
    
}
