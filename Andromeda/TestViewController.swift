//
//  TestViewController.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2020/8/6.
//  Copyright © 2020 蒼月喵. All rights reserved.
//

import UIKit
import SnapKit

class TestViewController: UIViewController, VLCMediaPlayerDelegate {
    
    /** player */
    var vlcPlayer = VLCMediaPlayer()
    /** UIView for player , put on viewForVlc */
    let playerView: UIView = {
        
        let view = UIView()
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .lightGray
        
        setView()
        setPlayer()
        
    }
    
    func setView(){
        view.addSubview(playerView)
        
        playerView.backgroundColor = .black
        
        playerView.snp.makeConstraints { (makes) in
            makes.edges.centerX.centerY.equalToSuperview()
//            makes.height.equalTo(400)
        }
    }
    
    func setPlayer(){
        vlcPlayer.delegate = self
        vlcPlayer.drawable = playerView
        vlcPlayer.media = nil
        
        let streamUrl = URL(string: "http://andromeda.tti.tv/AndromedaContent/Video_HLS/1/video_hls_1_1560845193_74.m3u8")
        let media = VLCMedia(url: streamUrl!)
        
        vlcPlayer.media = media
        
        vlcPlayer.play()
    }
}
