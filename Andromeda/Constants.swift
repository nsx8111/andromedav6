//
//  Constant.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2019/5/21.
//  Copyright © 2019 蒼月喵. All rights reserved.
//

import UIKit
import Foundation

// 01/22
let uuid = UIDevice.current.identifierForVendor!.uuidString
let systemVersion = UIDevice.current.systemVersion
// 01/23
let dvName = UIDevice.current.name
//左右滑動時menu的origin.x
var beganX2 = 0
/** 取得裝置大小 */
let fullScreenSize = UIScreen.main.bounds.size
let statusBarHeight = UIApplication.shared.statusBarFrame.height
var navBarHeight: CGFloat = 64
let currentDevice = UIDevice()
/** 元件大小 */
var rightBtnSize: CGFloat = fullScreenSize.height*0.07/2
var rightBtnSize2: CGFloat = UIScreen.main.bounds.size.height*0.06/2
var textFieldWidth: CGFloat = fullScreenSize.width*0.75
var textFieldWidth2: CGFloat = fullScreenSize.width*0.7
var buttonWidth: CGFloat = fullScreenSize.width*0.7
var navigationOriginX: CGFloat = fullScreenSize.width*0.8

var tableViewRowHeight : CGFloat = {
    switch currentDevice.deviceType {
    case .iPhone40:
        return 100
    case .iPhone47:
        return 120
    case .iPad:
        return 140
    default:
        return 120
    }
}()
/// 適應各種不同裝置尺寸使用
var HeightForScroll: CGFloat = fullScreenSize.height/736
var WidthForDevices: CGFloat = fullScreenSize.width/414
/** 間距 */
var offset0: CGFloat = 5
var offset1: CGFloat = 10
var offset2: CGFloat = 15
var offset3: CGFloat = 20
var offset4: CGFloat = 25
var offset5: CGFloat = 30
var offset6: CGFloat = 35
var offset7: CGFloat = 40
var offset8: CGFloat = 45
var offset9: CGFloat = 50
var offset10: CGFloat = 60
var offset11: CGFloat = 80
var offset12: CGFloat = 100
/** 其他全域變數 */
var productImageTypeID = 0
var accountForLogin : String = ""
var passwordForLogin : String = ""
var isFavoriteChanged = false
var favoriteLists: [FavoriteList] = []
var purchaseLists: [PurchaseList] = []
var rentLists: [RentList] = []
var purchaseOptionLists: [PurchaseOptionList] = []


let longSide : CGFloat = {
    if fullScreenSize.height > fullScreenSize.width{
        return fullScreenSize.height
    }else{
        return fullScreenSize.width
    }
}()
let shortSide : CGFloat = {
    if fullScreenSize.height > fullScreenSize.width{
        return fullScreenSize.width
    }else{
        return fullScreenSize.height
    }
}()

/** token */
var token: String = ""

/** record User data */
var accountInfo = AccountInfo(
    AccountId: 23,
    AccountGUID: "",
    AccountName: "",
    UidType: 0,
    ClientIpLong: "",
    Token: "",
    Birthday: nil,
    Gender: 0,
    Age: "",
    Mood: 0,
    Operator: 0,
    LastLogin: "",
    isForgot: false,
    nickName: ""
)

/** record User data (New API) */
var accountInfoNew = AccountInfoNew()

// to store the current active textfield
var activeTextField : UITextField? = nil

/**
 For switch UIDevice.current.userInterfaceIdiom 簡化用
 預設為 iPhone
 
 - Date: 08/13  Mike
 */
var deviceType : deviceEnum = .iPhone

/**
 For devictType，有兩種 case
 
 - Date: 08/13  Mike
 */
enum deviceEnum {
    case iPhone
    case iPad
}


