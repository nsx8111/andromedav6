//
//  authView.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2021/01/15.
//  Copyright © 2021 蒼月喵. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import SwiftyJSON
import LocalAuthentication
import AVFoundation

enum AuthResult{
    case success(String)
    case fail
}

class AuthVC : UIViewController, AVCaptureMetadataOutputObjectsDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, VLCMediaPlayerDelegate{
    
    var context = LAContext()
    var tempPwd : String? = nil
    
    enum AuthenticationState {
        case loggedin, loggedout, parsing
    }
    
    var state = AuthenticationState.loggedout {
        didSet {
            self.codeTextLabel.textColor = state == .loggedin ? .green : .yellow
            self.codeTextLabel.isHidden = state == .parsing
            if state == .loggedin{
                self.codeTextLabel.text = "Login succes"
                // FaceID or TouchID pass
                self.tempPwd = pcPwd
                self.afterCheckPwd()
            }else{
                self.codeTextLabel.text = "Login fail"
            }
        }
    }
    
    let codeTextLabel = UILabel()
    
    // 01/18
    let pwdTextFields : [UITextField] = {
        var pwdTextFields : [UITextField] = []
        for i in 0..<4{
            let tempTextField = UITextField()
            tempTextField.backgroundColor = .white
            // 10/27
            tempTextField.textColor = .black
            
            tempTextField.textAlignment = .center
            tempTextField.keyboardType = .numberPad
            pwdTextFields.append(tempTextField)
        }
        return pwdTextFields
    }()
    
    // 01/18
    let pwdBtns : [UIButton] = {
        var tempBtns : [UIButton] = []
        let cancelBtn = UIButton()
        // later
        cancelBtn.addTarget(self, action: #selector(tapCancelBtn(_:)), for: .touchDown)
        tempBtns.append(cancelBtn)
        let okBtn = UIButton()
        okBtn.addTarget(self, action: #selector(tapOKBtn(_:)), for: .touchDown)
        tempBtns.append(okBtn)
        
        return tempBtns
    }()
    
    // 01/18
    let okBtn : UIButton = {
        let btn : UIButton = UIButton()
        btn.addTarget(self, action: #selector(tapOKBtn(_:)), for: .touchDown)
        return btn
    }()
    
    // 01/18
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("textFieldShouldReturn")
        return true
    }
    // 01/18
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        //        print("range:\(range)")
        print("range.location:\(range.location)")
        print("string:\(string)")
        
        if textField == pwdTextFields[0]{
            pwdTextFields[0].text = string
            if string != ""{
                pwdTextFields[1].becomeFirstResponder()
            }
        }else if textField == pwdTextFields[1]{
            pwdTextFields[1].text = string
            if string != ""{
                pwdTextFields[2].becomeFirstResponder()
            }
        }else if textField == pwdTextFields[2]{
            pwdTextFields[2].text = string
            if string != ""{
                pwdTextFields[3].becomeFirstResponder()
            }
        }else if textField == pwdTextFields[3]{
            if range.location > 0{
                pwdTextFields[3].text = ""
            }
        }
        return true
    }
    // 01/18
    func inputPwd(){
        let pwdView = UIView()
        // for cancel btn to remove
        pwdView.tag = 100
        self.setPwdView(pwdView: pwdView, pwdBtns: pwdBtns, pwdTextFields: pwdTextFields)
    }
    // 01/18
    @objc func tapCancelBtn(_ sender: UIButton){
        self.removePwdView()
    }
    // 01/18
    func removePwdView(){
        for view in self.view.subviews {
            if view.tag == 100 {
                view.removeFromSuperview()
            }
        }
    }
    // 01/18
    @objc func tapOKBtn(_ sender: UIButton){
        print("Enter tapOKBtn @AuthVC")
    }
    
    func tryAuth(){
        context = LAContext()

        context.localizedCancelTitle = "Enter Username/Password"
        
        var error: NSError?
        if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {

            let reason = "Log in to your account"
            context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason ) { success, error in

                if success {

                    // Move to the main thread because a state update triggers UI changes.
                    DispatchQueue.main.async { [unowned self] in
                        self.state = .loggedin
                    }

                } else {
                    print(error?.localizedDescription ?? "Failed to authenticate")
                    print("AAAA")
                    DispatchQueue.main.async { [unowned self] in
                        self.inputPwd()
                    }
                }
            }
        } else {
            print(error?.localizedDescription ?? "Can't evaluate policy")
            print("BBBB")
            self.state = .loggedout
        }
    }
    
    func afterCheckPwd(){}
}

extension UIViewController {
    
    func setAuthViewTest() -> AuthResult{
        print("01/15")
        var tempResult : AuthResult = .success("0000")
        print(tempResult)
        tempResult = .fail
        tempResult = .success("1234")
        print(tempResult)
        
        return tempResult
    }
    
    func setAuthView(pwdView:UIView, pwdBtns: [UIButton] ,pwdTextFields:[UITextField]){
        
        self.view.addSubview(pwdView)
        pwdView.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
        }
        pwdView.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.8)
        
        
        for i in 0..<4 {
            pwdTextFields[i].text = ""
            pwdView.addSubview(pwdTextFields[i])
            pwdTextFields[i].snp.makeConstraints { (makes) in
                makes.centerY.equalToSuperview().offset(-100)
                makes.left.equalToSuperview().offset(fullScreenSize.width/5 * CGFloat(i+1) - 25)
                makes.width.height.equalTo(50)
            }
        }
        
        let pwdTitleLabel = UILabel()
        pwdTitleLabel.textColor = .white
        pwdTitleLabel.text = "Input the Password :"
        pwdView.addSubview(pwdTitleLabel)
        pwdTitleLabel.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.bottom.equalTo(pwdTextFields[0].snp.top).offset(-20)
        }
        
        
        pwdBtns[1].setTitle("OK", for: .normal)
        pwdBtns[1].backgroundColor = .white
        pwdBtns[1].titleLabel?.textColor = .black
        pwdBtns[1].setTitleColor(.black, for: .normal)
        pwdView.addSubview(pwdBtns[1])
        pwdBtns[1].snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(pwdTextFields[0].snp.bottom).offset(20)
            makes.width.equalTo(80)
            makes.height.equalTo(40)
        }
        
        pwdBtns[0].setTitle("Cancel", for: .normal)
        pwdBtns[0].backgroundColor = .white
        pwdBtns[0].titleLabel?.textColor = .black
        pwdBtns[0].setTitleColor(.black, for: .normal)
        pwdView.addSubview(pwdBtns[0])
        pwdBtns[0].snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(pwdBtns[1].snp.bottom).offset(20)
            makes.width.equalTo(80)
            makes.height.equalTo(40)
        }
    }
}
