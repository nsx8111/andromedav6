//
//  HistoryData.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2020/2/10.
//  Copyright © 2020 蒼月喵. All rights reserved.
//

import Foundation

struct HistoryData {
    var productID: Int = 0
    var contentID: Int = 0
    var lastestPlayTime: String = ""
    var start: String = ""
    var end: String = ""
    var contentName: String = ""
    var contentDescription: String = ""
    var contentImage: [CI] = []
}

struct CI {
    var contentImageCategoryID: Int = 0
    var contentImageCategory: String = ""
    var url: String = ""
    var name: String = ""
    var contentImageDescription: String = ""
}

