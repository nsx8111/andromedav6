//
//  PackDetailViewController.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2019/5/23.
//  Copyright © 2019 蒼月喵. All rights reserved.
//

import UIKit
import MarqueeLabel
import WebKit
import AVKit
import Alamofire
import SwiftyJSON
import SnapKit
import SCLAlertView
import SwiftyStarRatingView

// 01/15
//class PackDetailViewController: UIViewController, VLCMediaPlayerDelegate, UITextFieldDelegate{
class PackDetailViewController: AuthVC {
    // 01/04 mark 掉，測試暫時沒問題，過一陣子可刪除
//    var packData: Pack = Pack()
    var packDataNew: ProductCDetail = ProductCDetail()
    
    // 08/20
    var moveSec : Int = 0
    let buttonSize = fullScreenSize.width*0.1
    let inset = fullScreenSize.width*0.022
    
    var labelTopHeight: CGFloat = 0.0
    
    var scrollView = UIScrollView()
    var containerView = UIView()
    //For 導演作者演員Label
    var containerViewA = UIView()
    var containerViewB = UIView()
    var containerViewC = UIView()
    var viewForVlc = UIView()
    
    // for 預告片
    var trailStreamUrl: String = ""
    // for 正片
    var streamUrl: String = ""
    // 0303 字幕
    var subtitleURL: String = ""
    // 10/23 字幕 Array
    var subtitleURLs: [String] = []
    /**
     記錄目前使用哪個字幕
     若是 = 0，代表關閉字幕，或是無字幕
     - Date: 10/29  Mike
     */
    var currentSub: Int = 0
    
    /** player */
    var vlcPlayer = VLCMediaPlayer()
    // 12/03
    var adPlayer = VLCMediaPlayer()
    /** UIView for player , put on viewForVlc */
    let playerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    /// UIView for Ad 11/26 Mike
    let adView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    /// skipADBtn 12/07
    var skipADBtn : UIButton = UIButton()
    // 12/10
    var countForSkipTimer : Timer?
//    /// 只放一個 skipButton 常常會時隱時現的，嘗試先放一層 uiview  12/10
//    var skipView = UIView()
    /// 記錄目前影片播放的狀態 12/08
    var currentPlayerState: playerState = .none
    /// 紀錄 Ad Dates 11/26 Mike
    var adDatas:[AdData]=[]
    
    /// 檢查是否設定過 AdBlock（只要設定一次） 11/27 Mike
    var isSetAds: Bool = false
    /// for check timeLabel length  11/27 Mike
    // 01/04 mark 掉，測試暫時沒問題，過一陣子可刪除
//    var timeInt: Int = 0
    /// 紀錄是否有播放權限
    var checkAuth : Bool = false
    
//    var testImageView = UIImageView()
    var posterImageView = UIImageView()
    var optionView = UIView()
    
    var returnButton = UIButton()
    var favoriteButton = UIButton()
    var originalFavorite : Bool = false
    var currentFavorite : Bool = false
    
    var rateButton = UIButton()
    var rateImage = UIButton()
    
    var imageLabel1 = UILabel()
    var imageLabel2 = UILabel()
    var rateLabel = UILabel()
    var favoriteLabel1 = UILabel()
    var favoriteLabel2 = UILabel()
    
    let starRatingView = SwiftyStarRatingView()
    var commentTextField = UITextField()
    
    let rentImage = UIImage(named: "rent")?.withRenderingMode(.alwaysTemplate)
    var rentButton = UIButton(type: .custom)
    var playOrBuyButton = UIButton()
    var trailerButton = UIButton()
    var refundButton = UIButton()
    
    /** Button for 字幕 🗓0304*/
    var srtBtn: UIButton = UIButton()
    /** Button for 全屏播放 🗓 */
    var allScreenBtn:UIButton = UIButton()
    //1121
    var closeBtn: UIButton = UIButton()
    
    var playerPlayBtn: UIButton = UIButton()
//    var isPlaying: Bool = false
    
    var videoAllTime: Int = 0
    var videoCurrentTime: Int = 0
    var currentProgress: Float = 0.0
    
    // 11/16
    var videoRemeberTime: Int = 0
    
    var statusValue: Int = 0
    var rentPlanID: Int = 0
    var rentCount: Int = 0
    var purchaseCount: Int = 0

    //08/20
    var srcPosition : Float = 0.0
    
    let videoRightTimeLabel: UILabel = {
        let label = UILabel()
        label.text = "00:00"
        label.textColor = .white
        // 11/27
        label.font = UIFont(name: "Helvetica", size: 14)
//        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textAlignment = .right
        return label
    }()
    
    let videoLeftTimeLabel: UILabel = {
        let label = UILabel()
        label.text = "00:00"
        label.textColor = .white
        // 11/27
        label.font = UIFont(name: "Helvetica", size: 14)
//        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textAlignment = .right
        return label
    }()
    
    let videoSlider: UISlider = {
        
        let slider = UISlider()
        slider.minimumTrackTintColor = .red
        slider.thumbTintColor = .red
        
        slider.addTarget(self, action: #selector(handleSliderChange), for: .valueChanged)
        slider.addTarget(self, action: #selector(handleSliderTD), for: .touchDown)
        slider.addTarget(self, action: #selector(handleSliderTUI), for: .touchUpInside)
        
        return slider
    }()
    
    /// 12/02 寫死 slider 高度用
    let sliderHeight:CGFloat = 20
    
    var littlePlayerConstraint: Constraint?
    var littlePlayerConstraint2: Constraint?
    var fullPlayerConstraint: Constraint?
    var fullPlayerConstraints: [Constraint] = []
    var constraint1 : Constraint?
    var constraint2 : Constraint?
    var constraint3 : Constraint?
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    // 點擊才會浮現
    let controlView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    // var nameLable = UILabel()
    var nameLable = MarqueeLabel.init(frame: CGRect(x: 0, y: 20, width: fullScreenSize.width, height: 30), duration: 8.0, fadeLength: 1.0)
    
    // var separationLineLabel = UILabel()
    
    let separationLineLabel: UILabel = {
        let sbl = UILabel()
        sbl.text = "|"
        sbl.font = UIFont(name: "Georgia-Bold", size: 20)
        sbl.textColor = .white
        return sbl
    }()
    let separationLineLabel2: UILabel = {
        let sbl = UILabel()
        sbl.text = "|"
        sbl.font = UIFont(name: "Georgia-Bold", size: 20)
        sbl.textColor = .white
        return sbl
    }()
    let separationLineLabel3: UILabel = {
        let sbl = UILabel()
        sbl.text = "|"
        sbl.font = UIFont(name: "Georgia-Bold", size: 20)
        sbl.textColor = .white
        return sbl
    }()
    
    var yearLabel = UILabel()
    var ratingLabel = UILabel()
    var viedoLengthLabel = UILabel()
    //    var starLabel = UILabel()
    
    var videoImgView = UIImageView()
    var descriptionTextView = UITextView()
    
    var directorLabel = UILabel()
    var writerLabel = UILabel()
    var heroLabel = UILabel()
    
    weak var ButtonDelegate: ButtonDelegate?
    var tmpText: String = ""
    
    //    /** 環形進度圈，用來表示讀取資料中 */
    //    var activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
    //    /** 放著環形進度圈的View，平時為 Hide 狀態，讀取時顯示出來在最上方 */
    //    let actView = UIView()
    
    /**
     紀錄這個 pack 是否有過 History 紀錄
     */
    var hadHistory : Bool = false
    
    /**
     紀錄是否需要 parents pwd
     */
    var ifNeedPcPwd : Bool = false
    
    // 01/18 move to AuthVC
//    /**
//     設定 For parents pwd 的 textFields
//     */
//    let pwdTextFields : [UITextField] = {
//        var pwdTextFields : [UITextField] = []
//        for i in 0..<4{
//            let tempTextField = UITextField()
//            tempTextField.backgroundColor = .white
//            // 10/27
//            tempTextField.textColor = .black
//
//            tempTextField.textAlignment = .center
//            tempTextField.keyboardType = .numberPad
//            //            tempTextField.delegate = self
//            pwdTextFields.append(tempTextField)
//        }
//        return pwdTextFields
//    }()
    
    // 01/18 move to AuthVC
//    /// 存放輸入的密碼 01/15   Mike
//    var tempPwd : String? = nil
    
    // 01/18 move to AuthVC
//    /// pwdView 的 OK Button
//    let okBtn : UIButton = {
//        let btn : UIButton = UIButton()
//        btn.addTarget(self, action: #selector(tapOKBtn(_:)), for: .touchDown)
//        return btn
//    }()
    
    // 01/18 move to AuthVC
//    let pwdBtns : [UIButton] = {
//        var tempBtns : [UIButton] = []
//        let cancelBtn = UIButton()
//        cancelBtn.addTarget(self, action: #selector(tapCancelBtn(_:)), for: .touchDown)
//        tempBtns.append(cancelBtn)
//        let okBtn = UIButton()
//        okBtn.addTarget(self, action: #selector(tapOKBtn(_:)), for: .touchDown)
//        tempBtns.append(okBtn)
//
//        return tempBtns
//    }()
    
    // 判斷密碼是否正確
    var isPwdOk : Bool = false
    /**
     存放評分相關資料
     - Date: 11/06  Mike
     */
    var reviewData = ReviewData()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        print("viewDidLoad @ PackDetail")
        self.resetSubArray()

        self.setActView(openOrClose: true)
        for tf in pwdTextFields{
            tf.delegate = self
        }
        // appDelegate.blockRotation = false
        
        // 11/25 add 雙擊功能
        let doubleGesture = UITapGestureRecognizer(target: self, action: #selector(self.doubleClickControlView))
        doubleGesture.numberOfTapsRequired = 2
        playerView.addGestureRecognizer(doubleGesture)
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.clickControlView))
        gesture.name = "name1"
        gesture.numberOfTapsRequired = 1
        gesture.require(toFail: doubleGesture)
        playerView.addGestureRecognizer(gesture)
        
        self.setViews()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTapGesture(_:)))
        let pan: UIScreenEdgePanGestureRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(self.handleEdgePanGesture(_:)))
        let pan2: UIScreenEdgePanGestureRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(self.handleEdgePanGesture2(_:)))
        view.addGestureRecognizer(tap)
        view.addGestureRecognizer(pan)
        view.addGestureRecognizer(pan2)
    }
    
    @objc func handleSliderTD(){
        self.vlcPlayer.pause()
        srcPosition = videoSlider.value
    }
    @objc func handleSliderTUI(){
        print("videoSlider.value@handleSliderTUI :\(videoSlider.value)")
        if moveSec > 0{
            self.vlcPlayer.jumpForward(Int32(moveSec))
            print("11/19 test moveSec:\(moveSec), Int32(moveSec):\(Int32(moveSec))")
        }else{
            self.vlcPlayer.jumpBackward(Int32(-moveSec))
        }
        self.vlcPlayer.play()
    }
    
    @objc func handleSliderChange(){
        // 11/16 mark
//        // 根据拖动比例计算开始到播放节点的总秒数
//        let allSec: Int = Int(Float(self.videoAllTime) * videoSlider.value)
//        // 根据当前播放秒数计算需要seek的秒数
//        let sec: Int = abs(allSec - self.videoCurrentTime)
        
        /// 拖曳決定前往的秒數
        let decSec : Int = Int(Float(self.videoAllTime) * videoSlider.value)
        let srcSec : Int = Int(Float(self.videoAllTime) * currentProgress)
        
        moveSec = decSec - srcSec
        self.videoLeftTimeLabel.text = timeIntToStr(timeInt: decSec)
    }
    /**
     可以偵測 touch 的位置
     Modify By : https://itisjoe.gitbooks.io/swiftgo/content/uikit/uigesturerecognizer.html
     - Date: 11/23  Mike
     */
    func findFingersPositon(recognizer:UITapGestureRecognizer) {
        // 取得每指的位置
        let number = recognizer.numberOfTouches
        for i in 0..<number {
            let point = recognizer.location(
                ofTouch: i, in: recognizer.view)
            print(
                "第 \(i + 1) 指的位置：\(NSCoder.string(for: point)) @findFingersPositon")
        }
    }
    
    func checkFingersPositon(recognizer:UITapGestureRecognizer) -> Bool{
        var ifRight : Bool = false
        // 取得每指的位置
        let number = recognizer.numberOfTouches
        for i in 0..<number {
            let point = recognizer.location(
                ofTouch: i, in: recognizer.view)
            print(
                "第 \(i + 1) 指的位置：\(NSCoder.string(for: point)) @checkFingersPositon")
            if point.x > self.playerView.frame.midX{
                ifRight = true
            }
        }
        return ifRight
    }
    /**
     雙擊 controlView 的處理
     
     - Date: 11/23  Mike
     */
    @objc func doubleClickControlView(_ sender:UITapGestureRecognizer){
        print("doubleClickControlView")
        // 11/25 keep play
        if self.vlcPlayer.state == .paused{
            self.vlcPlayer.play()
        }
        if sender.location(in: sender.view).x > self.playerView.frame.midX{
            //右側
            self.vlcPlayer.shortJumpForward()
        }else{
            self.vlcPlayer.shortJumpBackward()
        }
    }
    
    @objc func clickControlView(_ sender:UITapGestureRecognizer){
        // 11/23 test
        print("someAction")
        controlView.isHidden = false
        delayForHideControlView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if navigationController?.view.superview?.frame.origin.x == 0 {
            removeSubview()
        }
        resetSubArray()
        setActView(openOrClose: true)
        setViewValue{ [weak self] in
            guard let self = self else { return }
            if self.checkAuth == false{
                self.getPurchaseOption{
                    self.setActView(openOrClose: false)
                }
            }else{
                self.setActView(openOrClose: false)
            }
        }
//        print("packDataNew_ID \(packDataNew.product_id)")
        insertWatchVideoHistory{}
        
        let name = NSNotification.Name("getFavoriteStatus")
        NotificationCenter.default.addObserver(self, selector:#selector(getFavoriteStatus(noti:)), name: name, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.vlcPlayer.stop()
        self.openSrtView(openOrClose: false)
        self.posterImageView.isHidden = false
        self.optionView.isHidden = false
        self.controlView.isHidden = true
        
        littlePlayerConstraint?.activate()
        littlePlayerConstraint2?.activate()
        
        constraint1?.deactivate()
        constraint2?.deactivate()
        constraint3?.deactivate()
        
        // 12/08
        UIUtils.lockOrientation(.portrait, andRotateTo: .portrait)
        
//        sendHistory {
//            print("sendHistory@viewWillAppear")
//        }
        
        updateWatchVideoHistory{}
        
        // 12/08 01/20
        if currentPlayerState == .adPlayed || currentPlayerState == .adBuffering{
            print("12/08 check currentPlayerState when viewWillDisappear")
            self.vlcPlayer.stop()
            self.vlcPlayer.media = nil
            self.adPlayer.stop()
            self.adView.isHidden = true
            self.adPlayer.media = nil
            currentPlayerState = .none
        }
        // 12/10
        if countForSkipTimer != nil{
            countForSkipTimer?.invalidate()
        }
        
        self.tempPwd = nil
        
        if currentFavorite != originalFavorite{
            modifyFavoriteStatusDone()
        }
    }
    
    func modifyFavoriteStatusDone(){
        isFavoriteChanged = true
//        print("currentFavorite != originalFavorite @ viewWillDisappear")
        modifyFavoriteStatus(bool: currentFavorite){
//            print("modifyFavoriteStatus done")
        }
    }
    
    @objc func getFavoriteStatus(noti: Notification) {
        self.getMyFavoriteData(){
//            print("favCount4 \(favoriteLists.count)")
        }
    }
    
    @objc func backToFrontPage(_ sender: Any) {
        let name = Notification.Name("removeContentView")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        removeSubview()
        ButtonDelegate?.backPage()
    }
    /**
     檢查是否有過評分，以及評分為多少
     - Date: 11/06  Mike
     */
    func getPersonlReview(completion : @escaping ()->()) {
//        print("11/06 enter getPersonlReview")
        let params = [
            "product_id": self.packDataNew.product_id,
            "token":"\(token)"
        ] as [String : Any]
        
        let getPersonlReviewAPI = serverUrlNew + "product/getPersonlReview"
        
        AF.request(getPersonlReviewAPI, method: .post, parameters: params)
            .responseJSON{ (response) in
                if response.value != nil{
                    let swiftyJsonVar = JSON(response.value!)
                    let status: Int = swiftyJsonVar["status"].int!
                    
                    switch status {
                    case 200:
                        // 正確取得 Datas
                        let result = JSON(swiftyJsonVar["result"])
                        let personlReview = result["personl_review"].arrayObject
                        for r in personlReview!{
                            let rJson = JSON(r)
                            self.reviewData.reviewsId = rJson["reviews_id"].int!
                            self.reviewData.accountId = rJson["account_id"].int!
                            self.reviewData.productId = rJson["product_id"].int!
                            self.reviewData.comments = rJson["comments"].string!
                            self.reviewData.rating = CGFloat(rJson["rating"].float!)
                            self.reviewData.createdAt = rJson["created_at"].string!
                            self.reviewData.updatedAt = rJson["updated_at"].string!
                        }
                        completion()
                    case 204:
                        // 送出正確，不過沒有資料回傳
                        completion()
                    case 404:
                        // token error  Mike
                        self.backToLoginVC(title:"Logout",msg: swiftyJsonVar["message"].string!,okMsg: "Back to LoginPage.")
                        completion()
                    default:
                        print("error status")
                        completion()
                    }
                }
            }
//        completion()
    }
    /**
     取得我的最愛類別
     - Date: 10/15 Yang
     */
    func getMyFavoriteData(completion : @escaping ()->()) {
        let params = [
            "token":"\(token)"
        ] as [String : Any]
        
        favoriteLists.removeAll()
        let getMyFavoriteAPI = serverUrlNew + "account/getMyFavorite"

        AF.request(getMyFavoriteAPI, method: .post, parameters: params)
            .responseJSON{ (response) in
                if response.value != nil{
                    let swiftyJsonVar = JSON(response.value!)
                    let status: Int = swiftyJsonVar["status"].int!
                    if status == 200 { ///包含類別
                        if let resultObject = swiftyJsonVar["result"].dictionaryObject, let myFavoriteArray = resultObject["my_favorite"] {
                            print("AnyObject200 \((myFavoriteArray as AnyObject).count!)")
                        }
                        completion()
                    }else if status == 204 { ///沒有類別
                        print("get my favorite category success, but my favorite category is empty")
                        
                        if let resultObject = swiftyJsonVar["result"].dictionaryObject, let myFavoriteArray = resultObject["my_favorite"] {
                            print("AnyObject204 \((myFavoriteArray as AnyObject).count!)")
                        }
                        completion()
                    }else{
                        print("token ＆ get datas error")
                        self.backToLoginVC(title:"Logout",msg: swiftyJsonVar["message"].string!,okMsg: "Back to LoginPage.")
                        completion()
                    }
                }else{
                    print("response.result.isFalse @getMyFavoriteCategory")
                    favoriteLists = []
                    completion()
                }
                
            }
        
        
        
    }
    /**
     檢查這部片是否有 History 紀錄
     API 3.1.8 getWatchVideoHistory
     - Date: 08/17  Mike
     */
    func checkVideoHistory(completion : @escaping ()->()){
        let params = [
            "token":"\(token)"
        ] as [String : Any]
        
        let getWatchVideoHistoryAPI = serverUrlNew + "video/getWatchVideoHistory"

        AF.request(getWatchVideoHistoryAPI, method: .post, parameters: params)
            .responseJSON { (response) in
                if response.value != nil {
                    let swiftyJsonVar = JSON(response.value!)
                    let status: Int = swiftyJsonVar["status"].int!
                    if status == 200{
                        let historys = swiftyJsonVar["result"].arrayObject
                        for history in historys! {
                            let historyJson = JSON(history)
                            if historyJson["content_id"].int != nil{
                                // 此 pack.content_id 有紀錄
                                if historyJson["content_id"].int! == self.packDataNew.content_id{
                                    self.hadHistory = true
                                    
                                    var playTimeStr : String = "00:00:00"
                                    if historyJson["lastest_play_time"].string != nil{
                                        playTimeStr = historyJson["lastest_play_time"].string!
                                    }
                                    
                                    let tt = playTimeStr.components(separatedBy: ":")
                                    self.videoRemeberTime = Int(tt[0])!*3600 + Int(tt[1])!*60 + Int(tt[2])!
                                }
                            }
                        }
                    }else{
                        // 沒有 History 不用處理
                    }
                    
                }
                completion()
            }
        
        
    }
    /**
     插入(建立)影片播放記錄
     相關 API 為 3.3.5 insertWatchVideoHistory
     暫時不用此function
     - Date: 08/14  Mike
     */
    func sendHistory(completion : @escaping ()->()){
        // 11/13
        self.setActView(openOrClose: true)
        // 11/16 videoRemeberTime
//        print("self.videoRemeberTime:\(self.videoRemeberTime)")
        let lastest_play_time = String(self.videoRemeberTime/3600) + ":" + String(self.videoRemeberTime/60%60) + ":" + String(self.videoRemeberTime%60)
        print("lastest_play_time \(lastest_play_time)")
        // 11/13 mark
//        print(getTimeNow())
        
        var params = [
            "product_id": packDataNew.product_id,
            "content_id": packDataNew.content_id,
            "lastest_play_time" : lastest_play_time,
            "token":"\(token)"
        ] as [String : Any]
        
        // params["test"] = 321
        
        if hadHistory{
            params["end"] = self.getTimeNow()
            print("params_update : \(params)")
            let updateWatchVideoHistoryAPI = serverUrlNew + "video/updateWatchVideoHistory"
            
            AF.request(updateWatchVideoHistoryAPI, method: .post, parameters: params)
                .responseJSON { (response) in
                    if response.value != nil {
                        print("update_response \(response.value!)")
                    }
                    // 11/13
                    self.setActView(openOrClose: false)
                    completion()
                }
        }else{
            params["start"] = self.getTimeNow()
            print("params_insert : \(params)")
            let insertWatchVideoHistoryAPI = serverUrlNew + "video/insertWatchVideoHistory"
            
            AF.request(insertWatchVideoHistoryAPI, method: .post, parameters: params)
                .responseJSON { (response) in
                    if response.value != nil {
                        print("insert_response \(response.value!)")
                    }
                    // 11/13
                    self.setActView(openOrClose: false)
                    completion()
                }
            
        }
    }
    
    var params = [String : Any]()
    /**
     3.3.5 插入(建立)影片播放記錄(影片書籤)
     - Date: 02/23 Yang
     */
    func insertWatchVideoHistory(completion : @escaping ()->()){
        
        let lastest_play_time = String(self.videoRemeberTime/3600) + ":" + String(self.videoRemeberTime/60%60) + ":" + String(self.videoRemeberTime%60)
        print("lastest_play_time \(lastest_play_time)")

        params = [
            "product_id": packDataNew.product_id,
            "content_id": packDataNew.content_id,
            "lastest_play_time" : lastest_play_time,
            "token":"\(token)"
        ] as [String : Any]
        
        params["start"] = self.getTimeNow()
        print("params_insert : \(params)")
        let insertWatchVideoHistoryAPI = serverUrlNew + "video/insertWatchVideoHistory"
        
        AF.request(insertWatchVideoHistoryAPI, method: .post, parameters: params)
            .responseJSON { (response) in
                if response.value != nil {
                    print("insert_response \(response.value!)")
                }
                self.setActView(openOrClose: false)
                completion()
            }
    }
    /**
     3.3.6 更新影片播放記錄
     - Date: 02/23 Yang
     */
    func updateWatchVideoHistory(completion : @escaping ()->()){
        
        let lastest_play_time = String(self.videoRemeberTime/3600) + ":" + String(self.videoRemeberTime/60%60) + ":" + String(self.videoRemeberTime%60)

        if let idx = params.index(forKey: "start") {
            params.remove(at: idx)
        }
        
        params["lastest_play_time"] = lastest_play_time
        params["end"] = self.getTimeNow()
        print("params_update : \(params)")
        let updateWatchVideoHistoryAPI = serverUrlNew + "video/updateWatchVideoHistory"
        
        AF.request(updateWatchVideoHistoryAPI, method: .post, parameters: params)
            .responseJSON { (response) in
                if response.value != nil {
                    print("update_response \(response.value!)")
                }
                self.setActView(openOrClose: false)
                completion()
            }
    }
    /**
     新增或移除我的最愛
     */
    func modifyFavoriteStatus(bool:Bool,completion : @escaping ()->()){
        var addOrDelete : String = ""
        if bool{
            //新增到最愛
            addOrDelete = "addMyFavorite"
        }else{
            //從最愛移除
            addOrDelete = "deleteMyFavorite"
        }
        
        let params = [
            "product_id": self.packDataNew.product_id,
            "token":"\(token)"
        ] as [String : Any]
        //        print(params)
        
        AF.request("\(serverUrlNew)product/\(addOrDelete)", method: .post, parameters: params)
            .responseJSON { (response) in
                if response.value != nil {
                    //                    print(response.result.value!)
                    let swiftyJsonVar = JSON(response.value!)
                    let status: Int = swiftyJsonVar["status"].int!
//                    print("status:\(status)")
                    if status == 404{
                        // token error
                        // 11/10
                        self.backToLoginVC(title:"Logout",msg: swiftyJsonVar["message"].string!,okMsg: "Back to LoginPage.")
                    }else{
                        // token 正確
                        completion()
                    }
                }else{
                    print("modifyFavoriteStatus Error")
                    completion()
                }
                
            }
        
        
    }
    /**
     撈 AdDatas
     - Date: 12/17  Mike
     */
    func getVAST(completion : @escaping ()->()){
        let params = [
            "content_id": self.packDataNew.content_id,
            "token":"\(token)"
        ] as [String : Any]
        
        AF.request("\(serverUrlNew)advertisment/getVAST", method: .post, parameters: params)
            .responseJSON { (response) in
                if response.value != nil{
                    self.adDatas = []
                    let swiftyJsonVar = JSON(response.value!)
                    let status: Int = swiftyJsonVar["status"].int!
                    if status == 404{
                        // token error
                        self.backToLoginVC(title:"Logout",msg: swiftyJsonVar["message"].string!,okMsg: "Back to LoginPage.")
                        completion()
                    }else if status == 200{
                        // token 正確
                        let restltJson = JSON(swiftyJsonVar["result"])
                        let adsArray = restltJson["ads"].arrayObject
                        // print(adsArray?.count)
                        
                        for adsData in adsArray!{
                            let adsJSON = JSON(adsData)
                            var tempAdData = AdData()
                            tempAdData.xmlUrl = adsJSON["VAST"].string!
                            let config = adsJSON["ad_breaks_config"].int!
                            tempAdData.config = config
                            if config == 0{
                                let adBreaksString = adsJSON["ad_breaks"].string!
                                let playTime = self.timeStrToInt(timeStr: adBreaksString)
                                tempAdData.playTime = playTime
                            }
                            self.adDatas.append(tempAdData)
                        }
//                        print("12/25 self.adDatas.count:\(self.adDatas.count)")
                        var count: Int = 0
                        for i in 0..<self.adDatas.count{
                            self.getXmlData(index: i){
                                count += 1
                                if count == self.adDatas.count{
                                    completion()
                                }
                            }
                        }
                    }
                    
                }else{
                    self.adDatas = []
                    completion()
                }
                
            }
        
        
    }
    
    func isProductMyFavorite(completion : @escaping ()->()){
        let params = [
            "product_id": self.packDataNew.product_id,
            "token":"\(token)"
        ] as [String : Any]
        
        AF.request("\(serverUrlNew)product/isProductMyFavorite", method: .post, parameters: params)
            .responseJSON { (response) in
                if response.value != nil {
                    let swiftyJsonVar = JSON(response.value!)
                    let status: Int = swiftyJsonVar["status"].int!
                    if status == 404{
                        // token error
                        // 11/10
                        self.backToLoginVC(title:"Logout",msg: swiftyJsonVar["message"].string!,okMsg: "Back to LoginPage.")
                    }else if status == 200{
                        // token 正確
                        let restltJson = JSON(swiftyJsonVar["result"])
                        if restltJson["is_product_my_favorite"].bool != nil{
                            self.originalFavorite = restltJson["is_product_my_favorite"].bool!
                            self.currentFavorite = restltJson["is_product_my_favorite"].bool!
                        }
                        completion()
                    }else{
                        //正常不會來到這 除非 status 新增回傳值
                        print("error @ isProductMyFavorite")
                        completion()
                    }
                }else{
                    print("modifyFavoriteStatus Error")
                    completion()
                }
                
            }
        
        
        
    }
    
    // 10/29 修改
    func setPlayer(streamUrl: String){
        print("11/11 enter setPlayer")
        vlcPlayer.media = nil
        
        if streamUrl != ""{
            let streamUrl = URL(string: streamUrl)
            
            let media = VLCMedia(url: streamUrl!)
            
            vlcPlayer.media = media
            
            // 10/23 For 字幕切換
            // 10/29 以後 array 至少有一個為 ""
            // 10/30 簡化用一個 func 即可
            self.setSrt()
        }else{
            vlcPlayer.media = nil
            
        }
        
    }
    
    // 08/11 預告片沒有成功顯示
    func getTrailerHlsUrl(completion : @escaping ()->()){
        
        var url: String = ""
        // 08/11 應該是回傳的資料格式有改 1 -> 0
        if packDataNew.trailerHLS.count > 0 {
            url = packDataNew.trailerHLS[0].assets_id
            
            // 08/11 move to ifElse
            let params = [
                "assets_id": url,
                "token":"\(token)"
            ] as [String : Any]
//            print("params@getTrailerHlsUrl :\(params)")
            
            AF.request("\(serverUrlNew)video/playTrailerHLS", method: .post, parameters: params)
                .responseJSON { (response) in
                    if response.value != nil {
                        //                    print(response.result.value!)
                        let swiftyJsonVar = JSON(response.value!)
                        let status: Int = swiftyJsonVar["status"].int!
//                        print("status:\(status)")
                        if status == 200{
                            // token 正確, product_list not null
                            let restltJson = JSON(swiftyJsonVar["result"])
                            let trailer_hlsDatas = restltJson["trailer_hls"].arrayObject
                            
                            if Int(trailer_hlsDatas!.count) != 0{
                                let trailer_hlsJson = JSON(trailer_hlsDatas![0])
                                self.trailStreamUrl = trailer_hlsJson["url"].string!
                            }else{
                                self.trailStreamUrl = ""
                            }
                        }else if status == 204{
                            print("error 1 in getTrailerHlsUrl")
                        }else{
                            print("error 2 in getTrailerHlsUrl")
                        }
                        completion()
                    }else{
                        print("PackDetailVC.getTrailerHlsUrl Error")
                        completion()
                    }
                    
                }
        }else{
            // 01/19 mark
//            print("packDataNew.trailerHLS.count:\(packDataNew.trailerHLS.count)")
            completion()
        }
        
    }
    
    func checkAuthPlayVideo (completion : @escaping ()->()){
        let params = [
            "product_id" : packDataNew.product_id,
            "content_id" : packDataNew.content_id,
            "token":"\(token)"
        ] as [String : Any]
//        print("02/17 params:\(params)")
        // API 3.3.2
        AF.request("\(serverUrlNew)video/checkAuthPlayVideo", method: .post, parameters: params)
            .responseJSON { (response) in
                if response.value != nil {
                    // print(response.result.value!)
                    let swiftyJsonVar = JSON(response.value!)
                    let status: Int = swiftyJsonVar["status"].int!
//                    print("status:\(status)")
                    if status == 200{
                        print("有權限播放免費或已購買影片")
                        self.checkAuth = true
                    }else if status == 401{
                        print("沒權限觀看，需要購買")
                        self.checkAuth = false
                    }else{
                        print("error not in docs")
                    }
                    completion()
                }else{
                    print("送 checkAuthPlayVideo API 出現意外問題")
                    completion()
                }
                
            }
        
        
        
    }
    
    func playVideo (completion : @escaping ()->()){
        // 09/10 let -> var
        let params = [
            "product_id" : packDataNew.product_id,
            "content_id" : packDataNew.content_id,
            "video_format" : 13,
            "lock_password" : tempPwd!,
            "token":"\(token)"
        ] as [String : Any]
        
//        if self.ifNeedPcPwd {
//            // 01/15 改名為 tempPwd
//            params["lock_password"] = tempPwd
//            // 使用一次即設為 nil，避免重複使用
//            // 01/15 改名為 tempPwd
//            tempPwd = nil
//        }
        print("02/04 params:\(params)")
        isPwdOk = false
        // API 3.3.3 playVideo
        AF.request("\(serverUrlNew)video/playVideo", method: .post, parameters: params)
            .responseJSON { (response) in
                if response.value != nil {
                    let swiftyJsonVar = JSON(response.value!)
                    let status: Int = swiftyJsonVar["status"].int!
                    if status == 200{
                        self.isPwdOk = true
                        let restltJson = JSON(swiftyJsonVar["result"])
                        //                        print("restltJson:\(restltJson)")
                        let video_url = restltJson["video_url"].arrayObject
                        
                        //                    print("video_url?.count:\(video_url?.count)")
                        if video_url?.count != 0{
                            let video_urlJson = JSON(video_url![0])
                            self.streamUrl = video_urlJson["url"].string!
                        }else{
                            self.alertMsg(msg: "streamUrl error")
                        }
                        
                        // 08/03 subtitle 這個 key 值被移除了
                        // 10/23 subtitle -> subtitle_url
                        // 03/03
                        let subtitle = restltJson["subtitle_url"].arrayObject
                        
                        self.resetSubArray()
                        
                        if subtitle?.count != 0{
                            // 10/23
                            for sub in subtitle! {
                                let subtitle_urlJson = JSON(sub)
                                self.subtitleURLs.append(subtitle_urlJson["url"].string!)
                            }
                            // 10/30
                            self.currentSub = 1
                        }else{
                            // 10/30
                            self.currentSub = 0
                        }
                        
                    }else if status == 401{
                        print("error in playVideo()")
                        // 10/27
                        self.streamUrl = ""
                    }else if status == 403{
                        // 10/27
                        /**
                         0115 如果是 FaceID or TouchID 解鎖，回傳 403，代表有另一台裝置改了密碼
                         - TODO:另外處理
                         */
                        self.streamUrl = ""
                        print("need parent control password")
//                        self.alertMsg2(msg: "Password Error")
                        self.alertMsg2(msg: "API 有問題，修復中")
                    }else{
                        print("error not in docs")
                    }
                    completion()
                }else{
                    print("送 playVideo API 出現意外問題")
                    completion()
                }
            }
        
    }
    
    func setAfterServer(){
        setActView(openOrClose: true)
        isProductMyFavorite{ [weak self] in
            guard let self = self else { return }
            self.setFavoriteButtonImage()
            self.checkAuthPlayVideo{ [weak self] in
                guard let self = self else { return }
                // 08/11
                let hadTrailer : Bool = (self.trailStreamUrl == "" ? false : true)
                self.trailerButton.isHidden = !hadTrailer
                
                if self.checkAuth {
                    //有權限
                    self.rentButton.isHidden = true
                    self.playOrBuyButton.isHidden = false
                    self.playOrBuyButton.setImage(UIImage(named: "play"), for: .normal)
                    self.refundButton.isHidden = false
                }else{
                    //無權限
                    self.rentButton.isHidden = false
                    self.playOrBuyButton.isHidden = false
                    self.playOrBuyButton.setImage(UIImage(named: "buy"), for: .normal)
                    self.refundButton.isHidden = true
                }
                // 12/17
                self.getVAST {
                    self.getPersonlReview {
                        self.setActView(openOrClose: false)
                    }
                }
            }
        }
    }
    
    //    func reSetData
    
    func setViews(){
        // 10/06
        view.addSubview(viewForVlc)
        viewForVlc.backgroundColor = .black
        viewForVlc.snp.makeConstraints { (makes) in
            littlePlayerConstraint = makes.top.left.right.equalToSuperview().constraint
            littlePlayerConstraint2 = makes.height.equalTo(fullScreenSize.width/16*9).constraint
            // 09/28
            constraint1 = makes.bottom.left.equalTo(self.view).constraint
            constraint2 = makes.width.equalTo(fullScreenSize.height).constraint
            constraint3 = makes.height.equalTo(fullScreenSize.width).constraint
        }
        littlePlayerConstraint?.activate()
        littlePlayerConstraint2?.activate()
        
        constraint1?.deactivate()
        constraint2?.deactivate()
        constraint3?.deactivate()
        
        // add scrollView
        view.addSubview(scrollView)
        scrollView.backgroundColor = .cellBgColor
        scrollView.bounces = false
        scrollView.snp.makeConstraints { (makes) in
            makes.top.equalTo(viewForVlc.snp.bottom)
            makes.left.right.bottom.equalToSuperview()
        }
        
        // add containerView
        scrollView.addSubview(containerView)
        containerView.backgroundColor = .cellBgColor
        containerView.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
            makes.width.equalTo(view.bounds.width)
        }
        
        // add vlcView
        viewForVlc.addSubview(playerView)
        playerView.backgroundColor = UIColor.black
        playerView.snp.makeConstraints { (makes) in
            makes.edges.equalTo(viewForVlc)
        }
        
        vlcPlayer.delegate = self
        vlcPlayer.drawable = playerView
        
        //10/14 10/12
        vlcPlayer.addObserver(self, forKeyPath: "remainingTime", options: .new, context: nil)
        // 11/20 mark isPlaying observer 裡面沒有用到
//        vlcPlayer.addObserver(self, forKeyPath: "isPlaying", options: .new, context: nil)
        
        // 01/05
//        self.getTrailerHlsUrl(){
//        }
        
        posterImageView = UIImageView(image:UIImage(named:"noContentImage"))
        // 11/13
        posterImageView.backgroundColor = .black
        viewForVlc.addSubview(posterImageView)
        posterImageView.snp.makeConstraints { (makes) in
            makes.edges.equalTo(viewForVlc)
        }
        
        //1113
        // 10/06
        viewForVlc.addSubview(controlView)
        controlView.backgroundColor = .clear
        // 12/02
        controlView.isHidden = true
        // 11/23 add 雙擊功能
        let doubleGesture = UITapGestureRecognizer(target: self, action: #selector(self.doubleClickControlView))
        doubleGesture.numberOfTapsRequired = 2
        controlView.addGestureRecognizer(doubleGesture)
        controlView.snp.makeConstraints { (makes) in
            makes.edges.equalTo(viewForVlc)
        }
        
        controlView.addSubview(allScreenBtn)
        allScreenBtn.setImage(UIImage(named: "FullScreen"), for: .normal)
        allScreenBtn.backgroundColor = .clear
        allScreenBtn.addTarget(self, action:#selector(tappedAllScreenBtn), for:.touchUpInside)
        allScreenBtn.snp.makeConstraints { (makes) in
            makes.width.height.equalTo(20)
            makes.right.bottom.equalTo(viewForVlc).offset(-10)
        }
        
        //0304
        controlView.addSubview(srtBtn)
        self.setSrtBtnImage()
        
        srtBtn.backgroundColor = .clear
        // 10/23
        srtBtn.addTarget(self, action:#selector(tappedSrtBtn), for:.touchUpInside)
        srtBtn.snp.makeConstraints { (makes) in
            makes.width.height.equalTo(20)
            makes.right.equalTo(allScreenBtn.snp.left).offset(-10)
            makes.centerY.equalTo(allScreenBtn)
        }
        
        //1121
        controlView.addSubview(closeBtn)
        closeBtn.setImage(UIImage(named:"playerClose"), for: .normal)
        closeBtn.backgroundColor = .clear
        closeBtn.addTarget(self, action:#selector(tappedCloseBtn), for:.touchUpInside)
        closeBtn.snp.makeConstraints { (makes) in
            makes.width.height.equalTo(20)
            makes.right.equalTo(viewForVlc).offset(-10)
            makes.top.equalTo(viewForVlc).offset(10)
        }
        
        controlView.addSubview(playerPlayBtn)
        playerPlayBtn.setImage(UIImage(named:"playerPlay"), for: .normal)
        playerPlayBtn.backgroundColor = .clear
        playerPlayBtn.addTarget(self, action:#selector(tappedPlay), for:.touchUpInside)
        playerPlayBtn.snp.makeConstraints { (makes) in
            makes.width.height.equalTo(20)
            makes.bottom.equalTo(viewForVlc).offset(-10)
            makes.left.equalTo(viewForVlc).offset(10)
        }
        
        // 11/24 Add 快進/退
        // player_shortJumpForward / player_shortJumpBackward
        let jfBtn:UIImageView = UIImageView(image: UIImage(named: "player_shortJumpForward"))
        jfBtn.addTapGestureRecognizer {
            if self.vlcPlayer.state == .paused{
                self.vlcPlayer.play()
            }
            self.vlcPlayer.shortJumpForward()
        }
        controlView.addSubview(jfBtn)
        jfBtn.snp.makeConstraints { (makes) in
            makes.width.height.equalTo(20)
            makes.right.equalTo(srtBtn.snp.left).offset(-10)
            makes.centerY.equalTo(playerPlayBtn)
        }
        
        let jbBtn:UIImageView = UIImageView(image: UIImage(named: "player_shortJumpBackward"))
        jbBtn.addTapGestureRecognizer {
            if self.vlcPlayer.state == .paused{
                self.vlcPlayer.play()
            }
            self.vlcPlayer.shortJumpBackward()
        }
        controlView.addSubview(jbBtn)
        jbBtn.snp.makeConstraints { (makes) in
            makes.width.height.equalTo(20)
            makes.left.equalTo(playerPlayBtn.snp.right).offset(10)
            makes.centerY.equalTo(playerPlayBtn)
        }
        
        controlView.addSubview(videoLeftTimeLabel)
        videoLeftTimeLabel.snp.makeConstraints { (makes) in
            makes.left.equalTo(jbBtn.snp.right).offset(10)
            makes.centerY.equalTo(playerPlayBtn)
        }
        
        controlView.addSubview(videoRightTimeLabel)
        videoRightTimeLabel.snp.makeConstraints { (makes) in
            makes.right.equalTo(jfBtn.snp.left).offset(-10)
            makes.centerY.equalTo(playerPlayBtn)
        }
        
        controlView.addSubview(videoSlider)
        
        videoSlider.snp.makeConstraints { (makes) in
            makes.left.equalTo(videoLeftTimeLabel.snp.right).offset(4)
            makes.right.equalTo(videoRightTimeLabel.snp.left).offset(-4)
            makes.centerY.equalTo(playerPlayBtn)
            // 12/02
            makes.height.equalTo(sliderHeight)
        }
        
        let renderer = UIGraphicsImageRenderer(size: CGSize(width: 4, height: sliderHeight))
        let thumbImage = renderer.image { ctx in
            ctx.cgContext.setStrokeColor(UIColor.red.cgColor)
            ctx.cgContext.setLineWidth(4)

            let rectangle = CGRect(x: 0, y: 0, width: 4, height: sliderHeight)
            ctx.cgContext.addRect(rectangle)
            ctx.cgContext.drawPath(using: .fillStroke)
        }
        videoSlider.setThumbImage(thumbImage, for: .normal)
        videoSlider.setThumbImage(thumbImage, for: .highlighted)
        
        //1114
        controlView.isHidden = true
        
        // add optionView
        viewForVlc.addSubview(optionView)
        optionView.backgroundColor = .clear
        
        optionView.snp.makeConstraints { (makes) in
            makes.edges.equalTo(viewForVlc)
        }
        
        // 12/03 add AD View
        viewForVlc.addSubview(adView)
        adView.backgroundColor = .black
        adView.snp.makeConstraints { (makes) in
            makes.edges.equalTo(viewForVlc)
        }
        
        adPlayer.delegate = self
        adPlayer.drawable = adView
        adView.isHidden = true
        
        // 12/07
        skipADBtn.backgroundColor = .yellow
        skipADBtn.setTitleColor(.black, for: .normal)
        skipADBtn.addTapGestureRecognizer {
            // 12/10
            if self.skipADBtn.titleLabel?.text == "Skip"{
                self.adPlayer.stop()
            }
        }
        // 12/10
        adView.addSubview(skipADBtn)
        skipADBtn.snp.makeConstraints { (makes) in
            makes.right.equalToSuperview()
            makes.bottom.equalToSuperview().offset(-10)
            makes.width.equalTo(40)
            makes.height.equalTo(20)
        }
        
        // add nameLable
        containerView.addSubview(nameLable)
        //for test
        nameLable.text = "Viedo name"
        nameLable.textColor = .white
        nameLable.font = UIFont(name: "Georgia-Bold", size: 30)
        
        nameLable.snp.makeConstraints { (makes) in
            // 10/06
            //            makes.top.equalTo(viewForVlc.snp.bottom).offset(10)
            makes.top.equalToSuperview().offset(10)
            makes.left.equalToSuperview().offset(20)
            makes.right.equalToSuperview().offset(-20)
        }
        
        // add yearLabel
        containerView.addSubview(yearLabel)
        yearLabel.text = " - "
        yearLabel.textColor = .white
        yearLabel.font = UIFont(name: "Georgia-Bold", size: 20)
        
        yearLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(nameLable.snp.bottom).offset(10)
            makes.left.equalToSuperview().offset(15)
        }
        
        // add separationLineLabel
        containerView.addSubview(separationLineLabel)
        
        separationLineLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(yearLabel)
            makes.left.equalTo(yearLabel.snp.right).offset(10)
        }
        
        // add ratingLabel
        containerView.addSubview(ratingLabel)
        ratingLabel.text = " - "
        ratingLabel.textColor = .white
        ratingLabel.font = UIFont(name: "Georgia-Bold", size: 20)
        
        ratingLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(yearLabel)
            makes.left.equalTo(separationLineLabel.snp.right).offset(10)
        }
        
        // add separationLineLabel 2
        containerView.addSubview(separationLineLabel2)
        separationLineLabel2.snp.makeConstraints { (makes) in
            makes.top.equalTo(yearLabel)
            makes.left.equalTo(ratingLabel.snp.right).offset(10)
        }
        
        // add viedoLengthLabel
        containerView.addSubview(viedoLengthLabel)
        viedoLengthLabel.text = "1:17:22"
        viedoLengthLabel.textColor = .white
        viedoLengthLabel.font = UIFont(name: "Georgia-Bold", size: 20)
        
        viedoLengthLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(yearLabel)
            makes.left.equalTo(separationLineLabel2.snp.right).offset(10)
        }
        
        //0816 move button down
        // add favoriteButton
        containerView.addSubview(favoriteButton)
        favoriteButton.backgroundColor = .clear
        setFavoriteButtonImage()
        favoriteButton.isEnabled = true
        favoriteButton.addTarget(self, action: #selector(self.clickFavorite), for: .touchUpInside)
        favoriteButton.imageEdgeInsets = UIEdgeInsets(top: inset,left: inset,bottom: inset,right: inset)
        favoriteButton.layer.cornerRadius = buttonSize/2
        favoriteButton.clipsToBounds = true
        favoriteButton.snp.makeConstraints { (makes) in
            makes.width.height.equalTo(buttonSize)
            makes.centerX.equalTo(view.bounds.width*5/6)
            makes.top.equalTo(yearLabel.snp.bottom).offset(20)
        }
        
        // add rateButton
        containerView.addSubview(rateButton)
        rateButton.backgroundColor = .clear
        rateButton.setImage(UIImage(named: "rating"), for: .normal)
        rateButton.isEnabled = true
        rateButton.addTarget(self, action: #selector(self.clickRate), for: .touchUpInside)
        rateButton.imageEdgeInsets = UIEdgeInsets(top: inset,left: inset,bottom: inset,right: inset)
        rateButton.layer.cornerRadius = buttonSize/2
        rateButton.clipsToBounds = true
        
        rateButton.snp.makeConstraints { (makes) in
            makes.width.height.equalTo(buttonSize)
            makes.centerX.equalTo(view.bounds.width/2)
            makes.top.equalTo(favoriteButton)
        }
        
        //0225 add rateImage
        containerView.addSubview(rateImage)
        rateImage.backgroundColor = .clear
        rateImage.setImage(UIImage(named: "ratingOn"), for: .normal)
        rateImage.isEnabled = true
        rateImage.adjustsImageWhenHighlighted = false
        rateImage.addTarget(self, action: #selector(self.clickRateImage), for: .touchUpInside)
        rateImage.imageEdgeInsets = UIEdgeInsets(top: inset,left: inset,bottom: inset,right: inset)
        rateImage.layer.cornerRadius = buttonSize/2
        rateImage.clipsToBounds = true
        
        rateImage.snp.makeConstraints { (makes) in
            makes.width.height.equalTo(buttonSize)
            makes.centerX.equalTo(view.bounds.width/6)
            makes.top.equalTo(favoriteButton)
        }
        
        // 0225 三個 Buttons 下方的 Label
        containerView.addSubview(imageLabel1)
        imageLabel1.font = UIFont(name: "Georgia-Bold", size: 20)
        // 11/11
        imageLabel1.text = "\(Float(Int(self.packDataNew.rating_avg_rating * 100))/100)/5"
        imageLabel1.textColor = .white
        imageLabel1.backgroundColor = .clear
        imageLabel1.snp.makeConstraints { (makes) in
            makes.centerX.equalTo(rateImage)
            makes.top.equalTo(favoriteButton.snp.bottom).offset(7-inset)
        }
        
        containerView.addSubview(imageLabel2)
        imageLabel2.font = UIFont(name: "Georgia-Bold", size: 20)
        imageLabel2.text = "\(self.packDataNew.rating_total_rating)"
        imageLabel2.textColor = .white
        imageLabel2.backgroundColor = .clear
        imageLabel2.snp.makeConstraints { (makes) in
            makes.centerX.equalTo(rateImage)
            makes.top.equalTo(imageLabel1.snp.bottom).offset(5)
        }
        
        containerView.addSubview(rateLabel)
        rateLabel.font = UIFont(name: "Georgia-Bold", size: 20)
        rateLabel.text = "Rate This"
        rateLabel.textColor = .white
        rateLabel.backgroundColor = .clear
        rateLabel.snp.makeConstraints { (makes) in
            makes.centerX.equalTo(rateButton)
            makes.centerY.equalTo(imageLabel1)
        }
        
        containerView.addSubview(favoriteLabel1)
        favoriteLabel1.font = UIFont(name: "Georgia-Bold", size: 20)
        favoriteLabel1.text = "Add to"
        favoriteLabel1.textColor = .white
        favoriteLabel1.backgroundColor = .clear
        favoriteLabel1.snp.makeConstraints { (makes) in
            makes.centerX.equalTo(favoriteButton)
            makes.centerY.equalTo(imageLabel1)
        }
        
        containerView.addSubview(favoriteLabel2)
        favoriteLabel2.font = UIFont(name: "Georgia-Bold", size: 20)
        favoriteLabel2.text = "Favorite"
        favoriteLabel2.textColor = .white
        favoriteLabel2.backgroundColor = .clear
        favoriteLabel2.snp.makeConstraints { (makes) in
            makes.centerX.equalTo(favoriteButton)
            makes.centerY.equalTo(imageLabel2)
        }
        
        // add viedoImgView
        containerView.addSubview(videoImgView)
        //        viedoImgView = UIImageView(image:UIImage(named:"testImg"))
        videoImgView.image = UIImage(named: "noContentImage")
        
        videoImgView.snp.makeConstraints { (makes) in
            //            makes.top.equalTo(favoriteButton.snp.bottom).offset(20)
            makes.top.equalTo(imageLabel2.snp.bottom).offset(15)
            makes.left.equalTo(yearLabel)
            makes.width.equalTo(fullScreenSize.width/3)
            makes.height.equalTo(fullScreenSize.width/3*205/144)
        }
        
        // add descriptionTextView
        containerView.addSubview(descriptionTextView)
        descriptionTextView.text = "ghds geauhgla.hbdh,fhal,hfba,hfjvbsh,fgab ,f akfhergfalgs  haflufgragfgbflabcflacvla hgaflh;ulh hflaudshg aahgla  lur aghlhvldsuhlgal  luhfuhaflc hfal "
        descriptionTextView.font = UIFont(name: "Georgia-Bold", size: 20)
        descriptionTextView.textColor = .white
        descriptionTextView.backgroundColor = .clear
        descriptionTextView.isScrollEnabled = true
        descriptionTextView.isEditable = false
        descriptionTextView.isSelectable = false
        descriptionTextView.snp.makeConstraints { (makes) in
            makes.left.equalTo(videoImgView.snp.right).offset(10)
            makes.right.equalToSuperview().offset(-10)
            makes.top.bottom.equalTo(videoImgView)
        }
        
        //add 3 containerView for 下方三欄位
        containerView.addSubview(containerViewA)
        containerViewA.snp.makeConstraints { (makes) in
            makes.left.right.equalToSuperview()
            makes.top.equalTo(videoImgView.snp.bottom)
        }
        containerView.addSubview(containerViewB)
        containerViewB.snp.makeConstraints { (makes) in
            makes.left.right.equalToSuperview()
            makes.top.equalTo(containerViewA.snp.bottom)
        }
        containerView.addSubview(containerViewC)
        containerViewC.snp.makeConstraints { (makes) in
            makes.left.right.equalToSuperview()
            makes.top.equalTo(containerViewB.snp.bottom)
            makes.bottom.equalToSuperview()
        }
        
        // add directorLabel
        containerViewA.addSubview(directorLabel)
        directorLabel.text = "導演 ："
        //0103
        directorLabel.tag = 200
        directorLabel.textColor = .white
        directorLabel.font = UIFont(name: "Georgia-Bold", size: 20)
        directorLabel.numberOfLines = 20
        
        directorLabel.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(20)
            makes.left.equalToSuperview().offset(15)
            makes.bottom.equalToSuperview().offset(-20)
        }
        
        // add directorLabel
        containerViewB.addSubview(writerLabel)
        writerLabel.text = "作者 ："
        //0103
        writerLabel.tag = 200
        writerLabel.textColor = .white
        writerLabel.font = UIFont(name: "Georgia-Bold", size: 20)
        writerLabel.numberOfLines = 20
        
        writerLabel.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(20)
            makes.left.equalToSuperview().offset(15)
            makes.bottom.equalToSuperview().offset(-20)
        }
        
        // add directorLabel
        containerViewC.addSubview(heroLabel)
        heroLabel.text = "演員 ："
        //0103
        heroLabel.tag = 200
        heroLabel.textColor = .white
        heroLabel.font = UIFont(name: "Georgia-Bold", size: 20)
        heroLabel.numberOfLines = 20
        
        heroLabel.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(20)
            makes.left.equalToSuperview().offset(15)
            makes.bottom.equalToSuperview().offset(-20)
        }
        
        // add playOrBuyButton
        optionView.addSubview(playOrBuyButton)
        playOrBuyButton.backgroundColor = .clear
        playOrBuyButton.setImage(UIImage(named: "buy"), for: .normal)
        playOrBuyButton.isEnabled = true
        playOrBuyButton.addTarget(self, action: #selector(self.clickPlayOrBuy), for: .touchUpInside)
        playOrBuyButton.imageEdgeInsets = UIEdgeInsets(top: inset,left: inset,bottom: inset,right: inset)
        playOrBuyButton.layer.cornerRadius = buttonSize/2
        playOrBuyButton.clipsToBounds = true
        playOrBuyButton.snp.makeConstraints { (makes) in
            makes.width.height.equalTo(buttonSize*1.5)
            makes.centerX.equalTo(view.bounds.width/6*3)
            makes.centerY.equalTo(optionView)
        }
        
        // add rentButton
        optionView.addSubview(rentButton)
        rentButton.backgroundColor = .clear
        rentButton.tintColor = .white
        rentButton.setImage(rentImage, for: .normal)
        rentButton.isEnabled = true
        rentButton.addTarget(self, action: #selector(self.clickPlayOrRent), for: .touchUpInside)
        rentButton.imageEdgeInsets = UIEdgeInsets(top: inset,left: inset,bottom: inset,right: inset)
        rentButton.layer.cornerRadius = buttonSize/2
        rentButton.clipsToBounds = true
        rentButton.snp.makeConstraints { (makes) in
            makes.width.height.equalTo(buttonSize*1.9)
            makes.centerX.equalTo(view.bounds.width/6*3)
            makes.top.equalTo(playOrBuyButton.snp.bottom).offset(offset0)
//            makes.bottom.equalToSuperview().offset(-offset0)
        }
        
        // add playOrBuyButton
        optionView.addSubview(trailerButton)
        trailerButton.backgroundColor = .clear
        trailerButton.setImage(UIImage(named: "trailer"), for: .normal)
        trailerButton.isEnabled = true
        trailerButton.addTarget(self, action: #selector(self.clickTrailer), for: .touchUpInside)
        trailerButton.imageEdgeInsets = UIEdgeInsets(top: inset,left: inset,bottom: inset,right: inset)
        trailerButton.layer.cornerRadius = buttonSize/2
        trailerButton.clipsToBounds = true
        
        trailerButton.snp.makeConstraints { (makes) in
            makes.width.height.equalTo(buttonSize*1.5)
            makes.centerX.equalTo(view.bounds.width/6*1)
            makes.centerY.equalTo(optionView)
        }
        
        // add playOrBuyButton
        optionView.addSubview(refundButton)
        refundButton.backgroundColor = .clear
        refundButton.setImage(UIImage(named: "refund"), for: .normal)
        refundButton.isEnabled = true
        refundButton.addTarget(self, action: #selector(self.clickRefund), for: .touchUpInside)
        refundButton.imageEdgeInsets = UIEdgeInsets(top: inset,left: inset,bottom: inset,right: inset)
        refundButton.layer.cornerRadius = buttonSize/2
        refundButton.clipsToBounds = true
        refundButton.isHidden = true
        
        refundButton.snp.makeConstraints { (makes) in
            makes.width.height.equalTo(buttonSize*1.5)
            makes.centerX.equalTo(view.bounds.width/6*5)
            makes.centerY.equalTo(optionView)
        }
        
        // add returnButton
        optionView.addSubview(returnButton)
        
        returnButton.backgroundColor = .clear
        returnButton.setImage(UIImage(named: "return"), for: .normal)
        returnButton.isEnabled = true
        returnButton.addTarget(self, action: #selector(self.backToFrontPage(_:)), for: .touchUpInside)
        
        returnButton.snp.makeConstraints { (makes) in
            makes.width.height.equalTo(buttonSize)
            makes.top.right.equalToSuperview()
        }
        self.view.bringSubviewToFront(viewForVlc)
    }
//    func setViewValue(){
    func setViewValue(completion : @escaping ()->()){
        // 08/17 初始化用
        self.videoCurrentTime = 0
        // 11/16 videoRemeberTime
        self.videoRemeberTime = 0
        // 12/29
        currentPlayerState = .none
        
        self.hadHistory = false
        // 11/06 初始化用
        self.reviewData = ReviewData()
        self.streamUrl = ""
        
        self.rentButton.isHidden = true
        self.playOrBuyButton.isHidden = true
        self.trailerButton.isHidden = true
        
        //1204 暫時這樣判斷是否購買 p.s. 都先當作沒有購買
        if self.refundButton.isHidden ==  false{
            self.refundButton.isHidden = true
            self.playOrBuyButton.setImage(UIImage(named: "buy"), for: .normal)
        }
        
        for subView in self.containerView.subviews{
            if subView.tag == 200{
                subView.removeFromSuperview()
            }
        }
        for subView in self.containerViewA.subviews{
            if subView.tag == 200{
                subView.removeFromSuperview()
            }
        }
        for subView in self.containerViewB.subviews{
            if subView.tag == 200{
                subView.removeFromSuperview()
            }
        }
        for subView in self.containerViewC.subviews{
            if subView.tag == 200{
                subView.removeFromSuperview()
            }
        }
        
        self.setAfterServer()
        
        // New API
        self.nameLable.text = packDataNew.content_name
        if self.nameLable.intrinsicContentSize.width > (fullScreenSize.width - 20*2){
            self.nameLable.text = packDataNew.content_name + "       "
        }
        
        let yearArray : [ContentAttributeValue] = packDataNew.getValueArray(attribute_id: 17)
        if yearArray.count > 0{
            yearLabel.text = yearArray[0].attribute_value
        }else{
            yearLabel.text = " - "
        }
        
        let ratingArray : [ContentAttributeValue] = packDataNew.getValueArray(attribute_id: 5)
        if ratingArray.count > 0{
            ratingLabel.text = ratingArray[0].attribute_value
        }else{
            ratingLabel.text = " - "
        }
        
        self.descriptionTextView.text = packDataNew.content_description
        
        if packDataNew.posters.count == 2{
            if packDataNew.posters[0].url == ""{
                self.posterImageView.image = UIImage(named: "noContentImage")
            }else{
                self.posterImageView.downloaded(from: packDataNew.posters[0].url)
            }
            if packDataNew.posters[1].url == ""{
                self.videoImgView.image = UIImage(named: "noContentImage")
            }else{
                self.videoImgView.downloaded(from: packDataNew.posters[1].url)
            }
        }else if packDataNew.posters.count == 1{
            // 正常來說不會進入這邊 先放著當備案 release 版本可以移除 #release
            print("packDataNew.posters.count == 1")
            self.posterImageView.downloaded(from: packDataNew.posters[0].url)
            self.videoImgView.downloaded(from: packDataNew.posters[0].url)
        }else if packDataNew.posters.count == 0{
            // 正常來說不會進入這邊 先放著當備案 release 版本可以移除 #release
            print("packDataNew.posters.count == 0")
            // 08/10 這邊有問題  Done
            self.posterImageView.image = UIImage(named: "noContentImage")
            self.videoImgView.image = UIImage(named: "noContentImage")
        }
        
        self.imageLabel1.text =
            "\(Float(Int(self.packDataNew.rating_avg_rating * 100))/100)/5"
        // Mike
        self.imageLabel2.text = "\(Int(self.packDataNew.rating_total_rating))👤"
        
        // 解析 Andromeda.ContentAttributeValue
        // 導演
        let directorArray: [ContentAttributeValue] = packDataNew.getValueArray(attribute_id: 3)
        
        containerViewA.addSubview(directorLabel)
        containerViewB.addSubview(writerLabel)
        containerViewC.addSubview(heroLabel)
        
        if directorArray.count < 1 {
            self.directorLabel.text = "導演 ："
            
            directorLabel.snp.makeConstraints { (makes) in
                makes.top.equalToSuperview().offset(20)
                makes.left.equalToSuperview().offset(15)
                makes.bottom.equalToSuperview()
            }
            
        }else{
            //            self.directorLabel.text = ""
            self.directorLabel.text = "導演 ："
            
            directorLabel.snp.makeConstraints { (makes) in
                makes.top.equalToSuperview().offset(20)
                makes.left.equalToSuperview().offset(15)
            }
            
            for index in 0 ..< directorArray.count{
                let dLabel = MarqueeLabel.init(frame: CGRect(x: 0, y: 20, width: fullScreenSize.width, height: 30), duration: 8.0, fadeLength: 1.0)
                containerViewA.addSubview(dLabel)
                dLabel.text = directorArray[index].attribute_value
                dLabel.textColor = .red
                dLabel.tag = 200 // tag = 200 -> remove it when viewDidAppear begin
                dLabel.font = UIFont(name: "Georgia-Bold", size: 20)
                dLabel.numberOfLines = 0
                dLabel.tappable = true
                dLabel.callback = {
                    if dLabel.textColor != .yellow{
                        dLabel.textColor = .yellow
                    }else{
                        dLabel.textColor = .red
                    }
                    
                    print("attribute_value_id:\(directorArray[index].attribute_value_id)")
                }
                if index == (directorArray.count - 1){
                    dLabel.snp.makeConstraints { (makes) in
                        makes.left.equalTo(self.directorLabel.snp.right)
                        makes.top.equalTo(self.directorLabel.snp.top).offset(CGFloat(index) * self.directorLabel.frame.height)
                        
                        makes.width.equalTo(fullScreenSize.width - 15 - self.directorLabel.frame.width)
                        makes.bottom.equalToSuperview()
                    }
                }else{
                    dLabel.snp.makeConstraints { (makes) in
                        makes.left.equalTo(self.directorLabel.snp.right)
                        makes.top.equalTo(self.directorLabel.snp.top).offset(CGFloat(index) * self.directorLabel.frame.height)
                        
                        makes.width.equalTo(fullScreenSize.width - 15 - self.directorLabel.frame.width)
                    }
                    
                }
                
            }
            
        }
        
        // 作者
        let writeArray: [ContentAttributeValue] = packDataNew.getValueArray(attribute_id: 16)
        
        if writeArray.count < 1 {
            self.writerLabel.text = "作者 ："
            writerLabel.snp.makeConstraints { (makes) in
                makes.top.equalToSuperview().offset(20)
                makes.left.equalToSuperview().offset(15)
                makes.bottom.equalToSuperview()
            }
        }else{
            self.writerLabel.text = "作者 ："
            
            writerLabel.snp.makeConstraints { (makes) in
                makes.top.equalToSuperview().offset(20)
                makes.left.equalToSuperview().offset(15)
                //                makes.bottom.equalToSuperview().offset(-20)
            }
            
            for index in 0 ..< writeArray.count{
                let wLabel = MarqueeLabel.init(frame: CGRect(x: 0, y: 20, width: fullScreenSize.width, height: 30), duration: 8.0, fadeLength: 1.0)
                containerViewB.addSubview(wLabel)
                wLabel.text = writeArray[index].attribute_value
                if (index % 2) == 0 {
                    wLabel.textColor = .red
                }else{
                    wLabel.textColor = .systemPink
                }
                wLabel.tag = 200 // tag = 200 -> remove it when viewDidAppear begin
                wLabel.font = UIFont(name: "Georgia-Bold", size: 20)
                wLabel.numberOfLines = 0
                wLabel.tappable = true
                wLabel.callback = {
                    if wLabel.textColor != .yellow{
                        wLabel.textColor = .yellow
                    }else{
                        wLabel.textColor = .red
                    }
                    print("attribute_value_id:\(writeArray[index].attribute_value_id)")
                }
                
                if index == (writeArray.count - 1){
                    wLabel.snp.makeConstraints { (makes) in
                        makes.left.equalTo(self.writerLabel.snp.right)
                        makes.top.equalTo(self.writerLabel.snp.top).offset(CGFloat(index) * self.directorLabel.frame.height)
                        
                        makes.width.equalTo(fullScreenSize.width - 15 - self.directorLabel.frame.width)
                        makes.bottom.equalToSuperview()
                    }
                }else{
                    wLabel.snp.makeConstraints { (makes) in
                        makes.left.equalTo(self.writerLabel.snp.right)
                        makes.top.equalTo(self.writerLabel.snp.top).offset(CGFloat(index) * self.directorLabel.frame.height)
                        
                        makes.width.equalTo(fullScreenSize.width - 15 - self.directorLabel.frame.width)
                    }
                }
            }
        }
        
        // 演員
        let attributeArray: [ContentAttributeValue] = packDataNew.getValueArray(attribute_id: 1)
        if attributeArray.count < 1{
            //1230
            //            print("attributeArray.count < 1 @viewdidappear")
            self.heroLabel.text = "演員 ："
            heroLabel.snp.makeConstraints { (makes) in
                makes.top.equalToSuperview().offset(20)
                makes.left.equalToSuperview().offset(15)
                makes.bottom.equalToSuperview().offset(-20)
            }
            
        }else{
            self.heroLabel.text = "演員 ："
            heroLabel.snp.makeConstraints { (makes) in
                makes.top.equalToSuperview().offset(20)
                makes.left.equalToSuperview().offset(15)
                //                makes.bottom.equalToSuperview().offset(-20)
            }
            for index in 0 ..< attributeArray.count{
                //                let hLabel: UILabel = UILabel()
                let hLabel = MarqueeLabel.init(frame: CGRect(x: 0, y: 20, width: fullScreenSize.width, height: 30), duration: 8.0, fadeLength: 1.0)
                containerViewC.addSubview(hLabel)
                hLabel.text = attributeArray[index].attribute_value
                hLabel.textColor = .red
                hLabel.tag = 200 // tag = 200 -> remove it when viewDidAppear begin
                hLabel.font = UIFont(name: "Georgia-Bold", size: 20)
                hLabel.numberOfLines = 0
                hLabel.tappable = true
                hLabel.callback = {
                    if hLabel.textColor != .yellow{
                        hLabel.textColor = .yellow
                    }else{
                        hLabel.textColor = .red
                    }
                    print("attribute_value_id:\(attributeArray[index].attribute_value_id)")
                }
                
                if index == (attributeArray.count - 1){
                    hLabel.snp.makeConstraints { (makes) in
                        makes.left.equalTo(self.heroLabel.snp.right)
                        //                    makes.bottom.equalTo(self.directorLabel.snp.bottom).offset(index*25)
                        makes.top.equalTo(self.heroLabel.snp.top).offset(CGFloat(index) * self.directorLabel.frame.height)
                        
                        makes.width.equalTo(fullScreenSize.width - 15 - self.directorLabel.frame.width)
                        makes.bottom.equalToSuperview().offset(-20)
                    }
                }else{
                    hLabel.snp.makeConstraints { (makes) in
                        makes.left.equalTo(self.heroLabel.snp.right)
                        //                    makes.bottom.equalTo(self.directorLabel.snp.bottom).offset(index*25)
                        makes.top.equalTo(self.heroLabel.snp.top).offset(CGFloat(index) * self.directorLabel.frame.height)
                        
                        makes.width.equalTo(fullScreenSize.width - 15 - self.directorLabel.frame.width)
                    }
                    
                }
                
            }
            
        }
        
        
        self.trailStreamUrl = ""
        self.getTrailerHlsUrl(){
            // 08/17 check History
            self.checkVideoHistory{
                completion()
            }
        }
        
        containerView.bringSubviewToFront(viewForVlc)
        containerView.bringSubviewToFront(optionView)
        containerView.bringSubviewToFront(controlView)
        
        
    }
    
    @objc func tappedPlay(){
        print("tappedPlay")
        // 11/20 isPlaying 代替
        let state: VLCMediaPlayerState = vlcPlayer.state
        if state == .paused{
            self.vlcPlayer.play()
            self.playerPlayBtn.setImage(UIImage(named: "playerPlay"), for: .normal)
        }else{
            self.vlcPlayer.pause()
            self.playerPlayBtn.setImage(UIImage(named: "playerPause"), for: .normal)
        }
    }
    
    @objc func tappedCloseBtn(){
        print("tappedCloseBtn")
        self.allScreenBtn.setImage(UIImage(named: "playerFullScreen"), for: .normal)
        littlePlayerConstraint?.activate()
        littlePlayerConstraint2?.activate()
        constraint1?.deactivate()
        constraint2?.deactivate()
        constraint3?.deactivate()
        
        // 12/29
        if currentPlayerState == .trailer{
            self.vlcPlayer.stop()
            self.vlcPlayer.media = nil
            currentPlayerState = .none
        }else{
            self.vlcPlayer.pause()
        }
//        self.vlcPlayer.pause()
        self.playerPlayBtn.setImage(UIImage(named:"playerPlay"), for: .normal)
        self.optionView.isHidden = false
        self.controlView.isHidden = true
        self.posterImageView.isHidden = false
        
    }
    /// 處理點擊 srtBtn 10/23   Mike
    @objc func tappedSrtBtn(){
        print("tappedSrtBtn")
        // 10/26
        self.openSrtView(openOrClose: true)
    }
    /** 切換為全屏播放 */
    @objc func tappedAllScreenBtn(){
        print("tappedAllScreenBtn")
        if self.allScreenBtn.imageView?.image == UIImage(named: "playerLittleScreen"){
            print("縮小 player")
            self.allScreenBtn.setImage(UIImage(named: "playerFullScreen"), for: .normal)
            littlePlayerConstraint?.activate()
            littlePlayerConstraint2?.activate()
            constraint1?.deactivate()
            constraint2?.deactivate()
            constraint3?.deactivate()
            // 10/28
            //            UIUtils.lockOrientation(.portrait, andRotateTo: .portrait)
            UIUtils.lockOrientation(.all, andRotateTo: .portrait)
        }else{
            print("放大 player")
            self.allScreenBtn.setImage(UIImage(named: "playerLittleScreen"), for: .normal)
            littlePlayerConstraint?.deactivate()
            littlePlayerConstraint2?.deactivate()
            constraint1?.activate()
            constraint2?.activate()
            constraint3?.activate()
            //            UIUtils.lockOrientation(.landscape, andRotateTo: .landscapeRight)
            // 10/28
            UIUtils.lockOrientation(.all, andRotateTo: .landscapeRight)
            UIApplication.shared.statusBarView?.backgroundColor = .none
        }
    }
    
    //    //yang
    //    override var shouldAutorotate: Bool {
    //        get {
    //            return true
    //        }
    //    }
    //    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
    //        get {
    //            return .landscapeRight
    //        }
    //    }
    //    // 强制旋转横屏 yang
    //    func forceOrientationLandscape() {
    //        let appdelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
    //        appdelegate.isForceLandscape = true
    //        appdelegate.isForcePortrait = false
    //        _ = appdelegate.application(UIApplication.shared, supportedInterfaceOrientationsFor: view.window)
    //        let oriention = UIInterfaceOrientation.landscapeRight // 设置屏幕为横屏
    //        UIDevice.current.setValue(oriention.rawValue, forKey: "orientation")
    //        UIViewController.attemptRotationToDeviceOrientation()
    //    }
    //    // 强制旋转竖屏 yang
    //    func forceOrientationPortrait() {
    //        let appdelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
    //        appdelegate.isForceLandscape = false
    //        appdelegate.isForcePortrait = true
    //        _ = appdelegate.application(UIApplication.shared, supportedInterfaceOrientationsFor: view.window)
    //        let oriention = UIInterfaceOrientation.portrait // 设置屏幕为竖屏
    //        UIDevice.current.setValue(oriention.rawValue, forKey: "orientation")
    //        UIViewController.attemptRotationToDeviceOrientation()
    //    }
    /**
     根據 tf 的值來改變 APP 關於橫置的設定
     
     - Date: 09/23  Mike
     */
    func forceOrientationAll(tf: Bool) {
        let appdelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.isForceAllDerictions = tf
        _ = appdelegate.application(UIApplication.shared, supportedInterfaceOrientationsFor: view.window)
    }
    /**
     根據 tf 的值來改變 APP 關於橫置的設定
     - true : change to landscape
     - false : back to portrait
     
     - Date: 09/29  Mike
     */
    func changeToLandscape (tf: Bool) {
        let appdelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.isForceLandscape = tf
        //        appdelegate
        _ = appdelegate.application(UIApplication.shared, supportedInterfaceOrientationsFor: view.window)
        
        let vc : UIViewController = UIViewController()
        self.present(vc, animated: false, completion: nil)
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func clickRefund(){
        print("refund")
        alertMsg(msg: "Waitting for API")
        //        self.refundButton.isHidden = true
        //        self.playOrBuyButton.setImage(UIImage(named: "buy"), for: .normal)
    }
    
    @objc func clickTrailer(){
        print("trailer")
        // 12/29
        for sv in controlView.subviews{
            if sv.tag == 50{
                sv.removeFromSuperview()
            }
        }
        // 11/05 注意 tempCurrentSub 的部分，為了讓 currentSub 暫時為 0
        let tempCurrentSub = self.currentSub
        self.currentSub = 0
        self.resetSubArray()
        self.currentPlayerState = .trailer
        self.setPlayer(streamUrl: self.trailStreamUrl)
        self.currentSub = tempCurrentSub
        
        self.posterImageView.isHidden = true
        self.vlcPlayer.play()
        // 11/20 mark isPlaying
//        self.isPlaying = true
        
        self.playerPlayBtn.setImage(UIImage(named:"playerPause"), for: .normal)
        
        //1114
        self.optionView.isHidden = true
        self.controlView.isHidden = false
        
        delayForHideControlView()
    }
    
    @objc func clickFavorite(){
        currentFavorite = !currentFavorite
        setFavoriteButtonImage()
//        modifyFavoriteStatusDone()
    }
    
    @objc func clickRateImage(){
        
    }
    
    @objc func clickRate(){
        let appearance = SCLAlertView.SCLAppearance(
            kWindowWidth: fullScreenSize.width/2 + 24, showCircularIcon: false
        )
        let alert = SCLAlertView(appearance: appearance)
        
        starRatingView.maximumValue = 5         //default is 5
        starRatingView.minimumValue = 0         //default is 0
//        starRatingView.value = 3                //default is 0
        
        if self.reviewData.createdAt == ""{
            //沒有評分過
            starRatingView.value = 3
        }else{
            starRatingView.value = self.reviewData.rating
        }
        
        starRatingView.backgroundColor = .black
//        starRatingView.allowsHalfStars = false
//        starRatingView.accurateHalfStars = false
        starRatingView.tintColor = UIColor.yellow
        
        starRatingView.addTarget(self, action: #selector(starChanged), for: .valueChanged)
        
        
        starRatingView.snp.makeConstraints { (makes) in
            makes.width.equalTo(fullScreenSize.width/2)
            makes.height.equalTo(fullScreenSize.width/2/5)
        }
        
        alert.customSubview = self.starRatingView
        print("alert.customSubview!.frame:\(alert.customSubview!.frame)")
        //        starRatingView.snp.makeConstraints { (makes) in
        //            makes.width.equalTo(fullScreenSize.width/2)
        //            makes.height.equalTo(fullScreenSize.width/2/5)
        //        }
        if alert.customSubview != nil{
            print("alert.customSubview:\(alert.customSubview!)")
        }
        
//        // 11/06
////        commentTextField = alert.addTextField("comment")
//        if self.reviewData.comments != ""{
//            commentTextField = alert.addTextField(self.reviewData.comments)
//        }else{
//            commentTextField = alert.addTextField("comment")
//        }
        
        // 11/09
        commentTextField = alert.addTextField("comment")
        
        if self.reviewData.comments != ""{
            commentTextField.text = self.reviewData.comments
        }
        
        // 添加确定按钮
        // 11/09
        alert.addButton("Submit", backgroundColor: UIColor.brown, textColor: UIColor.yellow) {
            print("Submit")
            
            // 11/09 Date 取得方法，暫時用不到，mark 留存
//            let formatter = DateFormatter()
//            formatter.dateFormat = "yyyy年MM月dd日"
//            let currentDate = Date()
//            print(formatter.string(from: currentDate))
            
//            print("comment:\(self.commentTextField.text!)")
//
//            self.starRatingView.value = CGFloat(Int(self.starRatingView.value * 10 ) ) / 10
//            print(self.starRatingView.value)
            
            self.setActView(openOrClose: true)
            self.writeProductReview {
                print("writeProductReview Done")
                self.setActView(openOrClose: false)
            }
            
        }
        
        alert.showInfo("Rating", subTitle: "", closeButtonTitle: "Back")
    }
    /**
     送出修改 request
     - Date: 11/09  Mike
     */
    func writeProductReview(completion : @escaping ()->()) {
//        print("""
//            send writeProductReview
//            comment : \(self.commentTextField.text!)
//            starRatingView.value : \(self.starRatingView.value)
//            product id : \(self.packDataNew.product_id)
//            """)
        let params = [
            "product_id": self.packDataNew.product_id,
            "comments": self.commentTextField.text!,
            "rating": self.starRatingView.value,
            "token":"\(token)"
        ] as [String : Any]
        
        let writeProductReviewAPI = serverUrlNew + "product/writeProductReview"
        
        // 正常用 Ppstman 測試應該是不會有問題
        
        AF.request(writeProductReviewAPI, method: .post, parameters: params)
            .responseJSON{ (response) in
                if response.value != nil{
                    let swiftyJsonVar = JSON(response.value!)
                    let status: Int = swiftyJsonVar["status"].int!
                    if status == 200{
                        print("writeProductReview Done")
                        // 11/10 更新 reviewData
                        self.reviewData.rating = self.starRatingView.value
                        self.reviewData.comments = self.commentTextField.text!
                        completion()
                    }else{
                        // 11/10 Mike
                        self.backToLoginVC(title:"Logout",msg: swiftyJsonVar["message"].string!,okMsg: "Back to LoginPage.")
                        completion()
                    }
                }else{
                    // TODO
                    completion()
                }
            }
        
    }
    
    //11/02
    @objc func starChanged(){
        // 半顆星移動
        self.starRatingView.value = CGFloat(Int(self.starRatingView.value * 2 ) ) / 2
        
        // 一顆星移動
//        print("self.starRatingView.value:\(self.starRatingView.value)")
//        print("self.starRatingView.value + 1:\(self.starRatingView.value + 1)")
//        print("Int(self.starRatingView.value + 1):\(Int(self.starRatingView.value + 1))")
//        self.starRatingView.value = CGFloat(Int(self.starRatingView.value + 1))
//        self.starRatingView.value = CGFloat(ceil(self.starRatingView.value))
        
//        self.starRatingView.value = ceil(self.starRatingView.value)
        
//        print("self.starRatingView.value:\(self.starRatingView.value)")
    }
    
    /**
     購買產品或產品內容
     - Date: 01/13  Yang
     */
    func purchaseProduct(completion : @escaping ()->()) {
        
        let productContent = [
            "product_id": self.packDataNew.product_id,
            "content_id": self.packDataNew.content_id
        ] as [String : Int]
        
        let productContentJson = JSON(productContent)
//        print("productContentJson \(productContentJson)")
        
        let params = [
            "product_content": [productContentJson],
            "payment_id": 1,
            "token":"\(token)"
        ] as [String : Any]
        
        let paramsJson = JSON(params)
//        print("params \(params)")
        print("paramsJson \(paramsJson)")
        
        let purchaseProductAPI = serverUrlNew + "purchase/purchaseProduct"
        
        AF.request(purchaseProductAPI, method: .post, parameters: paramsJson, encoder: JSONParameterEncoder.default).responseJSON{ (response) in
            if response.value != nil{
                let swiftyJsonVar = JSON(response.value!)
                let status: Int = swiftyJsonVar["status"].int!
                let message: String = swiftyJsonVar["message"].string!
                print("Purchase_Message：\(message)")
                
                if status == 200{
                    self.alertMsg3(title: "Success" , msg: "購買產品完成")
                    completion()
                }else{
                    print("status1 \(status)")
                    print("error1 \(message)")
                    completion()
                }
            }else{
                print("error2")
                self.alertMsg(msg: "暫無法購買此產品")
                completion()
            }
        }
    }
    /**
     租借產品或產品內容
     - Date: 01/26  Yang
     */
    func rentProduct(completion : @escaping ()->()) {
        
        let productContent = [
            "product_id": self.packDataNew.product_id,
            "content_id": self.packDataNew.content_id
        ] as [String : Int]
        
        let productContentJson = JSON(productContent)
        
        let params = [
            "rent_plan_id": rentPlanID,
            "product_content": [productContentJson],
            "payment_id": 1,
            "token":"\(token)"
        ] as [String : Any]
        
        let paramsJson = JSON(params)
        print("paramsJson \(paramsJson)")
        
        let purchaseProductAPI = serverUrlNew + "purchase/rentProduct"
        
        AF.request(purchaseProductAPI, method: .post, parameters: paramsJson, encoder: JSONParameterEncoder.default).responseJSON{ (response) in
            if response.value != nil{
                let swiftyJsonVar = JSON(response.value!)
                let status: Int = swiftyJsonVar["status"].int!
                let message: String = swiftyJsonVar["message"].string!
                print("Rent_Message：\(message)")
                
                if status == 200{
                    self.alertMsg3(title: "Success" , msg: "租借產品完成")
                    completion()
                }else if status == 400{
                    self.alertMsg(msg: message)
                    completion()
                }else{
                    print("status111 \(status)")
                    self.alertMsg(msg: message)
                    completion()
                }
            }else{
                print("error222")
                self.alertMsg(msg: "暫無法租借此產品")
                completion()
            }
        }
    }
    /**
     取得產品或產品內容銷售方案
     - Date: 01/21 Yang
     */
    func getPurchaseOption(completion : @escaping ()->()) {
        let params = [
            "product_id": self.packDataNew.product_id,
            "content_id": self.packDataNew.content_id,
            "token": "\(token)"
        ] as [String : Any]
        
        print("OptionParams \(params)")
        
        let getPurchaseOptionAPI = serverUrlNew + "product/getPurchaseOption"
        
        AF.request(getPurchaseOptionAPI, method: .post, parameters: params)
            .responseJSON{ [self] (response) in
                if response.value != nil{
                    let swiftyJsonVar = JSON(response.value!)
                    let status: Int = swiftyJsonVar["status"].int!
                    self.statusValue = status
                    print("statusValue \(self.statusValue)")
                    
                    if status == 200 {
                        if let resultObject = swiftyJsonVar["result"].dictionaryObject, let purchase_option = resultObject["purchase_option"] as? [String : Any], let rent_option = purchase_option["rent_option"], let rentArrayObject = JSON(rent_option).arrayObject! as? [Any], let sell_option = purchase_option["sell_option"], let sellArrayObject = JSON(sell_option).arrayObject! as? [Any]{
                            
                            print("purchase_option \(purchase_option)")
                            print("sellArrayObject \(sellArrayObject)")
                            print("rentArrayObject \(rentArrayObject)")
                            rentCount = rentArrayObject.count
                            purchaseCount = sellArrayObject.count
                            print("rentCount \(rentCount)")
                            print("purchaseCount \(purchaseCount)")
                            
                            
                            for sellData in sellArrayObject {
                                let sellJsonData = JSON(sellData)
                                // nil表示無期限
                                if sellJsonData["price_end_time"].string == nil{
                                    print("price_end_time  null")
//                                    print("price_end_time222 \(sellJsonData["price_end_time"].string!)")
                                }
//                                if sellJsonData["price_type"].string != nil{
//                                    print("price_type \(sellJsonData["price_type"].string!)")
//                                }
                            }
                            
                            for rentData in rentArrayObject {
                                let rentJsonData = JSON(rentData)
                                if rentJsonData["rent_plan_id"].int != nil{
                                    print("rent_plan_id \(rentJsonData["rent_plan_id"].int!)")
                                    rentPlanID = rentJsonData["rent_plan_id"].int!
                                }
                                if rentJsonData["price"].string != nil{
                                    print("price \(rentJsonData["price"].string!)")
                                }
                                if rentJsonData["price_type"].string != nil{
                                    print("price_type \(rentJsonData["price_type"].string!)")
                                }
                                if rentJsonData["rent_plan"].string != nil{
                                    print("rent_plan \(rentJsonData["rent_plan"].string!)")
                                }
                                if rentJsonData["rent_time"].string != nil{
                                    print("rent_time \(rentJsonData["rent_time"].string!)")
                                }
                                if rentJsonData["rent_use_time"].string != nil{
                                    print("rent_use_time \(rentJsonData["rent_use_time"].string!)")
                                }
                            }
                            
                            
                            if let subscription_option = purchase_option["subscription_option"], let subscriptionArrayObject = JSON(subscription_option).arrayObject! as? [Any]{
                                print("subscriptionArrayObject \(subscriptionArrayObject)")
                                
                                for subscriptionData in subscriptionArrayObject {
                                    let subscriptionJsonData = JSON(subscriptionData)
                                    if subscriptionJsonData["subscription_level"].int != nil{
                                        print("subscription_level \(subscriptionJsonData["subscription_level"].int!)")
                                    }
                                    if subscriptionJsonData["subscription_plan_id"].int != nil{
                                        print("subscription_plan_id \(subscriptionJsonData["subscription_plan_id"].int!)")
                                    }
                                    if subscriptionJsonData["subscription_status_id"].int != nil{
                                        print("subscription_status_id \(subscriptionJsonData["subscription_status_id"].int!)")
                                    }
                                    if subscriptionJsonData["subscription_time"].int != nil{
                                        print("subscription_time \(subscriptionJsonData["subscription_time"].int!)")
                                    }
                                    if subscriptionJsonData["subscription_name"].string != nil{
                                        print("subscription_name \(subscriptionJsonData["subscription_name"].string!)")
                                    }
                                    if subscriptionJsonData["subscription_plan_name"].string != nil{
                                        print("subscription_plan_name \(subscriptionJsonData["subscription_plan_name"].string!)")
                                    }
                                    if subscriptionJsonData["subscription_plan_description"].string != nil{
                                        print("subscription_plan_description \(subscriptionJsonData["subscription_plan_description"].string!)")
                                    }
                                    if subscriptionJsonData["subscription_description"].string != nil{
                                        print("not null")
                                        print("subscription_description \(subscriptionJsonData["subscription_description"].string!)")
                                    }else{
                                        print("null")
                                    }
                                }
                            }
                        }
                        completion()
                    }else if status == 204 {
//                        self.alertMsg(msg: "get purchase option success , but no purchase option")
                        print("get purchase option success , but no purchase option")
                        completion()
                    }else{
                        self.alertMsg(msg: "get option error")
                        completion()
                    }
                }else{
                    self.alertMsg(msg: "No Purchase Option")
                    completion()
                }
                
                
                
        }
        
    }
    //
    @objc func clickPlayOrRent(){
        if self.rentButton.imageView?.image == rentImage{
            print("rentButton")
            if statusValue == 204 || rentCount == 0{
                self.alertMsg(msg: "暫無法租借此產品")
            }else{
                rentProduct{
                }
            }
        }else{
            clickPlay()
        }
    }
    
    //1223
    //0804 這邊有問題
    @objc func clickPlayOrBuy(){
        // 01/19 mark
//        print("11/17 self.vlcPlayer.state.rawValue:\(self.vlcPlayer.state.rawValue) @ tappedCloseBtn")
        
        if self.playOrBuyButton.imageView?.image == UIImage(named: "buy"){
            print("Buy")
            if statusValue == 204 || purchaseCount == 0{
                self.alertMsg(msg: "暫無法購買此產品")
            }else{
                purchaseProduct{}
            }
        }else{
            clickPlay()
        }
    }
    
    func clickPlay(){
        print("play")
        if self.vlcPlayer.state.rawValue == 6{
            self.vlcPlayer.play()
            // 11/20 mark isPlaying
//                self.isPlaying = true
            self.playerPlayBtn.setImage(UIImage(named:"playerPause"), for: .normal)
            self.optionView.isHidden = true
            self.controlView.isHidden = false
            self.posterImageView.isHidden = true
            delayForHideControlView()
        }else{
            //應該只可能為 0 or 6
            // 11/13
            self.setActView(openOrClose: true)
            self.checkIfNeedPcPwd {
                print("self.ifNeedPcPwd:\(self.ifNeedPcPwd)")
                // 09/10
                // 01/15 改名為 tempPwd
                if self.ifNeedPcPwd && self.tempPwd == nil{
                    // 11/18
                    self.setActView(openOrClose: false)
                    // 01/15 mark
//                        self.inputPwd()
                    // 01/18
                    self.tryAuth()
                }else{
                    self.tempPwd = pcPwd
                    self.afterCheckPwd()
                }
            }
        }
    }
    
    // 01/18 AuthVC
    override func afterCheckPwd(){
        // API playVideo 是抓取影片跟字幕網址，並不是 play
        self.playVideo() {
            self.playHistoryOrNot {
                self.setPlayer(streamUrl: self.streamUrl)
                if self.streamUrl != ""{
                    //                            print("if self.streamUrl != nil")
                    self.posterImageView.isHidden = true
                    
                    self.vlcPlayer.play()
                    
                    self.playerPlayBtn.setImage(UIImage(named:"playerPause"), for: .normal)
                    self.optionView.isHidden = true
                    self.controlView.isHidden = false
                    // 12/02
                    self.delayForHideControlView()
                    print(self.videoSlider.frame)
                    
                }
                
            }
            
        }
        //        }
    }
    /**
     可以偵測到播放器的 state 改變
     
     - Date: 11/19  Mike
     */
    func mediaPlayerStateChanged(_ aNotification: Notification!) {
        
        let state: VLCMediaPlayerState = vlcPlayer.state
        let adState: VLCMediaPlayerState = adPlayer.state
        // 12/10
        self.adView.bringSubviewToFront(skipADBtn)
        if currentPlayerState == .adPlayed{
            // 12/07
            if adState == .ended || adState == .stopped{
//                print("adState == .ended || adState == .stopped")
                
//                // 12/08 暫時先以 none 代替 TODO
//                currentPlayerState = .none
                
                // 12/14 check 有沒有多重廣告要接續播放
                self.adPlayer.media = nil
                var checkNextAD = false
                if countForSkipTimer != nil{
                    countForSkipTimer?.invalidate()
                }
                
                for i in 0..<adDatas.count{
                    if adDatas[i].playTime == videoCurrentTime && !adDatas[i].played && adPlayer.media == nil{
                        playAD(index: i)
                        checkNextAD = true
                    }
                }
                
                if checkNextAD{
                    currentPlayerState = .adBuffering
                }else{
                    self.adView.isHidden = true
                    self.vlcPlayer.play()
                    
                    // 12/16
                    self.setAdTimeOnVideoSlider()
//                    print("12/28 setAdTimeOnVideoSlider @mediaPlayerStateChanged, adState == .ended || adState == .stopped, checkNextAD == false")
                    // 12/08 暫時先以 none 代替 TODO
                    currentPlayerState = .none
                }
            }
        }else if currentPlayerState == .adBuffering{
            // 12/09
            if adState == .playing{
                currentPlayerState = .adPlayed
                countForSkip()
            }
        }else{
            if state == .error {
    //            print("Error trying to play video!")
            }else if state == .ended || state == .stopped {
//                print("Video is done ended or stopped")
                // 12/29
                if currentPlayerState == .trailer{
                    currentPlayerState = .none
                }else{
                    // 01/05
//                    if state == .ended{
//                        print("end")
//                    }else{
//                        print("stop")
//                    }
                    if state != .stopped{
//                        print("01/05 set videoRemeberTime = 0")
                        self.videoRemeberTime = 0
                    }
                    
                }
                // 11/20
                // 影片播放結束，或是離開頁面，都會觸發
                self.allScreenBtn.setImage(UIImage(named: "playerFullScreen"), for: .normal)
                self.vlcPlayer.stop()
                self.openSrtView(openOrClose: false)
                UIUtils.lockOrientation(.portrait, andRotateTo: .portrait)
                self.optionView.isHidden = false

                // 12/03 reset
                for sv in controlView.subviews{
                    if sv.tag == 50{
                        sv.removeFromSuperview()
                    }
                }

                self.controlView.isHidden = true
                self.posterImageView.isHidden = false
                littlePlayerConstraint?.activate()
                littlePlayerConstraint2?.activate()

                constraint1?.deactivate()
                constraint2?.deactivate()
                constraint3?.deactivate()
                // 01/05
//                if currentPlayerState != .trailer{
//                    self.videoRemeberTime = 0
//                }
//                self.videoRemeberTime = 0
                // 11/27
                isSetAds = false

            }else if state == .opening{
//                print("Video is opening")
            }else if state == .buffering && currentPlayerState != .trailer{
//                print("Video is buffering, videoRemeberTime:\(videoRemeberTime),videoCurrentTime:\(videoCurrentTime)")
                if videoRemeberTime != videoCurrentTime{
                    let moveInt32 = Int32(self.videoRemeberTime - self.videoCurrentTime)
                    vlcPlayer.jumpForward(Int32(moveInt32))
//                    print("01/05 when jump, currentPlayerState = \(currentPlayerState),videoRemeberTime:\(videoRemeberTime),videoCurrentTime:\(videoCurrentTime)")
                    videoCurrentTime = videoRemeberTime
                }
            }
            // 11/20
            if state == .paused{
//                print("Video is paused")
                self.playerPlayBtn.setImage(UIImage(named: "playerPlay"), for: .normal)
            }else{
//                print("Video isn't paused")
                self.playerPlayBtn.setImage(UIImage(named: "playerPause"), for: .normal)
            }
            // 12/04 12/07
            if state != .ended && state != .stopped{
    //        if state != .ended && state != .stopped && state != .paused{
//                print("state != .ended && state != .stopped")
                UIUtils.lockOrientation(.all)
            }
            // 11/26
            if state == .playing{
//                print("playing")
//                print("11/26 videoAllTime:\(videoAllTime)")
                // 12/17 adData config to playtime
                for i in 0..<self.adDatas.count{
                    if adDatas[i].config == 2{
                        adDatas[i].playTime = videoAllTime/2
                    }
                }
                // 12/16
                self.setAdTimeOnVideoSlider()
//                print("12/28 setAdTimeOnVideoSlider @mediaPlayerStateChanged, state == .playing, videoAllTime:\(videoAllTime)")
//                for i in 0..<self.adDatas.count{
////                    if adDatas[i].config == 2{
////                        adDatas[i].playTime = videoAllTime/2
////                    }
//                    print(self.adDatas[i])
//                }
            }
        }
    }
    /**
     可以偵測到播放器的 timer 改變
     
     - Date: 11/19  Mike
     */
    func mediaPlayerTimeChanged(_ aNotification: Notification!) {
        if videoAllTime > 0 && !isSetAds{
            self.setAdTimeOnVideoSlider()
            isSetAds = true
        }
        
        // 12/07
        let state: VLCMediaPlayerState = vlcPlayer.state
        // 12/03
        for i in 0..<adDatas.count{
            // 12/14
            if adDatas[i].playTime == videoCurrentTime && state != .paused && !adDatas[i].played && adPlayer.media == nil && currentPlayerState != .trailer{
                currentPlayerState = .adBuffering
                // 12/07
                playAD(index: i)
            }
        }
    }
    /**
     處理手機橫置
     - Date: 10/28   Mike
     */
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
//        // 12/04
//        setAdTimeOnVideoSlider()
        
        switch UIDevice.current.orientation {
        case .landscapeLeft, .landscapeRight:
            print("橫的")
            self.allScreenBtn.setImage(UIImage(named: "playerLittleScreen"), for: .normal)
            littlePlayerConstraint?.deactivate()
            littlePlayerConstraint2?.deactivate()
            constraint1?.activate()
            constraint2?.activate()
            constraint3?.activate()
            // 12/04 12/16
//            setAdTimeOnVideoSlider()
//            print("12/16B @viewWillTransition self.videoSlider.frame:\(self.videoSlider.frame)")
            UIApplication.shared.statusBarView?.backgroundColor = .none
        case .portrait, .portraitUpsideDown:
            print("直的")
            self.allScreenBtn.setImage(UIImage(named: "playerFullScreen"), for: .normal)
            littlePlayerConstraint?.activate()
            littlePlayerConstraint2?.activate()
            constraint1?.deactivate()
            constraint2?.deactivate()
            constraint3?.deactivate()
//            print("12/16B @viewWillTransition self.videoSlider.frame:\(self.videoSlider.frame)")
            // 12/04 12/16
//            setAdTimeOnVideoSlider()
        default:
            print("error")
        }
//        print("@viewWillTransition self.viewForVlc.frame\(self.viewForVlc.frame)")
//        print("@viewWillTransition self.videoSlider.frame:\(self.videoSlider.frame)")
//        for sv in controlView.subviews{
//            if sv.tag == 50{
//                print("@viewWillTransition sv.frame:\(sv.frame)")
//            }
//        }
        coordinator.animate(alongsideTransition: nil) { _ in
            // Your code here
//            print("@viewWillTransition self.videoSlider.frame:\(self.videoSlider.frame)")
            self.setAdTimeOnVideoSlider()
            print("12/28 setAdTimeOnVideoSlider @viewWillTransition")
        }
    }
    
    func setFavoriteButtonImage(){
        if currentFavorite{
            self.favoriteButton.setImage(UIImage(named: "favoriteOn"), for: .normal)
            self.favoriteLabel1.text = "Remove from"
        }else{
            self.favoriteButton.setImage(UIImage(named: "favorite"), for: .normal)
            self.favoriteLabel1.text = "Add to"
        }
    }
    
    let contentView = UIView()
    /**
     點擊屏幕回彈用
     - Date: 10/20 Yang
     */
    @objc func showContentView(){
        contentView.tag = 1000
        contentView.isUserInteractionEnabled = true
        contentView.backgroundColor = .clear
        view.addSubview(contentView)
        contentView.snp.makeConstraints { (makes) in
            makes.edges.equalTo(view)
        }
    }
    /**
     側邊欄關閉後 就把contentView給移除掉 以便可以觸控此頁面元件
     - Date: 10/20 Yang
     */
    @objc func removeSubview(){
        if let viewWithTag = self.view.viewWithTag(1000) {
            viewWithTag.removeFromSuperview()
        }else{
//            print("No!")
        }
    }
    
    @IBAction func menuClick(_ sender: Any) {
        print("pack menuClick")
        navigationController?.view.menu()
        ButtonDelegate?.clickLeft()
        if navigationController?.view.superview?.frame.origin.x != 0 {
            showContentView()
        }else{
            removeSubview()
        }
    }
    
    @IBAction func userClick(_ sender: Any) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        navigationController?.view.menu2()
        ButtonDelegate?.clickRight()
        if navigationController?.view.superview?.frame.origin.x == -navigationOriginX {
            showContentView()
        }else{
            removeSubview()
        }
    }
    ///右滑
    @IBAction func handleEdgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger(location: location, state: sender.state)
        if beganX2 > 0 {
            ButtonDelegate?.clickLeft()
            showContentView()
            print("大於零 \(navigationController?.view.superview?.frame.origin.x as Any)")
        }else{
            print("等於零 \(navigationController?.view.superview?.frame.origin.x as Any)")
            contentView.removeFromSuperview()
        }
    }
    ///左滑
    @IBAction func handleEdgePanGesture2(_ sender: UIScreenEdgePanGestureRecognizer) {
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger2(location: location, state: sender.state)
        if beganX2 < 0{
            ButtonDelegate?.clickRight()
            showContentView()
        }else{
            contentView.removeFromSuperview()
        }
    }
    
    // 11/23 sender: Any -> UIGestureRecognizer
    @IBAction func handleTapGesture(_ sender: UIGestureRecognizer) {
        print("handleTapGesture \(navigationController?.view.superview?.frame.origin.x as Any)")
        // 11/23 test
        self.findFingersPositon(recognizer: sender as! UITapGestureRecognizer)
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        //1121
        //        self.optionView.alpha = 1.0
        //
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
        //            self.optionView.alpha = 0.0
        //        }
        if navigationController?.view.superview?.frame.origin.x != 0 {
            if navigationController?.view.superview?.frame.origin.x == -navigationOriginX {
                navigationController?.view.menu2()
                contentView.removeFromSuperview()
            }else{
                contentView.removeFromSuperview()
                navigationController?.view.menu()
            }
        }
    }
    
    /** 點擊 View 可以讓控制View浮現三秒 */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("touchesBegan")
//        for t in touches{
//            print(t.description)
//            print(t.view?.tag)
//        }
        self.view.endEditing(true)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        // 10/12
        //        if self.vlcPlayer.position > 0.8{
        //            print(self.vlcPlayer.state.rawValue)
        //        }
        //        print(self.vlcPlayer.state)
        //        if keyPath == "isPlaying"{
        //            print("isPlaying")
        //        }
        
        //觀測播放進度
        // 10/12
        //        if keyPath == "remainingTime"{
        //            print("position;\(self.vlcPlayer.position), state.rawValue:\(self.vlcPlayer.state.rawValue)")
        //1126 處理時間 Label
        
        currentProgress = vlcPlayer.position
        videoSlider.setValue(currentProgress, animated: true)
        
        //            videoCurrentTime = (Int(vlcPlayer.time.minuteStringValue) ?? 0) * 60
        //            videoAllTime = (Int(vlcPlayer.remainingTime.minuteStringValue) ?? 0) * 60 + videoCurrentTime
        //
        //            print("1.videoCurrentTime:\(videoCurrentTime)")
        
        let videoCurrentTimeStr = vlcPlayer.time.stringValue
        //            print("videoCurrentTime:\(videoCurrentTimeStr)")
        
        if timeStrToInt(timeStr: videoCurrentTimeStr!) != -1{
//            print("11/12 test videoCurrentTime:\(videoCurrentTime)")
            videoCurrentTime = timeStrToInt(timeStr: videoCurrentTimeStr!)
            // 11/16 01/05
            if currentPlayerState != .trailer{
                videoRemeberTime = videoCurrentTime
            }
//            videoRemeberTime = videoCurrentTime
        }
        //            videoCurrentTime = timeStrToInt(timeStr: videoCurrentTimeStr!)
        // 08/19
        /** 10/14 error
         Fatal error: Unexpectedly found nil while implicitly unwrapping an Optional value: file Andromeda/PackDetailViewController.swift, line 2319
         */
        //        videoAllTime = timeStrToInt(timeStr: vlcPlayer.media.length.stringValue!)
        //            print("videoCurrentTime:\(videoCurrentTime)")
        
        if (videoCurrentTimeStr == "00:00"){
            videoLeftTimeLabel.text = "--:--"
        }else{
            videoLeftTimeLabel.text = videoCurrentTimeStr
        }
        //            videoLeftTimeLabel.text = videoCurrentTimeStr
        
        // 10/15
        if vlcPlayer.media != nil{
            videoAllTime = timeStrToInt(timeStr: vlcPlayer.media.length.stringValue!)
            videoRightTimeLabel.text = vlcPlayer.media.length.stringValue
        }
    }
    
    func setAdTimeOnVideoSlider(){
        
        // 12/29
        if currentPlayerState == .trailer{
            return
        }
//        print("12/28 setAdTimeOnVideoSlider in, videoAllTime:\(videoAllTime)")
        for i in 0..<self.adDatas.count{
            if adDatas[i].config == 2{
                adDatas[i].playTime = videoAllTime/2
            }
//            print(self.adDatas[i])
        }
        
        // 12/03 reset
        for sv in controlView.subviews{
            if sv.tag == 50{
                sv.removeFromSuperview()
            }
        }
        // 12/16
        for i in 0..<adDatas.count{
            if adDatas[i].playTime != 0 && !adDatas[i].played{
                if i > 0{
                    if adDatas[i].playTime == adDatas[i-1].playTime{
                        break
                    }
                }
                let adValue: Float = Float(adDatas[i].playTime) / Float(videoAllTime)
                let adBlock = UIView()
                adBlock.tag = 50
                adBlock.backgroundColor = .yellow
                
                self.controlView.addSubview(adBlock)
                adBlock.snp.makeConstraints { (makes) in
                    makes.height.equalTo(sliderHeight/3)
                    makes.width.equalTo(4)
                    makes.left.equalTo(self.videoSlider).offset(self.videoSlider.frame.width * CGFloat(adValue) - 2)
                    makes.centerY.equalTo(self.videoSlider)
                }
            }
        }
    }
    /**
     time String -> Int
     
     - Date: 08/18  Mike
     */
    func timeStrToInt(timeStr:String)->Int{
        var timeInt:Int = 0
        let array = timeStr.components(separatedBy: ":")
        if array[0] == "--" {
            return -1
        }
        if array.count == 2{
            timeInt = Int(array[0])! * 60 + Int(array[1])!
        }else if array.count == 3{
            timeInt = Int(array[0])! * 3600 + Int(array[1])! * 60 + Int(array[2])!
        }
        return timeInt
    }
    
    /**
     time Int -> String
     
     - Date: 08/20  Mike
     */
    func timeIntToStr(timeInt:Int)->String{
        var timeStr:String = ""
        var timeH:String = ""
        var timeM:String = ""
        var timeS:String = ""
        if timeInt >= 3600{
            //補0
            if timeInt/3600 < 10 {
                timeH = "0\(timeInt/3600)"
            }else{
                timeH = String(timeInt/3600)
            }
            if timeInt/60%60 < 10 {
                timeM = "0\(timeInt/60%60)"
            }else{
                timeM = String(timeInt/60%60)
            }
            if timeInt%60 < 10 {
                timeS = "0\(timeInt%60)"
            }else{
                timeS = String(timeInt%60)
            }
            timeStr = "\(timeH):\(timeM):\(timeS)"
        }else{
            //補0
            if timeInt/60 < 10 {
                timeM = "0\(timeInt/60)"
            }else{
                timeM = String(timeInt/60)
            }
            if timeInt%60 < 10 {
                timeS = "0\(timeInt%60)"
            }else{
                timeS = String(timeInt%60)
            }
            timeStr = "\(timeM):\(timeS)"
        }
        return timeStr
    }
    
    /**
     播放前確認是否要重歷史紀錄播放
     */
    func playHistoryOrNot(completion : @escaping ()->()){
        print("playHistoryOrNot")
        
        // 11/13
        self.setActView(openOrClose: true)
        
        if isPwdOk {
            // 11/17
            self.setActView(openOrClose: false)
//            if self.videoCurrentTime == 0{
            if self.videoRemeberTime == 0{
                print("videoRemeberTime = 0 @playHistoryOrNot")
                // 11/13
//                self.setActView(openOrClose: false)
                
                completion()
                //            break
            }else{
                // 11/13
//                self.setActView(openOrClose: false)
                
                let alertController = UIAlertController(title: "", message: "Whether to play from history?", preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: "OK", style: .cancel, handler: {
                    (action: UIAlertAction!) -> Void in
//                    // 11/13
//                    self.setActView(openOrClose: false)
                    
                    completion()
                })
                
                let cancelAction = UIAlertAction(title: "cancel", style: .default, handler: {
                    (action: UIAlertAction!) -> Void in
                    // 11/16 videoRemeberTime
//                    self.videoCurrentTime = 0
                    self.videoRemeberTime = 0
//                    // 11/13
//                    self.setActView(openOrClose: false)
                    
                    completion()
                })
                
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                
                self.present(alertController, animated: true, completion: nil)
            }
        }else{
            // 11/13
            self.setActView(openOrClose: false)
            
            completion()
        }
        
    }
    
    /**
     抓取目前時間
     
     - Date: 08/28  Mike
     */
    func getTimeNow() -> String{
        //        var timeNow : String = ""
        
        let currentDate = Date()
        let dataFormatter = DateFormatter()
        dataFormatter.locale = Locale(identifier: "zh_Hant_TW")
        //參照2020-08-13 17:55:27
        dataFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"

        //        timeNow = dataFormatter.string(from: currentDate)
        
        return dataFormatter.string(from: currentDate)
    }
    
    /**
     抓取影片前，先判斷是否需要 parents pwd
     */
    func checkIfNeedPcPwd(completion : @escaping ()->()){
        let mpaa = self.ratingLabel.text
//        print("@ifNeedPcPwd, mpaa:\(mpaa), pcStatus:\(pcStatus.rawValue)")
        if mpaa == "G" || mpaa == "PG"{
            //            return false
            self.ifNeedPcPwd = false
            completion()
        }else if mpaa == "PG-13" && pcStatus == .off{
            //            return false
            self.ifNeedPcPwd = false
            completion()
        }else{
            /**
             剩下 mpaa == "R" or
             mpaa == "PG-13" && pcStatus == .on
             */
            //            return true
            self.ifNeedPcPwd = true
            completion()
        }
        
    }
    /**
     處理輸入密碼，重點在 completion( )
     不要執行到一半就往下走了
     */
    // 01/18 move to AuthVC
//    func inputPwd(){
//        let pwdView = UIView()
//        // for cancel btn to remove
//        pwdView.tag = 100
//        self.setPwdView(pwdView: pwdView, pwdBtns: pwdBtns, pwdTextFields: pwdTextFields)
//    }
    
    // 01/18 move to AuthVC
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        print("textFieldShouldReturn")
//        return true
//    }
    // 01/18 move to AuthVC
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//        //        print("range:\(range)")
//        print("range.location:\(range.location)")
//        print("string:\(string)")
//
//        if textField == pwdTextFields[0]{
//            pwdTextFields[0].text = string
//            if string != ""{
//                pwdTextFields[1].becomeFirstResponder()
//            }
//        }else if textField == pwdTextFields[1]{
//            pwdTextFields[1].text = string
//            if string != ""{
//                pwdTextFields[2].becomeFirstResponder()
//            }
//        }else if textField == pwdTextFields[2]{
//            pwdTextFields[2].text = string
//            if string != ""{
//                pwdTextFields[3].becomeFirstResponder()
//            }
//        }else if textField == pwdTextFields[3]{
//            if range.location > 0{
//                pwdTextFields[3].text = ""
//            }
//        }
//        return true
//    }
    
    // 01/18 move to AuthVC and override
    @objc override func tapOKBtn(_ sender: UIButton){
        print("Enter tapOKBtn")
        // 01/15 pcPwd -> tempPwd, "0000" -> pcPwd
        tempPwd = ""
        for tf in self.pwdTextFields{
            //            print(tf.text ?? "tf.text = nil")
            tempPwd! += tf.text ?? ""
        }
        if tempPwd!.count == 4{
            //            print(pcPwd!)
            self.removePwdView()
            self.afterCheckPwd()
        }else{
            // 有 textField 為空，無動作
        }
    }
    
    // 01/18 move to AuthVC
//    @objc func tapCancelBtn(_ sender: UIButton){
//        self.removePwdView()
//    }
    // 01/18 move to AuthVC
//    func removePwdView(){
//        for view in self.view.subviews {
//            if view.tag == 100 {
//                view.removeFromSuperview()
//            }
//        }
//    }
    /**
     開啟關閉 SrtView
     - Date: 10/26  Mike
     */
    func openSrtView(openOrClose: Bool){
        if openOrClose{
            //            if subtitleURLs.count != 0{
//            for view in self.view.subviews{
//                if view.tag == 300{
//                    view.removeFromSuperview()
//                }
//            }
            self.cleanSubViewsBy(tag: 300)
            let srtView = UIView()
            srtView.tag = 300
            srtView.addTapGestureRecognizer {
                self.openSrtView(openOrClose: false)
            }
            srtView.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.4)
            
            self.view.addSubview(srtView)
            srtView.snp.makeConstraints { (makes) in
                makes.edges.equalToSuperview()
            }
            
            for i in 0..<subtitleURLs.count{
                let switchSrtBtn = UIButton()
                var preText: String = ""
                if i == 0{
                    if self.subtitleURLs.count == 1{
                        preText = "No subtitles"
                    }else{
                        preText = "Close subtitles"
                    }
                }else{
                    preText = "Srt \(i)"
                }
                if i == currentSub && self.subtitleURLs.count != 1 {
                    preText += "(selected)"
                }
                switchSrtBtn.setTitle(preText, for: .normal)
                switchSrtBtn.backgroundColor = .gray
                switchSrtBtn.alpha = 0.8
                switchSrtBtn.layer.cornerRadius = 20
                switchSrtBtn.layer.borderWidth = 2
                switchSrtBtn.layer.borderColor = UIColor.black.cgColor
                switchSrtBtn.tag = 300 + i
                switchSrtBtn.addTarget(self, action: #selector(tappedswitchSrtBtn(_:)), for: .touchUpInside)
                
                srtView.addSubview(switchSrtBtn)
                switchSrtBtn.snp.makeConstraints { (makes) in
                    makes.height.equalTo(50)
                    makes.left.equalToSuperview().offset(10)
                    makes.right.equalToSuperview().offset(-10)
                    makes.bottom.equalToSuperview().offset((-55)*(subtitleURLs.count - i - 1) - 5)
                }
            }
            //            }
        }else{
            self.setSrtBtnImage()
            self.cleanSubViewsBy(tag: 300)
//            for view in self.view.subviews{
//                if view.tag == 300{
//                    view.removeFromSuperview()
//                }
//            }
        }
    }
    
    // 10/26
    // 10/29 修改
    @objc func tappedswitchSrtBtn(_ sender:AnyObject?){
        print("sender.tag:\(sender?.tag)")
        var tag = 0
        if sender != nil{
            tag = Int(sender!.tag) - 300
        }
        self.currentSub = tag
        if self.currentSub == 0{
            //            let url = URL(string: "")
            //            vlcPlayer.addPlaybackSlave(url, type: .subtitle, enforce: true)
            //            vlcPlayer.media.
            //            libvlc.
            let path = Bundle.main.path(forResource: "empty", ofType: "vtt")
            let url = URL(fileURLWithPath: path!)
            vlcPlayer.addPlaybackSlave(url, type: .subtitle, enforce: true)
        }else{
            print("self.subtitleURLs[self.currentSub]:\(self.subtitleURLs[self.currentSub])")
            let srtUrl = URL(string: self.subtitleURLs[self.currentSub])
            vlcPlayer.addPlaybackSlave(srtUrl, type: .subtitle, enforce: true)
        }
        //        let srtUrl = URL(string: self.subtitleURLs[self.currentSub])
        //        vlcPlayer.addPlaybackSlave(srtUrl, type: .subtitle, enforce: true)
        self.openSrtView(openOrClose: false)
    }
    /**
     每次都要用判斷是否要用 path 方法來設定字幕 URL 太麻煩 乾脆統整在這
     - Date: 10/30  Mike
     */
    func setSrt(){
        let url = self.subtitleURLs[self.currentSub]
        if url == ""{
            let path = Bundle.main.path(forResource: "empty", ofType: "vtt")
            let srtUrl = URL(fileURLWithPath: path!)
            vlcPlayer.addPlaybackSlave(srtUrl, type: .subtitle, enforce: true)
        }else{
            let srtUrl = URL(string: url)
            vlcPlayer.addPlaybackSlave(srtUrl, type: .subtitle, enforce: true)
        }
        self.setSrtBtnImage()
    }
    /**
     重設 SubArray 的地方不只一個 簡化在此
     - Date: 10/30  Mike
     */
    func resetSubArray(){
        self.subtitleURLs = []
        self.subtitleURLs.append("")
    }
    /**
     設定 srtBtn Image
     - Date: 10/30  Mike
     */
    func setSrtBtnImage(){
        if self.subtitleURLs.count != 1 && self.currentSub != 0 {
            self.srtBtn.setImage(UIImage(named: "playerSrt"), for: .normal)
        }else{
            self.srtBtn.setImage(UIImage(named: "playerSrt_Ori"), for: .normal)
        }
    }
    /**
     try to draw a thumb
     
     - Date: 11/30
     */
    func drawThumb() -> UIImage {
        // Create a context of the starting image size and set it as the current one
        let image : UIImage = UIImage()
        UIGraphicsBeginImageContext(image.size)
        
        // Draw the starting image in the current context as background
        image.draw(at: CGPoint.zero)
        
        // Get the current context
        let context = UIGraphicsGetCurrentContext()!
        
        // Draw a red line
        context.setLineWidth(2.0)
        context.setStrokeColor(UIColor.red.cgColor)
        context.move(to: CGPoint(x: 100, y: 100))
        context.addLine(to: CGPoint(x: 200, y: 200))
        context.strokePath()
        
//        // Draw a transparent green Circle
//        context.setStrokeColor(UIColor.green.cgColor)
//        context.setAlpha(0.5)
//        context.setLineWidth(10.0)
//        context.addEllipse(in: CGRect(x: 100, y: 100, width: 100, height: 100))
//        context.drawPath(using: .stroke) // or .fillStroke if need filling
        
        // Save the context as a new UIImage
        let myImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        // Return modified image
        return myImage!
    }
    /**
     Hide controlView
     - Date: 12/02  Mike
     */
    func delayForHideControlView(){
        /**
         Ｗhen test controlView, Mark Here
         When #release, Open Here
         - Note: Be care here before release
         - Date: 12/02   Mike
         */
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.controlView.isHidden = true
        }
    }
    /**
     Play Ad
     - Date: 12/03  Mike
     */
    func playAD(index: Int){
//        print("12/09 try playAD index:\(index)")
        if adDatas[index].adStreamUrl != "" && currentPlayerState != .trailer{
            self.vlcPlayer.pause()
            self.adView.isHidden = false
            let adMedia = VLCMedia(url: URL(string: adDatas[index].adStreamUrl)!)
            self.adPlayer.media = adMedia
            self.adPlayer.play()
            // 12/09
            print("12/09 self.adPlayer.media.length:\(self.adPlayer.media.length)")
            // 12/08
            self.adDatas[index].played = true
            self.adView.bringSubviewToFront(skipADBtn)
        }
    }
    // 12/10  Mike
    func countForSkip(){
        self.skipADBtn.setTitle("5", for: .normal)
        
        countForSkipTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (_) in
            
            switch self.skipADBtn.titleLabel!.text {
            case "5":
                self.skipADBtn.setTitle("4", for: .normal)
            case "4":
                self.skipADBtn.setTitle("3", for: .normal)
            case "3":
                self.skipADBtn.setTitle("2", for: .normal)
            case "2":
                self.skipADBtn.setTitle("1", for: .normal)
            case "1":
                self.skipADBtn.setTitle("Skip", for: .normal)
            case "Skip":
                print("Can skip Ad now")
                self.countForSkipTimer!.invalidate()
            case .none:
                print("none")
            case .some(_):
                print("some?")
            }
        })
    }
    /**
     撈取 xml 資料
     - Data: 12/24  Mike
     */
    func getXmlData(index: Int,completion : @escaping ()->()){
        AF.request(self.adDatas[index].xmlUrl)
            .response{ (response) in
                if response.value != nil{
                    if let data = response.value, let utf8Text = String(data: data!, encoding: .utf8) {
//                        print("12/25 get data")
                        self.pharseXmlData(index: index, xmlString: utf8Text)
                        completion()
                    }
                }else{
                    print("response.value = nil")
                    completion()
                }
            }
//        return
    }
    /**
     解析 xml 資料
     - Date: 12/24  Mike
     */
    func pharseXmlData(index: Int, xmlString: String){
//        if index == 0{
//            print("Befor: \(self.adDatas[index].adStreamUrl)")
//        }
        
        var cc1: [String] = []
        var cc2: [String] = []
        // skipoffset
        cc1 = xmlString.components(separatedBy: "skipoffset=\"")
        if cc1.count > 1 {
            cc2 = cc1[1].components(separatedBy: "\">")
            if cc2.count > 1{
                self.adDatas[index].skipTime = self.timeStrToInt(timeStr: cc2[0])
            }else{
                print("skipoffset=\" 2nd error")
            }
        }else{
            print("skipoffset=\" error")
        }
        
        // Duration
        cc1 = xmlString.components(separatedBy: "<Duration>")
        if cc1.count > 1 {
            cc2 = cc1[1].components(separatedBy: "<")
            if cc2.count > 1{
//                tempData.duration = cc2[0]
//                self.adDatas[index].
//                print("\(index).duration = \(cc2[0])")
            }else{
                print("<Duration> 2nd error")
            }
        }else{
            print("<Duration> error")
        }
        
        // mediaUrl
        cc1 = xmlString.components(separatedBy: "<MediaFile ")
        if cc1.count > 1 {
//            debugPrint(cc1[0])
//            debugPrint(cc1[1])
            cc2 = cc1[1].components(separatedBy: "<![CDATA[")
            if cc2.count > 1{
//                debugPrint(cc2[0])
//                debugPrint(cc2[1])
                cc1 = cc2[1].components(separatedBy: "]]>")
//                debugPrint(cc1[0])
                if cc1.count > 1{
//                    debugPrint(cc1[0])
//                    debugPrint(cc1[1])
//                    tempData.mediaUrl = cc1[0]
                    self.adDatas[index].adStreamUrl = cc1[0]
                }else{
                    print("<![CDATA[ 3rd error")
                }
//                tempData.mediaUrl = cc2[0]
//                cc1 =
            }else{
                print("<![CDATA[ 2nd error, no CDATA tag")
                // for no CDATA tag
                cc2 = cc1[1].components(separatedBy: ">")
//                debugPrint(cc2[0])
//                debugPrint(cc2[1])
                if cc2.count > 1{
                    cc1 = cc2[1].components(separatedBy: "</M")
                    if cc1.count > 1{
//                        tempData.mediaUrl = cc1[0]
                        self.adDatas[index].adStreamUrl = cc1[0]
                    }else{
                        print("no CDATA tag 3rd step error")
                    }
                }else{
                    print("no CDATA tag 2nd step error")
                }
                
            }
        }else{
            print("<![CDATA[ error")
        }
        
//        if index == 0{
//            print("After: \(self.adDatas[index].adStreamUrl)")
//        }
        return
    }
    
//    /**
//     用來記錄 xml 資料
//     - Date: 12/24  Mike
//     */
//    struct ＸmlData {
//        var skipoffset : String
//        var duration : String
//        var mediaUrl : String
//
//        init(){
//            skipoffset = ""
//            duration = ""
//            mediaUrl = ""
//        }
//    }
}
/**
 紀錄 AD Date 的 struct
 - Date: 11/26  Mike
 */
struct AdData {
    var playTime : Int = 0
    var xmlUrl : String = ""
    var adStreamUrl: String = ""
    var skipTime: Int = 0
    var length: Int = 0
    var played: Bool = false
    /**
     Record ad_breaks_config value
     0 : 自訂, 1 : 開頭, 2 : 中間, 3 : 結尾
     */
    var config: Int = -1
}


/**
 創一個狀態機來記錄目前影片播放的狀態
 配合各 player 的 state 來處理在 func mediaPlayerStateChanged 裡面的應對
 - Date: 12/08  Mike
 */
enum playerState{
    case none
    case playerPlayed
    case playerPaused
    case adPlayed
    case adBuffering
    case adEnded
    case trailer
}
///**
// Try to set slider height
// https://stackoverflow.com/questions/31590188/how-to-customise-uislider-height
// - Date: 12/02  Mike
// */
//class CustomSlide: UISlider {
//
//     @IBInspectable var trackHeight: CGFloat = 15
//
//    override func trackRect(forBounds bounds: CGRect) -> CGRect {
//         //set your bounds here
//        return CGRect(origin: bounds.origin, size: CGSize(width: bounds.width, height: trackHeight))
//
//
//
//       }
//}
