//
//  Catalog.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2019/6/6.
//  Copyright © 2019 蒼月喵. All rights reserved.
//

import Foundation

struct Catalog {
    var catalog_id: String = ""
    var name: String = ""
    var parent: String? = nil
    var is_adult: String = ""
    var icon_url: String? = nil
    /**
     子分類的 packList 狀態
     nil     : 不是子分類不用設
     .null   : 這個子分類裡面沒有 pack
     .noMore : pack 數目 <= 5
     .more   : pack 數目 > 5 => 會顯示 more 選項
     */
    var childCatalogPackStaus: packStaus? = nil
    /** 存放子分類要顯示在頁面上的 pack data  */
    var childShowPack: [Pack]? = nil
}

enum packStaus {
    case null
    case noMore
    case more
}
