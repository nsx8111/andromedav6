//
//  SearchData.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2020/10/22.
//  Copyright © 2020 蒼月喵. All rights reserved.
//

import Foundation

struct SearchData {
    var productID: Int = 0
    var productName: String = ""
//    var contentID: Int = 0
    // productDescription 抓到的值可能為 null
    var productDescription: String? = nil
    var productType: String = ""
//    var lastestPlayTime: String = ""
    var startTime: String = ""
    // endTime 可能為 nil
    var endTime: String? = nil
//    var contentName: String = ""
//    var contentDescription: String = ""
    var productImage: [PI] = []
}

struct PI {
//    "product_image_id": "product_176_2.jpg",
//    "product_image_type_id": 2,
//    "product_image_type": "Portrait thumbnail",
//    "name": "lucy-0612_poster_V",
//    "description": null,
//    "url": "https://rddev.andromeda.tti.tv/AndromedaProduct/image/176/product_176_2.jpg"
    var productImageId: String = ""
    var productImageTypeId: Int = 0
    // productImageType 也許可以改成 enum
    var productImageType: String = ""
    var url: String = ""
    // description 可能為 null
    var description: String? = nil
    var name: String = ""
}
