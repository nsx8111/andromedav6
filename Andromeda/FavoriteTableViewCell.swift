//
//  FavoriteTableViewCell.swift
//  Andromeda
//
//  Created by 洋洋 on 2020/10/21.
//  Copyright © 2020 蒼月喵. All rights reserved.
//

import UIKit
import SnapKit

class FavoriteTableViewCell: UITableViewCell {
    /** for content image */
    var packImage = UIImageView()
    /** for tapped cell */
    var bkImage = UIImageView()
    
    var nameTitleLabel = UILabel()
    var nameValueLabel = UILabel()
    
    var categoryTitleLabel = UILabel()
    var categoryValueLabel = UILabel()
    
    var typeTitleLabel = UILabel()
    var typeValueLabel = UILabel()
    
    var creationTitleLabel = UILabel()
    var creationValueLabel = UILabel()
    
    var updateTitleLabel = UILabel()
    var updateValueLabel = UILabel()
    
    var lineView1 = UIView()
    var lineView2 = UIView()
    var lineView3 = UIView()
    var lineView4 = UIView()
    var lineView5 = UIView()
    
    //    //waitting to mark
    //    var descriptionTextView = UITextView()
    
    /** pageScrol 影片縮圖的高度 */
    //    let pageScrollImageHeight: CGFloat = 292
    let pageScrollImageHeight : CGFloat = {
        var cc : CGFloat = 292
        //        if deviceType == .iPhone {
        //            cc = 292 / 2
        //        }
        cc = tableViewRowHeight
        return cc
    }()
    /** pageScrol 影片縮圖的高度 */
    //    let pageScrollImageWidth: CGFloat = 219
    let pageScrollImageWidth : CGFloat = {
        var cc : CGFloat = 219
        //        if deviceType == .iPhone {
        //            cc = 292 / 2
        //        }
        cc = tableViewRowHeight
        return cc
    }()
    
    let leftOffset : CGFloat = {
        var cc : CGFloat = 30
        if deviceType == .iPhone {
            cc = 30 / 2
        }
        return cc
    }()
    
    let topOrBottomOffset : CGFloat = {
        var cc : CGFloat = 16
        if deviceType == .iPhone {
            cc = 16 / 2
        }
        return cc
    }()
    
    let fontSize : CGFloat = {
        var cc : CGFloat = 28
        if deviceType == .iPhone {
            cc = 28 / 2
        }else{
            cc = 28 / 1.5
        }
        return cc
    }()
    
    let timeFontSize : CGFloat = {
        var cc : CGFloat = 28
        if deviceType == .iPhone {
            cc = 13
        }else{
            cc = 28 / 1.5
        }
        return cc
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        bkImage.backgroundColor = .clear
        contentView.addSubview(bkImage)
        bkImage.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
        }
        
        contentView.addSubview(packImage)
        packImage.snp.makeConstraints { (makes) in
            makes.left.equalToSuperview().offset(leftOffset*1.0)
            makes.centerY.equalToSuperview()
            if productImageTypeID == 2{
                makes.width.equalTo(pageScrollImageWidth/1)
                makes.height.equalTo(pageScrollImageHeight/1)
            }else{
                makes.width.equalTo(pageScrollImageWidth/1.2)
                makes.height.equalTo(pageScrollImageHeight/1.2)
            }
        }
        
        let lineViewOffset = pageScrollImageHeight/6
        
        contentView.addSubview(lineView1)
        lineView1.snp.makeConstraints { (makes) in
            makes.left.equalTo(packImage.snp.right).offset(0)
            makes.right.equalToSuperview()
            makes.top.equalToSuperview().offset(lineViewOffset)
            makes.height.equalTo(1)
        }
        
        contentView.addSubview(lineView2)
        lineView2.snp.makeConstraints { (makes) in
            makes.left.equalTo(packImage.snp.right).offset(0)
            makes.right.equalToSuperview()
            makes.top.equalToSuperview().offset(lineViewOffset*2)
            makes.height.equalTo(1)
        }
        
        contentView.addSubview(lineView3)
        lineView3.snp.makeConstraints { (makes) in
            makes.left.equalTo(packImage.snp.right).offset(0)
            makes.right.equalToSuperview()
            makes.top.equalToSuperview().offset(lineViewOffset*3)
            makes.height.equalTo(1)
        }
        
        contentView.addSubview(lineView4)
        lineView4.snp.makeConstraints { (makes) in
            makes.left.equalTo(packImage.snp.right).offset(0)
            makes.right.equalToSuperview()
            makes.top.equalToSuperview().offset(lineViewOffset*4)
            makes.height.equalTo(1)
        }
        
        contentView.addSubview(lineView5)
        lineView5.snp.makeConstraints { (makes) in
            makes.left.equalTo(packImage.snp.right).offset(0)
            makes.right.equalToSuperview()
            makes.top.equalToSuperview().offset(lineViewOffset*5)
            makes.height.equalTo(1)
        }
        
        creationTitleLabel.text = "Creation time : "
        creationTitleLabel.textColor = .white
        creationTitleLabel.font = UIFont(name: "Georgia-Bold", size: fontSize)
        contentView.addSubview(creationTitleLabel)
        creationTitleLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(lineView4)
            makes.left.equalTo(packImage.snp.right).offset(offset1)
//            makes.left.greaterThanOrEqualTo(packImage.snp.right).offset(offset1)
        }
        
        creationValueLabel.numberOfLines = 0
        creationValueLabel.text = ""
        creationValueLabel.textColor = .white
        creationValueLabel.font = UIFont(name: "Georgia-Bold", size: timeFontSize)
        contentView.addSubview(creationValueLabel)
        creationValueLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(lineView4)
            makes.left.equalTo(creationTitleLabel.snp.right)
//            makes.right.lessThanOrEqualToSuperview().offset(-1)
//            makes.left.greaterThanOrEqualTo(creationTitleLabel.snp.right).offset(0)
        }
        
        updateTitleLabel.text = "Update time : "
        updateTitleLabel.textColor = .white
        updateTitleLabel.font = UIFont(name: "Georgia-Bold", size: fontSize)
        contentView.addSubview(updateTitleLabel)
        updateTitleLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(lineView5)
            makes.right.equalTo(creationTitleLabel)
        }
        
        updateValueLabel.numberOfLines = 0
        updateValueLabel.text = ""
        updateValueLabel.textColor = .white
        updateValueLabel.font = UIFont(name: "Georgia-Bold", size: timeFontSize)
        contentView.addSubview(updateValueLabel)
        updateValueLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(lineView5)
            makes.left.equalTo(updateTitleLabel.snp.right)
        }
        
        typeValueLabel.numberOfLines = 1
        typeValueLabel.text = ""
        typeValueLabel.textColor = .white
        typeValueLabel.font = UIFont(name: "Georgia-Bold", size: fontSize)
        contentView.addSubview(typeValueLabel)
        typeValueLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(lineView3)
            makes.left.equalTo(creationValueLabel.snp.left)
        }
        
        typeTitleLabel.text = "Type : "
        typeTitleLabel.textColor = .white
        typeTitleLabel.font = UIFont(name: "Georgia-Bold", size: fontSize)
        contentView.addSubview(typeTitleLabel)
        typeTitleLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(lineView3)
            makes.right.equalTo(creationTitleLabel)
        }
        
        categoryValueLabel.numberOfLines = 1
        categoryValueLabel.text = ""
        categoryValueLabel.textColor = .white
        categoryValueLabel.font = UIFont(name: "Georgia-Bold", size: fontSize)
        contentView.addSubview(categoryValueLabel)
        categoryValueLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(lineView2)
            makes.left.equalTo(creationValueLabel.snp.left)
        }
        
        categoryTitleLabel.text = "Category : "
        categoryTitleLabel.textColor = .white
        categoryTitleLabel.font = UIFont(name: "Georgia-Bold", size: fontSize)
        contentView.addSubview(categoryTitleLabel)
        categoryTitleLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(lineView2)
            makes.right.equalTo(creationTitleLabel)
        }
        
        nameValueLabel.numberOfLines = 2
        nameValueLabel.text = ""
        nameValueLabel.textColor = .white
        nameValueLabel.font = UIFont(name: "Georgia-Bold", size: fontSize)
        contentView.addSubview(nameValueLabel)
        nameValueLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(lineView1)
            makes.left.equalTo(creationValueLabel.snp.left)
            makes.right.lessThanOrEqualToSuperview().offset(-offset1)
        }
        
        nameTitleLabel.text = "Name : "
        nameTitleLabel.textColor = .white
        nameTitleLabel.font = UIFont(name: "Georgia-Bold", size: fontSize)
        contentView.addSubview(nameTitleLabel)
        nameTitleLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(lineView1)
            makes.right.equalTo(creationTitleLabel)
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
  
}
