//
//  FaceADViewController.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2019/5/16.
//  Copyright © 2019 蒼月喵. All rights reserved.
//

import UIKit
import SnapKit

class FaceADViewController: UIViewController {
    weak var ButtonDelegate: ButtonDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /** For test */
        let b = UIButton()
        view.addSubview(b)
        
        b.snp.makeConstraints { (make) in
            make.width.equalTo(self.view.bounds.width*0.3)
            make.height.equalTo(self.view.bounds.width*0.3)
            make.center.equalToSuperview()
        }
        
        b.backgroundColor = .white
        b.setTitle("Test Button", for: .normal)
        b.titleLabel?.textColor = .blue
        b.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
    }
    
    @objc func buttonAction(){
        print("buttonAction")
        
        let tmpPack: Pack = Pack()
        
        ButtonDelegate?.toPackPage(packData: tmpPack)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("FAD prepare")
        if let destination = segue.destination as? PKDViewController{
            destination.tmpText = "from F"
        }
    }
    
    
    
    @IBAction func menuClick(_ sender: Any) {
        navigationController?.view.menu()
        ButtonDelegate?.clickLeft()
    }
    @IBAction func userClick(_ sender: Any) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        navigationController?.view.menu2()
        ButtonDelegate?.clickRight()
    }
        
    @IBAction func handleEdgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger(location: location, state: sender.state)
        if beganX2 > 0 {
            ButtonDelegate?.clickLeft()
        }
        
    }
    @IBAction func handleEdgePanGesture2(_ sender: UIScreenEdgePanGestureRecognizer) {
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger2(location: location, state: sender.state)
        if beganX2 < 0{
            ButtonDelegate?.clickRight()
        }
        
    }
    
    @IBAction func handleTapGesture(_ sender: Any) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        if navigationController?.view.superview?.frame.origin.x != 0 {
            navigationController?.view.menu()
        }
    }
    
}

