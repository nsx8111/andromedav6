//
//  MenuDelegate.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2018/10/3.
//  Copyright © 2018年 蒼月喵. All rights reserved.
//

import Foundation

protocol MenuDelegate:class {
    func reloadAfterLoad(ProductType: ProductType)
    func changeViedoMenuView(_ viewNum: Int)
    func changeUserMenuView(_ viewNum: Int)
    func toCatalogPage(ProductType: ProductType)
//    func showLeft()
}
