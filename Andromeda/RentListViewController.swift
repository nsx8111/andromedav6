//
//  RentListViewController.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2019/5/15.
//  Copyright © 2019 蒼月喵. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire
import SwiftyJSON

extension RentListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == purchaseTableView {
            return purchaseCount
        }else if tableView == rentTableView{
            return rentCount
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if purchaseTableView == tableView {
            let cell = purchaseTableView.dequeueReusableCell(withIdentifier: "RentCell") as! RentTableViewCell
            cell.backgroundColor = .clear
            cell.selectionStyle = .none
            cell.separatorInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
            
            if purchaseLists.count > 0{
                cell.nameValueLabel.text = purchaseLists[indexPath.row].productName
                cell.typeValueLabel.text = purchaseLists[indexPath.row].productType
                cell.timeValueLabel.text = purchaseLists[indexPath.row].purchaseTime
                
                if purchaseLists[indexPath.row].productImage.count > 1{
                    cell.packImage.downloaded(from: purchaseLists[indexPath.row].productImage[0].url)
                }else if purchaseLists[indexPath.row].productImage.count == 1{
                    cell.packImage.downloaded(from: purchaseLists[indexPath.row].productImage[0].url)
                }else{
                    cell.packImage.image = UIImage(named: "noContentImage")
                }
            }
            cell.tag = indexPath.row
            let tap = UITapGestureRecognizer(target: self, action: #selector(clickCell(_:)))
            cell.addGestureRecognizer(tap)
            return cell
        }else {
            let cell2 = rentTableView.dequeueReusableCell(withIdentifier: "RentCell") as! RentTableViewCell
            cell2.backgroundColor = .clear
            cell2.selectionStyle = .none
            cell2.separatorInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
            
            if rentLists.count > 0{
                cell2.nameValueLabel.text = rentLists[indexPath.row].productName
                cell2.typeValueLabel.text = rentLists[indexPath.row].productType
                cell2.timeValueLabel.text = rentLists[indexPath.row].rentEndTime
                
                if rentLists[indexPath.row].productImage.count > 1{
                    cell2.packImage.downloaded(from: rentLists[indexPath.row].productImage[0].url)
                }else if rentLists[indexPath.row].productImage.count == 1{
                    cell2.packImage.downloaded(from: rentLists[indexPath.row].productImage[0].url)
                }else{
                    cell2.packImage.image = UIImage(named: "noContentImage")
                }
            }
            cell2.tag = indexPath.row
            let tap = UITapGestureRecognizer(target: self, action: #selector(clickCell(_:)))
            cell2.addGestureRecognizer(tap)
            return cell2
        }
    }
    
    @objc func clickCell(_ sender:AnyObject?) {
        let touch = sender?.location(in: purchaseTableView)
        if purchaseTableView.indexPathForRow(at: touch!) != nil {
            tableViewTag = 1
        }
        let touch2 = sender?.location(in: rentTableView)
        if rentTableView.indexPathForRow(at: touch2!) != nil {
            tableViewTag = 2
        }
        
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        if navigationController?.view.superview?.frame.origin.x != 0 {
            if navigationController?.view.superview?.frame.origin.x == -navigationOriginX {
                navigationController?.view.menu2()
            }else{
                navigationController?.view.menu()
            }
        }else{
            print(sender?.view.tag as Any)
            let index : Int = sender?.view.tag ?? -1
            if index != -1 {
                setActView(openOrClose: true)
                getPackDetailNew(index: index) { [weak self] in
                    guard let self = self else { return }
                    self.setActView(openOrClose: false)
                    self.ButtonDelegate?.toPackPageNew(packDataNew: self.tmpPackDetailNew)
                    self.setActView(openOrClose: false)
                }
            }
        }
    }
}


class RentListViewController: UIViewController {
    weak var ButtonDelegate: ButtonDelegate?
    var p: PullToRefreshControl!
    
    @IBOutlet weak var pageTitleLabel: UILabel!
    var purchaseLabel = UILabel()
    var rentLabel = UILabel()
    
    var canRefresh = true
    var refreshControl: UIRefreshControl!
    var purchaseTableView = UITableView()
    var rentTableView = UITableView()
    var refreshScrollView = UIScrollView()
    var containerView = UIView()
    
    var purchaseCount = 0
    var rentCount = 0
    var tableViewTag = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
//        setPullToRefreshControl()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setActView(openOrClose: true)
        getPurchaseData(){ [weak self] in
            guard let self = self else { return }
            self.getRentData(){ [weak self] in
                guard let self = self else { return }
                self.purchaseTableView.snp.updateConstraints { (makes) in
                    makes.height.equalTo(self.purchaseTableView.rowHeight * CGFloat(self.purchaseCount))
                }
                self.rentTableView.snp.updateConstraints { (makes) in
                    makes.height.equalTo(self.rentTableView.rowHeight * CGFloat(self.rentCount))
                }
                self.purchaseTableView.reloadData()
                self.rentTableView.reloadData()
                self.setActView(openOrClose: false)
            }
        }
    }
    
    func setViews(){
        ///下滑刷新TableView用
        view.addSubview(refreshScrollView)
        refreshScrollView.bounces = true
        refreshScrollView.translatesAutoresizingMaskIntoConstraints = false
        refreshScrollView.backgroundColor = .tableColor
        refreshScrollView.snp.makeConstraints { (makes) in
            makes.top.equalTo(pageTitleLabel.snp.bottom).offset(offset3)
            makes.width.equalTo(view)
            makes.centerX.equalTo(view)
            makes.bottom.equalTo(view)
        }
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        refreshControl.attributedTitle = NSAttributedString(string: "loading...", attributes: attributes)
        refreshControl.tintColor = .white
        refreshScrollView.addSubview(refreshControl)
        
        refreshScrollView.addSubview(containerView)
        containerView.snp.makeConstraints { (makes) in
            makes.top.bottom.equalTo(refreshScrollView)
            makes.width.equalTo(view.bounds.width)
            makes.centerX.equalTo(view)
            makes.height.greaterThanOrEqualToSuperview().offset(1)
        }
        
        purchaseLabel.text = "Purchase List"
        purchaseLabel.textColor = .white
        purchaseLabel.textAlignment = .left
        purchaseLabel.font = .boldSystemFont(ofSize: offset4)
        containerView.addSubview(purchaseLabel)
        purchaseLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(containerView).offset(offset1)
            makes.left.equalTo(containerView).offset(offset2)
        }
        
        purchaseTableView.register(RentTableViewCell.self, forCellReuseIdentifier: "RentCell")
        purchaseTableView.delegate = self
        purchaseTableView.dataSource = self
        purchaseTableView.bounces = false
        purchaseTableView.rowHeight = tableViewRowHeight
        purchaseTableView.backgroundColor = .buttonColor
        purchaseTableView.separatorColor = .white
        purchaseTableView.tableFooterView = UIView()
        containerView.addSubview(purchaseTableView)
        purchaseTableView.snp.makeConstraints { (makes) in
            makes.left.right.equalToSuperview()
            makes.top.equalTo(purchaseLabel.snp.bottom).offset(offset1)
            makes.height.equalTo(purchaseTableView.rowHeight * CGFloat(purchaseCount))
        }
        
        rentLabel.text = "Rent List"
        rentLabel.textColor = .white
        rentLabel.textAlignment = .left
        rentLabel.font = .boldSystemFont(ofSize: offset4)
        containerView.addSubview(rentLabel)
        rentLabel.snp.makeConstraints { (makes) in
            makes.left.equalTo(containerView).offset(offset2)
            makes.top.equalTo(purchaseTableView.snp.bottom).offset(offset3)
        }
        
        rentTableView.register(RentTableViewCell.self, forCellReuseIdentifier: "RentCell")
        rentTableView.delegate = self
        rentTableView.dataSource = self
        rentTableView.bounces = false
        rentTableView.rowHeight = tableViewRowHeight
        rentTableView.backgroundColor = .labelTextColor4
        rentTableView.separatorColor = .white
        rentTableView.tableFooterView = UIView()
        containerView.addSubview(rentTableView)
        rentTableView.snp.makeConstraints { (makes) in
            makes.left.right.equalToSuperview()
            makes.top.equalTo(rentLabel.snp.bottom).offset(offset1)
            makes.height.equalTo(rentTableView.rowHeight * CGFloat(rentCount))
            makes.bottom.lessThanOrEqualToSuperview().offset(-offset5)
        }
        
    }
    /**
     下拉刷新資料
     - Date: 02/08 Yang
     */
    func setPullToRefreshControl(){
        p = PullToRefreshControl(scrollView: refreshScrollView).addGifHeader(config: { (gifHeader) in
            gifHeader.gifFrame = CGRect(x: 40, y: 20, width: 100, height: 60)
            let imgArr = [UIImage]()
            gifHeader.setImgArr(state: .pulling, imgs: imgArr)
            gifHeader.setImgArr(state: .refreshing, imgs: imgArr, animationTime: 2.0)
        }).addGifFooter(config: { (gifFooter) in })
        
        p.header?.addAction(with: .refreshing, action: { [unowned self] in
            //模拟数据请求
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: {
                self.refresh()
                self.p.header?.endRefresh()
            })
        }).addAction(with: .end, action: {
            print("那啥 结束了都")
        })
    }
    /**
     下拉刷新資料
     - Date: 02/08 Yang
     */
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        if !refreshScrollView.isDragging {
            refresh() // refresh is another method for your reloading jobs
        }
    }
    
    @objc func refresh(){
        getPurchaseData(){ [weak self] in
            guard let self = self else { return }
            self.getRentData(){ [weak self] in
                guard let self = self else { return }
                self.purchaseTableView.snp.updateConstraints { (makes) in
                    makes.height.equalTo(Int(self.purchaseTableView.rowHeight) * self.purchaseCount)
                }
                self.rentTableView.snp.updateConstraints { (makes) in
                    makes.height.equalTo(Int(self.rentTableView.rowHeight) * self.rentCount)
                }
                self.rentTableView.reloadData()
                self.purchaseTableView.reloadData()
                self.refreshControl.endRefreshing()
            }
        }
    }
    /**
     取得租借產品或產品內容
     - Date: 12/25 Yang
     */
    func getRentData(completion : @escaping ()->()) {
        let params = [
            "token":"\(token)"
        ] as [String : Any]
        
        var tempRentLists: [RentList] = []
        
        let getRentProductAPI = serverUrlNew + "purchase/getRentProduct"
        
        AF.request(getRentProductAPI, method: .post, parameters: params)
            .responseJSON{ (response) in
                if response.value != nil{
                    let swiftyJsonVar = JSON(response.value!)
                    let status: Int = swiftyJsonVar["status"].int!
                    
                    if status == 200 {
                        if let resultObject = swiftyJsonVar["result"].dictionaryObject, let productArray = resultObject["rent_product"] {
                            //                            print("productCount \((productArray as AnyObject).count!)")
                            //                            print("productArray \(productArray)")
                            
                            let swiftyJsonVar2 = JSON(productArray)
                            if let productArray2 = swiftyJsonVar2.arrayObject {
                                let reversedProductArray : [Any] = Array(productArray2.reversed())
                                
                                for productData in reversedProductArray {
                                    let productJson = JSON(productData)
                                    var tempProductData: RentList = RentList()
                                    
                                    if productJson["product_id"].int != nil{
                                        tempProductData.productID = productJson["product_id"].int!
                                        //                                        print("tempProductID \(tempProductData.productID)")
                                    }
                                    if productJson["product_type"].string != nil{
                                        tempProductData.productType = productJson["product_type"].string!
                                    }
                                    if productJson["product_name"].string != nil{
                                        tempProductData.productName = productJson["product_name"].string!
                                        //                                        print("productName \(tempProductData.productName)")
                                    }
                                    if productJson["rent_end_time"].string != nil{
                                        tempProductData.rentEndTime = productJson["rent_end_time"].string!
                                    }
                                    
                                    var CIs: [Product_image2] = []
                                    if productJson["product_image"].count > 0 {
                                        let cis = productJson["product_image"].arrayObject
                                        for ci in cis!{
                                            let ciJSON = JSON(ci)
                                            var tempCI: Product_image2 = Product_image2()
                                            
                                            if ciJSON["product_image_type_id"].int != nil{
                                                tempCI.product_image_type_id = ciJSON["product_image_type_id"].int!
                                                productImageTypeID = ciJSON["product_image_type_id"].int!
                                                //                                                print("productImageTypeID \(productImageTypeID)")
                                            }
                                            if ciJSON["product_image_id"].string != nil{
                                                tempCI.product_image_id = ciJSON["product_image_id"].string!
                                            }
                                            if ciJSON["product_image_type"].string != nil{
                                                tempCI.product_image_type = ciJSON["product_image_type"].string!
                                            }
                                            if ciJSON["url"].string != nil{
                                                tempCI.url = ciJSON["url"].string!
                                            }
                                            if ciJSON["name"].string != nil{
                                                tempCI.name = ciJSON["name"].string!
                                            }
                                            if ciJSON["description"].string != nil{
                                                tempCI.description = ciJSON["description"].string!
                                            }
                                            CIs.append(tempCI)
                                        }
                                        tempProductData.productImage = CIs
                                    }
                                    tempRentLists.append(tempProductData)
                                }
                            }
                        }
                        rentLists = tempRentLists
                        self.rentCount = rentLists.count
                        print("rentCount \(self.rentCount)")
                        //                        self.rentCount = 0
                        completion()
                    }else if status == 204 {
                        completion()
                    }else{
                        print("get rent error")
                        self.backToLoginVC(title:"Logout",msg: swiftyJsonVar["message"].string!,okMsg: "Back to LoginPage.")
                        completion()
                    }
                }else{
                    //                    print("response.result.isFalse @RentData")
                    rentLists = []
                    self.rentTableView.reloadData()
                    completion()
                }
            }
        
    }
    /**
     取得購買產品或產品內容
     - Date: 12/24 Yang
     */
    func getPurchaseData(completion : @escaping ()->()) {
        let params = [
            "token":"\(token)"
        ] as [String : Any]
        
        var tempPurchaseLists: [PurchaseList] = []
        
        let getPurchaseProductAPI = serverUrlNew + "purchase/getPurchaseProduct"
        
        AF.request(getPurchaseProductAPI, method: .post, parameters: params)
            .responseJSON{ (response) in
                if response.value != nil{
                    let swiftyJsonVar = JSON(response.value!)
                    let status: Int = swiftyJsonVar["status"].int!
                    
                    if status == 200 {
                        if let resultObject = swiftyJsonVar["result"].dictionaryObject, let productArray = resultObject["purchase_product"] {
                            
                            let swiftyJsonVar2 = JSON(productArray)
                            if let productArray2 = swiftyJsonVar2.arrayObject {
                                let reversedProductArray : [Any] = Array(productArray2.reversed())
                                
                                for productData in reversedProductArray {
                                    let productJson = JSON(productData)
                                    var tempProductData: PurchaseList = PurchaseList()
                                    
                                    if productJson["product_id"].int != nil{
                                        tempProductData.productID = productJson["product_id"].int!
                                    }
                                    if productJson["product_type"].string != nil{
                                        tempProductData.productType = productJson["product_type"].string!
                                    }
                                    if productJson["product_name"].string != nil{
                                        tempProductData.productName = productJson["product_name"].string!
                                        //                                        print("productName \(tempProductData.productName)")
                                    }
                                    if productJson["purchase_time"].string != nil{
                                        tempProductData.purchaseTime = productJson["purchase_time"].string!
                                    }
                                    
                                    var CIs: [Product_image2] = []
                                    if productJson["product_image"].count > 0 {
                                        let cis = productJson["product_image"].arrayObject
                                        for ci in cis!{
                                            let ciJSON = JSON(ci)
                                            var tempCI: Product_image2 = Product_image2()
                                            
                                            if ciJSON["product_image_type_id"].int != nil{
                                                tempCI.product_image_type_id = ciJSON["product_image_type_id"].int!
                                                productImageTypeID = ciJSON["product_image_type_id"].int!
                                                //                                                print("productImageTypeID \(productImageTypeID)")
                                            }
                                            if ciJSON["product_image_id"].string != nil{
                                                tempCI.product_image_id = ciJSON["product_image_id"].string!
                                            }
                                            if ciJSON["product_image_type"].string != nil{
                                                tempCI.product_image_type = ciJSON["product_image_type"].string!
                                            }
                                            if ciJSON["url"].string != nil{
                                                tempCI.url = ciJSON["url"].string!
                                            }
                                            if ciJSON["name"].string != nil{
                                                tempCI.name = ciJSON["name"].string!
                                            }
                                            if ciJSON["description"].string != nil{
                                                tempCI.description = ciJSON["description"].string!
                                            }
                                            CIs.append(tempCI)
                                        }
                                        tempProductData.productImage = CIs
                                    }
                                    tempPurchaseLists.append(tempProductData)
                                }
                            }
                        }
                        purchaseLists = tempPurchaseLists
                        self.purchaseCount = purchaseLists.count
                        print("purchaseCount \(self.purchaseCount)")
                        //                        self.purchaseCount = 0
                        completion()
                    }else if status == 204 {
                        completion()
                    }else{
                        print(status)
                        self.alertMsg(msg: "get purchase error")
                        completion()
                    }
                }else{
                    //                    print("response.result.isFalse @PurchaseData")
                    purchaseLists = []
                    self.purchaseTableView.reloadData()
                    completion()
                }
            }
        
    }
    
    @IBAction func menuClick(_ sender: Any) {
        navigationController?.view.menu()
        ButtonDelegate?.clickLeft()
    }
    
    @IBAction func userClick(_ sender: Any) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        navigationController?.view.menu2()
        ButtonDelegate?.clickRight()
    }
    
    @IBAction func handleEdgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger(location: location, state: sender.state)
        if beganX2 > 0 {
            ButtonDelegate?.clickLeft()
        }
    }
    
    @IBAction func handleEdgePanGesture2(_ sender: UIScreenEdgePanGestureRecognizer) {
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger2(location: location, state: sender.state)
        if beganX2 < 0{
            ButtonDelegate?.clickRight()
        }
    }
    
    @IBAction func handleTapGesture(_ sender: Any) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        if navigationController?.view.superview?.frame.origin.x != 0 {
            if navigationController?.view.superview?.frame.origin.x == -navigationOriginX {
                navigationController?.view.menu2()
            }else{
                navigationController?.view.menu()
            }
        }
    }
    
    var tmpPackDetailNew: ProductCDetail = ProductCDetail()
    
    func getPackDetailNew(index: Int,completion : @escaping ()->()){
        var tempProductID = 0
        var tempProductImage = [Product_image2]()
        
        if tableViewTag == 1{
            tempProductID = purchaseLists[index].productID
            tempProductImage = purchaseLists[index].productImage
        }else{
            tempProductID = rentLists[index].productID
            tempProductImage = rentLists[index].productImage
        }
        
        tmpPackDetailNew = ProductCDetail()
        let params = [
            "product_id": tempProductID,
            "token":"\(token)"
        ] as [String : Any]
        
        AF.request("\(serverUrlNew)product/getProductDetail", method: .post, parameters: params)
            .responseJSON { (response) in
                if response.value != nil {
                    //   print(response.result.value!)
                    self.tmpPackDetailNew.product_id = tempProductID
                    
                    let swiftyJsonVar = JSON(response.value!)
                    let status: Int = swiftyJsonVar["status"].int!
                    //                print("status:\(status)")
                    if status == 200{
                        // token 正確, product_list not null
                        let restltJson = JSON(swiftyJsonVar["result"])
                        //                        print("restltJson in getChildCatalogShowNew :\(restltJson)")
                        let productLists = restltJson["product_content_detail"].arrayObject
                        //                        print("productLists:\(String(describing: productLists))")
                        //                        if productLists != nil{
                        for product in productLists! {
                            //                                print("Hi i : \(i)")
                            let productJson = JSON(product)
                            
                            self.tmpPackDetailNew.content_id = productJson["content_id"].int!
                            self.tmpPackDetailNew.content_name = productJson["content_name"].string!
                            self.tmpPackDetailNew.content_description = productJson["content_description"].string!
                            
                            var tempContentAttributeValueArray: [ContentAttributeValue] = []
                            
                            let contentAttributeValueArrayDatas = productJson["content_attribute_value"].arrayObject
                            for contentAttributeValueArrayData in contentAttributeValueArrayDatas!{
                                let contentAttributeValueDataJson = JSON(contentAttributeValueArrayData)
                                var tempContentAttributeValue: ContentAttributeValue = ContentAttributeValue()
                                
                                tempContentAttributeValue.attribute_id = contentAttributeValueDataJson["attribute_id"].int!
                                tempContentAttributeValue.attribute = contentAttributeValueDataJson["attribute"].string!
                                tempContentAttributeValue.attribute_value_id = contentAttributeValueDataJson["attribute_value_id"].int!
                                tempContentAttributeValue.attribute_value = contentAttributeValueDataJson["attribute_value"].string!
                                
                                tempContentAttributeValueArray.append(tempContentAttributeValue)
                            }
                            self.tmpPackDetailNew.contentAttributeValueArray = tempContentAttributeValueArray
                            
                            var tempStills: [Still] = []
                            
                            let stillsDatas = productJson["Stills"].arrayObject
                            for stillData in stillsDatas!{
                                let stillJson = JSON(stillData)
                                var tempStill: Still = Still()
                                
                                tempStill.assets_id = stillJson["assets_id"].string!
                                tempStill.name = stillJson["name"].string!
                                tempStill.url = stillJson["name"].string!
                                
                                var tempMetaDatas: [MetaData] = []
                                
                                let metaDatas = stillJson["meta_data"].arrayObject
                                
                                if metaDatas != nil{
                                    
                                    for metaData in metaDatas!{
                                        var tempMetaData: MetaData = MetaData()
                                        
                                        let metaDataJson = JSON(metaData)
                                        
                                        tempMetaData.attribute = metaDataJson["attribute"].string!
                                        tempMetaData.value = metaDataJson["value"].string!
                                        
                                        tempMetaDatas.append(tempMetaData)
                                    }
                                    
                                }
                                
                                tempStill.meta_data = tempMetaDatas
                                
                                tempStills.append(tempStill)
                            }
                            
                            self.tmpPackDetailNew.stills = tempStills
                            
                            var tempPosters: [Poster] = []
                            
                            let postersDatas = productJson["Poster"].arrayObject
                            
//                            print("In History VC, func getPackDetailNew, postersDatas:\(postersDatas!),postersDatas.count:\(postersDatas!.count)")
                            
                            if postersDatas?.count == 2 {
                                for posterData in postersDatas!{
                                    
                                    let posterDataJson = JSON(posterData)
                                    var tempPoster: Poster = Poster()
                                    //
                                    tempPoster.assets_id = posterDataJson["assets_id"].string!
                                    tempPoster.name = posterDataJson["name"].string!
                                    tempPoster.url = posterDataJson["url"].string!
                                    
                                    var tempMetaDatas: [MetaData] = []
                                    
                                    let metaDatas = posterDataJson["meta_data"].arrayObject
                                    
                                    if metaDatas != nil{
                                        
                                        for metaData in metaDatas!{
                                            var tempMetaData: MetaData = MetaData()
                                            
                                            let metaDataJson = JSON(metaData)
                                            
                                            tempMetaData.attribute = metaDataJson["attribute"].string!
                                            tempMetaData.value = metaDataJson["value"].string!
                                            
                                            tempMetaDatas.append(tempMetaData)
                                        }
                                        
                                    }
                                    
                                    tempPoster.meta_data = tempMetaDatas
                                    
                                    if metaDatas!.count != 0{
                                        tempPosters.append(tempPoster)
                                    }
                                    
                                }
                            }else{
                                //                                print("postersDatas?.count != 2")
                                
                                var tempPoster: Poster = Poster()
                                tempPosters = []
                                if tempProductImage.count == 1{
                                    tempPoster.assets_id = ""
                                    tempPoster.name = ""
                                    tempPoster.url = tempProductImage[0].url
                                    tempPosters.append(tempPoster)
                                    tempPosters.append(tempPoster)
                                }else{
                                    tempPoster.assets_id = ""
                                    tempPoster.name = ""
                                    for pi in tempProductImage{
                                        tempPoster.url = pi.url
                                        tempPosters.append(tempPoster)
                                    }
                                }
                            }
                            
                            self.tmpPackDetailNew.posters = tempPosters
                            
                            let ratingJson = JSON(productJson["rating"])
                            //                        print("ratingJson:\(ratingJson)")
                            if ratingJson["avg_rating"].float != nil{
                                self.tmpPackDetailNew.rating_avg_rating = ratingJson["avg_rating"].float!
                            }
                            if ratingJson["total_rating"].float != nil{
                                self.tmpPackDetailNew.rating_total_rating = ratingJson["total_rating"].float!
                            }
                            
                            var tempTrailerHLSs: [TrailerHLS] = []
                            
                            let trailerHLSDatas = productJson["Trailer_HLS"].arrayObject
                            for trailerHLSData in trailerHLSDatas!{
                                let trailerHLSDataJson = JSON(trailerHLSData)
                                var tempTrailerHLS: TrailerHLS = TrailerHLS()
                                
                                tempTrailerHLS.assets_id = trailerHLSDataJson["assets_id"].string!
                                tempTrailerHLS.name = trailerHLSDataJson["name"].string!
                                
                                var tempMetaDatas: [MetaData] = []
                                
                                let metaDatas = trailerHLSDataJson["meta_data"].arrayObject
                                
                                if metaDatas != nil{
                                    
                                    for metaData in metaDatas!{
                                        var tempMetaData: MetaData = MetaData()
                                        
                                        let metaDataJson = JSON(metaData)
                                        
                                        tempMetaData.attribute = metaDataJson["attribute"].string!
                                        tempMetaData.value = metaDataJson["value"].string!
                                        
                                        tempMetaDatas.append(tempMetaData)
                                    }
                                    
                                }
                                
                                tempTrailerHLS.meta_data = tempMetaDatas
                                
                                tempTrailerHLSs.append(tempTrailerHLS)
                            }
                            self.tmpPackDetailNew.trailerHLS = tempTrailerHLSs
                        }
                    }else if status == 204{
                        // product_list = null
                        //                    completion()
                    }else{
                        // token error
                        self.backToLoginVC(title:"Logout",msg: swiftyJsonVar["message"].string!,okMsg: "Back to LoginPage.")
                    }
                }else{
                    print("RentListVC.getPackDetailNew Error")
                }
                completion()
                
            }
        
        
        
    }
    
}
