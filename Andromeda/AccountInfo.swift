//
//  AccountInfo.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2019/5/22.
//  Copyright © 2019 蒼月喵. All rights reserved.
//

import Foundation

struct AccountInfo {
    var AccountId: Int
    var AccountGUID: String
    var AccountName: String
    var UidType: Int
    var ClientIpLong: String
    var Token: String
    var Birthday: String?
    var Gender: Int
    var Age: String
    var Mood: Int
    var Operator: Int
    var LastLogin: String
    var isForgot: Bool
    var nickName: String
}

struct AccountInfoNew {
    var id: Int = 0
    var user_name: String = ""
    var first_name: String = ""
    var last_name: String = ""
    var sex_id: Int = 1
    var birthday: String = ""
//    // 暫時沒用到，而且可能是 null 先 mark 起來
//    var nikcname: String?
    var email: String = ""
    var phone: String = ""
    /**
     目前只有一個值
     1:一般用戶
     */
    var account_type_id: Int = 1
//    "id": 47,
//    "user_name": "test001",
//    "first_name": "Mike",
//    "last_name": "Lin",
//    "sex_id": 1,
//    "birthday": "2005-07-03",
//    "nikcname": null,
//    "email": "Test@tt01.test",
//    "phone": "+886_982769631",
//    "civil_id": "",
//    "account_status_id": 1,
//    "created_at": "2019-07-05 16:11:30",
//    "updated_at": "2019-07-05 16:12:24",
//    "account_type_id": 1,
//    "effective_time": "2019-07-05 16:11:30",
//    "expire_time": null
}
