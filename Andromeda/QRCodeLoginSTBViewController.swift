//
//  QRCodeLoginSTBViewController.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2019/5/16.
//  Copyright © 2019 蒼月喵. All rights reserved.
//

import UIKit
import AVFoundation
import SnapKit
import SwiftyJSON
import LocalAuthentication

// 01/15
class QRCodeLoginSTBViewController: AuthVC {
//class QRCodeLoginSTBViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    
    weak var ButtonDelegate: ButtonDelegate?
    
    let camView = UIView()
    // 01/18 move to AuthVC
//    let codeTextLabel = UILabel()
    let reBtn = UIButton()
    
    var captureSesion:AVCaptureSession?
    var previewLayer:AVCaptureVideoPreviewLayer!
    
    enum QRCodeType {
        case none, login, unLock
    }
    
    var parseType : QRCodeType = .none
    
    // 01/18 move to AuthVC
//    var context = LAContext()
//
//    enum AuthenticationState {
//        case loggedin, loggedout, parsing
//    }
//
//    var state = AuthenticationState.loggedout {
//        didSet {
//            self.codeTextLabel.textColor = state == .loggedin ? .green : .yellow
//            self.codeTextLabel.isHidden = state == .parsing
//            if state == .loggedin{
//                self.codeTextLabel.text = "Login succes"
//            }else{
//                self.codeTextLabel.text = "Login fail"
//            }
//        }
//    }
    // 01/18 move to AuthVC
//    let pwdBtns : [UIButton] = {
//        var tempBtns : [UIButton] = []
//        let cancelBtn = UIButton()
//        // later
//        cancelBtn.addTarget(self, action: #selector(tapCancelBtn(_:)), for: .touchDown)
//        tempBtns.append(cancelBtn)
//        let okBtn = UIButton()
//        okBtn.addTarget(self, action: #selector(tapOKBtn(_:)), for: .touchDown)
//        tempBtns.append(okBtn)
//
//        return tempBtns
//    }()
    
    // 01/18 move to AuthVC
//    let pwdTextFields : [UITextField] = {
//        var pwdTextFields : [UITextField] = []
//        for i in 0..<4{
//            let tempTextField = UITextField()
//            tempTextField.backgroundColor = .white
//            // 10/27
//            tempTextField.textColor = .black
//
//            tempTextField.textAlignment = .center
//            tempTextField.keyboardType = .numberPad
//            pwdTextFields.append(tempTextField)
//        }
//        return pwdTextFields
//    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        parseType = .none
        if(captureSesion?.isRunning == false){
            captureSesion?.startRunning()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        codeTextLabel.text = "codeText"
        codeTextLabel.textColor = .white
        setQRCodeScan()
    }
    
    //畫面不顯示即停止掃描
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        parseType = .none
        
        if(captureSesion?.isRunning == true){
            captureSesion?.stopRunning()
        }
    }
    
    //掃QRCode的動作
    func setQRCodeScan(){
        
        //實體化一個AVCaptureSession物件
        captureSesion = AVCaptureSession()
        
        //AVCaptureDevice可以抓到相機和其屬性
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else {return}
        let videoInput:AVCaptureDeviceInput
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        }catch let error {
            print(error)
            return
        }
        if (captureSesion?.canAddInput(videoInput) ?? false ){
            captureSesion?.addInput(videoInput)
        }else{
            return
        }
        
        //AVCaptureMetaDataOutput輸出影音資料，先實體化AVCaptureMetaDataOutput物件
        let metaDataOutput = AVCaptureMetadataOutput()
        if (captureSesion?.canAddOutput(metaDataOutput) ?? false){
            captureSesion?.addOutput(metaDataOutput)
            
            //關鍵！執行處理QRCode
            metaDataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            //metadataOutput.metadataObjectTypes表示要處理哪些類型的資料，處理QRCODE
            metaDataOutput.metadataObjectTypes = [.qr, .ean8 , .ean13 , .pdf417]
            
        }else{
            return
        }
        
        //用AVCaptureVideoPreviewLayer來呈現Session上的資料
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSesion!)
        //顯示size
        previewLayer.videoGravity = .resizeAspectFill
        //呈現在camView上面
        previewLayer.frame = camView.layer.frame
        //加入畫面
        view.layer.addSublayer(previewLayer)
        
        //設定scan Area window 框框
        let scanAreaView = UIView()
        scanAreaView.layer.borderColor = UIColor.gray.cgColor
        scanAreaView.layer.borderWidth = 2
        //        scanAreaView.frame = scanRect
        view.addSubview(scanAreaView)
        scanAreaView.snp.makeConstraints { (makes) in
            makes.center.equalTo(camView)
            makes.height.width.equalTo(fullScreenSize.width * 0.7 * 0.7)
        }
        view.bringSubviewToFront(scanAreaView)
        
        //開始影像擷取呈現鏡頭的畫面
        captureSesion?.startRunning()
    }
    
    //使用AVCaptureMetadataOutput物件辨識QR Code，此AVCaptureMetadataOutputObjectsDelegate的委派方法metadataOutout會被呼叫
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSesion?.startRunning()
        if let metadataObject = metadataObjects.first{
            
            //AVMetadataMachineReadableCodeObject是從OutPut擷取到barcode內容
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else {return}
            //將讀取到的內容轉成string
            guard let stringValue = readableObject.stringValue else {return}
            //掃到QRCode後的震動提示
            //            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            
//            if pharseString(stringValue: stringValue){
//                codeTextLabel.textColor = .red
//                //                codeTextLabel.text = stringValue
//                captureSesion?.stopRunning()
//                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
//            }else{
//                codeTextLabel.textColor = .white
//                codeTextLabel.text = stringValue
//                captureSesion?.stopRunning()
//                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
//            }
            
            switch parseString(stringValue: stringValue) {
            case .none:
                codeTextLabel.textColor = .white
                codeTextLabel.text = stringValue
                captureSesion?.stopRunning()
                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            case .login:
                codeTextLabel.textColor = .red
                captureSesion?.stopRunning()
                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            case .unLock:
                print("unLock")
                captureSesion?.stopRunning()
                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            }
        }
    }
    
    func parseString(stringValue: String) -> QRCodeType{
        print(stringValue)
        
        if stringValue == "unLockByMobile"{
            state = .parsing
            self.tryAuth()
            return .unLock
        }
        
        var cc1: [String] = []
        var cc2: [String] = []
        /**
         serial_num:unknown;
         client_ID:ff448f96-135e-384a-ab39-af4c3602f7c9;
         custom_data:{
         "AccountGUID":"",
         "TypeOfClient":"2",
         "DeviceVendor":"TTI",
         "DeviceName":"QTB-5322R_CHT",
         "OSVersion":"29",
         "CheckDomain":"0"
         }
         */
        var tempData = stbData()
        
        cc1 = stringValue.components(separatedBy: "_num:")
        if cc1.count != 2{
            return .none
        }else{
            cc2 = cc1[1].components(separatedBy: ";")
            tempData.serial_num = cc2[0]
        }
        
        cc1 = stringValue.components(separatedBy: "client_ID:")
        if cc1.count != 2{
            return .none
        }else{
            cc2 = cc1[1].components(separatedBy: ";")
            tempData.client_ID = cc2[0]
        }
        
        cc1 = stringValue.components(separatedBy: "custom_data:")
        if cc1.count != 2{
            return .none
        }else{
            if let data : Data = cc1[1].data(using: .utf8){
                if let jsonData = try? JSON(data: data) {
                    tempData.AccountGUID = jsonData["AccountGUID"].string!
                    tempData.CheckDomain = Int(jsonData["CheckDomain"].string!)!
                    tempData.DeviceName = jsonData["DeviceName"].string!
                    tempData.OSVersion = Int(jsonData["OSVersion"].string!)!
                    tempData.TypeOfClient = Int(jsonData["TypeOfClient"].string!)!
                    tempData.DeviceVendor = jsonData["DeviceVendor"].string!
                }
            }
        }
        
        /**
         serial_num:unknown;
         client_ID:ff448f96-135e-384a-ab39-af4c3602f7c9;
         custom_data:{
         "AccountGUID":"",
         "TypeOfClient":"2",
         "DeviceVendor":"TTI",
         "DeviceName":"QTB-5322R_CHT",
         "OSVersion":"29",
         "CheckDomain":"0"
         }
         */
        
        if tempData.client_ID != ""{
            self.codeTextLabel.text = "serial_num:\(tempData.serial_num)\n client_ID:\(tempData.client_ID)\n AccountGUID:\(tempData.AccountGUID)\n TypeOfClient:\(tempData.TypeOfClient)\n DeviceVendor:\(tempData.DeviceVendor)\n DeviceName:\(tempData.DeviceName)\n OSVersion:\(tempData.OSVersion)\n CheckDomain:\(tempData.CheckDomain)"
            return .login
        }else{
            return .none
        }
    }
    
    func setViews() {
        for tf in pwdTextFields{
            tf.delegate = self
        }
        let fullScreenSize = UIScreen.main.bounds.size
        view.addSubview(camView)
        camView.snp.makeConstraints { (makes) in
            makes.width.height.equalTo(fullScreenSize.width * 0.7)
            //            makes.top.equalToSuperview().offset(fullScreenSize.width * 0.3)
            makes.top.left.equalToSuperview().offset(fullScreenSize.width * 0.15)
        }
        camView.backgroundColor = .white
        
        view.addSubview(codeTextLabel)
        codeTextLabel.snp.makeConstraints { (makes) in
            makes.left.equalToSuperview().offset(30)
            makes.right.equalToSuperview().offset(-30)
            makes.top.equalTo(camView.snp.bottom).offset(30)
            makes.height.equalTo(50)
        }
        codeTextLabel.backgroundColor = .gray
        codeTextLabel.text = "codeText"
        codeTextLabel.textAlignment = .center
        
        codeTextLabel.addTapGestureRecognizer {
            if self.codeTextLabel.textColor == .red{
                //                self.alertMsg(msg: self.codeTextLabel.text!)
                self.alertMsg3(title: "Code Text", msg: self.codeTextLabel.text!)
            }
        }
        
        view.addSubview(reBtn)
        reBtn.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(codeTextLabel.snp.bottom).offset(30)
            makes.width.equalTo(150)
            makes.height.equalTo(40)
        }
        reBtn.layer.cornerRadius = 10.0
        reBtn.layer.masksToBounds = true
        reBtn.backgroundColor = .blue
        reBtn.setTitle("ReLoad", for: .normal)
        reBtn.addTarget(self, action: #selector(reBtnClick), for: .touchUpInside)
        
    }
    
    // 01/18 move to AuthVC
//    func tryAuth(){
//        context = LAContext()
//
//        context.localizedCancelTitle = "Enter Username/Password"
//
//        var error: NSError?
//        if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
//
//            let reason = "Log in to your account"
//            context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason ) { success, error in
//
//                if success {
//
//                    // Move to the main thread because a state update triggers UI changes.
//                    DispatchQueue.main.async { [unowned self] in
//                        self.state = .loggedin
//                    }
//
//                } else {
//                    print(error?.localizedDescription ?? "Failed to authenticate")
//                    print("AAAA")
//                    DispatchQueue.main.async { [unowned self] in
////                            self.state = .loggedin
////                            self.setActView(openOrClose: true)
////                            self.logOutBtn.isHidden = true
//                        self.inputPwd()
//                    }
//
//                    // Fall back to a asking for username and password.
//                    // ...
//                }
//            }
//        } else {
//            print(error?.localizedDescription ?? "Can't evaluate policy")
//            print("BBBB")
//            self.state = .loggedout
//        }
//    }
    // 01/18 move to AuthVC
//    func inputPwd(){
//        let pwdView = UIView()
//        // for cancel btn to remove
//        pwdView.tag = 100
//        self.setPwdView(pwdView: pwdView, pwdBtns: pwdBtns, pwdTextFields: pwdTextFields)
//    }
    // 01/18 move to AuthVC
//    @objc func tapCancelBtn(_ sender: UIButton){
//        self.removePwdView()
//    }
    // 01/18 move to AuthVC
//    func removePwdView(){
//        for view in self.view.subviews {
//            if view.tag == 100 {
//                view.removeFromSuperview()
//            }
//        }
//    }
    
    // 01/18 move to AuthVC and override
    @objc override func tapOKBtn(_ sender: UIButton){
        print("Enter tapOKBtn")
        // 01/15 pcPwd -> inputPwd, "0000" -> pcPwd

        var tempPwd = ""
        for tf in self.pwdTextFields{
            tempPwd += tf.text ?? ""
        }

        if tempPwd.count == 4{
            self.removePwdView()
            if tempPwd == pcPwd{
                self.state = .loggedin
            }else{
                self.state = .loggedout
            }
        }else{
            // 有 textField 為空，無動作
//            self.removePwdView()
        }
    }
    // 01/18 move to AuthVC
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        print("textFieldShouldReturn")
//        return true
//    }
    // 01/18 move to AuthVC
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//        //        print("range:\(range)")
//        print("range.location:\(range.location)")
//        print("string:\(string)")
//
//        if textField == pwdTextFields[0]{
//            pwdTextFields[0].text = string
//            if string != ""{
//                pwdTextFields[1].becomeFirstResponder()
//            }
//        }else if textField == pwdTextFields[1]{
//            pwdTextFields[1].text = string
//            if string != ""{
//                pwdTextFields[2].becomeFirstResponder()
//            }
//        }else if textField == pwdTextFields[2]{
//            pwdTextFields[2].text = string
//            if string != ""{
//                pwdTextFields[3].becomeFirstResponder()
//            }
//        }else if textField == pwdTextFields[3]{
//            if range.location > 0{
//                pwdTextFields[3].text = ""
//            }
//        }
//        return true
//    }
    
    @objc func reBtnClick(){
        if(captureSesion?.isRunning == false){
            codeTextLabel.text = "codeText"
            codeTextLabel.textColor = .white
            captureSesion?.startRunning()
        }
    }
    
    //    struct stbData {
    //        var serial_num: String = ""
    //        var client_ID: String = ""
    //        var AccountGUID: String = ""
    //        var TypeOfClient: Int = 0
    //        var DeviceVendor: String = ""
    //        var DeviceName: String = ""
    //        var OSVersion: Int = 0
    //        var CheckDomain: Int = 0
    //    }
    
    @IBAction func menuClick(_ sender: Any) {
        navigationController?.view.menu()
        ButtonDelegate?.clickLeft()
    }
    @IBAction func userClick(_ sender: Any) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        navigationController?.view.menu2()
        ButtonDelegate?.clickRight()
    }
    
    @IBAction func handleEdgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger(location: location, state: sender.state)
        if beganX2 > 0 {
            ButtonDelegate?.clickLeft()
        }
        
    }
    @IBAction func handleEdgePanGesture2(_ sender: UIScreenEdgePanGestureRecognizer) {
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger2(location: location, state: sender.state)
        if beganX2 < 0{
            ButtonDelegate?.clickRight()
        }
        
    }
    
    
    @IBAction func handleTapGesture(_ sender: Any) {
        let name = Notification.Name("SearchTextFieldResign")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        if navigationController?.view.superview?.frame.origin.x != 0 {
            navigationController?.view.menu()
        }
    }
    
    
}


struct stbData {
    var serial_num: String = ""
    var client_ID: String = ""
    var AccountGUID: String = ""
    var TypeOfClient: Int = 0
    var DeviceVendor: String = ""
    var DeviceName: String = ""
    var OSVersion: Int = 0
    var CheckDomain: Int = 0
}
