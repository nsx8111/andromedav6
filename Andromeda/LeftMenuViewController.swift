//
//  LeftMenuViewController.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2019/5/14.
//  Copyright © 2019 蒼月喵. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire
import SwiftyJSON

class LeftMenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let inset = fullScreenSize.width*0.022
    let buttonSize = fullScreenSize.width*0.14
    let offsetA = fullScreenSize.width*0.02
    let labelFontSizeB: CGFloat = 22.0
    let labelFontSizeS: CGFloat = 16.0
    
    weak var MenuDelegate: MenuDelegate?
    //0108
    var stickerButton = UIButton()
    var userNameButton = UIButton()
    var stickerImageView = UIImageView()
    var userNameLabel = UILabel()
    
    var scrollView = UIScrollView()
    var containerView = UIView()
    
    // Read catalog from server data
    private var CatalogMenuTableView: UITableView!
    
    let myListLabel = UILabel()
    let historyButton = UIButton()
    let historyLabel = UILabel()
    let rentListButton = UIButton()
    let rentListLabel = UILabel()
    let favListButton = UIButton()
    let favListLabel = UILabel()
    
    let systemSettingLabel = UILabel()
    let dmButton = UIButton()
    let dmLabel1 = UILabel()
    let dmLabel2 = UILabel()
    
    let pcButton = UIButton()
    let pcLabel1 = UILabel()
    let pcLabel2 = UILabel()
    
    let cpButton = UIButton()
    let cpLabel1 = UILabel()
    let cpLabel2 = UILabel()
    
    let qlButton = UIButton()
    let qlLabel1 = UILabel()
    let qlLabel2 = UILabel()
    
    let faButton = UIButton()
    let faLabel = UILabel()
    let logoutButton = UIButton()
    let logoutLabel = UILabel()
    
    
    let list = ["New Viedos", "Recommended Movies", "Hot Movies"]
    // save catalog data
    var catalogs: [Catalog] = []
    // new API type datas
    var ProductTypes: [ProductType] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
            statusBar.backgroundColor = .viewBgColor
            UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            UIApplication.shared.statusBarView?.backgroundColor = .viewBgColor
        }
        UIApplication.shared.statusBarView?.backgroundColor = .viewBgColor
        
        setBackGroundImage()
        setUser()
        setScrollView()
        setTableView()
        setMyList()
        setSystemSetting1()
        setSystemSetting2()
        loadTypeData(){
            self.CatalogMenuTableView.snp.updateConstraints({ (makes) in
                makes.height.equalTo(fullScreenSize.height*0.06*CGFloat(self.ProductTypes.count))
            })
            self.CatalogMenuTableView.reloadData()
            self.MenuDelegate?.reloadAfterLoad(ProductType: self.ProductTypes[0])
        }
        
    }
    
    /** set BackGround Image */
    func setBackGroundImage(){
        //Add BackGround
        let bkgImageView = UIImageView()
        bkgImageView.frame = CGRect(x: 0, y: 0, width: fullScreenSize.width, height: fullScreenSize.height)
        bkgImageView.backgroundColor = .blueBgColor
        self.view.addSubview(bkgImageView)
    }
    
    func setUser(){
        
        stickerImageView = UIImageView(image: UIImage(named: "oriSticker"))
//        stickerImageView.is
//        stickerImageView.callback = {
//            print("click stickerImageView")
//            self.MenuDelegate?.changeViedoMenuView(0)
//        }
        self.view.addSubview(stickerImageView)
//        MenuDelegate?.changeViedoMenuView(0)
        stickerImageView.snp.makeConstraints { (make) in
            make.width.equalTo(buttonSize)
            make.height.equalTo(buttonSize)
            make.top.equalToSuperview().offset(statusBarHeight*1.5)
            make.left.equalToSuperview().offset(20)
        }

        //0108
        self.view.addSubview(stickerButton)
        stickerButton.addTarget(self,action: #selector(self.toUserDataPage),for: .touchUpInside)
        stickerButton.snp.makeConstraints { (make) in
            make.edges.equalTo(stickerImageView)
        }
        
        switch currentDevice.deviceType {
        case .iPad:
            userNameLabel.font = UIFont(name: "Georgia-Bold", size: labelFontSizeB*2)
        default:
            userNameLabel.font = UIFont(name: "Georgia-Bold", size: labelFontSizeB)
        }
        
        userNameLabel.backgroundColor = .clear
        userNameLabel.text = "\(accountInfoNew.first_name) \(accountInfoNew.last_name)"
        //        print("In Left Menu : \(accountInfoNew.first_name) \(accountInfoNew.last_name)")
        userNameLabel.textColor = .white
        userNameLabel.adjustsFontSizeToFitWidth = true
//        userNameLabel.is
//        userNameLabel.callback = {
//            print("click userNameLabel")
//            self.MenuDelegate?.changeViedoMenuView(0)
//        }
        
        self.view.addSubview(userNameLabel)
        userNameLabel.snp.makeConstraints { (makes) in
            makes.left.equalTo(stickerImageView.snp.right).offset(10)
            makes.bottom.equalTo(stickerImageView)
        }
        
        self.view.addSubview(userNameButton)
        userNameButton.addTarget(self,action: #selector(self.toUserDataPage),for: .touchUpInside)
        userNameButton.snp.makeConstraints { (makes) in
            makes.edges.equalTo(self.userNameLabel)
        }
    }
    
    func setScrollView(){
        view.addSubview(scrollView)
        scrollView.backgroundColor = .blueBgColor
        scrollView.bounces = false
        scrollView.snp.makeConstraints { (makes) in
            makes.top.equalTo(stickerImageView.snp.bottom).offset(fullScreenSize.height*0.015)
            makes.left.right.bottom.equalToSuperview()
        }
        
        scrollView.addSubview(containerView)
        containerView.backgroundColor = .blueBgColor
        containerView.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
            makes.width.equalTo(view.bounds.width*0.8)
        }
    }
    
    func loadTypeData (completion : @escaping ()->()) {
        self.ProductTypes = []
        let params = [
            "token":"\(token)"
        ] as [String : Any]
        
        AF.request("\(serverUrlNew)product/getAllProductType", method: .post, parameters: params)
            .responseJSON { (response) in
                if response.value != nil {
                    //                print(response.result.value!)
                    let swiftyJsonVar = JSON(response.value!)
                    let status: Int = swiftyJsonVar["status"].int!
                    //                print("status:\(status)")
                    if status == 200{
                        // token 正確
                        let restltJson = JSON(swiftyJsonVar["result"])
                        //                    print("restltJson:\(restltJson)")
                        
                        let productTypes = restltJson["product_type"].arrayObject
                        for type in productTypes!{
                            let typeJson = JSON(type)
                            
                            var tempType: ProductType = ProductType()
                            tempType.name = typeJson["name"].string!
                            tempType.product_type_id = typeJson["product_type_id"].int!
                            
                            tempType.created_at = typeJson["created_at"].string!
                            tempType.updated_at = typeJson["updated_at"].string!
                            tempType.product_type_status_id = typeJson["product_type_status_id"].int!
                            if let description: String = typeJson["description"].string{
                                tempType.description = description
                            }
                            self.ProductTypes.append(tempType)
                        }
                    }else if status == 204{
                        // get product content category success, but its empty
                    }else{
                        // token error
                        let alertController = UIAlertController(title: "Logout", message: swiftyJsonVar["message"].string!, preferredStyle: .alert)
                        let cancelAction = UIAlertAction(
                            title: "Cancel",
                            style: .cancel,
                            handler: nil)
                        let okAction = UIAlertAction(
                            title: "Back to LoginPage.",
                            style: .default,
                            handler: {
                                (action: UIAlertAction!) -> Void in
                                self.dismiss(animated: true, completion: nil)
                        }
                        )
                        alertController.addAction(cancelAction)
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion: nil)
                    }
                }else{
                    print("Error")
                }
                completion()

        }
        
//        Alamofire.request("\(serverUrlNew)product/getAllProductType", method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON { (response) in
//            if response.result.isSuccess {
//                //                print(response.result.value!)
//                let swiftyJsonVar = JSON(response.result.value!)
//                let status: Int = swiftyJsonVar["status"].int!
//                //                print("status:\(status)")
//                if status == 200{
//                    // token 正確
//                    let restltJson = JSON(swiftyJsonVar["result"])
//                    //                    print("restltJson:\(restltJson)")
//
//                    let productTypes = restltJson["product_type"].arrayObject
//                    for type in productTypes!{
//                        let typeJson = JSON(type)
//
//                        var tempType: ProductType = ProductType()
//                        tempType.name = typeJson["name"].string!
//                        tempType.product_type_id = typeJson["product_type_id"].int!
//
//                        tempType.created_at = typeJson["created_at"].string!
//                        tempType.updated_at = typeJson["updated_at"].string!
//                        tempType.product_type_status_id = typeJson["product_type_status_id"].int!
//                        if let description: String = typeJson["description"].string{
//                            tempType.description = description
//                        }
//                        self.ProductTypes.append(tempType)
//                    }
//                }else if status == 204{
//                    // get product content category success, but its empty
//                }else{
//                    // token error
//                    let alertController = UIAlertController(title: "Logout", message: swiftyJsonVar["message"].string!, preferredStyle: .alert)
//
//                    let okAction = UIAlertAction(
//                        title: "Back to LoginPage.",
//                        style: .default,
//                        handler: {
//                            (action: UIAlertAction!) -> Void in
//                            self.dismiss(animated: true, completion: nil)
//                    }
//                    )
//                    alertController.addAction(okAction)
//
//                    self.present(alertController, animated: true, completion: nil)
//                }
//            }else{
//                print("Error")
//            }
//            completion()
//        }
        
    }
    
    func setTableView(){
        CatalogMenuTableView = UITableView()
        CatalogMenuTableView.register(UITableViewCell.self, forCellReuseIdentifier: "CatalogMenuCell")
        CatalogMenuTableView.delegate = self
        CatalogMenuTableView.dataSource = self
        CatalogMenuTableView.bounces = false
        CatalogMenuTableView.allowsSelection = true
        CatalogMenuTableView.backgroundColor = .tableColor
        CatalogMenuTableView.rowHeight = fullScreenSize.height*0.06
        self.containerView.addSubview(CatalogMenuTableView)
        CatalogMenuTableView.snp.makeConstraints { (make) in
//            make.top.equalTo(NRHMenuTableView.snp.bottom).offset(fullScreenSize.height*0.015)
            make.top.equalToSuperview().offset(fullScreenSize.height*0.015)
            make.width.equalTo(fullScreenSize.width*0.8) //0.58
            //這邊先預設三個
            make.height.equalTo(CatalogMenuTableView.rowHeight * CGFloat(ProductTypes.count))
//            make.left.equalToSuperview().offset(offsetA/2)
            make.left.equalToSuperview().offset(0)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if tableView == NRHMenuTableView{
//            return list.count
//            //             1003
//            //            return 0
//        }else{
//            //            return catalogs.count
            return ProductTypes.count
//        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        if tableView == NRHMenuTableView{
//            let cell = tableView.dequeueReusableCell(withIdentifier: "NRHMenuCell", for: indexPath)
//            cell.textLabel?.text = list[indexPath.row]
//            cell.textLabel?.textAlignment = .left
//            cell.textLabel?.textColor = .white
//            cell.backgroundColor = tableColor
//            return cell
//        }else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CatalogMenuCell", for: indexPath)
//        cell.textLabel?.text = catalogs[indexPath.row].name
        cell.textLabel?.text = ProductTypes[indexPath.row].name
        cell.textLabel?.textAlignment = .left
        cell.textLabel?.textColor = .white
        cell.backgroundColor = .tableColor
        switch currentDevice.deviceType {
        case .iPad:
            cell.textLabel?.font = .systemFont(ofSize: labelFontSizeS*2.2)
        default:
            cell.textLabel?.font = .systemFont(ofSize: labelFontSizeS*1.3)
        }
        return cell
        //        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let name = Notification.Name("removeContentView")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
        
//        if tableView == NRHMenuTableView{
//            //            if indexPath.row == 0{
//            //                MenuDelegate?.toCatalogPage(ProductType: ProductTypes[0])
//            //            }
//            MenuDelegate?.changeViedoMenuView(indexPath.row)
//        }else{
            //            MenuDelegate?.changeViedoMenuView(3)
            //            MenuDelegate?.toCatalogPage(Catalog: catalogs[indexPath.row])
            MenuDelegate?.toCatalogPage(ProductType: ProductTypes[indexPath.row])
//        }
        //        MenuDelegate?.changeViedoMenuView(indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func setMyList(){
        myListLabel.backgroundColor = .clear
        myListLabel.text = "My List"
        myListLabel.font = UIFont(name: "Georgia-Bold", size: labelFontSizeB)
        myListLabel.textColor = .white
        myListLabel.adjustsFontSizeToFitWidth = true
        
        self.containerView.addSubview(myListLabel)
        myListLabel.snp.makeConstraints { (make) in
            make.top.equalTo(CatalogMenuTableView.snp.bottom).offset(fullScreenSize.height*0.015*1.7)
            make.left.equalToSuperview().offset(offsetA*2)
        }
        
        historyButton.backgroundColor = .buttonColor
        historyButton.setImage(UIImage(named: "History"), for: .normal)
        historyButton.isEnabled = true
        historyButton.addTarget(self,action: #selector(self.clickHistoryButton),for: .touchUpInside)
        historyButton.imageEdgeInsets = UIEdgeInsets(top: inset,left: inset,bottom: inset,right: inset)
        historyButton.layer.cornerRadius = buttonSize/2
        historyButton.clipsToBounds = true
        
        self.containerView.addSubview(historyButton)
        historyButton.snp.makeConstraints { (make) in
            make.top.equalTo(myListLabel.snp.bottom).offset(fullScreenSize.height*0.01*2.5)
            make.width.equalTo(buttonSize)
            make.height.equalTo(buttonSize)
            make.left.equalToSuperview().offset(offsetA*5)
        }
        
        rentListButton.backgroundColor = .buttonColor
        rentListButton.setImage(UIImage(named: "RentList"), for: .normal)
        rentListButton.isEnabled = true
        rentListButton.addTarget(self,action: #selector(self.clickRentListButton),for: .touchUpInside)
        rentListButton.imageEdgeInsets = UIEdgeInsets(top: inset,left: inset,bottom: inset,right: inset)
        rentListButton.layer.cornerRadius = buttonSize/2
        rentListButton.clipsToBounds = true
        
        self.containerView.addSubview(rentListButton)
        rentListButton.snp.makeConstraints { (make) in
            //            make.top.equalTo(myListLabel.snp.bottom).offset(offsetA)
            make.centerY.equalTo(historyButton.snp.centerY)
            make.width.equalTo(buttonSize)
            make.height.equalTo(buttonSize)
            make.left.equalTo(historyButton.snp.right).offset(offsetA*5)
        }
        
        favListButton.backgroundColor = .buttonColor
        favListButton.setImage(UIImage(named: "favorite"), for: .normal)
        favListButton.isEnabled = true
        favListButton.addTarget(self,action: #selector(self.clickFavButton),for: .touchUpInside)
        favListButton.imageEdgeInsets = UIEdgeInsets(top: inset,left: inset,bottom: inset,right: inset)
        favListButton.layer.cornerRadius = buttonSize/2
        favListButton.clipsToBounds = true
        
        self.containerView.addSubview(favListButton)
        favListButton.snp.makeConstraints { (make) in
            //            make.top.equalTo(myListLabel.snp.bottom).offset(offsetA)
            make.centerY.equalTo(historyButton.snp.centerY)
            make.width.equalTo(buttonSize)
            make.height.equalTo(buttonSize)
            make.left.equalTo(rentListButton.snp.right).offset(offsetA*5)
        }
        
        historyLabel.backgroundColor = .clear
        historyLabel.text = "History List"
        historyLabel.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS)
        historyLabel.textColor = .white
        historyLabel.adjustsFontSizeToFitWidth = true
        
        self.containerView.addSubview(historyLabel)
        historyLabel.snp.makeConstraints { (make) in
            make.top.equalTo(historyButton.snp.bottom).offset(offsetA/2)
            make.centerX.equalTo(historyButton.snp.centerX)
        }
        
        rentListLabel.backgroundColor = .clear
        rentListLabel.text = "Rent List"
        rentListLabel.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS)
        rentListLabel.textColor = .white
        rentListLabel.adjustsFontSizeToFitWidth = true
        
        self.containerView.addSubview(rentListLabel)
        rentListLabel.snp.makeConstraints { (make) in
            make.top.equalTo(rentListButton.snp.bottom).offset(offsetA/2)
            make.centerX.equalTo(rentListButton.snp.centerX)
        }
        
        favListLabel.backgroundColor = .clear
        favListLabel.text = "Favorite List"
        favListLabel.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS)
        favListLabel.textColor = .white
        favListLabel.adjustsFontSizeToFitWidth = true
        
        self.containerView.addSubview(favListLabel)
        favListLabel.snp.makeConstraints { (make) in
            make.top.equalTo(favListButton.snp.bottom).offset(offsetA/2)
            make.centerX.equalTo(favListButton.snp.centerX)
        }
        
        switch currentDevice.deviceType {
        case .iPad:
            myListLabel.font = UIFont(name: "Georgia-Bold", size: labelFontSizeB*1.7)
            historyLabel.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS*1.7)
            rentListLabel.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS*1.7)
            favListLabel.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS*1.7)
        default:
            myListLabel.font = UIFont(name: "Georgia-Bold", size: labelFontSizeB)
            historyLabel.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS)
            rentListLabel.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS)
            favListLabel.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS)
        }
    }
    
    @objc func toUserDataPage(){
        print("toUserDataPage")
        MenuDelegate?.changeViedoMenuView(0)
        let name = Notification.Name("removeContentView")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
    }
    
    @objc func clickHistoryButton(){
        print("HistoryButton")
        MenuDelegate?.changeViedoMenuView(5)
        let name = Notification.Name("removeContentView")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
    }
    
    @objc func clickRentListButton(){
        print("clickRentListButton")
        MenuDelegate?.changeViedoMenuView(6)
    }
    
    func setSystemSetting1(){
        systemSettingLabel.backgroundColor = .clear
        systemSettingLabel.text = "System Setting"
        systemSettingLabel.font = UIFont(name: "Georgia-Bold", size: labelFontSizeB)
        systemSettingLabel.textColor = .white
        systemSettingLabel.adjustsFontSizeToFitWidth = true
        
        self.containerView.addSubview(systemSettingLabel)
        systemSettingLabel.snp.makeConstraints { (make) in
            make.top.equalTo(historyLabel.snp.bottom).offset(offsetA*2)
            make.left.equalToSuperview().offset(offsetA*2)
        }
        
        dmButton.backgroundColor = .buttonColor
        dmButton.setImage(UIImage(named: "DevicesManagement"), for: .normal)
        dmButton.isEnabled = true
        dmButton.addTarget(self,action: #selector(self.clickDmButton),for: .touchUpInside)
        dmButton.imageEdgeInsets = UIEdgeInsets(top: inset,left: inset,bottom: inset,right: inset)
        dmButton.layer.cornerRadius = buttonSize/2
        dmButton.clipsToBounds = true
        
        self.containerView.addSubview(dmButton)
        dmButton.snp.makeConstraints { (make) in
            make.top.equalTo(systemSettingLabel.snp.bottom).offset(fullScreenSize.height*0.01*2.5)
            make.width.equalTo(buttonSize)
            make.height.equalTo(buttonSize)
            make.left.equalToSuperview().offset(offsetA*5)
        }
        
        pcButton.backgroundColor = .buttonColor
        pcButton.setImage(UIImage(named: "ParentalControl"), for: .normal)
        pcButton.isEnabled = true
        pcButton.addTarget(self,action: #selector(self.clickPcButton),for: .touchUpInside)
        pcButton.imageEdgeInsets = UIEdgeInsets(top: inset,left: inset,bottom: inset,right: inset)
        pcButton.layer.cornerRadius = buttonSize/2
        pcButton.clipsToBounds = true
        
        self.containerView.addSubview(pcButton)
        pcButton.snp.makeConstraints { (make) in
            //            make.top.equalTo(systemSettingLabel.snp.bottom).offset(fullScreenSize.height*0.01)
            make.centerY.equalTo(dmButton.snp.centerY)
            make.width.equalTo(buttonSize)
            make.height.equalTo(buttonSize)
            make.left.equalTo(dmButton.snp.right).offset(offsetA*5)
        }
        
        cpButton.backgroundColor = .buttonColor
        cpButton.setImage(UIImage(named: "ChangePassword"), for: .normal)
        cpButton.isEnabled = true
        cpButton.addTarget(self,action: #selector(self.clickCpButton),for: .touchUpInside)
        cpButton.imageEdgeInsets = UIEdgeInsets(top: inset,left: inset,bottom: inset,right: inset)
        cpButton.layer.cornerRadius = buttonSize/2
        cpButton.clipsToBounds = true
        
        self.containerView.addSubview(cpButton)
        cpButton.snp.makeConstraints { (make) in
            //            make.top.equalTo(systemSettingLabel.snp.bottom).offset(fullScreenSize.height*0.01)
            make.centerY.equalTo(dmButton.snp.centerY)
            make.width.equalTo(buttonSize)
            make.height.equalTo(buttonSize)
            make.left.equalTo(pcButton.snp.right).offset(offsetA*5)
        }
        
        dmLabel1.backgroundColor = .clear
        dmLabel1.text = "Devices"
        dmLabel1.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS)
        dmLabel1.textColor = .white
        dmLabel1.adjustsFontSizeToFitWidth = true
        
        self.containerView.addSubview(dmLabel1)
        dmLabel1.snp.makeConstraints { (make) in
            make.top.equalTo(dmButton.snp.bottom).offset(offsetA/2)
            make.centerX.equalTo(dmButton.snp.centerX)
        }
        
        dmLabel2.backgroundColor = .clear
        dmLabel2.text = "Management"
        dmLabel2.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS)
        dmLabel2.textColor = .white
        dmLabel2.adjustsFontSizeToFitWidth = true
        
        self.containerView.addSubview(dmLabel2)
        dmLabel2.snp.makeConstraints { (make) in
            make.top.equalTo(dmLabel1.snp.bottom)
            make.centerX.equalTo(dmLabel1.snp.centerX)
        }
        
        pcLabel1.backgroundColor = .clear
        pcLabel1.text = "Parental"
        pcLabel1.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS)
        pcLabel1.textColor = .white
        pcLabel1.adjustsFontSizeToFitWidth = true
        
        self.containerView.addSubview(pcLabel1)
        pcLabel1.snp.makeConstraints { (make) in
            make.top.equalTo(pcButton.snp.bottom).offset(offsetA/2)
            make.centerX.equalTo(pcButton.snp.centerX)
        }
        
        pcLabel2.backgroundColor = .clear
        pcLabel2.text = "Control"
        pcLabel2.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS)
        pcLabel2.textColor = .white
        pcLabel2.adjustsFontSizeToFitWidth = true
        
        self.containerView.addSubview(pcLabel2)
        pcLabel2.snp.makeConstraints { (make) in
            make.top.equalTo(pcLabel1.snp.bottom)
            make.centerX.equalTo(pcLabel1.snp.centerX)
        }
        
        cpLabel1.backgroundColor = .clear
        cpLabel1.text = "Change"
        cpLabel1.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS)
        cpLabel1.textColor = .white
        cpLabel1.adjustsFontSizeToFitWidth = true
        
        self.containerView.addSubview(cpLabel1)
        cpLabel1.snp.makeConstraints { (make) in
            make.top.equalTo(cpButton.snp.bottom).offset(offsetA/2)
            make.centerX.equalTo(cpButton.snp.centerX)
        }
        
        cpLabel2.backgroundColor = .clear
        cpLabel2.text = "Password"
        cpLabel2.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS)
        cpLabel2.textColor = .white
        cpLabel2.adjustsFontSizeToFitWidth = true
        
        self.containerView.addSubview(cpLabel2)
        cpLabel2.snp.makeConstraints { (make) in
            make.top.equalTo(cpLabel1.snp.bottom)
            make.centerX.equalTo(cpLabel1.snp.centerX)
        }
        
        switch currentDevice.deviceType {
        case .iPad:
            systemSettingLabel.font = UIFont(name: "Georgia-Bold", size: labelFontSizeB*1.7)
            dmLabel1.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS*1.7)
            dmLabel2.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS*1.7)
            pcLabel1.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS*1.7)
            pcLabel2.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS*1.7)
            cpLabel1.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS*1.7)
            cpLabel2.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS*1.7)
        default:
            systemSettingLabel.font = UIFont(name: "Georgia-Bold", size: labelFontSizeB)
            dmLabel1.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS)
            dmLabel2.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS)
            pcLabel1.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS)
            pcLabel2.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS)
            cpLabel1.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS)
            cpLabel2.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS)
        }
    }
    
    func setSystemSetting2(){
        qlButton.backgroundColor = .buttonColor
        qlButton.setImage(UIImage(named: "QrCodeLogin"), for: .normal)
        qlButton.isEnabled = true
        qlButton.addTarget(self,action: #selector(self.clickQlButton),for: .touchUpInside)
        qlButton.imageEdgeInsets = UIEdgeInsets(top: inset,left: inset,bottom: inset,right: inset)
        qlButton.layer.cornerRadius = buttonSize/2
        qlButton.clipsToBounds = true
        self.containerView.addSubview(qlButton)
        qlButton.snp.makeConstraints { (make) in
            make.top.equalTo(dmLabel2.snp.bottom).offset(offsetA*2)
            make.width.equalTo(buttonSize)
            make.height.equalTo(buttonSize)
            make.left.equalToSuperview().offset(offsetA*5)
        }
        
        faButton.backgroundColor = .buttonColor
        faButton.setImage(UIImage(named: "FaceAD"), for: .normal)
        faButton.isEnabled = true
        faButton.addTarget(self,action: #selector(self.clickFaButton),for: .touchUpInside)
        faButton.imageEdgeInsets = UIEdgeInsets(top: inset,left: inset,bottom: inset,right: inset)
        faButton.layer.cornerRadius = buttonSize/2
        faButton.clipsToBounds = true
        
        self.containerView.addSubview(faButton)
        faButton.snp.makeConstraints { (make) in
            //            make.top.equalTo(systemSettingLabel.snp.bottom).offset(fullScreenSize.height*0.01)
            make.centerY.equalTo(qlButton.snp.centerY)
            make.width.equalTo(buttonSize)
            make.height.equalTo(buttonSize)
            make.left.equalTo(dmButton.snp.right).offset(offsetA*5)
        }
        
        logoutButton.backgroundColor = .buttonColor
        logoutButton.setImage(UIImage(named: "Logout"), for: .normal)
        logoutButton.isEnabled = true
        logoutButton.addTarget(self,action: #selector(self.clickLogoutButton),for: .touchUpInside)
        logoutButton.imageEdgeInsets = UIEdgeInsets(top: inset*1.5,left: inset*1.7,bottom: inset*1.5,right: inset)
        logoutButton.layer.cornerRadius = buttonSize/2
        logoutButton.clipsToBounds = true
        
        self.containerView.addSubview(logoutButton)
        logoutButton.snp.makeConstraints { (make) in
            //            make.top.equalTo(systemSettingLabel.snp.bottom).offset(fullScreenSize.height*0.01)
            make.centerY.equalTo(qlButton.snp.centerY)
            make.width.equalTo(buttonSize)
            make.height.equalTo(buttonSize)
            make.left.equalTo(pcButton.snp.right).offset(offsetA*5)
        }
        
        qlLabel1.backgroundColor = .clear
        qlLabel1.text = "QrCode"
        qlLabel1.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS)
        qlLabel1.textColor = .white
        qlLabel1.adjustsFontSizeToFitWidth = true
        
        self.containerView.addSubview(qlLabel1)
        qlLabel1.snp.makeConstraints { (make) in
            make.top.equalTo(qlButton.snp.bottom).offset(offsetA/2)
            make.centerX.equalTo(qlButton.snp.centerX)
        }
        
        qlLabel2.backgroundColor = .clear
        qlLabel2.text = "Login STB"
        qlLabel2.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS)
        qlLabel2.textColor = .white
        qlLabel2.adjustsFontSizeToFitWidth = true
        
        self.containerView.addSubview(qlLabel2)
        qlLabel2.snp.makeConstraints { (make) in
            make.top.equalTo(qlLabel1.snp.bottom)
            make.centerX.equalTo(qlLabel1.snp.centerX)
        }
        
        
        faLabel.backgroundColor = .clear
        faLabel.text = "Face AD"
        faLabel.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS)
        faLabel.textColor = .white
        faLabel.adjustsFontSizeToFitWidth = true
        
        self.containerView.addSubview(faLabel)
        faLabel.snp.makeConstraints { (make) in
            make.top.equalTo(faButton.snp.bottom).offset(offsetA/2)
            make.centerX.equalTo(faButton.snp.centerX)
        }
        
        logoutLabel.backgroundColor = .clear
        logoutLabel.text = "Logout"
        logoutLabel.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS)
        logoutLabel.textColor = .white
        logoutLabel.adjustsFontSizeToFitWidth = true
        
        self.containerView.addSubview(logoutLabel)
        logoutLabel.snp.makeConstraints { (make) in
            make.top.equalTo(logoutButton.snp.bottom).offset(offsetA/2)
            make.centerX.equalTo(logoutButton.snp.centerX)
            make.bottom.equalToSuperview().offset(-30*HeightForScroll)
        }
        
        switch currentDevice.deviceType {
        case .iPad:
            qlLabel1.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS*1.7)
            qlLabel2.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS*1.7)
            faLabel.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS*1.7)
            logoutLabel.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS*1.7)
        default:
            qlLabel1.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS)
            qlLabel2.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS)
            faLabel.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS)
            logoutLabel.font = UIFont(name: "Georgia-Bold", size: labelFontSizeS)
        }
        
    }
    @objc func clickDmButton(){
        print("clickDmButton")
        MenuDelegate?.changeViedoMenuView(7)
    }
    @objc func clickPcButton(){
        print("clickPcButton")
        MenuDelegate?.changeViedoMenuView(8)
    }
    @objc func clickCpButton(){
        print("clickCpButton")
        MenuDelegate?.changeViedoMenuView(9)
        let name = Notification.Name("removeContentView")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
    }
    @objc func clickQlButton(){
        print("clickQlButton")
        MenuDelegate?.changeViedoMenuView(10)
    }
    @objc func clickFaButton(){
        print("clickFaButton")
        MenuDelegate?.changeViedoMenuView(11)
    }
    @objc func clickFavButton(){
        print("clickFavButton")
        MenuDelegate?.changeViedoMenuView(12)
        let name = Notification.Name("removeContentView")
        NotificationCenter.default.post(name: name, object: nil, userInfo:nil)
    }
    
    @objc func clickLogoutButton(){
        
        let alertController = UIAlertController(title: "Logout", message: "Back to LoginPage.", preferredStyle: .alert)

        let okAction = UIAlertAction(
            title: "OK",
            style: .default,
            handler: {
                (action: UIAlertAction!) -> Void in
//                let actView = UIView()
//                self.setActView(actView: actView)
                self.setActView(openOrClose: true)
                self.logOut {
                    print("logOut")
                    Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { (_) in
                        let VC = getVC(storyboardName: "Main", identifier: "LoginPage")
                        VC.modalPresentationStyle = .fullScreen
                        guard let window = UIApplication.shared.keyWindow else {
                            return
                        }
                        self.dismiss(animated: true, completion: nil)
                        window.rootViewController = VC
//                        actView.removeFromSuperview()
                        self.setActView(openOrClose: false)
                    }
                }
            }
        )
        
        alertController.addAction(okAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
        
//        let params = [
//            "token":"\(token)"
//            //            "token":"ffwjkfwlfn"
//        ] as [String : Any]
//
//        AF.request("\(serverUrlNew)account/logout", method: .post, parameters: params)
//            .responseJSON { (response) in
//                if response.value != nil {
//                    print(response.value!)
//                    let swiftyJsonVar = JSON(response.value!)
//
//                    let alertController = UIAlertController(title: "Logout", message: swiftyJsonVar["message"].string!, preferredStyle: .alert)
//
//                    let okAction = UIAlertAction(
//                        title: "Back to LoginPage.",
//                        style: .default,
//                        handler: {
//                            (action: UIAlertAction!) -> Void in
//                            self.dismiss(animated: true, completion: nil)
//                            token = ""
//                        }
//                    )
//                    alertController.addAction(okAction)
//
//                    self.present(alertController, animated: true, completion: nil)
//
//                }else{
//                    print("正常不會進入這邊@clickLogoutButton")
//
//                    let alertController = UIAlertController(title: "Logout", message: "somrthing error", preferredStyle: .alert)
//
//                    let okAction = UIAlertAction(
//                        title: "Back to LoginPage.",
//                        style: .default,
//                        handler: {
//                            (action: UIAlertAction!) -> Void in
//                            self.dismiss(animated: true, completion: nil)
//                            token = ""
//                        }
//                    )
//                    alertController.addAction(okAction)
//
//                    self.present(alertController, animated: true, completion: nil)
//                }
//
//            }
        
    }
    
    func logOut(completion : @escaping ()->()){
        let params = [
            "token":"\(token)"
        ] as [String : Any]
        
        AF.request("\(serverUrlNew)account/logout", method: .post, parameters: params)
            .responseJSON { (response) in
                completion()
                                
//                if response.value != nil {
////                    print(response.value!)
//                    let swiftyJsonVar = JSON(response.value!)
//
//                    let alertController = UIAlertController(title: "Logout", message: swiftyJsonVar["message"].string!, preferredStyle: .alert)
//
//                    let okAction = UIAlertAction(
//                        title: "Back to LoginPage.",
//                        style: .default,
//                        handler: {
//                            (action: UIAlertAction!) -> Void in
//                            self.dismiss(animated: true, completion: nil)
//                            token = ""
//                            completion()
//                        }
//                    )
//                    alertController.addAction(okAction)
//
//                    self.present(alertController, animated: true, completion: nil)
//
//                }else{
//                    print("正常不會進入這邊@clickLogoutButton")
//
//                    let alertController = UIAlertController(title: "Logout", message: "somrthing error", preferredStyle: .alert)
//
//                    let okAction = UIAlertAction(
//                        title: "Back to LoginPage.",
//                        style: .default,
//                        handler: {
//                            (action: UIAlertAction!) -> Void in
//                            self.dismiss(animated: true, completion: nil)
//                            token = ""
//                            completion()
//                        }
//                    )
//                    alertController.addAction(okAction)
//
//                    self.present(alertController, animated: true, completion: nil)
//                }
                
            }
        
        
    }
    
}
